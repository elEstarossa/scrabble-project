/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Command } from '@app/classes/command';
import { Objectives } from '@app/classes/objectives';
import { Player } from '@app/classes/player';
import { VALIDATION_AWAIT } from '@app/constants/constants';
import { ObjectiveService } from './objective.service';

describe('ObjectifService', () => {
    let service: ObjectiveService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule, BrowserAnimationsModule],
        });
        service = TestBed.inject(ObjectiveService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////////////// checkConsecutiveDirection //////////////////////
    it('should checkConsecutiveDirection when its the same direction ', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'h' };
        service['checkConsecutiveDirection'](command, objective);
        expect(objective.counter).toEqual(1);
    });

    it('should checkConsecutiveDirection when its not the same direction ', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: '' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: '' };
        service['checkConsecutiveDirection'](command, objective);
        expect(objective.save).toEqual('');
        expect(objective.counter).toEqual(0);
    });

    it('should checkConsecutiveDirection when its there is no direction ', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };
        service['checkConsecutiveDirection'](command, objective);
        expect(objective.save).toEqual('h');
        expect(objective.counter).toEqual(1);
    });
    /// /////////////////// openSackBar //////////////////////
    it('should dismiss the snack bar when the wait is false  ', () => {
        const msg = '';
        const wait = false;

        const spy2 = spyOn(service['snackBar'], 'dismiss');
        jasmine.clock().install();
        service['openSackBar'](msg, wait);
        jasmine.clock().tick(VALIDATION_AWAIT);

        expect(spy2).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should openSackBar when the wait is open', () => {
        const msg = '';
        const wait = true;

        const spy = spyOn(service['snackBar'], 'open');
        const spy2 = spyOn(service['snackBar'], 'dismiss');
        jasmine.clock().install();
        service['openSackBar'](msg, wait);
        jasmine.clock().tick(VALIDATION_AWAIT);
        jasmine.clock().tick(VALIDATION_AWAIT);

        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
        jasmine.clock().uninstall();
        jasmine.clock().uninstall();
    });

    /// //////////////////////////// verifyObjective /////////////////////////////////
    it('verifyObjective if the id is between 0 and 7 and the objective is not achieved', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const player = new Player('Gille');
        player.objectives = [{ counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' }];

        spyOn<any>(service, 'isObjectifComplete').and.returnValue(true);
        const spy = spyOn<any>(service, 'openSackBar');
        service.verifyObjective(command, player);
        expect(spy).toHaveBeenCalled();
    });

    it('verifyObjective if the objective is  achieved', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const player = new Player('Gille');
        player.objectives = [{ counter: 0, id: 1, description: '', pts: 1, achived: true, save: '' }];

        spyOn<any>(service, 'isObjectifComplete').and.returnValue(true);
        const spy = spyOn<any>(service, 'openSackBar');
        service.verifyObjective(command, player);
        expect(spy).not.toHaveBeenCalled();
    });

    it('verifyObjective if the objective is not achieved', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const player = new Player('Gille');
        player.objectives = [{ counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' }];
        service['gameService'].multiplayerGame = true;
        spyOn<any>(service, 'isObjectifComplete').and.returnValue(true);
        const spy = spyOn<any>(service, 'openSackBar');
        service.verifyObjective(command, player);
        expect(spy).toHaveBeenCalled();
    });

    it('verifyObjective if the player is the creator', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const player = new Player('Gille');
        const obj: Objectives = { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' };
        player.creator = true;
        player.objectives = [
            { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' },
            { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' },
        ];
        service['gameService'].multiplayerGame = false;
        const player1 = (service['gameService'].player1 = new Player(''));
        player1.objectives = [obj, obj];
        const player2 = (service['gameService'].player2 = new Player(''));
        player2.objectives = [obj, obj];
        spyOn<any>(service, 'isObjectifComplete').and.returnValue(true);
        const spy = spyOn<any>(service, 'openSackBar');
        service.verifyObjective(command, player);
        expect(spy).toHaveBeenCalled();
    });

    it('verifyObjective if the player is not the creator', () => {
        const command: Command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };
        const player = new Player('Gille');
        const obj: Objectives = { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' };
        player.creator = false;
        player.objectives = [
            { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' },
            { counter: 0, id: 1, description: '', pts: 1, achived: false, save: '' },
        ];
        service['gameService'].multiplayerGame = false;
        const player1 = (service['gameService'].player1 = new Player(''));
        player1.objectives = [obj, obj];
        const player2 = (service['gameService'].player2 = new Player(''));
        player2.objectives = [obj, obj];
        spyOn<any>(service, 'isObjectifComplete').and.returnValue(true);
        const spy = spyOn<any>(service, 'openSackBar');
        service.verifyObjective(command, player);
        expect(spy).toHaveBeenCalled();
    });

    /// //////////////////////////// generateObjectives /////////////////////////////////
    it('generateObjectives', () => {
        const objective1: Objectives = {
            counter: 0,
            achived: false,
            save: '',
            id: 3,
            description: ' Placer en moins de cinq seconde 3 tours de suite +20pts',
            pts: 20,
        };
        spyOn(Math, 'random').and.returnValue(0);

        service['gameService'].player1.objectives = [];
        service.generateObjectives();
        expect(JSON.stringify(service['gameService'].player2.objectives[0])).toEqual(JSON.stringify(objective1));
    });
    /// /////////////////// checkConsecutiveExchange //////////////////////
    it('should checkConsecutiveExchange when its exchanging 3 times consecutive', () => {
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        service['checkConsecutiveExchange'](command, objective);
        expect(objective.counter).toEqual(1);
    });

    it('should checkConsecutiveExchange when its not exchanging 3 times consecutive', () => {
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };
        const command: Command = { word: 'word', position: { x: 8, y: 8 }, direction: 'h' };
        service['checkConsecutiveExchange'](command, objective);
        expect(objective.counter).toEqual(0);
    });

    /// /////////////////// checkPlacementOverThirtyPoints //////////////////////
    it('should checkPlacementOverThirtyPoints', () => {
        const command: Command = { word: 'word', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn(service['scoreService'], 'getScore');

        service['checkPlacementOverThirtyPoints'](command);
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////// checkWordBonusPlaced //////////////////////
    it('should checkPlacementOverThirtyPoints', () => {
        const command: Command = { word: 'bonus', position: { x: 8, y: 8 }, direction: 'h' };
        const result = service['checkWordBonusPlaced'](command);
        expect(result).toBeTrue();
    });

    /// /////////////////// checkFourVowels //////////////////////
    it('should checkFourVowels if the word contain 4 vowels and return true', () => {
        const command: Command = { word: 'aeiou', position: { x: 8, y: 8 }, direction: 'h' };
        const result = service['checkFourVowels'](command);
        expect(result).toBeTrue();
    });

    it('should checkFourVowels and return false if the word not contain 4 vowels', () => {
        const command: Command = { word: 'dkj', position: { x: 8, y: 8 }, direction: 'h' };
        const result = service['checkFourVowels'](command);
        expect(result).toBeFalse();
    });

    /// /////////////////// checkPlacementAlongEdge //////////////////////
    it('should checkPlacementAlongEdge if the direction is horizontal and the position is 1, 15', () => {
        const command: Command = { word: 'aeiou', position: { x: 1, y: 15 }, direction: 'h' };
        const result = service['checkPlacementAlongEdge'](command);
        expect(result).toBeTrue();
    });

    it('should checkPlacementAlongEdge  if the direction is vertical and the position is 15, 15', () => {
        const command: Command = { word: 'aeiou', position: { x: 15, y: 15 }, direction: 'v' };
        const result = service['checkPlacementAlongEdge'](command);
        expect(result).toBeTrue();
    });

    /// /////////////////// checkContainsDoubleBonus //////////////////////
    it('checkContainsDoubleBonus should not return true if there is no bonus tile', () => {
        spyOn(service['scoreService'], 'containBonus').and.returnValue(false);
        const command: Command = { word: 'a', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkContainsDoubleBonus'](command, objective);
        expect(result).toBeFalse();
        expect(objective.counter).toEqual(0);
    });

    it('checkContainsDoubleBonus should return false if the counter is not greater or equal than 1', () => {
        spyOn(service['scoreService'], 'containBonus').and.returnValue(true);
        const command: Command = { word: 'a', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkContainsDoubleBonus'](command, objective);
        expect(result).toBeFalse();
        expect(objective.counter).toEqual(0);
    });

    it('checkContainsDoubleBonus should return true if the counter is greater or equal than 1', () => {
        spyOn(service['scoreService'], 'containBonus').and.returnValue(true);
        const command: Command = { word: 'aw', position: { x: 8, y: 8 }, direction: 'v' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkContainsDoubleBonus'](command, objective);
        expect(result).toBeTrue();
        expect(objective.counter).toEqual(1);
    });

    /// /////////////////// checkUnderFiveSec //////////////////////
    it('should checkContainsDoubleBonus when there is word in the command and the counter is less than 8', () => {
        const command: Command = { word: 'Llo', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkUnderFiveSec'](command, 1, objective);
        expect(result).toBeFalse();
        expect(objective.counter).toEqual(1);
    });

    it('should checkContainsDoubleBonus when there is no word in the command and the counter is less than 8', () => {
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkUnderFiveSec'](command, 1, objective);
        expect(result).toBeFalse();
        expect(objective.counter).toEqual(0);
    });

    it('should checkContainsDoubleBonus when there is  word in the command and the counter is more than 8', () => {
        const command: Command = { word: 'dami', position: { x: 8, y: 8 }, direction: 'h' };
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const result = service['checkUnderFiveSec'](command, 9, objective);
        expect(result).toBeFalse();
        expect(objective.counter).toEqual(0);
    });

    /// /////////////////// isObjectifComplete //////////////////////
    it('should isObjectifComplete when its the id 0 it means the first objective', () => {
        const timerCounter = 0;
        const objective: Objectives = { counter: 0, id: 0, description: '', pts: 10, achived: false, save: 'v' };

        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkConsecutiveDirection');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 7 it means the last objective', () => {
        const objective: Objectives = { counter: 0, id: 7, description: '', pts: 10, achived: false, save: 'v' };

        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkConsecutiveExchange');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 1', () => {
        const objective: Objectives = { counter: 0, id: 1, description: '', pts: 10, achived: false, save: 'v' };

        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkFourVowels');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 2', () => {
        const objective: Objectives = { counter: 0, id: 2, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkContainsDoubleBonus');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 3', () => {
        const objective: Objectives = { counter: 0, id: 3, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkUnderFiveSec');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 4', () => {
        const objective: Objectives = { counter: 0, id: 4, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkWordBonusPlaced');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });
    it('should isObjectifComplete when its the id 5', () => {
        const objective: Objectives = { counter: 0, id: 5, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkPlacementAlongEdge');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id 6', () => {
        const objective: Objectives = { counter: 0, id: 6, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };
        const spy = spyOn<any>(service, 'checkPlacementOverThirtyPoints');

        service['isObjectifComplete'](objective, command, timerCounter);
        expect(spy).toHaveBeenCalled();
    });

    it('should isObjectifComplete when its the id is not between 0 and 7', () => {
        const objective: Objectives = { counter: 0, id: 9, description: '', pts: 10, achived: false, save: 'v' };
        const timerCounter = 0;
        const command: Command = { word: '', position: { x: 8, y: 8 }, direction: 'h' };

        const result = service['isObjectifComplete'](objective, command, timerCounter);

        expect(result).toBeFalse();
    });
});
