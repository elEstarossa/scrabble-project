/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable prettier/prettier */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { DictionaryService } from '@app/services/dictionnary.service';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

describe('DictionaryService', () => {
    let service: DictionaryService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [HttpClientTestingModule],
        });
        service = TestBed.inject(DictionaryService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////// dictionnarySubscription ////////////////////////////////
    it('dictionnarySubscription() should return the spicified dictionary ', () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.dictionnarySubscription().subscribe((result: WordDictionnary[]) => {
            expect(result[0].title).toEqual(loadableDict.title);
        });
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict');
        request.flush(loadableDict);
        expect(request.request.method).toEqual('GET');
    });

    /// /////////// getDict ////////////////////////////////
    it('getDict() should assigned the specified dictionary ', () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        const tab = [loadableDict, loadableDict];

        const spy = spyOn(service, 'dictionnarySubscription').and.returnValue(of(tab));
        service.getDict('dictionnaire');
        expect(spy).toHaveBeenCalled();
    });

    it('getDict() should not assigned the specified dictionary ', () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        const tab = [loadableDict, loadableDict];

        const spy = spyOn(service, 'dictionnarySubscription').and.returnValue(of(tab));
        service.getDict('dict');
        expect(spy).toHaveBeenCalled();
    });

    it('getDict() should not assigned the specified dictionary ', () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        const tab = [loadableDict, loadableDict];

        const spy = spyOn(service, 'dictionnarySubscription').and.returnValue(of(tab));
        service.getDict('dict');
        expect(spy).toHaveBeenCalled();
    });

    /// /////////// readFile ////////////////////////////////
    it('readFile() should read a file', async () => {
        const mockFile = new File([''], 'filename', { type: '' });
        const mockEvt = { target: { files: [mockFile] } };
        const spy = spyOn(service, 'uploadValidation').and.callFake(() => {
            return of(true);
        });

        await service.readFile(mockEvt as any);
        expect(spy).toHaveBeenCalled();
    });

    it('readFile() should not read a file if there is a problem', async () => {
        const mockFile = new File([''], 'filename', { type: '' });
        const mockEvt = { target: { files: [mockFile] } };
        const spy = spyOn(service, 'uploadValidation').and.callFake(() => {
            return of(false);
        });
        // const file = new File([""], "file");
        await service.readFile(mockEvt as any);
        expect(spy).toHaveBeenCalled();
    });

    // /// /////////// uploadValidation ////////////////////////////////
    it('uploadValidation() should send true if it is a valid file', () => {
        jasmine.clock().install();
        const text = JSON.stringify({ title: 'newDictio', description: 'ole', words: ['ee', 'zz'] });
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.listOfDictionnary = [loadableDict];
        const result = service.uploadValidation(text);

        result.subscribe((res) => {
            expect(res as boolean).toBe(true);
        });

        jasmine.clock().tick(1);
        jasmine.clock().uninstall();
    });

    it('uploadValidation() should send false if it is already in the list', async () => {
        jasmine.clock().install();
        const text = JSON.stringify({ title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] });
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.listOfDictionnary = [loadableDict];
        const result = service.uploadValidation(text);

        result.subscribe((res) => {
            expect(res as boolean).toBe(false);
        });

        jasmine.clock().tick(1);
        jasmine.clock().uninstall();
    });

    it('uploadValidation() should send false if it is not valid', async () => {
        jasmine.clock().install();
        const text = JSON.stringify({ title: 'dictionnaire', words: ['aa', 'ab'] });
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.listOfDictionnary = [loadableDict];
        const result = service.uploadValidation(text);

        result.subscribe((res) => {
            expect(res as boolean).toBe(false);
        });

        jasmine.clock().tick(1);
        jasmine.clock().uninstall();
    });

    // /// /////////// create ////////////////////////////////
    it('create() should add a new dict', async () => {
        service.text = JSON.stringify({ title: 'newDictio', description: 'ole', words: ['ee', 'zz'] });

        service.create();
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict', JSON.parse(service.text));
        request.flush(201);
        expect(request.request.method).toEqual('POST');
    });

    it('create() should not add a new dict', async () => {
        service.text = JSON.stringify({ title: 'newDictio', description: 'ole', words: ['ee', 'zz'] });

        let errorRes: any;
        service.create().catch((e) => {
            errorRes = e;
        });
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict', JSON.parse(service.text));
        request.flush('error', { status: 400, statusText: 'Bad Request' });
        expect(request.request.method).toEqual('POST');
        expect(errorRes).toBe(undefined);
    });

    // /// /////////// getList ////////////////////////////////
    it('getList() should send a get request', async () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        const tab = [loadableDict, loadableDict];
        service.getList();
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict');
        request.flush(tab);
        expect(request.request.method).toEqual('GET');
    });

    it('getList() should get an error if there is a problem with the request', async () => {
        let errorRes: any;
        service.getList().catch((e) => {
            errorRes = e;
        });
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict');
        request.flush('error', { status: 400, statusText: 'Bad Request' });
        expect(request.request.method).toEqual('GET');
        expect(errorRes).toBe(undefined);
    });

    it('getList() should get an error if there is a problem with the request', async () => {
        let errorRes: any;
        service.getList().catch((e) => {
            errorRes = e;
        });
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict');
        request.flush('error', { status: 400, statusText: 'Bad Request' });
        expect(request.request.method).toEqual('GET');
        expect(errorRes).toBe(undefined);
    });

    // /// /////////// getDescription ////////////////////////////////
    it('getDescription() should not call getList if there is a dictionnary', () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.listOfDictionnary = [loadableDict];
        const spy = spyOn(service, 'getList');
        service.getDescription();
        expect(spy).not.toHaveBeenCalled();
    });

    it('getDescription() should call getList if there is no dictionnary', async () => {
        const loadableDict: WordDictionnary = { title: 'dictionnaire', description: 'francais', words: ['aa', 'ab'] };
        service.listOfDictionnary = [];
        const spy = spyOn(service, 'getList').and.callFake(async () => {
            service.listOfDictionnary = [loadableDict];
            return Promise.resolve([]);
        });
        await service.getDescription();
        expect(spy).toHaveBeenCalled();
    });

    // /// /////////// delete ////////////////////////////////
    it('delete() should send a delete request', () => {
        service.delete(0);
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict' + '/' + 0);
        request.flush(201);
        expect(request.request.method).toEqual('DELETE');
    });

    it('delete() should get an error if there is a problem with the request', () => {
        let errorRes: any;

        service.delete(0).catch((e) => {
            errorRes = e;
        });
        const request = httpMock.expectOne(environment.serverUrl + '/api/dict' + '/' + 0);
        request.flush('error', { status: 400, statusText: 'Bad Request' });
        expect(request.request.method).toEqual('DELETE');
        expect(errorRes).toBe(undefined);
    });

    // /// /////////// nameValidation ////////////////////////////////
    it('should validate the same if its the same as the title and return false', () => {
        const name = 'default';
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        service.listOfDictionnary = listOfDictionnary;

        const result = service.nameValidation(name);
        expect(result).toBeFalse();
    });

    it('should validate the same if its not the same as the title and return true', () => {
        const name = 'hey';
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        service.listOfDictionnary = listOfDictionnary;

        const result = service.nameValidation(name);
        expect(result).toBeTrue();
    });

    // /// /////////// descriptionValidation ////////////////////////////////
    it('should descriptionValidation the same if its the same as the description and return false', () => {
        const name = 'default';
        const listOfDictionnary: WordDictionnary[] = [{ title: '', description: 'default', words: [''] }];
        service.listOfDictionnary = listOfDictionnary;

        const result = service.descriptionValidation(name);
        expect(result).toBeFalse();
    });

    it('should descriptionValidation if its the same not the same as the description and return true', () => {
        const name = 'default';
        const listOfDictionnary: WordDictionnary[] = [{ title: '', description: '', words: [''] }];
        service.listOfDictionnary = listOfDictionnary;

        const result = service.descriptionValidation(name);
        expect(result).toBeTrue();
    });
});
