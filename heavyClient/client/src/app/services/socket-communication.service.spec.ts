/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { EaselObject } from '@app/classes/easel-object';
import { MessageServer } from '@app/classes/message-server';
import { Player } from '@app/classes/player';
import { Scores } from '@app/classes/scores';
import { SocketTestHelper } from '@app/classes/socket-test-helper';
import { GameTime } from '@app/classes/time';
import { EMPTY_COMMAND_CODE } from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { Socket } from 'socket.io-client';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { SocketCommunicationService } from './socket-communication.service';

describe('SocketCommunicationService', () => {
    let service: SocketCommunicationService;
    let socketTestHelper: SocketTestHelper;
    let gameService: jasmine.SpyObj<GameService>;
    let easelService: jasmine.SpyObj<EaselOperationsService>;

    beforeEach(() => {
        socketTestHelper = new SocketTestHelper();
        gameService = jasmine.createSpyObj('GameService', [
            'alternateTurns',
            'getCreator',
            'setTempLetter',
            'getJoiner',
            'addEaselLetterScore',
            'subEaselLetterScore',
            'addScore',
        ]);
        gameService.player1 = new Player('Gilles');
        gameService.player2 = new Player('ali');
        gameService.player2.easel = new EaselObject(true);
        gameService.gameTimeTurn = { min: 0, sec: 40 };
        gameService.timer = { min: 0, sec: 40 };

        easelService = jasmine.createSpyObj('EaselOperationsService', ['redrawEasel', 'fillEasel', 'removeUsedLetters']);
        easelService.gridCtx = CanvasTestHelper['createCanvas'](15, 25).getContext('2d') as CanvasRenderingContext2D;
        TestBed.configureTestingModule({
            providers: [
                { provide: GameService, useValue: gameService },
                { provide: EaselOperationsService, useValue: easelService },
            ],
            imports: [HttpClientTestingModule, MatSnackBarModule, BrowserAnimationsModule],
        });
        service = TestBed.inject(SocketCommunicationService);
        service.socket = socketTestHelper as any as Socket;
        service.printValidation = new BehaviorSubject(EMPTY_COMMAND_CODE);
        service.chatInputOBS = new BehaviorSubject('');
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////// listen ////////////////////////////////
    it('should call listen', (done) => {
        jasmine.clock().install();

        const msg: MessageServer = {} as MessageServer;
        const eventName = 'event';

        const result = service.listen(eventName);

        result.subscribe((res) => {
            expect((res as unknown as MessageServer[])[0]).toBe(msg);
            done();
        });
        socketTestHelper.peerSideEmit(eventName, msg);

        jasmine.clock().tick(1);
        jasmine.clock().uninstall();
    });

    /// /////////// emit ////////////////////////////////
    it('should call emit', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'event';
        const cb: any = '';
        const msg: MessageServer = {} as MessageServer;

        service.emit(eventName, msg, cb);

        expect(socketEmit).toHaveBeenCalledWith(eventName, msg, cb);
    });

    /// /////////// showScoreClassic ////////////////////////////////
    it('should call emit for score in classic mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'showScoreClassic';
        service.showScoreClassic();

        expect(socketEmit).toHaveBeenCalledWith(eventName);
    });

    /// /////////// showScore2990 ////////////////////////////////
    it('should call emit for score in log299 mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'showScore2990';
        service.showScore2990();

        expect(socketEmit).toHaveBeenCalledWith(eventName);
    });

    /// /////////// sendObjectives ////////////////////////////////
    it('should call sendObjectives ', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'sendObjectives';
        const roomName = gameService.roomName;
        const objectives = gameService.player1.objectives;
        service.sendObjectives();

        expect(socketEmit).toHaveBeenCalledWith(eventName, roomName, objectives);
    });

    /// /////////// sendAchived ////////////////////////////////
    it('should call sendAchived ', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'sendAchived';
        const roomName = gameService.roomName;
        const description = '';
        const index = 0;
        service.sendAchived(description, index);

        expect(socketEmit).toHaveBeenCalledWith(eventName, roomName, description, index);
    });

    /// /////////// getObjectives ////////////////////////////////
    it('should call getObjectives ', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');

        service.getObjectives();

        expect(socketEmit).toHaveBeenCalled();
    });

    /// /////////// sendScore ////////////////////////////////
    it('should call emit when its a abondan and a multiplayer game in the classical mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [
            { name: 'Gilles', score: 0 },
            { name: 'ali', score: 0 },
        ];
        const eventName = 'sendScoreClassic';
        gameService.isAbandon = false;
        gameService.multiplayerGame = true;
        gameService.log2990 = false;
        service.sendScore();

        expect(socketEmit).toHaveBeenCalledWith(eventName, listOfScores);
    });

    it('should call emit its a abondan and a solo game in the classical mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [{ name: 'Gilles', score: 0 }];
        const eventName = 'sendScoreClassic';
        gameService.isAbandon = false;
        gameService.multiplayerGame = false;
        gameService.log2990 = false;

        service.sendScore();

        expect(socketEmit).toHaveBeenCalledWith(eventName, listOfScores);
    });

    it('should call emit its not a abondan and a solo game in the classical mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [{ name: 'Gilles', score: 0 }];
        const eventName = 'sendScoreClassic';
        gameService.isAbandon = true;
        gameService.multiplayerGame = false;
        gameService.log2990 = false;

        service.sendScore();

        expect(socketEmit).not.toHaveBeenCalledWith(eventName, listOfScores);
    });

    it('should call emit when its a abondan and a multiplayer game in the log2990 mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [
            { name: 'Gilles', score: 0 },
            { name: 'ali', score: 0 },
        ];
        const eventName = 'sendScore2990';
        gameService.isAbandon = false;
        gameService.multiplayerGame = true;
        gameService.log2990 = true;
        service.sendScore();

        expect(socketEmit).toHaveBeenCalledWith(eventName, listOfScores);
    });

    it('should call emit its a abondan and a solo game in the log2990 mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [{ name: 'Gilles', score: 0 }];
        const eventName = 'sendScore2990';
        gameService.isAbandon = false;
        gameService.multiplayerGame = false;
        gameService.log2990 = true;

        service.sendScore();

        expect(socketEmit).toHaveBeenCalledWith(eventName, listOfScores);
    });

    it('should call emit its not a abondan and a solo game in the log2990 mode', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const listOfScores: Scores[] = [{ name: 'Gilles', score: 0 }];
        const eventName = 'sendScore2990';
        gameService.isAbandon = true;
        gameService.multiplayerGame = false;
        gameService.log2990 = true;

        service.sendScore();

        expect(socketEmit).not.toHaveBeenCalledWith(eventName, listOfScores);
    });

    /// /////////// receiveReserveFirstTurn ////////////////////////////////
    it('should call receiveReserveFirstTurn', () => {
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const eventName = 'updateReserveInClient';
        spyOn(service['reserveService'], 'redefineReserve');

        service.receiveReserveFirstTurn();
        socketTestHelper.peerSideEmit(eventName, '', 0, []);

        expect(socketOnSpy).toHaveBeenCalled();
    });

    /// /////////// getAchived ////////////////////////////////
    // it('should call getAchived', () => {
    //     const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
    //     const eventName = 'getAchived';
    //     spyOn(service['snackBar'], 'open');
    //     gameService.player1 = new Player('');

    //     service.getAchived();
    //     socketTestHelper.peerSideEmit(eventName, '', 5);

    //     expect(socketOnSpy).toHaveBeenCalled();
    // });

    /// /////////// sendAchived ////////////////////////////////
    it('should call sendAchived', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const eventName = 'sendAchived';
        const description = '';
        const index = 0;
        const roomName = gameService.roomName;

        service.sendAchived(description, index);

        expect(socketEmit).toHaveBeenCalledWith(eventName, roomName, description, index);
    });

    /// /////////// defineWhoStarts ////////////////////////////////
    it('should call emit with defineWhoStarts', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');
        const roomName = (gameService.roomName = '');

        service.defineWhoStarts();

        expect(socketEmit).toHaveBeenCalledWith('defineStarter', roomName);
    });
    /// /////////// sendReserve ////////////////////////////////
    it('should call emit with sendReserve', () => {
        const socketEmit = spyOn<any>(service.socket, 'emit');

        service.sendReserve();

        expect(socketEmit).toHaveBeenCalled();
    });
    it('should call joinerFillEasel when the boolean first is true ', () => {
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const socketEmitSpy = spyOn<any>(service.socket, 'emit').and.callThrough();
        const eventName = 'updateReserveInClient';
        const roomName = (gameService.roomName = '');

        spyOn(service['reserveService'], 'redefineReserve');
        service.first = true;
        service.joinerFillEasel();
        socketTestHelper.peerSideEmit(eventName, '', 0, []);
        expect(socketOnSpy).toHaveBeenCalled();
        expect(service.first).toBeFalse();

        expect(socketEmitSpy).toHaveBeenCalledWith('sendReserveJoin', roomName);
    });
    it('should call joinerFillEasel when the boolean first is false ', () => {
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const socketEmitSpy = spyOn<any>(service.socket, 'emit').and.callThrough();
        const eventName = 'updateReserveInClient';
        const roomName = (gameService.roomName = '');

        spyOn(service['reserveService'], 'redefineReserve');
        service.first = false;
        service.joinerFillEasel();
        socketTestHelper.peerSideEmit(eventName, '', 0, []);
        expect(socketOnSpy).toHaveBeenCalled();

        expect(socketEmitSpy).toHaveBeenCalledWith('sendReserveJoin', roomName);
    });
    /// /////////////// getTime ////////////////////////////////
    it('should call getTime when its not the creator turn ', () => {
        const timeServer: GameTime = { min: 0, sec: 40 };
        const expected = gameService.timer;
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const eventName = 'getTime';
        service.getTime();
        socketTestHelper.peerSideEmit(eventName, timeServer);

        expect(socketOnSpy).toHaveBeenCalled();
        expect(expected).toEqual(timeServer);
    });

    it('should call getTime when its the creator turn ', () => {
        const timeServer: GameTime = { min: 0, sec: 40 };
        const result = (gameService.gameTimeTurn = { min: 0, sec: 40 });
        const expected = gameService.timer;
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const eventName = 'getTime';
        gameService.player1.turnToPlay = true;

        service.getTime();
        socketTestHelper.peerSideEmit(eventName, timeServer);

        expect(socketOnSpy).toHaveBeenCalled();
        expect(expected).toEqual(result);
    });

    /// /////////// getPassTurn ////////////////////////////////
    it('should call getPassTurn', () => {
        const socketOnSpy = spyOn<any>(service.socket, 'on').and.callThrough();
        const eventName = 'getPassTurn';

        service.getPassTurn();
        socketTestHelper.peerSideEmit(eventName, '', 0, []);

        expect(socketOnSpy).toHaveBeenCalled();
        expect(gameService.alternateTurns).toHaveBeenCalled();
    });

    /// /////////// passTurn ////////////////////////////////
    it('should call passTurn', () => {
        const socketEmitSpy = spyOn<any>(service.socket, 'emit');
        const roomName = (gameService.roomName = '');
        gameService.multiplayerGame = true;
        gameService.log2990 = true;

        spyOn(service['reserveService'], 'redefineReserve');
        const spy = spyOn(service['objectiveService'], 'verifyObjective');
        service.passTurn();

        expect(socketEmitSpy).toHaveBeenCalledWith('passTurn', roomName);
        expect(gameService.alternateTurns).toHaveBeenCalled();
        expect(spy).toHaveBeenCalled();
    });

    it('should call passTurn', () => {
        gameService.multiplayerGame = false;

        spyOn(service['reserveService'], 'redefineReserve');
        const spy = spyOn(service['virtualPlayer'], 'play');
        service.passTurn();

        expect(spy).toHaveBeenCalled();
    });
});
