/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Letter } from '@app/classes/letter';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { M, NB_TILES, NOT_A_LETTER, O, UNDEFINED_INDEX } from '@app/constants/constants';
import { ScoreService } from './score.service';

describe('ScoreService', () => {
    let service: ScoreService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
        });
        service = TestBed.inject(ScoreService);
        service.indexOfLetterToCheck = [];
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    // //////////////wordInDictionnary//////////////////////////
    it('wordInDictionnary should be true if word is in dictionnary', () => {
        const word = '';
        const wordDict: WordDictionnary = { title: '', description: '', words: [''] };
        service.dict = wordDict;
        const result = service.wordInDictionnary(word);
        expect(result).toBeTrue();
    });

    it('wordInDictionnary should be false if word is in dictionnary', () => {
        const word = 'tfou';
        const wordDict: WordDictionnary = { title: '', description: '', words: [''] };
        service.dict = wordDict;
        const result = service.wordInDictionnary(word);
        expect(result).toBeFalse();
    });

    // //////////////resetIndex//////////////////////////
    it('resetIndex should reset index of letters to check', () => {
        service.resetIndex();
        expect(service.indexOfLetterToCheck.length).toEqual(0);
    });

    // //////////////getLetterScore//////////////////////////
    it('getLetterScore should return the letter score', () => {
        const letter = 'k';
        const EXPECTED_SCORE = 10;
        const letterScore = service.getLetterScore(letter);
        expect(letterScore).toEqual(EXPECTED_SCORE);
    });

    it('getLetterScore should return 0', () => {
        const letter = NOT_A_LETTER.charac;
        const EXPECTED_SCORE = 0;
        const letterScore = service.getLetterScore(letter);
        expect(letterScore).toEqual(EXPECTED_SCORE);
    });

    // //////////////getScore//////////////////////////
    it('getScore should return score', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        const EXPECTED_SCORE = 3;
        const score = service.getScore(command);
        expect(score).toEqual(EXPECTED_SCORE);
    });
    it('should add to the score the value of the letter', () => {
        const word = 'oui';
        const position = { x: 7, y: 7 };
        const direction = 'h';
        const command = { word, position, direction };
        const EXPECTED_SCORE = 3;
        const score = service.getWordBonusScore(command);
        expect(score).toEqual(EXPECTED_SCORE);
    });

    it('getScore should return -1', () => {
        const word = '1';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        spyOn(service, 'checkNewFormedWords').and.returnValue(UNDEFINED_INDEX);
        const EXPECTED_SCORE = UNDEFINED_INDEX;
        const score = service.getScore(command);
        expect(score).toEqual(EXPECTED_SCORE);
    });

    // //////////////containBonus//////////////////////////
    it('containBonus should return true if position contain bonus', () => {
        const position = { x: 1, y: 1 };
        const bonus = [{ x: 1, y: 1 }];
        const result = service.containBonus(position, bonus);
        expect(result).toBeTrue();
    });

    it('containBonus should return false if position does not contain bonus', () => {
        const position = { x: 1, y: 1 };
        const bonus = [{ x: 0, y: 1 }];
        const result = service.containBonus(position, bonus);
        expect(result).toBeFalse();
    });

    // //////////////spotNewLetters//////////////////////////
    it('spotNewLetters should spot new letters if direction is horizontal', () => {
        const word = 'abri';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        service.spotNewLetters(command);
        const EXPECTED_LENGTH = 4;
        expect(service.indexOfLetterToCheck.length).toEqual(EXPECTED_LENGTH);
    });

    it('spotNewLetters should spot new letters if direction is vertical', () => {
        const word = 'abri';
        const position = { x: 1, y: 1 };
        const direction = 'v';
        const command = { word, position, direction };
        service.spotNewLetters(command);
        const EXPECTED_LENGTH = 4;
        expect(service.indexOfLetterToCheck.length).toEqual(EXPECTED_LENGTH);
    });

    // //////////////getAxe//////////////////////////
    it('getAxe should return the y axis when direction is not h', () => {
        const word = 'abri';
        const position = { x: 1, y: 2 };
        const direction = 'v';
        const command = { word, position, direction };
        const EXPECTED_POSITION = 1;
        const result = service.getAxe(command);
        expect(result).toEqual(EXPECTED_POSITION);
    });

    it('getAxe should return the x axis when direction is h', () => {
        const word = 'abri';
        const position = { x: 1, y: 2 };
        const direction = 'h';
        const command = { word, position, direction };
        const EXPECTED_POSITION = 2;
        const result = service.getAxe(command);
        expect(result).toEqual(EXPECTED_POSITION);
    });

    // //////////////getWordBonusScore//////////////////////////
    it('getWordBonusScore should go return the right score with the letter bonus, direction v', () => {
        const command = { word: 'table', position: { x: 1, y: 5 }, direction: 'v' };
        const EXPECTED_SCORE = 21;
        const usedPosition = new Array<Letter[]>(NB_TILES);
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        expect(EXPECTED_SCORE).toEqual(service.getScore(command));
    });

    it('getWordBonusScore should go return the right score with the letter bonus, direction v', () => {
        const command = { word: 'table', position: { x: 6, y: 2 }, direction: 'h' };
        const EXPECTED_SCORE = 11;
        const usedPosition = new Array<Letter[]>(NB_TILES);
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        expect(EXPECTED_SCORE).toEqual(service.getScore(command));
    });

    it('getWordBonusScore should go return the right score with the letter bonus, direction h', () => {
        const command = { word: 'table', position: { x: 4, y: 4 }, direction: 'h' };
        const EXPECTED_SCORE = 16;
        const usedPosition = new Array<Letter[]>(NB_TILES);
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        expect(EXPECTED_SCORE).toEqual(service.getScore(command));
    });

    it('getWordBonusScore should return the score of a word that length is equal to easel length', () => {
        const word = 'mondial';
        const position = { x: 16, y: 14 };
        const direction = 'h';
        const command = { word, position, direction };
        for (let i = 0; i < command.word.length; i++) {
            service.indexOfLetterToCheck.push(i);
        }
        const EXPECTED_SCORE = 59;
        const result = service.getWordBonusScore(command);
        expect(result).toEqual(EXPECTED_SCORE);
    });

    // //////////////checkNewFormedWords//////////////////////////
    it('should call checkNewFormedWords', () => {
        const word = 'monde';
        const position = { x: 11, y: 4 };
        const direction = 'h';
        const command = { word, position, direction };
        const wordDict: WordDictionnary = { title: '', description: '', words: [''] };
        service.dict = wordDict;
        for (let i = 0; i < command.word.length; i++) {
            service.indexOfLetterToCheck.push(i);
        }
        spyOn(service, 'getAxe').and.returnValue(2);
        spyOn(service, 'getPreviousTile').and.returnValue(M);
        const result = service.checkNewFormedWords(command);
        expect(result).toEqual(0);
    });

    it('should call checkNewFormedWords', () => {
        const word = 'monde';
        const position = { x: 11, y: 4 };
        const direction = 'h';
        const command = { word, position, direction };
        for (let i = 0; i < command.word.length; i++) {
            service.indexOfLetterToCheck.push(i);
        }
        spyOn(service, 'getPreviousTile').and.returnValue(M);
        spyOn(service, 'getLetterWithDirection').and.returnValue(O);
        const wordDict: WordDictionnary = { title: '', description: '', words: [''] };
        service.dict = wordDict;
        const result = service.checkNewFormedWords(command);
        expect(result).toEqual(UNDEFINED_INDEX);
    });
});
