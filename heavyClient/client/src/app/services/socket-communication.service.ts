import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Letter } from '@app/classes/letter';
import { MessageServer } from '@app/classes/message-server';
import { Objectives } from '@app/classes/objectives';
import { Scores } from '@app/classes/scores';
import { GameTime } from '@app/classes/time';
import { EMPTY_COMMAND, EMPTY_COMMAND_CODE, OBJ, VALIDATION_AWAIT } from '@app/constants/constants';
import { BehaviorSubject, Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { ObjectiveService } from './objective.service';
import { ReserveService } from './reserve.service';
import { VirtualPlayerService } from './virtual-player.service';

@Injectable({
    providedIn: 'root',
})
export class SocketCommunicationService {
    socket;
    first: boolean = true;
    chatInputOBS = new BehaviorSubject('init');
    resetEaselOBS = new BehaviorSubject('init');
    printValidation = new BehaviorSubject(EMPTY_COMMAND_CODE);
    listOfScores: Scores[] = [];

    constructor(
        private easelOperation: EaselOperationsService,
        private gameService: GameService,
        private reserveService: ReserveService,
        private virtualPlayer: VirtualPlayerService,
        private snackBar: MatSnackBar,
        private objectiveService: ObjectiveService,
    ) {
        this.socket = io(environment.serverUrl) as unknown as Socket;
    }
    listen(eventName: string): Observable<MessageServer> {
        return new Observable<MessageServer>((subscriber) => {
            this.socket.on(eventName, (data: MessageServer) => {
                subscriber.next(data);
            });
        });
    }
    emit(eventName: string, message?: MessageServer, cb?: unknown) {
        this.socket.emit(eventName, message, cb);
    }
    sendReserve() {
        this.socket.emit(
            'updateReserveInServer',
            this.gameService.roomName,
            JSON.stringify(Array.from(this.reserveService.letters)),
            this.reserveService.reserveSize,
            this.gameService.player1.easel.easelLetters,
            this.gameService.player1.creator,
        );
    }
    sendObjectives() {
        const idObj: number[] = [];
        for (const obj of this.gameService.player1.objectives) idObj.push(obj.id);
        this.socket.emit('sendObjectives', this.gameService.roomName, idObj);
    }
    getObjectives() {
        this.socket.emit('getObjectives', this.gameService.roomName, (res: number[]) => {
            const objectives = OBJ.slice();
            for (const index of res) {
                objectives.splice(index, 1);
                this.gameService.player1.objectives.push(new Objectives(OBJ[index].id, OBJ[index].description, OBJ[index].pts));
            }
            const prvtObj = Math.floor(Math.random() * objectives.length);
            this.gameService.player1.objectives[0] = new Objectives(objectives[prvtObj].id, objectives[prvtObj].description, objectives[prvtObj].pts);
        });
    }
    receiveReserveFirstTurn() {
        this.socket.on('updateReserveInClient', (map: string, size: number, easel: Letter[]) => {
            this.reserveService.redefineReserve(map, size);
            this.gameService.player2.easel.easelLetters = easel;
        });
    }

    joinerFillEasel() {
        this.socket.emit('sendReserveJoin', this.gameService.roomName);

        this.socket.on('updateReserveInClient', (map: string, size: number, easel: Letter[]) => {
            if (this.first) {
                this.first = false;
                this.reserveService.redefineReserve(map, size);
                this.easelOperation.fillEasel(this.gameService.player1.easel, true);
                this.gameService.player2.easel.easelLetters = easel;
                this.sendReserve();
            }
        });
    }

    defineWhoStarts() {
        this.socket.emit('defineStarter', this.gameService.roomName);
    }
    passTurn() {
        if (this.gameService.log2990) {
            this.objectiveService.verifyObjective(EMPTY_COMMAND, this.gameService.player1);
        }
        this.gameService.alternateTurns();
        this.gameService.timer = this.gameService.gameTimeTurn;
        this.printValidation.next({ isValid: true, errorMsg: '!passer', score: 0 });
        if (this.gameService.multiplayerGame) this.socket.emit('passTurn', this.gameService.roomName);
        else {
            this.gameService.passTurnCounter++;
            this.virtualPlayer.play();
        }
    }
    getPassTurn() {
        this.socket.on('getPassTurn', () => {
            this.gameService.alternateTurns();
            this.gameService.timer = this.gameService.gameTimeTurn;
            this.chatInputOBS.next('!passer');
        });
    }
    getWhoStarts() {
        this.socket.on('whoStarts', (digit: number) => {
            if (digit === 0) {
                this.gameService.getCreator().turnToPlay = true;
                this.gameService.getJoiner().turnToPlay = false;
            } else {
                this.gameService.getCreator().turnToPlay = false;
                this.gameService.getJoiner().turnToPlay = true;
            }
        });
    }
    getTime() {
        this.socket.on('getTime', (timeServer: GameTime) => {
            if (timeServer.min === this.gameService.gameTimeTurn.min && timeServer.sec === this.gameService.gameTimeTurn.sec) {
                this.gameService.timer = this.gameService.gameTimeTurn;
                this.gameService.timeCounter = 0;
                if (this.gameService.player1.turnToPlay) {
                    this.resetEaselOBS.next('reset');
                    this.passTurn();
                }
            } else {
                this.gameService.timeCounter++;
                this.gameService.timer = timeServer;
            }
        });
    }
    showScoreClassic() {
        this.socket.emit('showScoreClassic');
    }
    showScore2990() {
        this.socket.emit('showScore2990');
    }

    sendScore() {
        if (!this.gameService.log2990) {
            if (!this.gameService.isAbandon && this.gameService.multiplayerGame) {
                this.listOfScores.push({ name: this.gameService.player1.name, score: this.gameService.player1.score });
                this.listOfScores.push({ name: this.gameService.player2.name, score: this.gameService.player2.score });
                this.socket.emit('sendScoreClassic', this.listOfScores);
            } else if (!this.gameService.isAbandon && !this.gameService.multiplayerGame) {
                this.listOfScores.push({ name: this.gameService.player1.name, score: this.gameService.player1.score });
                this.socket.emit('sendScoreClassic', this.listOfScores);
            }
        } else {
            if (!this.gameService.isAbandon && this.gameService.multiplayerGame) {
                this.listOfScores.push({ name: this.gameService.player1.name, score: this.gameService.player1.score });
                this.listOfScores.push({ name: this.gameService.player2.name, score: this.gameService.player2.score });
                this.socket.emit('sendScore2990', this.listOfScores);
            } else if (!this.gameService.isAbandon && !this.gameService.multiplayerGame) {
                this.listOfScores.push({ name: this.gameService.player1.name, score: this.gameService.player1.score });
                this.socket.emit('sendScore2990', this.listOfScores);
            }
        }
    }
    sendAchived(description: string, index: number) {
        this.socket.emit('sendAchived', this.gameService.roomName, description, index);
    }
    getAchived() {
        this.socket.on('getAchived', (description: string, index: number) => {
            this.snackBar.open('OBJECTIF ATTEINT PAR VOTRE ADVERSAIRE!: ' + description, 'fermer');
            setTimeout(() => {
                this.snackBar.dismiss();
            }, VALIDATION_AWAIT);
            if (index !== 0) this.gameService.player1.objectives[index].achived = true;
        });
    }
}
