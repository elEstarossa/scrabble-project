import { APP_BASE_HREF } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import {
    BOARD_HEIGHT,
    BOARD_WIDTH,
    CLEAR_RECT_FIX,
    EASEL_LENGTH,
    HAND_POSITION_START,
    LEFTSPACE,
    MEDIUM_PX,
    NB_LETTER_HAND,
    NB_TILES,
    NOT_A_LETTER,
    OFFSET_DRAW_LETTER,
    TILE_SIZE,
    TOPSPACE,
    UNDEFINED_INDEX,
} from '@app/constants/constants';
import { ReserveService } from './reserve.service';

@Injectable({
    providedIn: 'root',
})
export class EaselOperationsService {
    gridCtx: CanvasRenderingContext2D;
    pixelSize: TileContent = MEDIUM_PX;
    constructor(private reserveService: ReserveService, @Inject(APP_BASE_HREF) public baseHref: string) {}

    placeEaselLetters(easel: EaselObject): void {
        let counter = 0;
        for (const letter of easel.easelLetters) {
            const position = counter;

            if (letter !== NOT_A_LETTER && !easel.posTempLetters[position]) {
                const img = new Image();
                const url = location.origin + this.baseHref + 'assets/letter-joker.png';
                img.src = url;
                img.onload = () => {
                    this.gridCtx.drawImage(
                        img,
                        LEFTSPACE + ((HAND_POSITION_START + position) * BOARD_WIDTH) / NB_TILES,
                        TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2,
                        BOARD_WIDTH / NB_TILES,
                        BOARD_HEIGHT / NB_TILES,
                    );

                    this.drawEaselLetter(letter, position);
                    this.drawEaselScore(letter, position);
                };
            } else {
                this.gridCtx.clearRect(
                    LEFTSPACE + ((HAND_POSITION_START + position) * BOARD_WIDTH) / NB_TILES + 2,
                    TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + 2,
                    BOARD_WIDTH / NB_TILES - CLEAR_RECT_FIX,
                    BOARD_HEIGHT / NB_TILES - CLEAR_RECT_FIX,
                );
            }
            counter++;
        }
    }

    drawEaselLetter(letter: Letter, index: number) {
        this.gridCtx.font = this.pixelSize.alpha.toString() + 'px' + ' Calibri';
        const ltrWidth = this.gridCtx.measureText(letter.charac).width;
        this.gridCtx.textBaseline = 'top';
        const ltrHeight = this.gridCtx.measureText(letter.charac).actualBoundingBoxDescent;

        this.gridCtx.fillText(
            letter.charac,
            LEFTSPACE + ((HAND_POSITION_START + index) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - ltrWidth / OFFSET_DRAW_LETTER),
            TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + (TILE_SIZE / 2 - ltrHeight / 2),
        );
    }
    drawEaselScore(letter: Letter, index: number) {
        this.gridCtx.font = this.pixelSize.score.toString() + 'px' + ' Calibri';
        const scrWidth = this.gridCtx.measureText(letter.score.toString()).width;
        const scrHeight = this.gridCtx.measureText(letter.charac.toString()).actualBoundingBoxDescent;
        this.gridCtx.fillText(
            letter.score.toString(),
            LEFTSPACE + ((HAND_POSITION_START + index) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - scrWidth / 2) + scrWidth,
            TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + (TILE_SIZE / 2 - scrHeight / 2) + scrHeight,
        );
    }

    getLetterFromEasel(easel: EaselObject, index: number): Letter {
        const temp: Letter = easel.easelLetters[index];
        if (easel.easelLetters[index] !== NOT_A_LETTER) {
            easel.easelLetters[index] = NOT_A_LETTER;
            return temp;
        }
        return NOT_A_LETTER;
    }

    refillEasel(easel: EaselObject, user: boolean): void {
        for (const nb of easel.indexOfUsedLetters) {
            if (!this.reserveService.isReserveEmpty()) easel.add(this.reserveService.getRandomLetter(), nb);
            else {
                easel.add(NOT_A_LETTER, nb);
                easel.easelSize--;
            }
        }
        easel.resetTempIndex();
        easel.resetUsedLetters();
        if (user) this.placeEaselLetters(easel);
    }
    fillEasel(easel: EaselObject, user: boolean): void {
        for (let i = 0; i < NB_LETTER_HAND; i++) {
            if (!this.reserveService.isReserveEmpty()) {
                easel.add(this.reserveService.getRandomLetter(), i);
            }
        }
        if (user) {
            this.placeEaselLetters(easel);
        }
    }

    redrawEasel(easel: EaselObject) {
        for (let i = 0; i < NB_LETTER_HAND; i++) {
            this.gridCtx.clearRect(
                LEFTSPACE + ((HAND_POSITION_START + i) * BOARD_WIDTH) / NB_TILES + 2,
                TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + 2,
                BOARD_WIDTH / NB_TILES - CLEAR_RECT_FIX,
                BOARD_HEIGHT / NB_TILES - CLEAR_RECT_FIX,
            );
        }
        this.placeEaselLetters(easel);
    }

    getTempLetter(letter: string, easel: EaselObject): Letter {
        let counter = 0;
        for (const lett of easel.easelLetters) {
            const pos = counter;
            if (lett.charac === letter && !easel.posTempLetters[pos]) {
                this.gridCtx.clearRect(
                    LEFTSPACE + ((HAND_POSITION_START + pos) * BOARD_WIDTH) / NB_TILES + 2,
                    TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + 2,
                    BOARD_WIDTH / NB_TILES - CLEAR_RECT_FIX,
                    BOARD_HEIGHT / NB_TILES - CLEAR_RECT_FIX,
                );
                easel.posTempLetters[pos] = true;
                easel.indexTempLetters.push(pos);
                return lett;
            }
            counter++;
        }
        return NOT_A_LETTER;
    }
    replaceTempInEasel(easel: EaselObject) {
        if (easel.indexTempLetters.length !== 0) {
            const pos = easel.indexTempLetters.pop() || 0;
            easel.posTempLetters[pos] = false;
            this.placeEaselLetters(easel);
        }
    }
    resetTempLetter(easel: EaselObject) {
        easel.resetTempIndex();
        this.placeEaselLetters(easel);
    }
    moveLeft(easel: EaselObject) {
        let save: Letter;
        if (easel.indexToMove !== 0) {
            save = easel.easelLetters[easel.indexToMove - 1];
            easel.easelLetters[easel.indexToMove - 1] = easel.easelLetters[easel.indexToMove];
            easel.easelLetters[easel.indexToMove] = save;
            easel.indexToMove--;
        } else {
            easel.indexToMove = EASEL_LENGTH - 1;
            save = easel.easelLetters[easel.indexToMove];
            easel.easelLetters[easel.indexToMove] = easel.easelLetters[0];
            for (let i = 0; i < EASEL_LENGTH - 1; i++) {
                easel.easelLetters[i] = easel.easelLetters[i + 1];
                if (i === EASEL_LENGTH + UNDEFINED_INDEX - 1) easel.easelLetters[i] = save;
            }
        }

        this.placeEaselLetters(easel);
    }
    moveRight(easel: EaselObject) {
        let save: Letter;
        if (easel.indexToMove !== EASEL_LENGTH - 1) {
            save = easel.easelLetters[easel.indexToMove + 1];
            easel.easelLetters[easel.indexToMove + 1] = easel.easelLetters[easel.indexToMove];
            easel.easelLetters[easel.indexToMove] = save;
            easel.indexToMove++;
        } else {
            easel.indexToMove = 0;
            save = easel.easelLetters[easel.indexToMove];
            easel.easelLetters[easel.indexToMove] = easel.easelLetters[EASEL_LENGTH - 1];
            for (let i = EASEL_LENGTH - 1; i > 0; i--) {
                easel.easelLetters[i] = easel.easelLetters[i - 1];
                if (i === 1) easel.easelLetters[i] = save;
            }
        }

        this.placeEaselLetters(easel);
    }
}
