/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { A, N, NOT_A_LETTER, UNDEFINED_INDEX } from '@app/constants/constants';
import { DictionaryService } from './dictionnary.service';
import { ScoreService } from './score.service';
import { WordsService } from './words.service';

describe('WordsService', () => {
    let service: WordsService;
    let serviceScore: jasmine.SpyObj<ScoreService>;
    let serviceDictionary: jasmine.SpyObj<DictionaryService>;
    const dictSpy = jasmine.createSpyObj('DictionnaryService', ['dictionnarySubscription']);
    const scoreSpy = jasmine.createSpyObj('ScoreService', [
        'spotNewLetters',
        'wordInDictionnary',
        'resetIndex',
        'getWordBonusScore',
        'checkNewFormedWords',
    ]);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [WordsService, { provide: ScoreService, useValue: scoreSpy }, { provide: DictionaryService, useValue: dictSpy }],
            imports: [HttpClientTestingModule],
        });
        serviceScore = TestBed.inject(ScoreService) as jasmine.SpyObj<ScoreService>;
        serviceDictionary = TestBed.inject(DictionaryService) as jasmine.SpyObj<DictionaryService>;
        serviceDictionary.wordDictionnary = { title: 'fakeDict', description: 'fake', words: ['a', 'b', 'c'] };
        service = TestBed.inject(WordsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// //////////////////////////  generateRegEx  /////////////////////////
    it('generateRegEx should return matching words', () => {
        const letters = [A, N];

        expect(service.generateRegEx(letters)).toEqual('(^a{1}n{1}$)');
    });

    it('generateRegEx should return matching words with noLetters', () => {
        const letters = [A, NOT_A_LETTER, NOT_A_LETTER];

        expect(service.generateRegEx(letters)).toEqual('(^a{1}$)|(^a{1}.?$)|(^a{1}.?.?$)');
    });

    it('generateRegEx should return matching words with noLetters and spaces', () => {
        const letters = [NOT_A_LETTER, A, NOT_A_LETTER, A, NOT_A_LETTER];

        expect(service.generateRegEx(letters)).toEqual('(^a{1}$)|(^a{1}.?$)|(^.?a{1}$)|(^.?a{1}.a{1}$)|(^.?a{1}.a{1}.?$)');
    });

    /// //////////////////////////  wordValidationInit  /////////////////////////
    it('wordValidationInit should return a message if the word is not in the dictionnary', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        const msg = 'Votre mot: ' + command.word + ', ne figure pas dans le dictionnaire!';
        serviceScore.spotNewLetters.and.callFake(() => {
            return;
        });
        serviceScore.wordInDictionnary.and.returnValue(false);
        serviceScore.resetIndex.and.callFake(() => {
            return;
        });
        expect(service.wordValidationInit(command).errorMsg).toEqual(msg);
    });

    it('wordValidationInit should not return a message if the word is in the dictionnary', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };

        serviceScore.spotNewLetters.and.callFake(() => {
            return;
        });
        serviceScore.checkNewFormedWords.and.returnValue(-1);
        serviceScore.wordInDictionnary.and.returnValue(true);
        serviceScore.resetIndex();

        expect(service.wordValidationInit(command).errorMsg).toEqual("Votre Mot forme des mots qui n'existe pas dans le dictionnaire!");
    });

    it('wordValidationInit should validate the command if the new formed words return an index', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };

        serviceScore.spotNewLetters.and.callFake(() => {
            return;
        });
        serviceScore.wordInDictionnary.and.returnValue(true);
        serviceScore.resetIndex.and.callFake(() => {
            return;
        });
        serviceScore.checkNewFormedWords.and.returnValue(0);
        expect(service.wordValidationInit(command).isValid).toEqual(true);
    });
    it('wordValidationInit should not validate the command if the new formed words return an undefined index', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };

        serviceScore.spotNewLetters.and.callFake(() => {
            return;
        });
        serviceScore.wordInDictionnary.and.returnValue(true);
        serviceScore.resetIndex.and.callFake(() => {
            return;
        });
        serviceScore.checkNewFormedWords.and.returnValue(UNDEFINED_INDEX);
        expect(service.wordValidationInit(command).isValid).toEqual(false);
    });

    /// //////////////////////////  buildChatInput  /////////////////////////
    it('buildChatInput should take a command and return it as a valid chat input without easelLetters', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        const points = 10;

        expect(service.buildChatInput(command, points)).toEqual('!placer a1h a  (10)');
    });

    it('buildChatInput should take a command and return it as a valid chat input with easelLetters', () => {
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const command = { word, position, direction };
        const points = 10;
        const easelLetter = 'a';

        expect(service.buildChatInput(command, points, easelLetter)).toEqual('!placer a1h a  (10)');
    });

    /// //////////////////////////  generateWords  /////////////////////////
    it('generateWords should return matching words', () => {
        const letters = [A];
        service.dict = { title: 'fakeDict', description: 'fake', words: ['a', 'b', 'c'] };
        expect(service.generateWords(letters)).toEqual(['a']);
    });
});
