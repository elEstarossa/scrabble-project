import { Injectable } from '@angular/core';
import { Command } from '@app/classes/command';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import { GameTime } from '@app/classes/time';
import { Vec2 } from '@app/classes/vec2';
import {
    BOARD_HEIGHT,
    BOARD_WIDTH,
    LEFTSPACE,
    LETTERS_OBJECT,
    MEDIUM_PX,
    NB_TILES,
    NOT_A_LETTER,
    THIRTY_SEC,
    TOPSPACE,
} from '@app/constants/constants';
import { EaselOperationsService } from './easel-operations.service';
import { GridService } from './grid.service';
import { ReserveService } from './reserve.service';

@Injectable({
    providedIn: 'root',
})
export class CommandsService {
    gridCtx: CanvasRenderingContext2D;
    letterCtx: CanvasRenderingContext2D;
    gridTiles = new Array<Letter[]>(NB_TILES);
    pixelSize: TileContent = MEDIUM_PX;

    constructor(private easelOperation: EaselOperationsService, private reserveService: ReserveService, private gridService: GridService) {
        this.fillGridTile();
    }

    tileIsEmpty(position: Vec2): boolean {
        return this.gridTiles[position.y - 1][position.x - 1] === NOT_A_LETTER;
    }

    redefineLetterSize() {
        this.letterCtx.clearRect(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
        const tmpArray: Letter[][] = [[], []];
        for (let i = 0; i < this.gridTiles.length; i++) {
            tmpArray[i] = this.gridTiles[i].slice();
        }
        this.fillGridTile();
        for (let i = 0; i < tmpArray.length; i++) {
            for (let j = 0; j < tmpArray[i].length; j++) {
                if (tmpArray[i][j] !== NOT_A_LETTER) {
                    this.placeLetter(tmpArray[i][j], { x: j + 1, y: i + 1 });
                }
            }
        }
    }

    placeWord(command: Command) {
        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h') {
                this.placeLetter(LETTERS_OBJECT.get(command.word.charAt(i)) ?? NOT_A_LETTER, { x: command.position.x + i, y: command.position.y });
            } else {
                this.placeLetter(LETTERS_OBJECT.get(command.word.charAt(i)) ?? NOT_A_LETTER, { x: command.position.x, y: command.position.y + i });
            }
        }
    }

    exchangeLetters(letterToChange: string, easel: EaselObject): boolean {
        const temp: Letter[] = [];
        if (easel.contains(letterToChange) && !this.reserveService.isReserveEmpty()) {
            for (let i = 0; i < letterToChange.length; i++) {
                temp.push(this.easelOperation.getLetterFromEasel(easel, easel.indexOfUsedLetters[i]));
            }
            this.easelOperation.refillEasel(easel, true);
            for (const lett of temp) {
                this.reserveService.reFillReserve(lett);
            }

            easel.resetUsedLetters();
            return true;
        }
        return false;
    }
    buildWord(command: Command) {
        let word = '';
        let j = 0;
        let k = 1;
        if (this.tileIsEmpty(command.position)) {
            while (this.getPosition(command) - 1 > 0 && this.getPreviousLetters(command, k) !== NOT_A_LETTER) k++;
            k--;
            if (command.direction === 'h') command.position.x -= k;
            else command.position.y -= k;
            while (this.getPreviousLetters(command, k) !== NOT_A_LETTER) {
                word += this.getPreviousLetters(command, k).charac;
                k--;
            }
        }
        for (let i = 0; i < command.word.length; i++) {
            while (this.getPosition(command) - 1 + j < NB_TILES && this.getLetterDirection(command, j) !== NOT_A_LETTER) {
                word += this.getLetterDirection(command, j).charac;
                j++;
            }
            word += command.word.charAt(i);
            j++;
            while (this.getPosition(command) - 1 + j < NB_TILES && this.getLetterDirection(command, j) !== NOT_A_LETTER) {
                word += this.getLetterDirection(command, j).charac;
                j++;
            }
        }
        command.word = this.normalizeCharacters(word);
    }
    wordIsPlacable(command: Command, easel: EaselObject): string {
        let saveLetter = '';
        let letterFromEasel = '';

        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h') {
                saveLetter = this.gridTiles[command.position.y - 1][command.position.x - 1 + i].charac;
            } else if (command.direction === 'v') {
                saveLetter = this.gridTiles[command.position.y - 1 + i][command.position.x - 1].charac;
            }
            if (saveLetter !== command.word.charAt(i)) {
                letterFromEasel += command.word.charAt(i);
            }
        }
        if (easel.contains(letterFromEasel)) {
            return letterFromEasel;
        }
        return '';
    }
    incrementTimer(increment: boolean, timer: GameTime) {
        switch (timer.sec) {
            case 0:
                if (increment) timer.sec += THIRTY_SEC;
                else {
                    timer.min--;
                    timer.sec = THIRTY_SEC;
                }
                break;
            case THIRTY_SEC:
                if (increment) {
                    timer.sec = 0;
                    timer.min++;
                } else {
                    timer.sec = 0;
                }
                break;
        }
    }
    timerToString(timer: GameTime): string {
        let timerString = '';
        timerString += timer.min + ' : ';
        if (timer.sec === 0) timerString += '00';
        else timerString += timer.sec;
        return timerString;
    }
    placeLetter(letter: Letter, position: Vec2) {
        if (this.tileIsEmpty(position)) {
            const imgLetter = new Image();
            imgLetter.src = './assets/letter-joker.png';
            imgLetter.onload = () => {
                this.letterCtx.drawImage(
                    imgLetter,
                    LEFTSPACE + ((position.x - 1) * BOARD_WIDTH) / NB_TILES,
                    TOPSPACE + ((position.y - 1) * BOARD_WIDTH) / NB_TILES,
                    BOARD_WIDTH / NB_TILES,
                    BOARD_HEIGHT / NB_TILES,
                );

                this.gridService.drawLetter(letter, position, this.pixelSize, this.letterCtx);
                this.gridService.drawScore(letter, position, this.pixelSize, this.letterCtx);
            };
            this.gridTiles[position.y - 1][position.x - 1] = letter;
        }
    }
    fillGridTile() {
        for (let i = 0; i < this.gridTiles.length; ++i) {
            this.gridTiles[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
    }
    getPosition(command: Command): number {
        return command.direction === 'h' ? command.position.x : command.position.y;
    }
    normalizeCharacters(word: string): string {
        let newWord = '';
        for (let n = 0; n < word.length; n++) {
            newWord += LETTERS_OBJECT.get(word.charAt(n))?.charac;
        }
        newWord.toLowerCase();
        return newWord;
    }
    private getLetterDirection(command: Command, j: number): Letter {
        return command.direction === 'h'
            ? this.gridTiles[command.position.y - 1][command.position.x - 1 + j]
            : this.gridTiles[command.position.y - 1 + j][command.position.x - 1];
    }
    private getPreviousLetters(command: Command, k: number): Letter {
        return command.direction === 'h'
            ? this.gridTiles[command.position.y - 1][command.position.x - 1 - k]
            : this.gridTiles[command.position.y - 1 - k][command.position.x - 1];
    }
}
