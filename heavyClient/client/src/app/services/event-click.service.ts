import { Injectable } from '@angular/core';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import {
    BOARD_HEIGHT,
    BOARD_WIDTH,
    EASEL_LENGTH,
    HAND_POSITION_END,
    HAND_POSITION_START,
    H_ARROW,
    LEFTSPACE,
    NB_TILES,
    NOT_A_LETTER,
    TILE_SIZE,
    TOPSPACE,
    UNDEFINED_INDEX,
    UNDEFINED_POSITION,
} from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { CommandsService } from './commands.service';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { SuperGridService } from './super-grid.service';

@Injectable({
    providedIn: 'root',
})
export class EventClickService {
    confirmPlacementOBS = new BehaviorSubject('init');
    lastGridClickPosition: Vec2 = { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX };
    letterGridIsPlaced: boolean = false;
    isExchangeLetterSelected: boolean = false;
    letterToChange: string = '';
    boardLimit: boolean = false;

    constructor(
        private easelOperationsService: EaselOperationsService,
        private gameService: GameService,
        private superGridService: SuperGridService,
        private commandService: CommandsService,
    ) {}

    handleSwapClick(event: MouseEvent) {
        const index = this.getEaselPosition({ x: event.offsetX, y: event.offsetY }, true);
        if (index !== UNDEFINED_INDEX) {
            if (this.lastGridClickPosition.x !== UNDEFINED_INDEX) {
                this.resetPlacement();
            }
            if (this.gameService.player1.easel.indexToMove === index) {
                this.cancelExchangeClick();
                this.gameService.player1.easel.indexToMove = UNDEFINED_INDEX;
            } else {
                this.cancelExchangeClick();
                this.gameService.player1.easel.indexToMove = index;
                this.superGridService.letterEaselToSwap(index);
            }
        }
    }
    handleEaselClick(event: MouseEvent) {
        const index = this.getEaselPosition({ x: event.offsetX, y: event.offsetY });
        if (index !== UNDEFINED_INDEX) {
            if (this.lastGridClickPosition.x !== UNDEFINED_INDEX || this.gameService.player1.easel.indexToMove !== UNDEFINED_INDEX) {
                this.resetPlacement();
                this.cancelSwap();
            }
            switch (this.gameService.player1.easel.posTempLetters[index]) {
                case false:
                    this.selectEaselLetter(index);
                    break;
                case true:
                    this.gameService.player1.easel.posTempLetters[index] = false;
                    this.letterToChange = '';
                    this.superGridService.clearTempCanvas();
                    for (let i = 0; i < EASEL_LENGTH; i++) {
                        if (this.gameService.player1.easel.posTempLetters[i]) this.selectEaselLetter(i);
                    }
                    if (this.letterToChange === '') this.cancelExchangeClick();
                    break;
            }
        }
    }
    swapLetter(direction: string) {
        if (this.gameService.player1.easel.indexToMove !== UNDEFINED_INDEX)
            switch (direction) {
                case 'LEFT':
                    this.easelOperationsService.moveLeft(this.gameService.player1.easel);
                    this.superGridService.clearTempCanvas();
                    this.superGridService.letterEaselToSwap(this.gameService.player1.easel.indexToMove);
                    break;
                case 'RIGHT':
                    this.easelOperationsService.moveRight(this.gameService.player1.easel);
                    this.superGridService.clearTempCanvas();
                    this.superGridService.letterEaselToSwap(this.gameService.player1.easel.indexToMove);
                    break;
            }
    }
    selectEaselLetter(index: number) {
        this.gameService.player1.easel.posTempLetters[index] = true;
        this.letterToChange += this.gameService.player1.easel.easelLetters[index].charac;
        this.superGridService.letterEaselToChange(index);
        this.isExchangeLetterSelected = true;
    }
    exchangeAction() {
        const input = '!echanger ' + this.letterToChange;
        this.confirmPlacementOBS.next(input);
        this.cancelExchangeClick();
    }
    cancelExchangeClick() {
        this.letterToChange = '';
        this.isExchangeLetterSelected = false;
        this.superGridService.clearTempCanvas();
        this.gameService.player1.easel.resetTempIndex();
    }
    cancelSwap() {
        this.gameService.player1.easel.indexToMove = UNDEFINED_INDEX;
        this.superGridService.clearTempCanvas();
    }
    handleGridClick(event: MouseEvent) {
        const tilePosition = this.convertMousePositionToGridTilePosition({ x: event.offsetX, y: event.offsetY });
        this.superGridService.gridIsClicked = tilePosition !== UNDEFINED_POSITION;
        if (this.gameService.player1.turnToPlay && this.superGridService.gridIsClicked && this.commandService.tileIsEmpty(tilePosition)) {
            this.cancelExchangeClick();
            this.cancelSwap();
            this.superGridService.initTile = JSON.parse(JSON.stringify(tilePosition));
            if (JSON.stringify(tilePosition) === JSON.stringify(this.lastGridClickPosition)) {
                this.superGridService.clearTempCanvas();
                this.easelOperationsService.resetTempLetter(this.gameService.player1.easel);
                this.superGridService.switchArrowDirection();
                this.superGridService.selectGridTile(tilePosition);
            } else {
                this.resetPlacement();
                this.lastGridClickPosition = { x: tilePosition.x, y: tilePosition.y };
                this.superGridService.selectGridTile(tilePosition);
            }
        }
    }
    resetPlacement() {
        this.easelOperationsService.resetTempLetter(this.gameService.player1.easel);
        this.superGridService.resetTempCanvas();
        this.boardLimit = false;
        this.lastGridClickPosition = { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX };
    }
    convertMousePositionToGridTilePosition(mousePosition: Vec2): Vec2 {
        if (
            mousePosition.x <= BOARD_WIDTH + LEFTSPACE &&
            mousePosition.y <= BOARD_HEIGHT + TOPSPACE &&
            mousePosition.x >= LEFTSPACE &&
            mousePosition.y >= TOPSPACE
        ) {
            const tilePositionX = Math.floor((mousePosition.x - LEFTSPACE) / TILE_SIZE);
            const tilePositionY = Math.floor((mousePosition.y - TOPSPACE) / TILE_SIZE);
            const tilePosition = { x: tilePositionX + 1, y: tilePositionY + 1 };
            return tilePosition;
        }

        return UNDEFINED_POSITION;
    }
    getEaselPosition(mousePosition: Vec2, swap?: boolean): number {
        if (
            (swap ? true : this.gameService.player1.turnToPlay) &&
            mousePosition.x >= LEFTSPACE + HAND_POSITION_START * TILE_SIZE &&
            mousePosition.x <= LEFTSPACE + HAND_POSITION_END * TILE_SIZE &&
            mousePosition.y >= TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 &&
            mousePosition.y <= TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + BOARD_HEIGHT / NB_TILES
        ) {
            return Math.floor((mousePosition.x - LEFTSPACE) / TILE_SIZE) - HAND_POSITION_START;
        }
        return UNDEFINED_INDEX;
    }

    keyValidation(key: string) {
        if (this.gameService.player1.turnToPlay && !this.boardLimit) {
            if (
                this.superGridService.arrowDirection === H_ARROW
                    ? this.superGridService.currentTilePosition.x === NB_TILES
                    : this.superGridService.currentTilePosition.y === NB_TILES
            )
                this.boardLimit = true;
            const keyIsLetter = !/[^a-zA-Z\u00C0-\u00FF]/.test(key);
            if (keyIsLetter && this.superGridService.gridIsClicked) {
                const letter: Letter = this.easelOperationsService.getTempLetter(key, this.gameService.player1.easel);
                if (letter !== NOT_A_LETTER) {
                    this.superGridService.placeTempSteps(letter);
                    this.letterGridIsPlaced = true;
                }
            }
        }
    }

    unClick() {
        this.letterGridIsPlaced = false;
        this.superGridService.gridIsClicked = false;
        this.lastGridClickPosition = UNDEFINED_POSITION;
        this.easelOperationsService.resetTempLetter(this.gameService.player1.easel);
        this.superGridService.resetTempCanvas();
        this.isExchangeLetterSelected = false;
        this.boardLimit = false;
    }

    confirmPlacement() {
        const input: string =
            '!placer ' +
            this.getLineLetter(this.superGridService.initTile.y) +
            this.superGridService.initTile.x.toString() +
            this.superGridService.getDirectionFromArrow() +
            ' ' +
            this.superGridService.tempWord;

        this.lastGridClickPosition = UNDEFINED_POSITION;
        this.easelOperationsService.resetTempLetter(this.gameService.player1.easel);
        this.superGridService.resetTempCanvas();
        this.letterGridIsPlaced = false;
        this.confirmPlacementOBS.next(input);
    }

    getLineLetter(number: number): string {
        const CHAR_OFFSET = 96;
        return String.fromCharCode(CHAR_OFFSET + number);
    }
    onBackSpaceLetters() {
        if (this.superGridService.tempWord !== '') {
            this.letterGridIsPlaced = this.superGridService.removeLastLetter();
            this.easelOperationsService.replaceTempInEasel(this.gameService.player1.easel);
            this.boardLimit = false;
        }
    }
    onPlaceLetters() {
        if (this.superGridService.tempWord !== '') {
            this.confirmPlacement();
        }
    }
    onEscapeLetters() {
        if (this.superGridService.tempWord !== '') {
            this.lastGridClickPosition = UNDEFINED_POSITION;
            this.superGridService.resetTempCanvas();
            this.easelOperationsService.resetTempLetter(this.gameService.player1.easel);
            this.letterGridIsPlaced = false;
        }
    }
}
