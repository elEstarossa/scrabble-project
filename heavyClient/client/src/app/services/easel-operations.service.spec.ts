/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { A, B, C, E, F, NOT_A_LETTER } from '@app/constants/constants';
import { EaselOperationsService } from './easel-operations.service';

describe('EaselOperationsService', () => {
    let service: EaselOperationsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
        });
        service = TestBed.inject(EaselOperationsService);
        service.gridCtx = CanvasTestHelper.createCanvas(15, 25).getContext('2d') as CanvasRenderingContext2D;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
    /// //////////////////   placeEaselLetters //////////////////////////////////

    it('should placeEaselLetters in the easel', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [A, B];
        chevalet.foundLetter[0] = false;
        chevalet.posTempLetters = new Array<boolean>(5);

        service.placeEaselLetters(chevalet);

        expect(chevalet.easelLetters[1].charac).toBe('b');
    });

    it('should placeEaselLetters in the easel', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [NOT_A_LETTER];
        chevalet.foundLetter[0] = false;
        chevalet.posTempLetters = new Array<boolean>(5);
        const spy = spyOn(service.gridCtx, 'clearRect');

        service.placeEaselLetters(chevalet);

        expect(spy).toHaveBeenCalled();
    });

    // /// //////////////////   getLetterFromEasel //////////////////////////////////
    it('should getLetterFromEasel and return a letter', () => {
        const index = 0;
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [C, F];
        const result = service.getLetterFromEasel(chevalet, index);

        expect(result).toEqual(C);
    });

    it(' should getLetterFromEasel and return a not letter', () => {
        const chevalet = new EaselObject(true);
        const index = 0;
        chevalet.easelLetters = [NOT_A_LETTER];

        const result = service.getLetterFromEasel(chevalet, index);

        expect(result).toBe(NOT_A_LETTER);
    });

    // // /// //////////////////   refillEasel //////////////////////////////////
    it('should refillEasel when the reserve is empty ', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [E, E, E, E, E, E, E];
        const user = true;
        chevalet.indexOfUsedLetters = [0];
        spyOn(service['reserveService'], 'isReserveEmpty').and.returnValue(true);

        service.refillEasel(chevalet, user);

        expect(chevalet.easelLetters[0]).toEqual(NOT_A_LETTER);
    });

    it('should refillEasel when the reserve is not empty', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [E, E, E, E, E, E, E];
        const user = true;
        chevalet.indexOfUsedLetters = [0];
        spyOn(service['reserveService'], 'isReserveEmpty').and.returnValue(false);

        service.refillEasel(chevalet, user);

        expect(chevalet.easelLetters[0]).not.toEqual(NOT_A_LETTER);
    });

    it('should refillEasel when the reserve is not empty', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [E, E, E, E, E, E, E];
        const user = false;
        chevalet.indexOfUsedLetters = [0];
        spyOn(service['reserveService'], 'isReserveEmpty').and.returnValue(false);

        service.refillEasel(chevalet, user);

        expect(chevalet.easelLetters[0]).not.toEqual(NOT_A_LETTER);
    });

    // // /// //////////////////   fillEasel //////////////////////////////////
    it('should fillEasel when the reserve is not empty', () => {
        const chevalet = new EaselObject(true);
        const user = true;
        spyOn<any>(service['reserveService'], 'isReserveEmpty').and.returnValue(false);
        const spy = spyOn(chevalet, 'add');

        service.fillEasel(chevalet, user);

        expect(spy).toHaveBeenCalled();
    });

    it('should fillEasel when the reserve is  empty', () => {
        const chevalet = new EaselObject(true);
        const user = true;
        spyOn<any>(service['reserveService'], 'isReserveEmpty').and.returnValue(true);
        const spy = spyOn(chevalet, 'add');

        service.fillEasel(chevalet, user);

        expect(spy).not.toHaveBeenCalled();
    });

    it('should fillEasel when the reserve is  empty and the user is false', () => {
        const chevalet = new EaselObject(true);
        const user = false;
        spyOn<any>(service['reserveService'], 'isReserveEmpty').and.returnValue(true);
        const spy = spyOn(chevalet, 'add');

        service.fillEasel(chevalet, user);

        expect(spy).not.toHaveBeenCalled();
    });

    // // /// //////////////////   redrawEasel //////////////////////////////////
    it('should redrawEasel when the easel must be init', () => {
        const chevalet = new EaselObject(true);
        const spy = spyOn(service, 'placeEaselLetters');

        service.redrawEasel(chevalet);

        expect(spy).toHaveBeenCalled();
    });

    // // /// //////////////////   drawEaselLetter //////////////////////////////////
    it('should drawEaselLetter when the gridCtx call  a textBaseline ', () => {
        const letter: Letter = { score: 1, charac: 'a' };

        service.drawEaselLetter(letter, 5);

        expect(service.gridCtx.textBaseline).toEqual('top');
    });

    // // /// //////////////////   drawEaselScore //////////////////////////////////
    it('should drawEaselScore when the fill text is called ', () => {
        const letter: Letter = { score: 1, charac: 'a' };
        const spy = spyOn(service.gridCtx, 'fillText');
        service.drawEaselScore(letter, 5);

        expect(spy).toHaveBeenCalled();
    });

    // /// //////////////////   getTempLetter //////////////////////////////////
    it('should getTempLetter ', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [A];
        chevalet.posTempLetters = new Array<boolean>(1);
        const spy = spyOn(service.gridCtx, 'clearRect');

        const result = service.getTempLetter('a', chevalet);
        expect(spy).toHaveBeenCalled();
        expect(result).toEqual(A);
    });

    it('should getTempLetter ', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [B];
        chevalet.posTempLetters = new Array<boolean>(1);

        const result = service.getTempLetter('a', chevalet);
        expect(result).toEqual(NOT_A_LETTER);
    });

    // /// //////////////////   replaceTempInEasel //////////////////////////////////
    it('should replaceTempInEasel ', () => {
        const chevalet = new EaselObject(true);
        chevalet.indexTempLetters = [0, 2];
        const spy = spyOn(service, 'placeEaselLetters');

        service.replaceTempInEasel(chevalet);
        expect(spy).toHaveBeenCalled();
    });

    it('should replaceTempInEasel ', () => {
        const chevalet = new EaselObject(true);
        chevalet.indexTempLetters = [];
        const spy = spyOn(service, 'placeEaselLetters');

        service.replaceTempInEasel(chevalet);
        expect(spy).not.toHaveBeenCalled();
    });

    // /// //////////////////   resetTempLetter //////////////////////////////////
    it('should replaceTempInEasel ', () => {
        const chevalet = new EaselObject(true);
        chevalet.resetTempIndex();
        const spy = spyOn(service, 'placeEaselLetters');

        service.resetTempLetter(chevalet);
        expect(spy).toHaveBeenCalled();
    });

    // /// //////////////////   moveLeft //////////////////////////////////
    it('should moveLeft if we select a tile in the middle', () => {
        const chevalet = new EaselObject(true);
        chevalet.resetTempIndex();
        chevalet.easelLetters = [E, C, C, C, E, C, C];
        chevalet.indexToMove = 1;

        service.moveLeft(chevalet);
        expect(chevalet.easelLetters[1]).toEqual(E);
    });

    it('should moveLeft if we select a tile in the left extremity', () => {
        const chevalet = new EaselObject(true);
        chevalet.resetTempIndex();
        chevalet.easelLetters = [E, C, C, C, E, C, C];
        chevalet.indexToMove = 0;

        service.moveLeft(chevalet);
        expect(chevalet.easelLetters[0]).toEqual(C);
        expect(chevalet.easelLetters[6]).toEqual(E);
    });

    // /// //////////////////   moveRight //////////////////////////////////
    it('should moveRight if we select a tile in the middle', () => {
        const chevalet = new EaselObject(true);
        chevalet.resetTempIndex();
        chevalet.easelLetters = [E, C, C, C, E, C, C];
        chevalet.indexToMove = 4;

        service.moveRight(chevalet);
        expect(chevalet.easelLetters[4]).toEqual(C);
    });

    it('should moveRight if we select a tile in the right extremity', () => {
        const chevalet = new EaselObject(true);
        chevalet.resetTempIndex();
        chevalet.easelLetters = [E, C, C, C, E, E, C];
        chevalet.indexToMove = 6;

        service.moveRight(chevalet);
        expect(chevalet.easelLetters[0]).toEqual(C);
        expect(chevalet.easelLetters[6]).toEqual(E);
    });
});
