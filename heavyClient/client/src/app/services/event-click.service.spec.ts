/* eslint-disable max-len */
/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import { A, H_ARROW, NB_TILES, NOT_A_LETTER, UNDEFINED_POSITION, V_ARROW } from '@app/constants/constants';
import { EventClickService } from '@app/services/event-click.service';

describe('EventClickService', () => {
    let service: EventClickService;
    let gridCtx: CanvasRenderingContext2D;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, MatSnackBarModule],
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
        });
        service = TestBed.inject(EventClickService);

        gridCtx = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;
        service['superGridService'].gridCtx = gridCtx;
        service['easelOperationsService'].gridCtx = gridCtx;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////////////// getEaselPosition //////////////////////
    // <= 2440 pr x
    // >== 325 et plus petit que 15.47

    it('should getEaselPosition', () => {
        const mousePosition = { x: 2161, y: 14 } as Vec2;
        const swap = true;
        service['gameService'].player1.turnToPlay = true;
        const result = service.getEaselPosition(mousePosition, swap);

        expect(mousePosition.x).toEqual(2161);
        expect(result).toEqual(-1);
    });

    /// /////////////////// handleSwapClick //////////////////////
    it('should handleSwapClick if the index is not -1 and the last grid click position either', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(1);
        service.lastGridClickPosition.x = 1;
        const spy = spyOn(service, 'resetPlacement');

        service.handleSwapClick(mouseClickEvent);

        expect(spy).toHaveBeenCalled();
    });

    it('should handleSwapClick if the index is -1 and the last grid click position is not -1', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(-1);
        service.lastGridClickPosition.x = 1;
        const spy = spyOn(service, 'resetPlacement');

        service.handleSwapClick(mouseClickEvent);

        expect(spy).not.toHaveBeenCalled();
    });

    it('should handleSwapClick if the index is 1 and the last grid click position is  -1 and the index to move for the player is the same as the index', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(1);
        service.lastGridClickPosition.x = -1;
        service['gameService'].player1.easel.indexToMove = 1;
        const spy = spyOn(service, 'cancelExchangeClick');

        service.handleSwapClick(mouseClickEvent);

        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////// handleEaselClick //////////////////////
    it('should handleEaselClick if the index is not -1 and the last grid click position either and the index to move for the player is the same as the index ', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(1);
        service.lastGridClickPosition.x = 1;
        service['gameService'].player1.easel.indexToMove = 1;

        const spy = spyOn(service, 'resetPlacement');
        const spy2 = spyOn(service, 'cancelSwap');

        service.handleEaselClick(mouseClickEvent);

        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    it('should handleEaselClick if the index is not -1 and the last grid click position is -1 and the index to move for the player is -1 ', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(1);
        service.lastGridClickPosition.x = -1;
        service['gameService'].player1.easel.indexToMove = -1;

        const spy = spyOn(service, 'resetPlacement');
        const spy2 = spyOn(service, 'cancelSwap');

        service.handleEaselClick(mouseClickEvent);

        expect(spy).not.toHaveBeenCalled();
        expect(spy2).not.toHaveBeenCalled();
    });

    it('should handleEaselClick if the index is  -1 ', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(-1);
        service.lastGridClickPosition.x = -1;
        service['gameService'].player1.easel.indexToMove = -1;

        const spy = spyOn(service, 'resetPlacement');
        const spy2 = spyOn(service, 'cancelSwap');

        service.handleEaselClick(mouseClickEvent);

        expect(spy).not.toHaveBeenCalled();
        expect(spy2).not.toHaveBeenCalled();
    });

    it('should handleEaselClick when the posTemLetters is true  ', () => {
        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'getEaselPosition').and.returnValue(1);
        service['gameService'].player1.easel.posTempLetters = [true, true];
        const spy = spyOn(service['superGridService'], 'clearTempCanvas');
        const spy2 = spyOn(service, 'cancelExchangeClick');

        service.handleEaselClick(mouseClickEvent);

        expect(spy).toHaveBeenCalled();
        expect(spy2).not.toHaveBeenCalled();
    });

    /// /////////////////// swapLetter //////////////////////
    it('should swapLetter if  the direction is left  ', () => {
        const direction = 'LEFT';
        service['gameService'].player1.easel.indexToMove = 1;

        const spy = spyOn(service['easelOperationsService'], 'moveLeft');
        const spy2 = spyOn(service['superGridService'], 'clearTempCanvas');

        service.swapLetter(direction);

        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });
    it('should swapLetter if  the direction is left  ', () => {
        const direction = 'RIGHT';
        service['gameService'].player1.easel.indexToMove = 1;

        const spy = spyOn(service['easelOperationsService'], 'moveRight');
        const spy2 = spyOn(service['superGridService'], 'clearTempCanvas');

        service.swapLetter(direction);

        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    it('should swapLetter if index to move is -1  ', () => {
        const direction = '';
        service['gameService'].player1.easel.indexToMove = -1;

        const spy = spyOn(service['easelOperationsService'], 'moveRight');

        service.swapLetter(direction);

        expect(spy).not.toHaveBeenCalled();
    });

    /// /////////////////// exchangeAction //////////////////////
    it('should exchangeAction  ', () => {
        const spy = spyOn(service, 'cancelExchangeClick');

        service.exchangeAction();

        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////// handleGridClick //////////////////////
    it('handleGridClick shoud do nothing if it is not the user turn', () => {
        service['gameService'].player1.turnToPlay = false;
        service.lastGridClickPosition = { x: 1, y: 1 };

        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'convertMousePositionToGridTilePosition').and.returnValue({ x: 1, y: 1 });
        const spyCancelExchange = spyOn(service, 'cancelExchangeClick').and.callFake(() => {
            return;
        });
        const spyClearCanvas = spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return;
        });
        const spyArrowDirection = spyOn(service['superGridService'], 'switchArrowDirection').and.callFake(() => {
            return;
        });
        service.handleGridClick(mouseClickEvent);
        expect(spyCancelExchange).not.toHaveBeenCalled();
        expect(spyClearCanvas).not.toHaveBeenCalled();
        expect(spyArrowDirection).not.toHaveBeenCalled();
    });

    it('handleGridClick shoud not switch arrowDirection if the user does not click on the same tile', () => {
        service['gameService'].player1.turnToPlay = true;
        service.lastGridClickPosition = { x: 2, y: 1 };

        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'convertMousePositionToGridTilePosition').and.returnValue({ x: 1, y: 1 });

        spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return;
        });
        const spy = spyOn(service['superGridService'], 'switchArrowDirection').and.callFake(() => {
            return;
        });
        service.handleGridClick(mouseClickEvent);
        expect(spy).not.toHaveBeenCalled();
    });

    it('handleGridClick shoud switch arrowDirection if the user clicks on the same tile', () => {
        service['gameService'].player1.turnToPlay = true;
        service.lastGridClickPosition = { x: 1, y: 1 };

        const mouseClickEvent = { offsetX: 0, offsetY: 0, type: 'click' } as MouseEvent;
        spyOn(service, 'convertMousePositionToGridTilePosition').and.returnValue({ x: 1, y: 1 });

        spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return;
        });
        const spy = spyOn(service['superGridService'], 'switchArrowDirection').and.callFake(() => {
            return;
        });
        service.handleGridClick(mouseClickEvent);
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////// convertMousePositionToGridTilePosition //////////////////////
    it('convertMousePositionToGridTilePosition should return an undefined position if the user does not click on the grid', () => {
        const mousePosition = { x: 0, y: 0 };
        const result = service.convertMousePositionToGridTilePosition(mousePosition);
        expect(result).toEqual(UNDEFINED_POSITION);
    });

    it('convertMousePositionToGridTilePosition should return a tile position if the user clicks on the grid', () => {
        const mousePosition = { x: 100, y: 100 };
        const tilePositon = { x: 2, y: 2 };
        const result = service.convertMousePositionToGridTilePosition(mousePosition);
        expect(result).toEqual(tilePositon);
    });

    /// /////////////////// onBackSpaceLetters //////////////////////
    it('onBackSpaceLetters should do nothing if the tempWord is empty', () => {
        service['superGridService'].tempWord = '';
        spyOn(service['easelOperationsService'], 'replaceTempInEasel').and.callFake(() => {
            return;
        });
        const spyRemoveLastLetter = spyOn(service['superGridService'], 'removeLastLetter').and.callFake(() => {
            return true;
        });
        service.onBackSpaceLetters();
        expect(spyRemoveLastLetter).not.toHaveBeenCalled();
    });

    it('onBackSpaceLetters should call removeLastLetter if the tempWord is not empty', () => {
        service['superGridService'].tempWord = 'a';
        spyOn(service['easelOperationsService'], 'replaceTempInEasel').and.callFake(() => {
            return;
        });

        const spyRemoveLastLetter = spyOn(service['superGridService'], 'removeLastLetter').and.callFake(() => {
            return true;
        });
        service.onBackSpaceLetters();
        expect(spyRemoveLastLetter).toHaveBeenCalled();
    });

    /// /////////////////// onEscapeLetters //////////////////////
    it('onEscapeLetters should do nothing if the tempWord is empty', () => {
        service['superGridService'].tempWord = '';
        spyOn(service['easelOperationsService'], 'resetTempLetter').and.callFake(() => {
            return;
        });
        const spyResetTempCanvas = spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return true;
        });
        service.onEscapeLetters();
        expect(spyResetTempCanvas).not.toHaveBeenCalled();
    });

    it('onEscapeLetters should call resetTempCanvas if the tempWord is not empty', () => {
        service['superGridService'].tempWord = 'a';
        spyOn(service['easelOperationsService'], 'resetTempLetter').and.callFake(() => {
            return;
        });

        const spyResetTempCanvas = spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return true;
        });
        service.onEscapeLetters();
        expect(spyResetTempCanvas).toHaveBeenCalled();
    });

    /// /////////////////// keyValidation //////////////////////
    it('keyValidation should do nothing if the boardLimit has been reached', () => {
        service.boardLimit = false;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = '→';
        service['superGridService'].gridIsClicked = true;
        service['superGridService'].currentTilePosition = { x: 15, y: 15 };

        const letter: Letter = {
            score: 0,
            charac: 'a',
        };
        const spyGetTempLetter = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });
        service.keyValidation('a');
        expect(spyGetTempLetter).toHaveBeenCalled();
        expect(service.boardLimit).toBeTrue();
    });

    it('keyValidation should call getTempLetter if the boardLimit has not been reached and the grid gets clicked on', () => {
        service.boardLimit = false;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = '→';
        service['superGridService'].gridIsClicked = true;
        service['superGridService'].currentTilePosition = { x: 1, y: 15 };

        const letter: Letter = {
            score: 0,
            charac: 'a',
        };
        const spyGetTempLetter = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });
        service.keyValidation('a');
        expect(spyGetTempLetter).toHaveBeenCalled();
    });

    it('keyValidation should not call  getTempLetter if the boardLimit has  been reached', () => {
        service.boardLimit = true;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = '→';
        service['superGridService'].gridIsClicked = true;
        service['superGridService'].currentTilePosition = { x: 1, y: 15 };

        const letter: Letter = {
            score: 0,
            charac: 'a',
        };
        const spyGetTempLetter = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });
        service.keyValidation('a');
        expect(spyGetTempLetter).not.toHaveBeenCalled();
    });

    it('keyValidation should not call placeTempSteps if getTempLetter does not return a letter', () => {
        service.boardLimit = false;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = V_ARROW;
        service['superGridService'].gridIsClicked = true;
        service['superGridService'].currentTilePosition = { x: 1, y: NB_TILES };

        const letter: Letter = NOT_A_LETTER;

        const spy = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });

        service.keyValidation('a');
        expect(spy).toHaveBeenCalled();
    });

    it('keyValidation should  call getTempLetter and placeTempSteps if gridIsClicked is true and return a letter like A', () => {
        service.boardLimit = false;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = V_ARROW;
        service['superGridService'].gridIsClicked = true;
        service['superGridService'].currentTilePosition = { x: 1, y: NB_TILES };

        const letter: Letter = A;
        const spy = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });

        const spy2 = spyOn(service['superGridService'], 'placeTempSteps');

        service.keyValidation('a');
        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
        expect(service.letterGridIsPlaced).toBeTrue();
    });

    it('keyValidation should not call getTempLetter if gridIsClicked est false', () => {
        service.boardLimit = false;
        service['gameService'].player1.turnToPlay = true;
        service['superGridService'].arrowDirection = V_ARROW;
        service['superGridService'].gridIsClicked = false;
        service['superGridService'].currentTilePosition = { x: 1, y: NB_TILES };

        const letter: Letter = A;
        const spy = spyOn(service['easelOperationsService'], 'getTempLetter').and.callFake(() => {
            return letter;
        });

        service.keyValidation('a');
        expect(spy).not.toHaveBeenCalled();
    });

    /// /////////////////// unClick //////////////////////
    it('unClick should call resetTempCanvas', () => {
        const spyResetTempCanvas = spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return true;
        });
        service.unClick();
        expect(spyResetTempCanvas).toHaveBeenCalled();
    });

    /// /////////////////// confirmPlacement //////////////////////
    it('confirmPlacement should build the command and send a notification', () => {
        const input = '!placer h8h a';
        service['superGridService'].tempWord = 'a';
        service['superGridService'].arrowDirection = H_ARROW;
        service['superGridService'].initTile = { x: 8, y: 8 };
        spyOn(service['superGridService'], 'resetTempCanvas').and.callFake(() => {
            return true;
        });

        const spy = spyOn(service.confirmPlacementOBS, 'next').and.callFake(() => {
            return;
        });
        service.confirmPlacement();
        expect(spy).toHaveBeenCalledWith(input);
    });

    /// ///////////////////////////////// onPlaceLetters ///////////////////////////////////////////////
    it(' should onExchangeLettersClick', () => {
        service['superGridService'].tempWord = 'dani';
        const spy = spyOn(service, 'confirmPlacement');
        service.onPlaceLetters();

        expect(spy).toHaveBeenCalled();
    });

    it(' should onExchangeLettersClick', () => {
        service['superGridService'].tempWord = '';
        const spy = spyOn(service, 'confirmPlacement');
        service.onPlaceLetters();

        expect(spy).not.toHaveBeenCalled();
    });
});
