/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { TestBed } from '@angular/core/testing';
import { Letter } from '@app/classes/letter';
import { A, LETTERS_RESERVE_QTY } from '@app/constants/constants';
import { ReserveService } from './reserve.service';

describe('ReserveService', () => {
    let service: ReserveService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ReserveService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// ///////////////  getRandomLetter ////////////////////////////////////////
    it('should firstly return a letter in a random way from the reserve', () => {
        spyOn(Math, 'random').and.returnValue(1);
        const expected = service.reserveSize;
        const result = 100;
        service.getRandomLetter();
        expect(expected).toEqual(result);
    });

    /// ///////////////  reFillReserve ////////////////////////////////////////
    it('should refill the reserve when the letter in parametter is pushed in the tab of Letter  ', () => {
        const expected = service.reserveSize;
        const result = 100;

        service.reFillReserve(A);
        expect(expected).toBe(result);
    });

    /// ///////////////  isReserveEmpty ////////////////////////////////////////
    it('should return a true when the reserve is empty ', () => {
        for (let i = 0; i < service.letters.size; i++) {
            service.letters.delete(A);
        }
        const expected = service.reserveSize;
        service.isReserveEmpty();
        expect(expected).not.toEqual(0);
    });

    /// ///////////////  showReserve ////////////////////////////////////////
    it('should showReserve ', () => {
        service.letters = new Map<Letter, number>(LETTERS_RESERVE_QTY);
        const spy = spyOn(service.arrayOfReserveLetters, 'splice');
        service.showReserve();
        expect(spy).toHaveBeenCalled();
    });

    it('should showReserve ', () => {
        service.letters = new Map<Letter, number>();
        service.showReserve();
        expect(service.letters).toEqual(new Map<Letter, number>());
    });
});
