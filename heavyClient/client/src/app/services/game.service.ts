import { Injectable } from '@angular/core';
import { Message } from '@app/classes/message';
import { Player } from '@app/classes/player';
import { GameTime } from '@app/classes/time';
import { FIFTY_NINE_SEC, PASS_TURN_MAX, TIMER_INTERVAL } from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { EaselOperationsService } from './easel-operations.service';
import { ReserveService } from './reserve.service';
import { SuperGridService } from './super-grid.service';

@Injectable({
    providedIn: 'root',
})
export class GameService {
    player1: Player = new Player('');
    player2: Player = new Player('');
    multiplayerGame: boolean = false;
    score = new BehaviorSubject(0);
    scoreOpponent = new BehaviorSubject(0);
    passOBS = new BehaviorSubject('init');
    endGameOBS = new BehaviorSubject(false);

    roomName: string = '';
    isGameOver = false;
    isAbandon: boolean = false;
    isEaselEmpty: boolean = false;
    isThreePasses: boolean = false;
    abandonWinner: string = '';
    gameTimeTurn: GameTime = { min: 0, sec: 0 };
    timer: GameTime = { min: 0, sec: 0 };
    passTurnCounter: number = 0;
    log2990: boolean = false;
    timeCounter: number = 0;

    constructor(
        private reserveService: ReserveService,
        private superGridService: SuperGridService,
        private easelOperationService: EaselOperationsService,
    ) {}

    addScore(score: number) {
        this.player1.score += score;
        this.score.next(this.player1.score);
    }

    getJoiner(): Player {
        if (this.player1.creator) return this.player2;
        else return this.player1;
    }
    getCreator(): Player {
        if (this.player1.creator) return this.player1;
        else return this.player2;
    }
    alternateTurns() {
        this.easelOperationService.resetTempLetter(this.player1.easel);
        this.superGridService.resetTempCanvas();
        this.player1.turnToPlay = !this.player1.turnToPlay;
        this.player2.turnToPlay = !this.player2.turnToPlay;
        if (!this.multiplayerGame) {
            setTimeout(() => {
                this.checkEndGame();
                if (this.isGameOver) this.endGameOBS.next(true);
                this.timer = { min: this.gameTimeTurn.min, sec: this.gameTimeTurn.sec };
                this.timeCounter = 0;
            }, 0);
        }
    }
    getTimer(): string {
        let time = '';

        time += this.timer.min;
        time += ' : ';
        if (this.timer.sec === 0) time += '00';
        else {
            time += this.timer.sec;
        }

        return time;
    }

    getWinnerName(): string {
        if (this.isAbandon) {
            return this.player1.name;
        }
        if (this.player1.score > this.player2.score) {
            return this.player1.name;
        } else if (this.player1.score < this.player2.score) {
            return this.player2.name;
        } else {
            return 'egal';
        }
    }
    subEaselLetterScore() {
        for (const letter of this.player1.easel.easelLetters) this.player1.score -= letter.score;
        for (const letter of this.player2.easel.easelLetters) this.player2.score -= letter.score;
    }
    addEaselLetterScore() {
        if (this.player2.easel.getEaselSize() === 0) {
            for (const letter of this.player1.easel.easelLetters) {
                this.player2.score += letter.score;
            }
        }
        if (this.player1.easel.getEaselSize() === 0) {
            for (const letter of this.player2.easel.easelLetters) {
                this.player1.score += letter.score;
            }
        }
    }

    setIsGameOve() {
        this.isGameOver = true;
    }

    getIsGameOver(): boolean {
        return this.isGameOver;
    }
    getEndMessage(): Message {
        const winner = this.getWinnerName();
        if (this.isThreePasses) {
            if (winner === 'egal') {
                return {
                    colorRef: 'black',
                    name: 'système: ',
                    dm: 'partie terminée pour la raison 3 passes consécutives\n Egalité: ',
                };
            } else {
                return {
                    colorRef: 'black',
                    name: 'système: ',
                    dm: 'partie terminée pour la raison 3 passes consécutives\n Gagnant: ' + winner,
                };
            }
        } else if (this.isEaselEmpty) {
            if (winner === 'egal') {
                return {
                    colorRef: 'black',
                    name: 'système: ',
                    dm: 'partie terminée pour la raison chevalet et reserve vides\n Egalité: ',
                };
            } else {
                return {
                    colorRef: 'black',
                    name: 'système: ',
                    dm: 'partie terminée pour la raison chevalet et reserve vides\n Gagnant: ' + winner,
                };
            }
        } else if (this.isAbandon) {
            return {
                colorRef: 'black',
                name: 'système: ',
                dm: 'Votre adversaire a abondanné la partie! vous êtes le gagnant!',
            };
        }
        return { colorRef: '', name: '', dm: '' };
    }
    defineWhoStarts() {
        const digit = Math.round(Math.random());
        if (digit === 0) {
            this.player1.turnToPlay = true;
            this.player2.turnToPlay = false;
        } else {
            this.player1.turnToPlay = false;
            this.player2.turnToPlay = true;
        }
    }
    startTimer() {
        this.timer = { min: this.gameTimeTurn.min, sec: this.gameTimeTurn.sec };
        const interval = setInterval(() => {
            if (!this.isGameOver) {
                if (this.timer.min === 0 && this.timer.sec === 0) {
                    this.passOBS.next('!passer');
                    this.passTurnCounter++;

                    this.alternateTurns();
                } else if (this.timer.min !== 0 && this.timer.sec === 0) {
                    this.timer.min--;
                    this.timer.sec = FIFTY_NINE_SEC;
                } else {
                    this.timer.sec--;
                    this.timeCounter++;
                }
            } else clearInterval(interval);
        }, TIMER_INTERVAL);
    }

    checkEndGame() {
        if (this.passTurnCounter === PASS_TURN_MAX) {
            this.isGameOver = true;
            this.isThreePasses = true;
        }
        if ((this.player1.easel.getEaselSize() === 0 || this.player2.easel.getEaselSize() === 0) && this.reserveService.reserveSize === 0) {
            this.isEaselEmpty = true;
            this.isGameOver = true;
            this.addEaselLetterScore();
        }
    }
}
