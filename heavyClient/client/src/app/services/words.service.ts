import { Injectable } from '@angular/core';
import { CommandCode } from '@app/classes/comand-code';
import { Command } from '@app/classes/command';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { ASCI_CODE_A, GENERATE_REGEX, NOT_A_LETTER, UNDEFINED_INDEX } from '@app/constants/constants';
import { ScoreService } from './score.service';

@Injectable({
    providedIn: 'root',
})
export class WordsService {
    concatWord: string = '';
    isWordValid: boolean = false;
    usedWords = new Map<string, Vec2[]>();
    dict: WordDictionnary = { title: '', description: '', words: [] };

    constructor(private scoreService: ScoreService) {}

    wordValidationInit(command: Command): CommandCode {
        this.scoreService.spotNewLetters(command);
        const placement: CommandCode = { isValid: false, score: 0, errorMsg: '' };
        if (this.scoreService.wordInDictionnary(command.word)) {
            placement.score += this.scoreService.getWordBonusScore(command);
            const otherWords: number = this.scoreService.checkNewFormedWords(command);

            if (otherWords !== UNDEFINED_INDEX) {
                placement.score += otherWords;
                placement.isValid = true;
            } else {
                this.scoreService.resetIndex();
                return { isValid: false, score: 0, errorMsg: "Votre Mot forme des mots qui n'existe pas dans le dictionnaire!" };
            }
        } else {
            this.scoreService.resetIndex();
            return { isValid: false, errorMsg: 'Votre mot: ' + command.word + ', ne figure pas dans le dictionnaire!', score: 0 };
        }

        this.scoreService.resetIndex();
        return placement;
    }
    generateRegEx(lett: Letter[]): string {
        let regEx = '';
        let concat = '(^';
        let newRange = new Array<Letter>();
        const allRegEx = new Array<string>();
        let lastIsLetter = false;
        for (let i = 0; i < lett.length; i++) {
            switch (lett[i]) {
                case NOT_A_LETTER:
                    if (lastIsLetter) {
                        concat += '$)';
                        allRegEx.push(concat);
                        concat = concat.slice(0, GENERATE_REGEX);
                        let counterSpace = 0;
                        let j = i;
                        while (j < lett.length && lett[j] === NOT_A_LETTER) {
                            counterSpace++;
                            j++;
                        }
                        let save = concat;
                        if (j !== lett.length) {
                            newRange = lett.slice();
                            newRange.splice(0, i + 1);
                            regEx = this.generateRegEx(newRange);
                        }
                        switch (counterSpace) {
                            case 1:
                                if (j !== lett.length) concat += '.';
                                else {
                                    concat += '.?$)';
                                    allRegEx.push(concat);
                                }

                                break;
                            default:
                                while (counterSpace > 1) {
                                    save += '.';
                                    concat += '.?';
                                    counterSpace--;
                                }
                                concat += '$)';
                                allRegEx.push(concat);
                                concat = concat.slice(0, GENERATE_REGEX);
                                if (j !== lett.length) {
                                    save += '.';
                                    concat = save;
                                } else {
                                    concat += '.?$)';
                                    allRegEx.push(concat);
                                }
                                break;
                        }
                        i = j - 1;
                    } else {
                        concat += '.?';
                    }
                    lastIsLetter = false;
                    break;
                default:
                    concat += lett[i].charac + '{1}';
                    if (i === lett.length - 1) {
                        concat += '$)';
                        allRegEx.push(concat);
                    }
                    lastIsLetter = true;
                    break;
            }
        }
        if (regEx !== '') regEx += '|';
        for (const reg of allRegEx) regEx += reg + '|';
        regEx = regEx.slice(0, UNDEFINED_INDEX);
        return regEx;
    }

    generateWords(word: Letter[]): string[] {
        const matchWords: string[] = [];
        for (const letter of word) {
            this.concatWord += letter.charac;
        }

        for (let i = this.concatWord.length; i >= 1; i--) {
            const regex = new RegExp(`[${this.concatWord}]{${i}}`, 'g');

            for (const dictionaryWord of this.dict.words) {
                if (i === dictionaryWord.length) {
                    const match = regex.test(dictionaryWord);
                    if (match) {
                        matchWords.push(dictionaryWord);
                    }
                }
            }
        }
        this.concatWord = '';
        return matchWords;
    }

    buildChatInput(command: Command, points: number, easelLetters?: string): string {
        return (
            '!placer ' +
            String.fromCharCode(ASCI_CODE_A + (command.position.y - 1)) +
            command.position.x +
            command.direction +
            ' ' +
            (easelLetters ? easelLetters : command.word) +
            '  (' +
            points +
            ')'
        );
    }
}
