import { Injectable } from '@angular/core';
import { CommandCode } from '@app/classes/comand-code';
import { Command } from '@app/classes/command';
import { Vec2 } from '@app/classes/vec2';
import {
    ASCI_A,
    ASCI_O,
    COMMAND_ECHANGER,
    COMMAND_INDICE,
    COMMAND_PASSER,
    COMMAND_PLACER,
    COMMAND_POSITION,
    EMPTY_COMMAND,
    MAX_CHARACTER,
    MAX_SIZE,
    NB_TILES,
} from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { CommandsService } from './commands.service';
import { GameService } from './game.service';
import { MultiplayerService } from './multiplayer.service';
import { ReserveService } from './reserve.service';
import { SocketCommunicationService } from './socket-communication.service';
import { VirtualPlayerService } from './virtual-player.service';

@Injectable({
    providedIn: 'root',
})
export class ChatHandlerService {
    command: Command = JSON.parse(JSON.stringify(EMPTY_COMMAND));
    reserveCommandOBS = new BehaviorSubject(false);
    helpCommandOBS = new BehaviorSubject(false);

    helpTab: string[] = [];

    constructor(
        private multiplayerService: MultiplayerService,
        private virtualPlayer: VirtualPlayerService,
        private commandService: CommandsService,
        private gameService: GameService,
        private socketComm: SocketCommunicationService,
        private reserveService: ReserveService,
    ) {}

    chatImputValidation(chatInput: string): CommandCode {
        if (chatInput.length > MAX_CHARACTER) {
            return { isValid: false, errorMsg: 'Votre entrez dépasse la limite autorisé!', score: 0 };
        }
        if (chatInput[0] !== '!') {
            if (chatInput === '') return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
            return { isValid: false, errorMsg: 'msg', score: 0 };
        }

        const splitCommand = chatInput.split(' ');
        if (splitCommand[0] === '!reserve' && !splitCommand[1]) {
            this.reserveService.showReserve();
            this.reserveCommandOBS.next(true);
            return { isValid: true, errorMsg: 'serverWait', score: 0 };
        }
        if (splitCommand[0] === '!aide' && !splitCommand[1]) {
            this.helpDisplay();
            this.helpCommandOBS.next(true);
            return { isValid: true, errorMsg: 'serverWait', score: 0 };
        }

        if (this.gameService.player1.turnToPlay) {
            switch (splitCommand[0]) {
                case COMMAND_PLACER:
                    return this.placeCommand(splitCommand, chatInput);

                case COMMAND_ECHANGER:
                    return this.changeCommand(splitCommand);

                case COMMAND_PASSER:
                    return this.passCommand(splitCommand);
                case COMMAND_INDICE:
                    this.virtualPlayer.hintCommand = true;
                    this.virtualPlayer.placeSteps();
                    return { isValid: true, errorMsg: 'serverWait', score: 0 };
            }
        } else {
            return { isValid: false, errorMsg: "Ce n'est pas votre tour!", score: 0 };
        }
        return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
    }
    checkPlaceCommand(lettersFromEasel: string, chatInput: string): CommandCode {
        if (!this.validPosition(chatInput)) {
            return { isValid: false, errorMsg: 'Votre mot dépasse les limites de la grille!', score: 0 };
        } else {
            this.commandService.buildWord(this.command);
            if (
                this.commandService.tileIsEmpty({ x: 8, y: 8 }) &&
                (this.command.position.x !== COMMAND_POSITION || this.command.position.y !== COMMAND_POSITION)
            ) {
                return { isValid: false, errorMsg: 'Au premier tour vous devez placer votre mot en h8h!', score: 0 };
            }
            if (!this.commandService.tileIsEmpty({ x: 8, y: 8 }) && !this.wordIsAttached(this.command)) {
                return { isValid: false, score: 0, errorMsg: 'Votre mot dois être attaché aux mots sur la grille!' };
            }
            if (this.gameService.player1.easel.contains(lettersFromEasel)) {
                this.multiplayerService.sendCommandPlace(this.command);
            } else {
                this.gameService.player1.easel.resetUsedLetters();
                return { isValid: false, errorMsg: 'Votre chevalet ne contient pas tout les lettres entrées!', score: 0 };
            }
        }
        return { isValid: true, errorMsg: 'serverWait', score: 0 };
    }
    getLineLetter(number: number): string {
        const CHAR_OFFSET = 96;
        return String.fromCharCode(CHAR_OFFSET + number);
    }
    validPosition(dm: string): boolean {
        const splittedWord = dm.split(' ');
        const position = splittedWord[1].substring(1, splittedWord[1].length - 1);
        if (
            (splittedWord[1][splittedWord[1].length - 1] === 'h' || splittedWord[1][splittedWord[1].length - 1] === 'v') &&
            Number(position) >= 1 &&
            Number(position) <= MAX_SIZE &&
            splittedWord[1][0].charCodeAt(0) >= ASCI_A &&
            splittedWord[1][0].charCodeAt(0) <= ASCI_O
        ) {
            this.command.direction = splittedWord[1][splittedWord[1].length - 1];
            this.command.position = { x: Number(position), y: this.getLineNumber(splittedWord[1][0]) };
            this.command.word = splittedWord[2];
            return true;
        }
        return false;
    }
    wordIsAttached(command: Command): boolean {
        for (let i = 0; i < command.word.length; i++) {
            if (
                !this.commandService.tileIsEmpty(this.getTileDirection(command, i)) ||
                (this.getAxePosition(command) - 1 < NB_TILES ? !this.commandService.tileIsEmpty(this.getPerpNextTile(command, i)) : false) ||
                (this.getAxePosition(command) - 1 > 0 ? !this.commandService.tileIsEmpty(this.getPerpPrevTile(command, i)) : false)
            )
                return true;
        }

        return false;
    }
    helpDisplay() {
        const letter1 =
            '!placer<ligne><colonne>[(h|v)] :Lajout dune ou plusieurs lettres sur la grille pour former un ou plusieurs mots et accumuler des points';
        const letter2 = '!echanger<lettre>... : L echange d une ou plusieurs lettres du chevalet jusqu a concurrence de la totalite de ses lettres. ';
        const letter3 = '!indice : Obtenir trois possibilites de placement ';
        const letter4 = ' !reserve : Obtenir l etat courant de la reserve pendant le jeu ';
        const letter5 = ' !aide : Obtenir l ensemble des commandes disponibles et décrit briévement leur utilisation ';
        const letter6 = ' !passer : Passer le tour de jeux pour mettre fin au tour du joueur';
        this.helpTab.push(letter1);
        this.helpTab.push(letter2);
        this.helpTab.push(letter3);
        this.helpTab.push(letter4);
        this.helpTab.push(letter5);
        this.helpTab.push(letter6);
    }
    private changeCommand(splitCommand: string[]): CommandCode {
        let changeIsDone = false;

        if (splitCommand[2]) return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        if (!splitCommand[1] || splitCommand[1] === '') {
            return { isValid: false, errorMsg: 'Vous n avez entré aucune lettre à échanger', score: 0 };
        }
        changeIsDone = this.commandService.exchangeLetters(splitCommand[1], this.gameService.player1.easel);
        if (changeIsDone) this.multiplayerService.sendAction('A changé ' + splitCommand[1].length + ' lettres!');

        return {
            isValid: changeIsDone,
            errorMsg: changeIsDone ? 'change' : 'Les lettres à échanger ne sont pas dans le chevalet!',
            score: 0,
        };
    }
    private placeCommand(splitCommand: string[], chatInput: string): CommandCode {
        if (splitCommand.length === 1) return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        return this.checkPlaceCommand(splitCommand[2], chatInput);
    }

    private getLineNumber(letter: string): number {
        const asciiCode = letter.toLowerCase().charCodeAt(0);
        const CHAR_OFFSET = 96;
        const ligne = asciiCode - CHAR_OFFSET;
        return ligne;
    }

    private getTileDirection(command: Command, i: number): Vec2 {
        return command.direction === 'h'
            ? { x: command.position.x + i, y: command.position.y }
            : { x: command.position.x, y: command.position.y + i };
    }
    private getPerpNextTile(command: Command, i: number): Vec2 {
        return command.direction === 'h'
            ? { x: command.position.x + i, y: command.position.y + 1 }
            : { x: command.position.x + 1, y: command.position.y + i };
    }
    private getPerpPrevTile(command: Command, i: number): Vec2 {
        return command.direction === 'h'
            ? { x: command.position.x + i, y: command.position.y - 1 }
            : { x: command.position.x - 1, y: command.position.y + i };
    }
    private getAxePosition(command: Command): number {
        return command.direction === 'h' ? command.position.y : command.position.x;
    }
    private passCommand(splitCommand: string[]): CommandCode {
        if (splitCommand[1]) return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        this.socketComm.passTurn();
        if (splitCommand.length === 1) return { isValid: true, errorMsg: '', score: 0 };
        return { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
    }
}
