/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Command } from '@app/classes/command';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import { H_ARROW, NB_TILES, UNDEFINED_POSITION, V_ARROW } from '@app/constants/constants';
import { SuperGridService } from './super-grid.service';

describe('SuperGridService', () => {
    let service: SuperGridService;
    let gridCtx: CanvasRenderingContext2D;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
        });
        service = TestBed.inject(SuperGridService);

        gridCtx = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;
        service.gridCtx = gridCtx;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////////////////////// placeTempWord ///////////////////////////////////////////////
    it('should call the placetempLetter function when the direction is horizontal and take a NOT LETTER char', () => {
        const command: Command = { position: { x: -1, y: -1 }, word: '1', direction: 'h' };
        const spy = spyOn(service, 'placeTempLetter').and.callThrough();

        service.placeTempWord(command);
        expect(spy).toHaveBeenCalled();
    });

    it('should call the placetempLetter function when the direction is horizontal', () => {
        const command: Command = { position: { x: -1, y: -1 }, word: 'abc', direction: 'h' };
        const spy = spyOn(service, 'placeTempLetter').and.callThrough();

        service.placeTempWord(command);
        expect(spy).toHaveBeenCalled();
    });

    it('should call the placetempLetter function when the direction is vertical and take a  NOT LETTER char', () => {
        const command: Command = { position: { x: -1, y: -1 }, word: '1', direction: 'v' };
        const spy = spyOn(service, 'placeTempLetter');

        service.placeTempWord(command);
        expect(spy).toHaveBeenCalled();
    });

    it('should call the placetempLetter function when the direction is not vertical or horizontal', () => {
        const command: Command = { position: { x: -1, y: -1 }, word: 'allo', direction: '' };
        const spy = spyOn(service, 'placeTempLetter');

        service.placeTempWord(command);
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////////////// clearTempCanvas ///////////////////////////////////////////////
    it('should call the clearRect function', () => {
        const spy = spyOn(service.gridCtx, 'clearRect');
        service.clearTempCanvas();
        expect(spy).toHaveBeenCalled();
    });
    /// /////////////////////////// placeTempLetter ///////////////////////////////////////////////
    it('should call the clearRect function', () => {
        const vec: Vec2 = { x: 1, y: 1 };
        const letter: Letter = { score: 4, charac: 'h' };
        spyOn(service.gridCtx, 'drawImage');
        service.placeTempLetter(letter, vec);
        expect(letter).toEqual({ score: 4, charac: 'h' });
    });

    /// /////////////////////////// placeTempSteps ///////////////////////////////////////////////
    it('placeTempSteps should call selectGridTile if findNextTile is true', () => {
        const letter: Letter = { score: 4, charac: 'h' };
        spyOn(service.gridCtx, 'drawImage');
        spyOn(service, 'findNextTile').and.returnValue(true);
        const spyGridTile = spyOn(service, 'selectGridTile');

        service.placeTempSteps(letter);
        expect(spyGridTile).toHaveBeenCalled();
    });

    it('placeTempSteps should not call selectGridTile if findNextTile is false', () => {
        const letter: Letter = { score: 4, charac: 'h' };
        spyOn(service.gridCtx, 'drawImage');
        spyOn(service, 'findNextTile').and.returnValue(false);
        const spyGridTile = spyOn(service, 'selectGridTile');

        service.placeTempSteps(letter);
        expect(spyGridTile).not.toHaveBeenCalled();
    });

    /// /////////////////////////// findNextTile ///////////////////////////////////////////////
    it('findNextTile should return false if the currentPosition is at the border horizontally', () => {
        service.arrowDirection = H_ARROW;
        service.currentTilePosition = { x: NB_TILES, y: 1 };
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        const spyAddLetter = spyOn(service, 'addLetterFromGrid');
        const result = service.findNextTile();
        expect(spyAddLetter).toHaveBeenCalled();
        expect(result).toEqual(false);
    });

    it('findNextTile should return false if the currentPosition is at the border vertically', () => {
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: NB_TILES };
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        const spyAddLetter = spyOn(service, 'addLetterFromGrid');
        const result = service.findNextTile();
        expect(spyAddLetter).toHaveBeenCalled();
        expect(result).toEqual(false);
    });

    it('findNextTile should return true if the tile is Empty', () => {
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: NB_TILES };
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true);
        const result = service.findNextTile();
        expect(result).toEqual(true);
    });

    it('findNextTile should return false if the currentPosition is not a the border (it will increment)', () => {
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: 1 };
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        const result = service.findNextTile();
        expect(result).toEqual(false);
    });

    /// /////////////////////////// addLetterFromGrid ///////////////////////////////////////////////
    it('addLetterFromGrid should call incrementDirection', () => {
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: NB_TILES };
        const spyIncrement = spyOn<any>(service, 'incrementDirection');
        service.addLetterFromGrid();
        expect(spyIncrement).toHaveBeenCalled();
    });

    /// /////////////////////////// removeLastLetter ///////////////////////////////////////////////
    it('removeLastLetter should return false if the tempWord contains one letter', () => {
        service.initTile = { x: 1, y: 1 };
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: 2 };
        service.tempWord = 'a';
        expect(service.removeLastLetter()).toEqual(false);
    });

    it('removeLastLetter should return true if the tempWord contains more than one letter', () => {
        service.initTile = { x: 1, y: 1 };
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: 3 };
        service.tempWord = 'ab';
        spyOn(service, 'placeTempSteps').and.callFake(() => {
            return;
        });

        expect(service.removeLastLetter()).toEqual(false);
    });

    /// /////////////////////////// switchArrowDirection ///////////////////////////////////////////////
    it('switchArrowDirection should change h to v', () => {
        service.arrowDirection = H_ARROW;
        service.switchArrowDirection();
        expect(service.arrowDirection).toEqual(V_ARROW);
    });

    it('switchArrowDirection should change v to h', () => {
        service.arrowDirection = V_ARROW;
        service.switchArrowDirection();
        expect(service.arrowDirection).toEqual(H_ARROW);
    });

    /// /////////////////////////// selectGridTile ///////////////////////////////////////////////
    it('selectGridTile should do nothing if the tilePosition is undefined', () => {
        const tilePosition = UNDEFINED_POSITION;
        const spy = spyOn(service, 'selectTile');
        service.selectGridTile(tilePosition);
        expect(spy).not.toHaveBeenCalled();
    });

    /// /////////////////////////// resetArrowDirection  ///////////////////////////////////////////////
    it('resetArrowDirection should put the arrow direction equal to h', () => {
        service.arrowDirection = V_ARROW;
        service.resetArrowDirection();
        expect(service.arrowDirection).toEqual(H_ARROW);
    });

    /// /////////////////////////// getDirectionFromArrow  ///////////////////////////////////////////////
    it('getDirectionFromArrow should return the string of the direction (H_ARROW)', () => {
        service.arrowDirection = H_ARROW;
        expect(service.getDirectionFromArrow()).toEqual('h');
    });

    it('getDirectionFromArrow should return the string of the direction (V_ARROW)', () => {
        service.arrowDirection = V_ARROW;
        expect(service.getDirectionFromArrow()).toEqual('v');
    });

    /// /////////////////////////// resetTempCanvas  ///////////////////////////////////////////////
    it('resetTempCanvas should call clearTempCanvas', () => {
        service.arrowDirection = V_ARROW;
        const spy = spyOn(service, 'clearTempCanvas');
        service.resetTempCanvas();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////////////// drawArrowDirection  ///////////////////////////////////////////////
    it('drawArrowDirection should draw the arrow horizontally', () => {
        const position = { x: 1, y: 1 };
        service.arrowDirection = H_ARROW;
        const spy = spyOn(service.gridCtx, 'fillText');
        service['drawArrowDirection'](position);
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////////////////// incrementDirection  ///////////////////////////////////////////////

    it('incrementDirection should increment tilePosition.y when the direction is horizontal', () => {
        service.arrowDirection = H_ARROW;
        service.currentTilePosition = { x: 1, y: 1 };
        service['incrementDirection']();
        expect(JSON.stringify(service.currentTilePosition)).toEqual(JSON.stringify({ x: 2, y: 1 }));
    });

    it('incrementDirection should increment tilePosition.y when the direction is vertical', () => {
        service.arrowDirection = V_ARROW;
        service.currentTilePosition = { x: 1, y: 1 };
        service['incrementDirection']();
        expect(JSON.stringify(service.currentTilePosition)).toEqual(JSON.stringify({ x: 1, y: 2 }));
    });

    it('incrementDirection should do nothing tilePosition.y when the direction is nothing', () => {
        service.arrowDirection = '';
        service.currentTilePosition = { x: 1, y: 1 };
        service['incrementDirection']();
        expect(JSON.stringify(service.currentTilePosition)).toEqual(JSON.stringify({ x: 1, y: 1 }));
    });
});
