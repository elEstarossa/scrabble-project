import { Injectable } from '@angular/core';
import { Command } from '@app/classes/command';
import { JvP } from '@app/classes/jv-placement';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import {
    EASEL_LENGTH,
    EMPTY_COMMAND,
    MAX_SCORE,
    MEDIUM_SCORE,
    NB_LETTER_HAND,
    NB_TILES,
    NOT_A_LETTER,
    PASSING_ROUND,
    POURCENT,
    PROB_FOR_EXCHANGE,
    PROB_FOR_MAX_SCORE,
    PROB_FOR_MEDIUM_SCORE,
    PROB_FOR_PLACEMENT,
    PROB_FOR_SMALL_SCORE,
    SMALL_SCORE,
    TIME_FOR_JV_TO_PLAY,
    UNDEFINED_INDEX,
    VIRTUAL_PLAYER_NAMES,
} from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { CommandsService } from './commands.service';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { ObjectiveService } from './objective.service';
import { ReserveService } from './reserve.service';
import { ScoreService } from './score.service';
import { WordsService } from './words.service';

@Injectable({
    providedIn: 'root',
})
export class VirtualPlayerService {
    probAction: string = '';
    probScore: string = '';
    command: Command = JSON.parse(JSON.stringify(EMPTY_COMMAND));
    jvChatOBS = new BehaviorSubject('init');
    turnPlacement: JvP[] = [];
    played: boolean = false;
    hintCommand: boolean = false;
    hintOBS = new BehaviorSubject(new Array<string>());
    expert: boolean = false;

    constructor(
        private gameService: GameService,
        private commandService: CommandsService,
        private wordService: WordsService,
        private scoreService: ScoreService,
        private easelOperation: EaselOperationsService,
        private reserveService: ReserveService,
        private objectiveService: ObjectiveService,
    ) {}

    generateVirtualPlayerName() {
        let randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        while (VIRTUAL_PLAYER_NAMES[randomNameInex] === this.gameService.player1.name) {
            randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        }

        return VIRTUAL_PLAYER_NAMES[randomNameInex];
    }

    play() {
        this.played = false;
        this.generateActionProb();
        this.generatePointsProg();
        setTimeout(() => {
            this.actionHandler();
        }, TIME_FOR_JV_TO_PLAY);
    }

    actionHandler() {
        switch (this.probAction) {
            case 'place':
                this.placeSteps();
                if (!this.played) {
                    switch (this.expert) {
                        case true:
                            this.exchangeAction();
                            if (!this.played) this.passAction();
                            break;
                        case false:
                            this.passAction();
                            break;
                    }
                }
                break;
            case 'change':
                this.exchangeAction();
                if (!this.played) this.passAction();
                break;
            case 'pass':
                this.passAction();
                break;
        }
    }

    placeSteps() {
        let words: string[] = [];
        if (this.commandService.tileIsEmpty({ x: EASEL_LENGTH + 1, y: EASEL_LENGTH + 1 })) {
            words = this.wordService.generateWords(
                this.hintCommand ? this.gameService.player1.easel.easelLetters : this.gameService.player2.easel.easelLetters,
            );
            for (const word of words) {
                if (this.hintCommand ? this.gameService.player1.easel.contains(word) : this.gameService.player2.easel.contains(word)) {
                    this.buildCommand(word, { x: 8, y: 8 }, 'h', word);
                }
                if (this.hintCommand) this.gameService.player1.easel.resetUsedLetters();
                else this.gameService.player2.easel.resetUsedLetters();
            }
        } else {
            this.stockPlacement('h', this.commandService.gridTiles);
            this.stockPlacement('v', this.commandService.gridTiles);
        }
        if (this.turnPlacement.length > 0) this.placeAction();
    }
    stockPlacement(direction: string, grid: Letter[][]) {
        const line: Letter[] = [];
        const lineLetters: Letter[] = [];

        let pos: Vec2 = { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX };

        for (let i = 0; i < NB_TILES; i++) {
            for (let j = 0; j < NB_TILES; j++) {
                if (direction === 'v') pos = { x: i, y: j };
                else pos = { x: j, y: i };

                if (grid[pos.y][pos.x] !== NOT_A_LETTER) {
                    lineLetters.push(grid[pos.y][pos.x]);
                }
                line.push(grid[pos.y][pos.x]);
            }
            if (lineLetters.length > 0) {
                this.findLinePlacement(line, lineLetters, direction, pos.x, pos.y);
            }
            lineLetters.splice(0, lineLetters.length);
            line.splice(0, line.length);
        }
    }

    findLinePlacement(line: Letter[], letters: Letter[], direction: string, x: number, y: number) {
        const regEx = new RegExp(this.wordService.generateRegEx(line));
        const words: string[] = this.generateWords(letters);

        for (const word of words) {
            if (regEx.test(word)) {
                const pos = this.findIndexInLine(word, line);
                if (pos !== UNDEFINED_INDEX) {
                    const easelLetters = this.commandService.wordIsPlacable(
                        {
                            word,
                            position: direction === 'v' ? { x: x + 1, y: pos + 1 } : { x: pos + 1, y: y + 1 },
                            direction,
                        },
                        this.hintCommand ? this.gameService.player1.easel : this.gameService.player2.easel,
                    );
                    if (easelLetters !== '')
                        this.buildCommand(word, direction === 'v' ? { x: x + 1, y: pos + 1 } : { x: pos + 1, y: y + 1 }, direction, easelLetters);
                }
            }
            if (this.hintCommand) this.gameService.player1.easel.resetUsedLetters();
            else this.gameService.player2.easel.resetUsedLetters();
        }
    }

    placeAction() {
        if (this.hintCommand) {
            const hints: string[] = [];
            for (let i = 0; i < this.turnPlacement.length; i++) {
                const index = Math.floor(Math.random() * this.turnPlacement.length);
                hints.push(
                    this.wordService.buildChatInput(
                        this.turnPlacement[index].command,
                        this.turnPlacement[index].score,
                        this.turnPlacement[index].easelLetters,
                    ),
                );
                this.turnPlacement.splice(index, 1);
                if (i === 2) break;
            }
            this.hintOBS.next(hints);
            this.resetJV();
        } else {
            const index = this.expert ? 0 : Math.floor(Math.random() * this.turnPlacement.length);
            if (this.gameService.log2990) this.objectiveService.verifyObjective(this.turnPlacement[index].command, this.gameService.player2);
            this.commandService.placeWord(this.turnPlacement[index].command);
            if (this.gameService.player2.easel.contains(this.turnPlacement[index].easelLetters))
                this.easelOperation.refillEasel(this.gameService.player2.easel, false);
            this.gameService.player2.score += this.turnPlacement[index].score;
            this.jvChatOBS.next(this.wordService.buildChatInput(this.turnPlacement[index].command, this.turnPlacement[index].score));
            this.played = true;
            this.gameService.passTurnCounter = 0;
            this.resetJV();
        }
    }
    exchangeAction() {
        const numberOfLettersToExchange = this.expert
            ? this.reserveService.reserveSize < EASEL_LENGTH
                ? this.reserveService.reserveSize
                : NB_LETTER_HAND
            : Math.floor(Math.random() * EASEL_LENGTH) + 1;
        switch (numberOfLettersToExchange) {
            case 0:
                this.passAction();
                break;
            default:
                if (this.expert || this.reserveService.reserveSize >= EASEL_LENGTH) {
                    for (let i = 0; i < numberOfLettersToExchange; i++) {
                        const letterTemp = this.gameService.player2.easel.easelLetters[i];
                        this.gameService.player2.easel.add(this.reserveService.getRandomLetter(), i);
                        this.reserveService.reFillReserve(letterTemp);
                    }

                    this.played = true;
                    this.gameService.passTurnCounter = 0;
                    this.jvChatOBS.next('A échangé ' + numberOfLettersToExchange + ' lettres');
                    if (this.gameService.log2990) this.objectiveService.verifyObjective(EMPTY_COMMAND, this.gameService.player2);
                }
                break;
        }
    }
    passAction() {
        setTimeout(() => {
            this.played = true;
            this.gameService.passTurnCounter++;
            this.jvChatOBS.next('!passer');
        }, PASSING_ROUND);
    }

    buildCommand(word: string, position: Vec2, direction: string, easelLetters: string) {
        this.command = { word, position, direction };
        const score: number = this.scoreService.getScore(this.command);
        switch (true) {
            case this.hintCommand:
                if (score !== UNDEFINED_INDEX) this.turnPlacement.push({ command: this.command, score, easelLetters });
                break;
            case this.expert:
                if (score !== UNDEFINED_INDEX && (this.turnPlacement.length === 0 || this.turnPlacement[0].score < score)) {
                    this.turnPlacement.splice(0, this.turnPlacement.length);
                    this.turnPlacement.push({ command: this.command, score, easelLetters });
                }
                break;
            default:
                if (this.fitsTheProb(score)) this.turnPlacement.push({ command: this.command, score, easelLetters });
                break;
        }
    }
    private fitsTheProb(score: number): boolean {
        if (score === UNDEFINED_INDEX) return false;
        switch (this.probScore) {
            case 'SMALL_SCORE':
                return score > 0 && score <= PROB_FOR_SMALL_SCORE;
            case 'MEDIUM_SCORE':
                return score > PROB_FOR_SMALL_SCORE && score <= PROB_FOR_MEDIUM_SCORE;
            case 'MAX_SCORE':
                return score > PROB_FOR_MEDIUM_SCORE && score <= PROB_FOR_MAX_SCORE;
        }
        return false;
    }
    private generateActionProb() {
        if (this.expert) this.probAction = 'place';
        else {
            const prob = Math.floor(Math.random() * POURCENT);
            switch (true) {
                case prob < PROB_FOR_PLACEMENT:
                    this.probAction = 'place';
                    break;
                case prob < PROB_FOR_EXCHANGE:
                    this.probAction = 'change';
                    break;
                case prob < POURCENT:
                    this.probAction = 'pass';
                    break;
            }
        }
    }
    private generatePointsProg() {
        const prob = Math.floor(Math.random() * POURCENT);
        switch (true) {
            case prob < SMALL_SCORE:
                this.probScore = 'SMALL_SCORE';
                break;
            case prob < MEDIUM_SCORE:
                this.probScore = 'MEDIUM_SCORE';
                break;
            case prob < MAX_SCORE:
                this.probScore = 'MAX_SCORE';
                break;
        }
    }
    private generateWords(letter: Letter[]): string[] {
        for (const lett of this.hintCommand ? this.gameService.player1.easel.easelLetters : this.gameService.player2.easel.easelLetters) {
            letter.push(lett);
        }
        return this.wordService.generateWords(letter);
    }
    private findIndexInLine(word: string, line: Letter[]): number {
        let posInit = UNDEFINED_INDEX;
        let equal = false;
        let reRightCounter = 0;
        for (let i = 0; i < NB_TILES - word.length; i++) {
            for (let j = 0; j < word.length; j++) {
                if (word.charAt(j) === line[i + j].charac) {
                    reRightCounter++;
                    equal = true;
                }
                if (word.charAt(j) !== line[i + j].charac && line[i + j].charac !== NOT_A_LETTER.charac) {
                    equal = false;
                    break;
                }
                if (j === word.length - 1 && equal)
                    if (
                        (i + j < NB_TILES - 1 ? line[i + j + 1].charac === NOT_A_LETTER.charac : true) &&
                        (i !== 0 ? line[i - 1].charac === NOT_A_LETTER.charac : true) &&
                        reRightCounter !== 0
                    )
                        posInit = i;
            }
            if (posInit !== UNDEFINED_INDEX && reRightCounter < word.length) {
                break;
            } else {
                reRightCounter = 0;
                posInit = UNDEFINED_INDEX;
            }
        }
        return posInit;
    }
    private resetJV() {
        this.probAction = '';
        this.probScore = '';
        this.command = JSON.parse(JSON.stringify(EMPTY_COMMAND));
        this.hintCommand = false;
        this.turnPlacement.splice(0, this.turnPlacement.length);
    }
}
