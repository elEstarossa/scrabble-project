import { Injectable } from '@angular/core';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import { Vec2 } from '@app/classes/vec2';
import { AZUR_TILE, BLUE_TILE, PINK_TILE, RED_TILE } from '@app/constants/array-constant';
import {
    ADJUSTEMENT_TOPSPACE,
    BOARD_HEIGHT,
    BOARD_WIDTH,
    CLEAR_RECT_FIX,
    CTX_PX,
    HAND_POSITION_END as EASEL_END,
    HAND_POSITION_START as EASEL_START,
    HAND_POSITION_START,
    HEIGHT_IN_BOARD,
    LEFTSPACE,
    LIGHT_GREEN_CASE,
    NB_LETTER_HAND,
    NB_TILES,
    NUMBER_OF_TILES,
    OFFSET_DRAW_LETTER,
    TILE_SIZE,
    TOPSPACE,
} from '@app/constants/constants';
@Injectable({
    providedIn: 'root',
})
export class GridService {
    gridContext: CanvasRenderingContext2D;
    color: string;
    private alpha: string = 'abcdefghijklmno';
    private canvasSize: Vec2 = { x: BOARD_WIDTH, y: BOARD_HEIGHT };

    drawGrid() {
        this.gridContext.beginPath();
        this.gridContext.strokeStyle = 'black';
        this.gridContext.lineWidth = 3;
        for (let i = 0; i <= NB_TILES; i++) {
            this.gridContext.moveTo(LEFTSPACE, TOPSPACE + (i * BOARD_HEIGHT) / NB_TILES);
            this.gridContext.lineTo(LEFTSPACE + BOARD_WIDTH, TOPSPACE + (i * BOARD_HEIGHT) / NB_TILES);

            this.gridContext.moveTo(LEFTSPACE + (i * BOARD_WIDTH) / NB_TILES, TOPSPACE);
            this.gridContext.lineTo(LEFTSPACE + (i * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT);
        }

        this.gridContext.stroke();
    }
    drawEasel() {
        this.gridContext.beginPath();
        this.gridContext.strokeStyle = 'black';
        this.gridContext.lineWidth = 3;
        for (let i = 0; i <= NB_LETTER_HAND; i++) {
            this.gridContext.moveTo(LEFTSPACE + ((EASEL_START + i) * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2);
            this.gridContext.lineTo(
                LEFTSPACE + ((EASEL_START + i) * BOARD_WIDTH) / NB_TILES,
                TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + BOARD_WIDTH / NB_TILES,
            );
        }
        this.gridContext.moveTo(LEFTSPACE + (EASEL_START * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2);
        this.gridContext.lineTo(LEFTSPACE + (EASEL_END * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2);
        this.gridContext.moveTo(LEFTSPACE + (EASEL_START * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + BOARD_WIDTH / NB_TILES);
        this.gridContext.lineTo(LEFTSPACE + (EASEL_END * BOARD_WIDTH) / NB_TILES, TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2 + BOARD_WIDTH / NB_TILES);

        this.gridContext.stroke();
    }

    drawPositions() {
        this.gridContext.font = 'bold 15px system-ui';
        this.gridContext.strokeStyle = 'black';

        for (let i = 1; i <= NB_TILES; i++) {
            this.gridContext.fillText(
                String(i),
                LEFTSPACE + (i * BOARD_WIDTH) / NB_TILES - BOARD_WIDTH / (2 * NB_TILES),
                TOPSPACE - ADJUSTEMENT_TOPSPACE,
            );
            this.gridContext.fillText(this.alpha[i - 1], LEFTSPACE - CTX_PX, TOPSPACE + (i * BOARD_WIDTH) / NB_TILES - BOARD_WIDTH / (2 * NB_TILES));
        }
    }

    drawBox(): void {
        this.drawBonusBox(AZUR_TILE);
        this.drawBonusBox(RED_TILE);
        this.drawBonusBox(BLUE_TILE);
        this.drawBonusBox(PINK_TILE);
    }

    drawCentralTile() {
        const img = new Image();
        img.src = './assets/black-star.png';

        img.onload = () => {
            this.gridContext.drawImage(
                img,
                LEFTSPACE + (NB_LETTER_HAND * BOARD_WIDTH) / NB_TILES + 2,
                TOPSPACE + (NB_LETTER_HAND * BOARD_WIDTH) / NB_TILES + 2,
                BOARD_WIDTH / NB_TILES - CLEAR_RECT_FIX,
                BOARD_HEIGHT / NB_TILES - CLEAR_RECT_FIX,
            );
        };
        return img;
    }

    drawLetter(letter: Letter, position: Vec2, pixelSize: TileContent, letterCtx: CanvasRenderingContext2D) {
        letterCtx.font = pixelSize.alpha.toString() + 'px' + ' Calibri';
        const ltrWidth = letterCtx.measureText(letter.charac).width;
        letterCtx.textBaseline = 'top';
        const ltrHeight = letterCtx.measureText(letter.charac).actualBoundingBoxDescent;

        letterCtx.fillText(
            letter.charac,
            LEFTSPACE + ((position.x - 1) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - ltrWidth / OFFSET_DRAW_LETTER),
            TOPSPACE + ((position.y - 1) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - ltrHeight / 2),
        );
    }

    drawScore(letter: Letter, position: Vec2, pixelSize: TileContent, letterCtx: CanvasRenderingContext2D) {
        letterCtx.font = pixelSize.score.toString() + 'px' + ' Calibri';
        const scrWidth = letterCtx.measureText(letter.score.toString()).width;
        const scrHeight = letterCtx.measureText(letter.charac.toString()).actualBoundingBoxDescent;
        letterCtx.fillText(
            letter.score.toString(),
            LEFTSPACE + ((position.x - 1) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - scrWidth / 2) + scrWidth,
            TOPSPACE + ((position.y - 1) * BOARD_WIDTH) / NB_TILES + (TILE_SIZE / 2 - scrHeight / 2) + scrHeight,
        );
    }

    get width(): number {
        return this.canvasSize.x;
    }
    get height(): number {
        return this.canvasSize.y;
    }

    private drawBonusBox(bonusTiles: Vec2[]): void {
        let text = '';

        switch (bonusTiles[0].x) {
            case 0:
                this.color = 'red';
                text = 'MOT  X3';
                break;
            case 3:
                this.color = 'cyan';
                text = 'L.  X2';
                break;
            case 1:
                this.color = 'pink';
                text = 'MOT  X2';
                break;
            case LIGHT_GREEN_CASE:
                this.color = 'lightgreen';
                text = 'L.  X3';
                break;
        }
        this.gridContext.font = 'bold 12px system-ui';
        for (const tile of bonusTiles) {
            this.drawBonus(tile, text);
        }
    }

    private drawBonus(tile: Vec2, str: string) {
        this.gridContext.fillStyle = this.color;
        this.gridContext.fillRect(
            LEFTSPACE + (tile.x * BOARD_WIDTH) / NB_TILES,
            TOPSPACE + (tile.y * BOARD_HEIGHT) / NB_TILES,
            BOARD_WIDTH / NB_TILES,
            BOARD_HEIGHT / NB_TILES,
        );
        this.gridContext.fillStyle = 'black';
        const array = str.split(' ');
        for (let i = 0; i < array.length; i++) {
            this.gridContext.fillText(
                array[i],
                LEFTSPACE + (tile.x * BOARD_WIDTH) / NB_TILES + BOARD_WIDTH / NB_TILES / HAND_POSITION_START,
                TOPSPACE + (tile.y * BOARD_HEIGHT) / NB_TILES + this.height / NUMBER_OF_TILES + i * HEIGHT_IN_BOARD,
                BOARD_WIDTH / NB_TILES,
            );
        }
    }
}
