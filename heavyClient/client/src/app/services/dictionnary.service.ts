import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class DictionaryService {
    wordDictionnary: WordDictionnary;
    dictionnaryOBS = new BehaviorSubject({ title: '', description: '', words: [''] });
    text: string = '';
    errorMessage: string;
    deletionDialogForDict = false;
    listOfDictionnary: WordDictionnary[] = [];
    constructor(private http: HttpClient) {}

    dictionnarySubscription(): Observable<WordDictionnary[]> {
        return this.http.get<WordDictionnary[]>(environment.serverUrl + '/api/dict');
    }
    getDict(name: string) {
        this.dictionnarySubscription().subscribe((data: WordDictionnary[]) => {
            for (const dict of data) {
                if (dict.title === name) {
                    this.wordDictionnary = { title: dict.title, description: dict.description, words: dict.words };
                    this.dictionnaryOBS.next(this.wordDictionnary);
                }
            }
        });
    }
    async readFile(e: Event) {
        return new Promise((resolve, reject) => {
            const target = e.target as HTMLInputElement;
            const file: File = (target.files as FileList)[0];
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = async () => {
                const txt = reader.result as string;
                this.text = txt;
                this.uploadValidation(txt).subscribe(
                    (response) => {
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                    },
                );
            };
            reader.onerror = () => {
                this.errorMessage = 'Erreur lors de la lecture du fichier';
            };
        });
    }
    async create() {
        return new Promise((resolve, reject) => {
            const dict = JSON.parse(this.text);
            this.http.post(environment.serverUrl + '/api/dict', dict).subscribe(
                (response) => {
                    resolve(response);
                },
                (error) => {
                    reject(error);
                },
            );
        });
    }
    async getList() {
        return new Promise((resolve, reject) => {
            this.http.get(environment.serverUrl + '/api/dict').subscribe(
                (response) => {
                    if (response) {
                        this.listOfDictionnary = response as WordDictionnary[];
                    }
                },
                (error) => {
                    reject(error);
                },
            );
        });
    }
    uploadValidation(txt: string): Observable<boolean> {
        return new Observable<boolean>((subscriber) => {
            this.errorMessage = '';
            let nameAndFormatValid = true;
            try {
                JSON.parse(txt);
            } catch {
                this.errorMessage = 'Not a JSON file';
                subscriber.next(false);
            }
            const obj = JSON.parse(txt);
            for (const dict of this.listOfDictionnary) {
                if (dict.title === obj.title) {
                    nameAndFormatValid = false;
                    this.errorMessage = 'Nom existe déjà';
                }
            }
            if (!('title' in obj && 'description' in obj && 'words' in obj)) {
                nameAndFormatValid = false;
                this.errorMessage = 'Format du dictionnaire non valide';
            }

            subscriber.next(nameAndFormatValid);
        });
    }
    getDescription(): string {
        if (this.listOfDictionnary.length === 0) {
            this.getList();
            return this.listOfDictionnary[0].description;
        } else {
            return this.listOfDictionnary[0].description;
        }
    }
    async delete(deletionIndex: number) {
        return new Promise((resolve, reject) => {
            this.http.delete(environment.serverUrl + '/api/dict' + '/' + deletionIndex).subscribe(
                (response) => {
                    resolve(response);
                },
                (error) => {
                    reject(error);
                },
            );
        });
    }
    download(name: string) {
        this.http.get(environment.secondServerUrl + name).subscribe((dataReceived) => {
            this.downloadFile(JSON.stringify(dataReceived), name);
        });
    }
    downloadFile(data: string, name: string) {
        const blob = new Blob([data], { type: 'application/json' });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.download = name;
        link.href = url;
        link.click();
        window.URL.revokeObjectURL(url);
    }
    nameValidation(name: string) {
        let nameValid = true;
        for (const dict of this.listOfDictionnary) {
            if (dict.title === name) nameValid = false;
        }
        return nameValid;
    }
    descriptionValidation(description: string) {
        let descriptionValid = true;
        for (const dict of this.listOfDictionnary) {
            if (dict.description === description) descriptionValid = false;
        }
        return descriptionValid;
    }
    async modify(index: number, dictionnary: WordDictionnary) {
        return new Promise((resolve, reject) => {
            this.http.put(environment.serverUrl + '/api/dict' + '/' + index, dictionnary).subscribe(
                (response) => {
                    resolve(response);
                },
                (error) => {
                    reject(error);
                },
            );
        });
    }
}
