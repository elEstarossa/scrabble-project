/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Command } from '@app/classes/command';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { GameTime } from '@app/classes/time';
import { Vec2 } from '@app/classes/vec2';
import { A, E, L, NOT_A_LETTER } from '@app/constants/constants';
import { CommandsService } from './commands.service';

describe('CommandsService', () => {
    let service: CommandsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],

        });
        service = TestBed.inject(CommandsService);

        service.gridCtx = CanvasTestHelper.createCanvas(25, 25).getContext('2d') as CanvasRenderingContext2D;
        service.letterCtx = CanvasTestHelper.createCanvas(25, 25).getContext('2d') as CanvasRenderingContext2D;
    });

    afterEach(() => {
        service.fillGridTile();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// //////////////////////////  placeLetter /////////////////////////
    it('should placeLetter when the tile is empty', () => {
        const vec: Vec2 = { x: 1, y: 1 };
        const letter: Letter = { score: 4, charac: 'h' };

        spyOn(service.letterCtx, 'drawImage');
        service.placeLetter(letter, vec);
        expect(service.gridTiles[0][0]).toEqual(letter);
    });

    /// ///////////////////  redefineLetterSize /////////////////////////
    it('redefineLetterSize should not call PlaceLetter when there is no letter', () => {
        const letter: Letter[] = [{ score: 0, charac: '1' }];
        const gridTiles: Letter[][] = [[]];
        gridTiles.push(letter);
        const spy = spyOn(service.letterCtx, 'clearRect').and.callFake(() => {
            return;
        });
        spyOn(service.gridTiles, 'slice').and.returnValue(gridTiles);
        const spy1 = spyOn(service, 'fillGridTile');
        const spy2 = spyOn(service, 'placeLetter');

        service.redefineLetterSize();

        expect(spy).toHaveBeenCalled();
        expect(spy1).toHaveBeenCalled();
        expect(spy2).not.toHaveBeenCalled();
    });

    it('redefineLetterSize should call PlaceLetter when there is a letter', () => {
        const letter: Letter[] = [{ score: 1, charac: 'a' }];
        service.gridTiles.push(letter);

        const spy = spyOn(service.letterCtx, 'clearRect').and.callFake(() => {
            return;
        });
        const spy1 = spyOn(service, 'fillGridTile');
        const spyPlaceLetter = spyOn(service, 'placeLetter');

        service.redefineLetterSize();

        expect(spy).toHaveBeenCalled();
        expect(spy1).toHaveBeenCalled();
        expect(spyPlaceLetter).toHaveBeenCalled();
    });

    /// ///////////////////  placeWord /////////////////////////
    it('placeWord should call PlaceLetter with command.position.x + i if the command direction is h', () => {
        const newCommand: Command = { word: 'al', position: { x: 1, y: 1 }, direction: 'h' };
        const placeLetterSpy = spyOn(service, 'placeLetter');

        service.placeWord(newCommand);
        expect(placeLetterSpy).toHaveBeenCalledWith(L, { x: 2, y: 1 });
    });

    it('placeWord should call PlaceLetter with command.position.y + i if the command direction is not h', () => {
        const newCommand: Command = { word: 'al', position: { x: 1, y: 1 }, direction: 'v' };
        const placeLetterSpy = spyOn(service, 'placeLetter');

        service.placeWord(newCommand);
        expect(placeLetterSpy).toHaveBeenCalledWith(L, { x: 1, y: 2 });
    });

    it('placeWord should call PlaceLetter and detect if the word does not contain a letter when the direction is h', () => {
        const newCommand: Command = { word: '1', position: { x: 1, y: 1 }, direction: 'h' };
        const placeLetterSpy = spyOn(service, 'placeLetter');

        service.placeWord(newCommand);
        expect(placeLetterSpy).toHaveBeenCalledWith(NOT_A_LETTER, { x: 1, y: 1 });
    });

    it('placeWord should call PlaceLetter and detect if the word does not contain a letter when the direction is not h', () => {
        const newCommand: Command = { word: '1', position: { x: 1, y: 1 }, direction: 'v' };
        const placeLetterSpy = spyOn(service, 'placeLetter');

        service.placeWord(newCommand);
        expect(placeLetterSpy).toHaveBeenCalledWith(NOT_A_LETTER, { x: 1, y: 1 });
    });

    /// ///////////////////  exchangeLetters /////////////////////////
    it('exchangeLetters should return true if the easel contains the letters and if the reserve is not empty', () => {
        const easelObject = new EaselObject(false);
        const letterToChange = 'al';
        easelObject.add(A, 0);
        easelObject.add(L, 1);

        spyOn(service['reserveService'], 'isReserveEmpty').and.returnValue(false);
        spyOn<any>(service['easelOperation'], 'refillEasel').and.returnValue(true);
        spyOn<any>(service['reserveService'], 'reFillReserve').and.returnValue(true);

        const result = service.exchangeLetters(letterToChange, easelObject);

        expect(result).toBeTrue();
    });

    it('should return false when the letters are not availabe on the easel', () => {
        const easelObject = new EaselObject(true);
        const letterToChange = 'abc';
        easelObject.contains(letterToChange);

        spyOn(service['reserveService'], 'isReserveEmpty').and.returnValue(true);

        const result = service.exchangeLetters(letterToChange, easelObject);

        expect(result).toBeFalse();
    });

    /// ///////////////////  buildWord /////////////////////////
    it('buildWord should call normalizeCharacters', () => {
        const newCommand: Command = { word: 'a', position: { x: 1, y: 1 }, direction: 'v' };
        service.gridTiles[0][0] = NOT_A_LETTER;
        service.gridTiles[0][1] = { score: 0, charac: 'c' };
        const normalizeCharactersSpy = spyOn(service, 'normalizeCharacters');
        service.buildWord(newCommand);

        expect(normalizeCharactersSpy).toHaveBeenCalled();
    });

    it('buildWord should call getPosition', () => {
        const newCommand: Command = { word: 't', position: { x: 9, y: 8 }, direction: 'h' };
        service.gridTiles[7][8] = E;
        const getPositionSpy = spyOn(service, 'getPosition');
        service.buildWord(newCommand);
        expect(getPositionSpy).toHaveBeenCalled();
    });

    /// ///////////////////  getPreviousletters /////////////////////////
    it('getPreviousLetters should return the previous letter when the command direction is h', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'h' };
        service.gridTiles[1][0] = { score: 0, charac: 'b' };

        const result = service['getPreviousLetters'](newCommand, 1);

        expect(result).toEqual({ score: 0, charac: 'b' });
    });

    it('getPreviousLetters should return the previous letter when the command direction is not h', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'v' };
        service.gridTiles[0][1] = { score: 0, charac: 'b' };

        const result = service['getPreviousLetters'](newCommand, 1);

        expect(result).toEqual({ score: 0, charac: 'b' });
    });

    /// ///////////////////  normalizeCharacters /////////////////////////
    it('normalizeCharacters should remove accents', () => {
        const word = 'élo';
        const result = service.normalizeCharacters(word);

        expect(result).toEqual('elo');
    });

    /// ///////////////////  tileIsEmpty /////////////////////////
    it('should return a boolean true when the tile is empty', () => {
        const vec: Vec2 = { x: 1, y: 1 };

        service.tileIsEmpty(vec);

        expect(service.tileIsEmpty(vec)).toBeTrue();
    });

    /// ///////////////////  incrementTimer /////////////////////////
    /// /////////////////// CASE  /////////////////////////////////
    it('should incrementTimer when the increment is true', () => {
        const increment = true;
        const timer: GameTime = { min: 0, sec: 0 };

        service.incrementTimer(increment, timer);

        expect(timer.sec).toEqual(30);
    });

    it('should incrementTimer when the increment is false', () => {
        const increment = false;
        const timer: GameTime = { min: 0, sec: 0 };

        service.incrementTimer(increment, timer);

        expect(timer.min).toEqual(-1);
        expect(timer.sec).toEqual(30);
    });

    /// /////////////////// CASE 30SEC  /////////////////////////////////
    it('should incrementTimer when the increment is true', () => {
        const increment = true;
        const timer: GameTime = { min: 0, sec: 30 };

        service.incrementTimer(increment, timer);

        expect(timer.min).toEqual(1);
        expect(timer.sec).toEqual(0);
    });

    it('should incrementTimer when the increment is false', () => {
        const increment = false;
        const timer: GameTime = { min: 0, sec: 30 };

        service.incrementTimer(increment, timer);

        expect(timer.sec).toEqual(0);
    });

    /// /////////////////// wordIsPlacable  /////////////////////////////////
    it('should wordIsPlacable when the direction is horizontal', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'h' };
        const easelObject = new EaselObject(true);
        const saveLetter = '';
        service.wordIsPlacable(newCommand, easelObject);

        expect(saveLetter).toEqual('');
    });

    it('should wordIsPlacable when the direction is vertical', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'v' };
        const easelObject = new EaselObject(true);
        const saveLetter = '';
        service.wordIsPlacable(newCommand, easelObject);

        expect(saveLetter).toEqual('');
    });

    it('should wordIsPlacable when the direction is not vertical or horizontal', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'z' };
        const easelObject = new EaselObject(true);
        const saveLetter = '';
        service.wordIsPlacable(newCommand, easelObject);

        expect(saveLetter).toEqual('');
    });

    it('should wordIsPlacable when the direction is not vertical or horizontal', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'h' };
        const easelObject = new EaselObject(true);
        const letterFromEasel = '';
        easelObject.contains(letterFromEasel);
        const result = service.wordIsPlacable(newCommand, easelObject);

        expect(result).toEqual(letterFromEasel);
    });

    /// /////////////////// getLetterDirection  /////////////////////////////////
    it('should getLetterDirection and return the letter', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'h' };
        const letter: Letter = { score: 0, charac: '1' };

        const result = service['getLetterDirection'](newCommand, 1);

        expect(result).toEqual(letter);
    });

    /// /////////////////// getPosition  /////////////////////////////////
    it('should getPosition and return the position', () => {
        const newCommand: Command = { word: 'a', position: { x: 2, y: 2 }, direction: 'h' };

        const result = service.getPosition(newCommand);

        expect(result).toEqual(2);
    });

    /// /////////////////// normalizeCharacters  /////////////////////////////////
    it('should normalizeCharacters', () => {
        const result = service.normalizeCharacters('a');
        expect(result).toEqual('a');
    });
});
