/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable no-unused-expressions */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import { Vec2 } from '@app/classes/vec2';
import { BOARD_HEIGHT, BOARD_WIDTH, LARGE_PX } from '@app/constants/constants';
import { GridService } from '@app/services/grid.service';

describe('GridService', () => {
    let service: GridService;
    let fakeGridContext: CanvasRenderingContext2D;
    let fakeLetterContext: CanvasRenderingContext2D;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(GridService);
        fakeGridContext = CanvasTestHelper.createCanvas(BOARD_WIDTH, BOARD_HEIGHT).getContext('2d') as CanvasRenderingContext2D;
        fakeLetterContext = CanvasTestHelper.createCanvas(BOARD_WIDTH, BOARD_HEIGHT).getContext('2d') as CanvasRenderingContext2D;
        service.gridContext = fakeGridContext;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('getWidth should return the width', () => {
        expect(service.width).toEqual(BOARD_WIDTH);
    });

    it('getHeight should return the height', () => {
        expect(service.height).toEqual(BOARD_HEIGHT);
    });

    it('drawGrid should call beginPath and stroke', () => {
        const beginPathSpy = spyOn(service.gridContext, 'beginPath').and.callThrough();
        const strokeSpy = spyOn(service.gridContext, 'stroke').and.callThrough();
        service.drawGrid();
        expect(beginPathSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('drawEasel should call beginPath and stroke', () => {
        const beginPathSpy = spyOn(service.gridContext, 'beginPath').and.callThrough();
        const strokeSpy = spyOn(service.gridContext, 'stroke').and.callThrough();
        service.drawEasel();
        expect(beginPathSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('drawPositions should call fillText', () => {
        const fillTextSpy = spyOn(service.gridContext, 'fillText').and.callThrough();

        service.drawPositions();
        expect(fillTextSpy).toHaveBeenCalled();
    });

    it('drawLetter should call fillText with the letter character', () => {
        const fillTextSpy = spyOn(fakeLetterContext, 'fillText').and.callThrough();
        const letter: Letter = { score: 1, charac: 'A' };
        const position: Vec2 = { x: 1, y: 1 };
        const pixelSize: TileContent = LARGE_PX;

        service.drawLetter(letter, position, pixelSize, fakeLetterContext);
        expect(fillTextSpy).toHaveBeenCalledWith('A', jasmine.anything(), jasmine.anything());
    });

    it('drawScore should call fillText with the letter score', () => {
        const fillTextSpy = spyOn(fakeLetterContext, 'fillText').and.callThrough();
        const letter: Letter = { score: 1, charac: 'A' };
        const position: Vec2 = { x: 1, y: 1 };
        const pixelSize: TileContent = LARGE_PX;

        service.drawScore(letter, position, pixelSize, fakeLetterContext);
        expect(fillTextSpy).toHaveBeenCalledWith('1', jasmine.anything(), jasmine.anything());
    });

    it('drawBox should call the drawBonnusBox', () => {
        const spy = spyOn<any>(service, 'drawBonusBox');

        service.drawBox();
        expect(spy).toHaveBeenCalledTimes(4);
    });

    it('drawCentralTile', () => {
        const spy = spyOn(service.gridContext, 'drawImage');
        const image = service.drawCentralTile();
        if (image.onload !== null) image.onload({} as unknown as Event);
        expect(spy).toHaveBeenCalled();
    });

    it('drawBonusBox in the case of pink color', () => {
        const vec: Vec2[] = [{ x: 1, y: 1 }];

        service['drawBonusBox'](vec);
        expect(service.color).toEqual('pink');
    });

    it('drawBonusBox in the case of red color', () => {
        const vec: Vec2[] = [{ x: 0, y: 1 }];

        service['drawBonusBox'](vec);
        expect(service.color).toEqual('red');
    });

    it('drawBonusBox in the case of cyan color', () => {
        const vec: Vec2[] = [{ x: 3, y: 1 }];

        service['drawBonusBox'](vec);
        expect(service.color).toEqual('cyan');
    });

    it('drawBonusBox in the case of cyan color', () => {
        const vec: Vec2[] = [{ x: 5, y: 1 }];

        service['drawBonusBox'](vec);
        expect(service.color).toEqual('lightgreen');
    });
});
