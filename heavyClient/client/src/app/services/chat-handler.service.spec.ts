/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable prettier/prettier */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { CommandCode } from '@app/classes/comand-code';
import { Command } from '@app/classes/command';
import { EaselObject } from '@app/classes/easel-object';
import { ChatHandlerService } from './chat-handler.service';
import { VirtualPlayerService } from './virtual-player.service';

describe('ChatHandler', () => {
    let service: ChatHandlerService;
    let serviceVirtualPlayer: jasmine.SpyObj<VirtualPlayerService>;
    let gridCtxEaselGrid: CanvasRenderingContext2D;
    let gridCtxSuperGrid: CanvasRenderingContext2D;

    const spyVirtualPlayer = jasmine.createSpyObj('VirtualPlayerService', ['placeSteps', 'play']);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ChatHandlerService, { provide: VirtualPlayerService, useValue: spyVirtualPlayer }, { provide: APP_BASE_HREF, useValue: {} }],

            imports: [HttpClientTestingModule, MatSnackBarModule],
            schemas: [NO_ERRORS_SCHEMA],
        });
        service = TestBed.inject(ChatHandlerService);
        serviceVirtualPlayer = TestBed.inject(VirtualPlayerService) as jasmine.SpyObj<VirtualPlayerService>;
        service['gameService'].timer = { min: 0, sec: 30 };
        service['gameService'].gameTimeTurn = { min: 1, sec: 0 };
        gridCtxEaselGrid = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;
        gridCtxSuperGrid = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;

        service['gameService']['easelOperationService'].gridCtx = gridCtxEaselGrid;
        service['gameService']['superGridService'].gridCtx = gridCtxSuperGrid;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// //////////////// getLineNumber //////////////////////////////////////////

    it('getLineNumber should get line number', () => {
        const res = 'a';
        const result = service['getLineNumber'](res);
        expect(result).toEqual(1);
    });

    /// //////////////// validPosition //////////////////////////////////////////
    it('should validPosition when the direction is horizontal and the position is free ', () => {
        const word = '!placer h8h';
        const result = service['validPosition'](word);

        expect(service.command.direction).toEqual('h');
        expect(result).toBeTrue();
    });

    it('should validPosition when the direction is  vertical and the position is free ', () => {
        const word = '!placer h8v';
        const result = service['validPosition'](word);

        expect(service.command.direction).toEqual('v');
        expect(result).toBeTrue();
    });

    it('should validPosition when the direction is not  horizontal or vertical  or  the position is not free ', () => {
        const word = '!placer h8z';
        const result = service['validPosition'](word);

        expect(result).toBeFalse();
    });

    /// //////////////// chatImputValidation //////////////////////////////////////////
    it('should chatImputValidation when the command is not started with the "!" by returning a message error', () => {
        const input = 'placer';
        input.split('');
        const commandCode: CommandCode = { isValid: false, errorMsg: 'msg', score: 0 };

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should chatImputValidation when the command is not started with the "!" by returning a message error', () => {
        const input = '';
        input.split('');
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should chatImputValidation when the command is reserve', () => {
        const input = '!reserve';
        input.split('');
        const commandCode: CommandCode = { isValid: true, errorMsg: 'serverWait', score: 0 };

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should chatImputValidation when the command is aide', () => {
        const input = '!aide';
        input.split('');
        const commandCode: CommandCode = { isValid: true, errorMsg: 'serverWait', score: 0 };
        const spy = spyOn(service, 'helpDisplay');
        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
        expect(spy).toHaveBeenCalled();
    });

    it('should return a specific message error when the command is more than 512 imputs', () => {
        const input =
            'Accueilli avec enthousiasme lors de son annonce  le projet de train lger automatis a cependant essuy plusieurs critiques touchant son acceptabilité sociale remise en cause de la planification du projet; impact sur le rseau de transport existantimpact sur la valeur foncire des quartiers traverss; financement; transparence; et choix du trac Sur ce dernier aspect larrondissement RDP-PAT a entam ds 2021 des reprsentations pour que le REM soit prolongé au cœur de Rivire-des-Prairies tandis que MontralEst a rclam une station sur son territoire';
        input.split('');
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Votre entrez dépasse la limite autorisé!', score: 0 };

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    /// //////////////// CASE COMMAND PLACER ///////////////////////////////////
    it('should return a specific message error when the position of the command is wrong', () => {
        const input = '!placer h8h mot';
        input.split('');
        service['gameService'].player1.turnToPlay = true;
        const spy = spyOn(service, 'checkPlaceCommand');

        service.chatImputValidation(input);

        expect(spy).toHaveBeenCalled();
    });
    /// //////////////// CASE COMMAND_ECHANGER ///////////////////////////////////
    it('should return a specific message error when the command is exchange without any letter ', () => {
        const input = '!echanger';
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Vous n avez entré aucune lettre à échanger', score: 0 };
        service['gameService'].player1.turnToPlay = true;

        input.split('');

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should return a specific message error when the letters are not on the easel ', () => {
        const input = '!échanger abc';
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        service['gameService'].player1.turnToPlay = true;
        service['gameService'].player1.easel = new EaselObject(true);

        input.split('');

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should return a specific message error when the letters are not on the easel ', () => {
        const input = '!échanger abc';
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        service['gameService'].player1.turnToPlay = true;
        service['gameService'].player1.easel = new EaselObject(true);

        input.split('');

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    /// //////////////// CASE COMMAND_PASSER ///////////////////////////////////
    it('should return a specific message error when command is !passer', () => {
        const commandCode: CommandCode = { isValid: true, errorMsg: '', score: 0 };
        const input = '!passer';
        input.split('');
        service['gameService'].player1.turnToPlay = true;
        service['gameService'].player1.easel = new EaselObject(true);

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    /// //////////////// ANY CASE  ///////////////////////////////////
    it('should return a specific message error when command is not of our 3 commands', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };
        const input = '!passer abc';
        input.split('');
        service['gameService'].player1.turnToPlay = true;
        service['gameService'].player1.easel = new EaselObject(true);
        serviceVirtualPlayer.placeSteps.and.callFake(() => {
            return;
        });
        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    it('should return a specific message error when its not the turn of the player', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: "Ce n'est pas votre tour!", score: 0 };
        const input = '!passer abc';
        input.split('');
        service['gameService'].player1.turnToPlay = false;

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    /// //////////////// COMMAND_INDICE  ///////////////////////////////////
    it('should return a specific message error when command is not of our 3 commands', () => {
        const commandCode: CommandCode = { isValid: true, errorMsg: 'serverWait', score: 0 };
        const input = '!indice';
        input.split('');
        service['gameService'].player1.turnToPlay = true;
        service['virtualPlayer'].hintCommand = true;

        const result = service.chatImputValidation(input);

        expect(result).toEqual(commandCode);
    });

    /// //////////////// checkPlaceCommand  ///////////////////////////////////
    it('should return a specific message error when command is out of the grid', async () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Votre mot dépasse les limites de la grille!', score: 0 };
        const letters = 'abd';
        const input = '!placer h15v mot';
        spyOn(service, 'validPosition').and.returnValue(false);

        const result = service.checkPlaceCommand(letters, input);

        expect(await result).toEqual(commandCode);
    });

    it('should return a specific message error when the valid position is true ', () => {
        const letters = '';
        const input = '!placer h5h mot';
        spyOn(service, 'validPosition').and.returnValue(true);
        const spy = spyOn(service['commandService'], 'buildWord');

        service.checkPlaceCommand(letters, input);

        expect(spy).toHaveBeenCalled();
    });

    it('should return a specific message error when the easel have not enough letters ', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Votre chevalet ne contient pas tout les lettres entrées!', score: 0 };
        const letters = 'abc';
        service.command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };

        const input = '!placer h5h mot';
        spyOn(service, 'validPosition').and.returnValue(true);
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true)({ x: 8, y: 8 });

        const result = service.checkPlaceCommand(letters, input);

        expect(result).toEqual(commandCode);
    });

    it('should return a specific message error when word is not attacher to the other placement ', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Votre mot dois être attaché aux mots sur la grille!', score: 0 };
        const letters = 'abc';
        service.command = { word: 'mot', position: { x: 8, y: 8 }, direction: 'h' };

        const input = '!placer h5h mot';
        spyOn(service, 'wordIsAttached').and.returnValue(false);
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false)({ x: 8, y: 8 });

        const result = service.checkPlaceCommand(letters, input);

        expect(result).toEqual(commandCode);
    });

    it('should return a specific message error when centerTile is not placed first', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Au premier tour vous devez placer votre mot en h8h!', score: 0 };
        const letters = 'abc';
        service.command = { word: 'mot', position: { x: 7, y: 7 }, direction: 'h' };

        const input = '!placer h5h mot';
        spyOn(service, 'wordIsAttached').and.returnValue(false);
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true)({ x: 8, y: 8 });

        const result = service.checkPlaceCommand(letters, input);

        expect(result).toEqual(commandCode);
    });

    it('should call sendCommandPlace if the player contains the letters in the easel', () => {
        const commandCode: CommandCode = { isValid: true, errorMsg: 'serverWait', score: 0 };
        const letters = 'abc';
        service.command = { word: 'mot', position: { x: 7, y: 7 }, direction: 'h' };

        const input = '!placer h5h mot';
        spyOn(service['commandService'], 'buildWord').and.callFake(() => {
            return;
        });
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        spyOn(service, 'wordIsAttached').and.returnValue(true);
        spyOn(service['gameService'].player1.easel, 'contains').and.returnValue(true);
        const spySendCommandPlace = spyOn(service['multiplayerService'], 'sendCommandPlace');

        const result = service.checkPlaceCommand(letters, input);

        expect(spySendCommandPlace).toHaveBeenCalled();
        expect(result).toEqual(commandCode);
    });

    /// /////////////////////////////////// wordIsAttached ////////////////////////////////////
    it('should return true  when the tile is not empty ', () => {
        const command: Command = { word: 'allo', position: { x: 1, y: 1 }, direction: 'h' };

        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);

        const result = service.wordIsAttached(command);

        expect(result).toBeTrue();
    });

    it('should return false  when the tile is  empty ', () => {
        const command: Command = { word: 'allo', position: { x: 1, y: 2 }, direction: 'h' };

        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true);

        // spyOn(service['gameService'].player1.easel, 'contains').and.returnValue(true);

        const result = service.wordIsAttached(command);

        expect(result).toBeFalse();
    });

    /// /////////////////////////////// getAxePosition ////////////////////////////////////
    it('should return the x axe of position when the direction is horizontal  ', () => {
        const command: Command = { word: 'allo', position: { x: 2, y: 0 }, direction: 'h' };

        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);

        spyOn(service['gameService'].player1.easel, 'contains').and.returnValue(true);

        const result = service['getAxePosition'](command);

        expect(result).toEqual(0);
    });

    it('should return the x axe of position when the direction is vertical  ', () => {
        const command: Command = { word: 'allo', position: { x: 0, y: 2 }, direction: 'v' };

        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);

        spyOn(service['gameService'].player1.easel, 'contains').and.returnValue(true);

        const result = service['getAxePosition'](command);

        expect(result).toEqual(0);
    });

    /// /////////////////////////////////// passCommand ////////////////////////////////////
    it('should return true  when the tile is not empty ', () => {
        const commandCode: CommandCode = { isValid: true, errorMsg: '', score: 0 };

        const result = service['passCommand'](['']);

        expect(result).toEqual(commandCode);
    });

    it('should return true  when the tile is not empty ', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };

        const result = service['passCommand'](['', '']);

        expect(result).toEqual(commandCode);
    });

    /// /////////////////////////////////// changeCommand ////////////////////////////////////
    it('changeCommand should not change letters if the splitCommand contains spaces', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Vous n avez entré aucune lettre à échanger', score: 0 };

        const result = service['changeCommand'](['', '']);

        expect(result).toEqual(commandCode);
    });

    it('changeCommand should not change letters if the splitCommand does not contain letters in the easel ', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Les lettres à échanger ne sont pas dans le chevalet!', score: 0 };

        const result = service['changeCommand'](['!echanger', '!pacer']);

        expect(result).toEqual(commandCode);
    });

    it('changeCommand should not change letters if the splitCommand contains more than 2 elements ', () => {
        const commandCode: CommandCode = { isValid: false, errorMsg: 'Commande éroné!', score: 0 };

        const result = service['changeCommand'](['!echanger', 'a', 'b']);

        expect(result).toEqual(commandCode);
    });

    it('changeCommand should call sendAction if the change is done ', () => {
        const commandCode: CommandCode = { isValid: true, errorMsg: 'change', score: 0 };

        spyOn(service['commandService'], 'exchangeLetters').and.returnValue(true);
        const spySendAction = spyOn(service['multiplayerService'], 'sendAction');
        const result = service['changeCommand'](['!echanger', 'a']);
        expect(spySendAction).toHaveBeenCalled();
        expect(result).toEqual(commandCode);
    });

    /// /////////////////////////////////// placeCommand ////////////////////////////////////
    it('placeCommand should not validate the command if the splitCommand has a length of 1', () => {
        const result = service['placeCommand'](['!echanger'], 'chatInput');

        expect(result.isValid).toEqual(false);
    });

    /// /////////////////////////////////// getTileDirection ////////////////////////////////////
    it('getTileDirection should return the correct incrementation for a vertical direction', () => {
        const command: Command = { word: 'a', position: { x: 1, y: 1 }, direction: 'v' };
        const result = service['getTileDirection'](command, 1);

        expect(result).toEqual({ x: 1, y: 2 });
    });

    /// /////////////////////////////////// getPerpNextTile ////////////////////////////////////
    it('getPerpNextTile should return the correct incrementation for a vertical direction', () => {
        const command: Command = { word: 'a', position: { x: 2, y: 1 }, direction: 'h' };
        const result = service['getPerpNextTile'](command, 1);

        expect(result).toEqual({ x: 3, y: 2 });
    });

    /// /////////////////////////////////// getPerpPrevTile ////////////////////////////////////
    it('getPerpPrevTile should return the correct incrementation for an horizontal direction', () => {
        const command: Command = { word: 'a', position: { x: 1, y: 1 }, direction: 'h' };
        const result = service['getPerpPrevTile'](command, 1);

        expect(result).toEqual({ x: 2, y: 0 });
    });

    it('getPerpPrevTile should return the correct incrementation for a vertical direction', () => {
        const command: Command = { word: 'a', position: { x: 1, y: 1 }, direction: 'v' };
        const result = service['getPerpPrevTile'](command, 1);

        expect(result).toEqual({ x: 0, y: 2 });
    });

    /// /////////////////////////////////// getLineLetter ////////////////////////////////////
    it('getLineLetter should get line letter', () => {
        const result = service.getLineLetter(1);

        expect(result).toEqual('a');
    });

    /// /////////////////////////////////// helpDisplay ////////////////////////////////////
    it('should print all the infos when its the command help', () => {
        const letter1 =
            '!placer<ligne><colonne>[(h|v)] :Lajout dune ou plusieurs lettres sur la grille pour former un ou plusieurs mots et accumuler des points';
        const letter2 = '!echanger<lettre>... : L echange d une ou plusieurs lettres du chevalet jusqu a concurrence de la totalite de ses lettres. ';
        const letter3 = '!indice : Obtenir trois possibilites de placement ';
        const letter4 = ' !reserve : Obtenir l etat courant de la reserve pendant le jeu ';
        const letter5 = ' !aide : Obtenir l ensemble des commandes disponibles et décrit briévement leur utilisation ';
        const letter6 = ' !passer : Passer le tour de jeux pour mettre fin au tour du joueur';
        service.helpTab = [];
        service.helpDisplay();

        expect(service.helpTab).toEqual([letter1, letter2, letter3, letter4, letter5, letter6]);
    });
});
