/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Letter } from '@app/classes/letter';
import {
    A,
    B,
    EMPTY_COMMAND,
    NB_TILES,
    NOT_A_LETTER,
    PROB_FOR_MAX_SCORE,
    PROB_FOR_MEDIUM_SCORE,
    PROB_FOR_SMALL_SCORE,
    TIME_FOR_JV_TO_PLAY,
    UNDEFINED_INDEX,
} from '@app/constants/constants';
import { VirtualPlayerService } from './virtual-player.service';

describe('VirtualPlayerService', () => {
    let service: VirtualPlayerService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, MatSnackBarModule],
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
        });
        service = TestBed.inject(VirtualPlayerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// //////////////////////////  play  /////////////////////////
    it('play should call actionHandler', () => {
        jasmine.clock().install();
        const spyActionHandler = spyOn(service, 'actionHandler');
        service.play();
        jasmine.clock().tick(TIME_FOR_JV_TO_PLAY);
        expect(spyActionHandler).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    /// //////////////////////////  actionHandler  /////////////////////////
    it('actionHandler should call placeSteps if the probAction is "place"', () => {
        service.probAction = 'place';
        const spyPlaceSteps = spyOn(service, 'placeSteps');
        service.actionHandler();
        expect(spyPlaceSteps).toHaveBeenCalled();
    });

    it('actionHandler should not call passAction if the probAction is "place" and played is true', () => {
        service.probAction = 'place';
        service.played = true;
        const spyPlaceSteps = spyOn(service, 'placeSteps');
        const spyPassAction = spyOn(service, 'passAction');
        service.actionHandler();
        expect(spyPlaceSteps).toHaveBeenCalled();
        expect(spyPassAction).not.toHaveBeenCalled();
    });

    it('actionHandler should call exchangeAction if the probAction is "change"', () => {
        service.probAction = 'change';
        const spyExchangeAction = spyOn(service, 'exchangeAction');
        service.actionHandler();
        expect(spyExchangeAction).toHaveBeenCalled();
    });

    it('actionHandler should call exchangeAction if expert mode is on and probAction is "place" ', () => {
        service.expert = true;
        service.probAction = 'place';
        spyOn(service, 'placeSteps').and.callFake(() => {
            return;
        });
        const spyExchangeAction = spyOn(service, 'exchangeAction');
        service.actionHandler();
        expect(spyExchangeAction).toHaveBeenCalled();
    });

    it('actionHandler should not call passAction if the probAction is "change" and played is true', () => {
        service.probAction = 'change';
        service.played = true;
        const spyExchangeAction = spyOn(service, 'exchangeAction');
        const spyPassAction = spyOn(service, 'passAction');
        service.actionHandler();
        expect(spyExchangeAction).toHaveBeenCalled();
        expect(spyPassAction).not.toHaveBeenCalled();
    });

    it('actionHandler should call passAction if the probAction is "pass"', () => {
        service.probAction = 'pass';
        const spyPassAction = spyOn(service, 'passAction');
        service.actionHandler();
        expect(spyPassAction).toHaveBeenCalled();
    });

    /// //////////////////////////  placeSteps /////////////////////////
    it('placeSteps should call stockPlacement if tileEmpty is false', () => {
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        const spyStockPlacement = spyOn(service, 'stockPlacement');
        service.placeSteps();
        expect(spyStockPlacement).toHaveBeenCalled();
    });

    it('placeSteps should call placeAction if the turnPlacement has a length greater than 0', () => {
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(false);
        service.turnPlacement.push({ command: EMPTY_COMMAND, score: 0, easelLetters: 'a' });
        const spyPlaceAction = spyOn(service, 'placeAction');
        service.placeSteps();
        expect(spyPlaceAction).toHaveBeenCalled();
    });

    it('placeSteps should call buildCommand if the easel contains the word', () => {
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true);
        spyOn(service['wordService'], 'generateWords').and.returnValue(['b']);
        spyOn(service['gameService'].player2.easel, 'contains').and.returnValue(true);
        const spyBuildCommand = spyOn(service, 'buildCommand');
        service.placeSteps();
        expect(spyBuildCommand).toHaveBeenCalled();
    });

    it('placeSteps should call resetUsedLetters if there is a hintCommand', () => {
        spyOn(service['commandService'], 'tileIsEmpty').and.returnValue(true);
        spyOn(service['wordService'], 'generateWords').and.returnValue(['b']);
        spyOn(service['gameService'].player2.easel, 'contains').and.returnValue(false);
        const spyResetUsedLetters = spyOn(service['gameService'].player1.easel, 'resetUsedLetters');
        service.hintCommand = true;
        service.placeSteps();
        expect(spyResetUsedLetters).toHaveBeenCalled();
    });

    /// //////////////////////////  findLinePlacement /////////////////////////
    it('findLinePlacement should call buildCommand if findIndexInLine returns an index', () => {
        const line = [B];
        const letters = [B];
        const direction = 'h';
        const x = 1;
        const y = 1;
        spyOn<any>(service, 'generateWords').and.returnValue(['b']);
        spyOn(service['commandService'], 'wordIsPlacable').and.returnValue('b');
        spyOn<any>(service, 'findIndexInLine').and.returnValue(0);
        const spyBuildCommand = spyOn(service, 'buildCommand');
        service.findLinePlacement(line, letters, direction, x, y);
        expect(spyBuildCommand).toHaveBeenCalled();
    });

    it('findLinePlacement should call buildCommand vertically if the direction is v', () => {
        const line = [B];
        const letters = [B];
        const direction = 'v';
        const x = 1;
        const y = 1;
        service.hintCommand = true;
        spyOn<any>(service, 'generateWords').and.returnValue(['b']);
        spyOn(service['commandService'], 'wordIsPlacable').and.returnValue('b');
        spyOn<any>(service, 'findIndexInLine').and.returnValue(0);
        const spyBuildCommand = spyOn(service, 'buildCommand');
        service.findLinePlacement(line, letters, direction, x, y);
        expect(spyBuildCommand).toHaveBeenCalled();
    });

    it('findLinePlacement should not call buildCommand if findIndexInLine returns undefined_index', () => {
        const line = [B];
        const letters = [B];
        const direction = 'h';
        const x = 1;
        const y = 1;
        spyOn<any>(service, 'generateWords').and.returnValue(['b']);
        spyOn(service['commandService'], 'wordIsPlacable').and.returnValue('b');
        spyOn<any>(service, 'findIndexInLine').and.returnValue(UNDEFINED_INDEX);
        const spyBuildCommand = spyOn(service, 'buildCommand');
        service.findLinePlacement(line, letters, direction, x, y);
        expect(spyBuildCommand).not.toHaveBeenCalled();
    });

    it('findLinePlacement should not call buildCommand if wordIsPlace returns an empty string', () => {
        const line = [B];
        const letters = [B];
        const direction = 'h';
        const x = 1;
        const y = 1;
        spyOn<any>(service, 'generateWords').and.returnValue(['b']);
        spyOn(service['commandService'], 'wordIsPlacable').and.returnValue('');
        spyOn<any>(service, 'findIndexInLine').and.returnValue(0);
        const spyBuildCommand = spyOn(service, 'buildCommand');
        service.findLinePlacement(line, letters, direction, x, y);
        expect(spyBuildCommand).not.toHaveBeenCalled();
    });

    /// //////////////////////////  placeAction /////////////////////////
    it('placeAction should call refillEasel if the player 2 easel contains easelLetters from the TurnPlacement', () => {
        service.hintCommand = false;
        service.expert = true;
        service.turnPlacement.push({ command: EMPTY_COMMAND, score: 0, easelLetters: 'a' });
        spyOn(service['gameService'].player2.easel, 'contains').and.returnValue(true);
        spyOn(service['commandService'], 'placeWord').and.callFake(() => {
            return;
        });
        const spyRefillEasel = spyOn(service['easelOperation'], 'refillEasel');
        service.placeAction();
        expect(spyRefillEasel).toHaveBeenCalled();
    });

    it('placeAction should not call refillEasel if the player 2 easel does not contain easelLetters from the TurnPlacement', () => {
        service.hintCommand = false;
        service.turnPlacement.push({ command: EMPTY_COMMAND, score: 0, easelLetters: 'a' });
        spyOn(service['gameService'].player2.easel, 'contains').and.returnValue(false);
        spyOn(service['commandService'], 'placeWord').and.callFake(() => {
            return;
        });
        const spyRefillEasel = spyOn(service['easelOperation'], 'refillEasel');
        service.placeAction();
        expect(spyRefillEasel).not.toHaveBeenCalled();
    });

    it('placeAction should call buildChatInput if there is a hint command', () => {
        service.hintCommand = true;
        service.turnPlacement.push({ command: EMPTY_COMMAND, score: 0, easelLetters: 'a' });
        const spyBuildChatInput = spyOn(service['wordService'], 'buildChatInput');
        service.placeAction();
        expect(spyBuildChatInput).toHaveBeenCalled();
    });

    /// //////////////////////////  exchangeAction /////////////////////////
    it('exchangeAction should call refillReserve', () => {
        const prob = 0.5;
        spyOn(Math, 'random').and.returnValue(prob);
        service['reserveService'].reserveSize = 15;
        const spyRefillReserve = spyOn(service['reserveService'], 'reFillReserve');
        service.exchangeAction();
        expect(spyRefillReserve).toHaveBeenCalled();
    });

    it('exchangeAction should not call refillReserve if the reserve size is lower than the easel length', () => {
        const prob = 0.5;
        spyOn(Math, 'random').and.returnValue(prob);
        service['reserveService'].reserveSize = 6;
        const spyRefillReserve = spyOn(service['reserveService'], 'reFillReserve');
        service.exchangeAction();
        expect(spyRefillReserve).not.toHaveBeenCalled();
    });

    /// //////////////////////////  fitsTheProb /////////////////////////
    it('fitsTheProb should return false if the probScore is small but the score is higher than PROB_FOR_SMALL_SCORE', () => {
        service.probScore = 'SMALL_SCORE';
        expect(service['fitsTheProb'](PROB_FOR_SMALL_SCORE + 1)).toBeFalsy();
    });
    it('fitsTheProb should return false if the probScore is medium but the score is higher than PROB_FOR_MEDIUM_SCORE', () => {
        service.probScore = 'MEDIUM_SCORE';
        expect(service['fitsTheProb'](PROB_FOR_MEDIUM_SCORE + 1)).toBeFalsy();
    });
    it('fitsTheProb should return false if the probScore is max but the score is higher than PROB_FOR_MAX_SCORE', () => {
        service.probScore = 'MAX_SCORE';
        expect(service['fitsTheProb'](PROB_FOR_MAX_SCORE + 1)).toBeFalsy();
    });

    it('fitsTheProb should return false if the probScore is empty', () => {
        service.probScore = '';
        expect(service['fitsTheProb'](PROB_FOR_MAX_SCORE + 1)).toBeFalsy();
    });

    it('fitsTheProb should return false if the probScore is empty', () => {
        service.probScore = '';
        expect(service['fitsTheProb'](UNDEFINED_INDEX)).toBeFalsy();
    });

    /// //////////////////////////  generateActionProb /////////////////////////
    it('generateActionProb should make probAction equal to "place" if the prob is under PROB_FOR_PLACEMENT', () => {
        const prob = 0.35;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generateActionProb']();
        expect(service.probAction).toEqual('place');
    });

    it('generateActionProb should make probAction equal to "place" if the expert mode is on', () => {
        service.expert = true;
        service.probAction = '';
        service['generateActionProb']();
        expect(service.probAction).toEqual('place');
    });

    it('generateActionProb should make probAction equal to "change" if the prob is between PROB_FOR_PLACEMENT and PROB_FOR_EXCHANGE', () => {
        const prob = 0.85;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generateActionProb']();
        expect(service.probAction).toEqual('change');
    });

    it('generateActionProb should make probAction equal to "pass" if the prob is between PROB_FOR_EXCHANGE and POURCENT', () => {
        const prob = 0.95;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generateActionProb']();
        expect(service.probAction).toEqual('pass');
    });

    /// //////////////////////////  generatePointsProg /////////////////////////
    it('generatePointsProg should make probScore equal to "SMALL_SCORE" if the prob is under SMALL_SCORE', () => {
        const prob = 0.35;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generatePointsProg']();
        expect(service.probScore).toEqual('SMALL_SCORE');
    });

    it('generatePointsProg should make probScore equal to "MEDIUM_SCORE" if the prob is between SMALL_SCORE and MEDIUM_SCORE', () => {
        const prob = 0.65;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generatePointsProg']();
        expect(service.probScore).toEqual('MEDIUM_SCORE');
    });

    it('generatePointsProg should make probScore equal to "MAX_SCORE" if the prob is between MEDIUM_SCORE and MAX_SCORE', () => {
        const prob = 0.95;
        spyOn(Math, 'random').and.returnValue(prob);
        service['generatePointsProg']();
        expect(service.probScore).toEqual('MAX_SCORE');
    });

    /// //////////////////////////  generateWords /////////////////////////
    it('generateWords should call wordService.generateWords if hintCommand is false', () => {
        const lett: Letter[] = [];
        service.hintCommand = false;
        service['gameService'].player1.easel.easelLetters = [A, A, A, A, A, A, A];
        const spyWordServiceGenerateWords = spyOn(service['wordService'], 'generateWords');
        service['generateWords'](lett);
        expect(spyWordServiceGenerateWords).toHaveBeenCalled();
    });

    it('generateWords should call wordService.generateWords if hintCommand is true', () => {
        const lett: Letter[] = [];
        service.hintCommand = true;
        service['gameService'].player1.easel.easelLetters = [A, A, A, A, A, A, A];
        const spyWordServiceGenerateWords = spyOn(service['wordService'], 'generateWords');
        service['generateWords'](lett);
        expect(spyWordServiceGenerateWords).toHaveBeenCalled();
    });

    /// //////////////////////////  findIndexInLine /////////////////////////
    it('findIndexInLine should call wordService.generateWords if hintCommand is true', () => {
        const word = 'a';
        const line = [];
        for (let i = 0; i < NB_TILES; i++) {
            line.push(NOT_A_LETTER);
        }
        line[0] = A;
        expect(service['findIndexInLine'](word, line)).toEqual(UNDEFINED_INDEX);
    });

    it('findIndexInLine should call wordService.generateWords if hintCommand is true', () => {
        const word = 'ab';
        const line = [];
        for (let i = 0; i < NB_TILES; i++) {
            line.push(NOT_A_LETTER);
        }
        line[0] = A;
        expect(service['findIndexInLine'](word, line)).toEqual(0);
    });

    it('findIndexInLine should call wordService.generateWords if hintCommand is true', () => {
        const word = 'b';
        const line = [];
        for (let i = 0; i < NB_TILES; i++) {
            line.push(NOT_A_LETTER);
        }
        line[0] = A;
        expect(service['findIndexInLine'](word, line)).toEqual(UNDEFINED_INDEX);
    });

    /// //////////////////////////  buildCommand /////////////////////////
    it('buildCommand should push in TurnPlacement', () => {
        service.turnPlacement = [];
        service.hintCommand = true;
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        const command = { word, position, direction };
        service.buildCommand(word, position, direction, easelLetters);
        spyOn(service['scoreService'], 'getScore').and.returnValue(3);
        expect(JSON.stringify(service.turnPlacement[0])).toEqual(JSON.stringify({ command, score: 3, easelLetters }));
    });

    it('buildCommand should not push in TurnPlacement if score is equal to undefined_index even if hintCommand is true', () => {
        service.turnPlacement = [];
        service.hintCommand = true;
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        // const command = { word, position, direction };
        spyOn(service['scoreService'], 'getScore').and.returnValue(UNDEFINED_INDEX);
        service.buildCommand(word, position, direction, easelLetters);
        expect(service.turnPlacement.length).toEqual(0);
    });

    it('buildCommand should not push in TurnPlacement if score is equal to undefined_index even if expert mode is on', () => {
        service.turnPlacement = [];
        service.hintCommand = false;
        service.expert = true;
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        // const command = { word, position, direction };
        spyOn(service['scoreService'], 'getScore').and.returnValue(UNDEFINED_INDEX);
        service.buildCommand(word, position, direction, easelLetters);
        expect(service.turnPlacement.length).toEqual(0);
    });

    it('buildCommand should not push in TurnPlacement if score is equal to undefined_index if fitsTheProb returns false', () => {
        service.turnPlacement = [];
        service.hintCommand = false;
        service.expert = false;
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        // const command = { word, position, direction };
        spyOn(service['scoreService'], 'getScore').and.returnValue(UNDEFINED_INDEX);
        service.buildCommand(word, position, direction, easelLetters);
        expect(service.turnPlacement.length).toEqual(0);
    });

    it('buildCommand should push in TurnPlacement if the expert mode is on', () => {
        service.turnPlacement = [];
        service.expert = true;
        service.hintCommand = false;
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        const command = { word, position, direction };
        service.buildCommand(word, position, direction, easelLetters);
        spyOn(service['scoreService'], 'getScore').and.returnValue(3);
        expect(JSON.stringify(service.turnPlacement[0])).toEqual(JSON.stringify({ command, score: 3, easelLetters }));
    });

    it('buildCommand should use fitsTheProb', () => {
        service.turnPlacement = [];
        service.hintCommand = false;
        service.probScore = 'SMALL_SCORE';
        const word = 'a';
        const position = { x: 1, y: 1 };
        const direction = 'h';
        const easelLetters = 'abc';
        const command = { word, position, direction };
        service.buildCommand(word, position, direction, easelLetters);
        spyOn(service['scoreService'], 'getScore').and.returnValue(3);
        expect(JSON.stringify(service.turnPlacement[0])).toEqual(JSON.stringify({ command, score: 3, easelLetters }));
    });

    /// ///////////////////////////////////////// generateVirtualPlayerName ///////////////////////
    it('generateVirtualPlayerName should call and navigate to the game page', () => {
        spyOn(Math, 'random').and.returnValue(0);
        expect(service.generateVirtualPlayerName()).toEqual('Bender');
    });
});
