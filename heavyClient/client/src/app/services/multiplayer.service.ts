import { Injectable } from '@angular/core';
import { Command } from '@app/classes/command';
import { MessageServer } from '@app/classes/message-server';
import { Scores } from '@app/classes/scores';
import { EMPTY_COMMAND, EMPTY_COMMAND_CODE, VALIDATION_AWAIT } from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { CommandsService } from './commands.service';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { ObjectiveService } from './objective.service';
import { ReserveService } from './reserve.service';
import { SocketCommunicationService } from './socket-communication.service';
import { SuperGridService } from './super-grid.service';
import { VirtualPlayerService } from './virtual-player.service';
import { WordsService } from './words.service';
@Injectable({
    providedIn: 'root',
})
export class MultiplayerService {
    printValidation = new BehaviorSubject(EMPTY_COMMAND_CODE);
    chatInput = '';
    chatInputOBS = new BehaviorSubject('init');
    endGameObs = new BehaviorSubject('init');
    rejectedOBS = new BehaviorSubject('init');
    roomIsFullOBS = new BehaviorSubject('init');
    joinerCancelOBS = new BehaviorSubject('init');
    emptyMess: MessageServer = {};
    roomListOBS = new BehaviorSubject([[''], ['']]);
    joinedOBS = new BehaviorSubject(false);
    listOfScoresClassicOBS = new BehaviorSubject(new Array<Scores>());
    listOfScores2990OBS = new BehaviorSubject(new Array<Scores>());
    listOfScores2990: Scores[] = [];
    listOfScoresClassic: Scores[] = [];

    constructor(
        private commandService: CommandsService,
        private easelOperation: EaselOperationsService,
        private gameService: GameService,
        private reserveService: ReserveService,
        private superGrid: SuperGridService,
        private wordService: WordsService,
        private virtualPlayer: VirtualPlayerService,
        private socketComm: SocketCommunicationService,
        private objectiveService: ObjectiveService,
    ) {}

    listenListRoomsWithDetailsResult() {
        this.socketComm.listen('listRoomsWithDetailsResult').subscribe((message) => {
            this.roomListOBS.next([
                message.roomList ?? [],
                message.creatorList ?? [],
                message.allRoomList ?? [],
                message.allRoomListLog2990 ?? [],
                message.creatorListLog2990 ?? [],
            ]);
        });
    }

    listenStartGame() {
        this.socketComm.listen('startGame').subscribe((message) => {
            this.gameService.player2.name = message.joinerName ?? '';
            this.joinedOBS.next(true);
            this.joinedOBS.next(false);
        });
    }

    listenStartGameAccepted() {
        this.socketComm.listen('startGameAccepted').subscribe((message) => {
            this.gameService.roomName = message.roomName ?? '';
            this.rejectedOBS.next('not');
            this.rejectedOBS.next('init');
            this.socketComm.defineWhoStarts();
            this.gameService.gameTimeTurn = message.timer ?? { min: 0, sec: 0 };
            this.gameService.timer = message.timer ?? { min: 0, sec: 0 };

            this.initTime();
        });
    }

    listenStartGameRejected() {
        this.socketComm.listen('startGameRejected').subscribe(() => {
            this.rejectedOBS.next('did');
            this.rejectedOBS.next('init');
        });
    }

    createRoom() {
        this.socketComm.emit('create', {
            roomName: this.gameService.roomName,
            timer: this.gameService.gameTimeTurn,
            creatorName: this.gameService.player1.name,
            log2990: this.gameService.log2990,
        });
    }
    createSoloRoom() {
        this.socketComm.emit('createSolo', {
            roomName: this.gameService.roomName,
            timer: this.gameService.gameTimeTurn,
            creatorName: this.gameService.getCreator().name,
            log2990: this.gameService.log2990,
        });
    }
    joinRoom(roomName: string, joinerName: string) {
        this.socketComm.emit('join', { joinerName, roomName }, (res: string) => {
            this.roomIsFullOBS.next(res);
        });
    }

    cancelJoin(roomName: string) {
        this.socketComm.emit('cancelJoin', { roomName });
    }

    joinerCanceled() {
        this.socketComm.listen('joinerCanceled').subscribe(() => {
            this.joinerCancelOBS.next('joinerLeft');
            this.joinerCancelOBS.next('init');
        });
    }
    acceptJoinRoom(roomName: string): void {
        this.socketComm.emit('acceptJoinRoom', { roomName });
    }

    rejectJoinRoom(roomName: string): void {
        this.socketComm.emit('rejectJoinRoom', { roomName });
    }

    getAllRoomsWithDetails(): void {
        this.socketComm.emit('listRoomsWithDetails');
    }

    deleteRoom(roomName: string): void {
        this.socketComm.emit('deleteRoom', { roomName });
    }

    sendCommandPlace(command: Command): void {
        this.superGrid.placeTempWord(command);
        this.gameService.player1.easel.setTempLetter();
        this.easelOperation.redrawEasel(this.gameService.player1.easel);
        const messageServer: MessageServer = {
            command,
            roomName: this.gameService.roomName,
            creator: this.gameService.player1.creator,
        };
        if (this.gameService.multiplayerGame) {
            this.socketComm.emit('sendingPlaceCommand', messageServer);
        } else {
            setTimeout(() => {
                this.superGrid.clearTempCanvas();
                if (this.gameService.player1.turnToPlay) {
                    const commandCode = this.wordService.wordValidationInit(command);
                    if (commandCode.isValid) {
                        if (this.gameService.log2990) this.objectiveService.verifyObjective(command, this.gameService.player1);
                        this.commandService.placeWord(command);
                        this.easelOperation.refillEasel(this.gameService.player1.easel, true);
                        this.gameService.addScore(commandCode.score);
                        this.gameService.alternateTurns();
                        this.gameService.passTurnCounter = 0;
                        this.virtualPlayer.play();
                    } else {
                        this.gameService.player1.easel.resetUsedLetters();
                        this.easelOperation.redrawEasel(this.gameService.player1.easel);
                        this.socketComm.passTurn();
                    }
                    this.printValidation.next(commandCode);
                }
            }, VALIDATION_AWAIT);
        }
    }
    receiveValidation() {
        this.socketComm.listen('returnValidation').subscribe((data) => {
            setTimeout(() => {
                if (this.gameService.player1.creator === data.creator) {
                    this.superGrid.clearTempCanvas();
                    if (this.gameService.player1.turnToPlay) {
                        if (data.commandCode && data.commandCode.isValid && data.command) {
                            this.commandService.placeWord(data.command);
                            this.easelOperation.refillEasel(this.gameService.player1.easel, true);
                            this.gameService.addScore(data.commandCode.score);
                            this.sendAction(this.chatInput, data.command);
                        } else {
                            this.socketComm.passTurn();
                        }
                        if (data.commandCode) {
                            this.printValidation.next(data.commandCode);
                        }
                    }
                    this.gameService.player1.easel.resetUsedLetters();
                    this.easelOperation.redrawEasel(this.gameService.player1.easel);
                }
            }, VALIDATION_AWAIT);
        });
    }
    sendAction(chatInput: string, placed?: Command) {
        if (this.gameService.log2990) this.objectiveService.verifyObjective(placed ?? EMPTY_COMMAND, this.gameService.player1);
        const message: MessageServer = {
            roomName: this.gameService.roomName,
            reserve: JSON.stringify(Array.from(this.reserveService.letters)),
            easel: this.gameService.player1.easel.easelLetters,
            command: placed,
            score: this.gameService.player1.score,
            reserveSize: this.reserveService.reserveSize,
            chatInput,
            creator: this.gameService.player1.creator,
        };
        this.gameService.alternateTurns();
        this.gameService.timer = this.gameService.gameTimeTurn;
        if (this.gameService.multiplayerGame) {
            this.socketComm.emit('sendActionToOppnent', message);
        } else {
            this.gameService.passTurnCounter = 0;
            this.virtualPlayer.play();
        }
    }
    getAction() {
        this.socketComm.listen('getAction').subscribe((data) => {
            this.reserveService.redefineReserve(data.reserve ?? '', data.reserveSize ?? 0);
            this.gameService.player2.score = data.score ?? 0;
            this.gameService.player2.easel.easelLetters = data.easel ?? [];
            if (data.command) this.commandService.placeWord(data.command);
            this.chatInputOBS.next(data.chatInput ?? '');
            this.gameService.timer = this.gameService.gameTimeTurn;
            this.gameService.alternateTurns();
        });
    }

    sendChat(chatInput: string) {
        this.socketComm.emit('sendChat', { chatInput, roomName: this.gameService.roomName });
    }
    receiveChat() {
        this.socketComm.listen('receiveChat').subscribe((data) => {
            this.chatInputOBS.next(data.chatInput ?? '');
        });
    }

    initTime() {
        this.socketComm.emit('startTimer', { timer: this.gameService.gameTimeTurn, roomName: this.gameService.roomName });
    }

    sendAbandonGame(browserClose?: boolean) {
        if (this.gameService.multiplayerGame) this.socketComm.emit('abandonGame', { roomName: this.gameService.roomName, browserClose });
    }
    listenEndGame() {
        this.socketComm.listen('endGame').subscribe((message) => {
            if (message.isEaselEmpty) {
                this.gameService.addEaselLetterScore();
                this.gameService.isEaselEmpty = true;
                this.endGameObs.next(' ');
                this.socketComm.sendScore();
            }
            if (message.isAbandon) {
                this.endGameObs.next('abandon');
            }
            if (message.isThreePasses) {
                this.gameService.isThreePasses = true;
                this.gameService.subEaselLetterScore();
                this.endGameObs.next(' ');
                this.socketComm.sendScore();
            }
        });
    }
    listenShowScoresClassic() {
        this.socketComm.showScoreClassic();
        this.socketComm.listen('showScoreClassic').subscribe((data) => {
            this.listOfScoresClassicOBS.next(data.highScore ?? []);
        });
    }
    listenShowScores2990() {
        this.socketComm.showScore2990();
        this.socketComm.listen('showScore2990').subscribe((data) => {
            this.listOfScores2990OBS.next(data.highScore ?? []);
        });
    }
}
