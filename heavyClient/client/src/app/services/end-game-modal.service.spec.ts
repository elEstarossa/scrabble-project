/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import { A, B, K, NOT_A_LETTER } from '@app/constants/constants';
import { EndGameModalService } from './end-game-modal.service';

describe('EndGameModalService', () => {
    let service: EndGameModalService;
    let ctx: CanvasRenderingContext2D;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
        });
        service = TestBed.inject(EndGameModalService);
        ctx = CanvasTestHelper.createCanvas(15, 25).getContext('2d') as CanvasRenderingContext2D;
        service.easelOneCtx = ctx;
        service.easelTwoCtx = ctx;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// ////////////////// drawHands //////////////////////////////////
    it('should drawHands in modal when the game is over', () => {
        const spy = spyOn<any>(service, 'drawHand');
        service.drawHands();
        expect(spy).toHaveBeenCalled();
    });

    // /// ////////////////// drawEasel //////////////////////////////////
    it('should drawEasel when the contain of easel is a letter ', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [A, B];
        chevalet.foundLetter[0] = false;

        service.drawEasel(chevalet, ctx);

        expect(chevalet.easelLetters[1].charac).toBe('b');
    });

    it('should drawEasel when the contain of easel is not a letter ', () => {
        const chevalet = new EaselObject(true);
        chevalet.easelLetters = [NOT_A_LETTER, NOT_A_LETTER];
        chevalet.foundLetter[0] = false;

        service.drawEasel(chevalet, ctx);

        expect(chevalet.easelLetters[1].charac).toBe('1');
    });

    /// ////////////////// drawHand //////////////////////////////////
    it('should drawHand in the modal ', () => {
        const spy = spyOn(ctx, 'beginPath');
        service['drawHand'](ctx);

        expect(spy).toHaveBeenCalled();
    });

    /// ////////////// drawScore //////////////////////////////////
    it('should drawScore  by calling the fillText function and put the parameter not undefined', () => {
        const letter: Letter = K;
        const pixelSize: TileContent = { alpha: 1, score: 1, label: '' };
        const spy = spyOn(ctx, 'fillText');
        service.drawScore(letter, 2, pixelSize, ctx);

        expect(spy).toHaveBeenCalled();
    });

    /// ////////////// drawLetter //////////////////////////////////
    it('should drawLetter by calling the fillText function and put the parameter not undefined', () => {
        const letter: Letter = K;
        const pixelSize: TileContent = { alpha: 1, score: 1, label: '' };
        const spy = spyOn(ctx, 'fillText');
        service.drawLetter(letter, 2, pixelSize, ctx);

        expect(spy).toHaveBeenCalled();
    });
});
