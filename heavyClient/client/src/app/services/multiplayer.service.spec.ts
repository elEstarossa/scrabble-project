/* eslint-disable max-len */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable prettier/prettier */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { CommandCode } from '@app/classes/comand-code';
import { Command } from '@app/classes/command';
import { EaselObject } from '@app/classes/easel-object';
import { MessageServer } from '@app/classes/message-server';
import { Player } from '@app/classes/player';
import { Scores } from '@app/classes/scores';
import { of } from 'rxjs';
import { CommandsService } from './commands.service';
import { EaselOperationsService } from './easel-operations.service';
import { GameService } from './game.service';
import { MultiplayerService } from './multiplayer.service';
import { SuperGridService } from './super-grid.service';

describe('MultiplayerService', () => {
    let service: MultiplayerService;
    let gameService: jasmine.SpyObj<GameService>;
    let superGrid: jasmine.SpyObj<SuperGridService>;
    let easelService: jasmine.SpyObj<EaselOperationsService>;
    let commandService: jasmine.SpyObj<CommandsService>;
    beforeEach(() => {
        gameService = jasmine.createSpyObj('GameService', [
            'alternateTurns',
            'getCreator',
            'setTempLetter',
            'getJoiner',
            'addEaselLetterScore',
            'subEaselLetterScore',
            'addScore',
            'verifyObjective',
        ]);
        gameService.player1 = new Player('Gilles');
        gameService.player2 = new Player('ali');
        gameService.player2.easel = new EaselObject(true);
        gameService.gameTimeTurn = { min: 0, sec: 40 };

        superGrid = jasmine.createSpyObj('SuperGridService', ['placeTempWord', 'clearTempCanvas']);
        superGrid.gridCtx = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;

        easelService = jasmine.createSpyObj('EaselOperationsService', ['redrawEasel', 'fillEasel', 'removeUsedLetters', 'refillEasel']);
        easelService.gridCtx = CanvasTestHelper['createCanvas'](15, 25).getContext('2d') as CanvasRenderingContext2D;

        commandService = jasmine.createSpyObj('CommandsService', ['placeWord']);
        TestBed.configureTestingModule({
            providers: [
                { provide: GameService, useValue: gameService },
                { provide: SuperGridService, useValue: superGrid },
                { provide: EaselOperationsService, useValue: easelService },
                { provide: CommandsService, useValue: commandService },
            ],
            imports: [HttpClientTestingModule, MatSnackBarModule],
        });
        service = TestBed.inject(MultiplayerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /// /////////////// acceptJoinRoom ////////////////////////////////
    it('should call emit with acceptJoinRoom', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = 'room1';

        service.acceptJoinRoom(roomName);
        expect(socketEmit).toHaveBeenCalledWith('acceptJoinRoom', {
            roomName: 'room1',
        });
    });
    /// /////////////// rejectJoinRoom ////////////////////////////////
    it('should call emit with rejectJoinRoom', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = 'room1';

        service.rejectJoinRoom(roomName);
        expect(socketEmit).toHaveBeenCalledWith('rejectJoinRoom', {
            roomName: 'room1',
        });
    });
    /// /////////////// getAllRoomsWithDetails ////////////////////////////////
    it('should call emit with getAllRoomsWithDetails', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');

        service.getAllRoomsWithDetails();
        expect(socketEmit).toHaveBeenCalled();
    });

    /// /////////////// deleteRoom ////////////////////////////////
    it('should call emit with deleteRoom', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = 'room1';

        service.deleteRoom(roomName);
        expect(socketEmit).toHaveBeenCalledWith('deleteRoom', {
            roomName: 'room1',
        });
    });

    /// /////////////// joinRoom ////////////////////////////////
    it('should call emit with joinRoom', async () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = '';
        const joinerName = '';

        service.joinRoom(joinerName, roomName);
        expect(socketEmit).toHaveBeenCalled();
    });

    /// /////////////// cancelJoin ////////////////////////////////
    it('should call emit with cancelJoin', async () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = '';

        service.cancelJoin(roomName);
        expect(socketEmit).toHaveBeenCalledWith('cancelJoin', { roomName });
    });

    /// /////////////// createRoom ////////////////////////////////
    it('should call emit with createRoom', async () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const player = new Player('');
        gameService.getCreator.and.returnValue(player);
        const timer = (gameService.gameTimeTurn = { min: 0, sec: 0 });
        gameService.roomName = '';
        const roomName = '';
        const creatorName = 'Gilles';
        const log2990 = gameService.log2990;
        const createEvent = {
            roomName,
            timer,
            creatorName,
            log2990,
        };

        service.createRoom();
        expect(socketEmit).toHaveBeenCalledWith('create', createEvent);
    });
    /// /////////////// createSoloRoom ////////////////////////////////
    it('should call emit with createRoom', async () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const player = new Player('');
        gameService.getCreator.and.returnValue(player);
        gameService.getJoiner.and.returnValue(player);
        const timer = (gameService.gameTimeTurn = { min: 0, sec: 0 });
        gameService.roomName = '';
        const log2990 = gameService.log2990;
        const roomName = '';
        const creatorName = '';
        const createEvent = {
            roomName,
            timer,
            creatorName,
            log2990,
        };

        service.createSoloRoom();
        expect(socketEmit).toHaveBeenCalledWith('createSolo', createEvent);
    });

    /// /////////////// sendCommandPlace ////////////////////////////////
    it('should call emit with sendCommandPlace', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const messageServer: MessageServer = {
            command,
            roomName,
            creator: service['gameService'].player1.creator,
        };
        const spy = spyOn(gameService.player1.easel, 'setTempLetter');
        gameService.multiplayerGame = true;
        service.sendCommandPlace(command);
        expect(socketEmit).toHaveBeenCalledWith('sendingPlaceCommand', messageServer);
        expect(spy).toHaveBeenCalled();
        expect(superGrid.placeTempWord).toHaveBeenCalled();
    });

    it('should call emit with sendCommandPlace when the command is valid and the turnplay is true for the player 1', () => {
        jasmine.clock().install();
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const commandCode: CommandCode = { isValid: true, errorMsg: '', score: 0 };
        gameService.player1.turnToPlay = true;
        gameService.multiplayerGame = false;
        const spy = spyOn(service['wordService'], 'wordValidationInit').and.returnValue(commandCode);
        const spy2 = spyOn(service['objectiveService'], 'verifyObjective');
        gameService.log2990 = true;
        service.sendCommandPlace(command);
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        expect(gameService.addScore).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should call emit with sendCommandPlace when the command is valid and the turnplay is true for the player 1', () => {
        jasmine.clock().install();
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const commandCode: CommandCode = { isValid: true, errorMsg: '', score: 0 };
        gameService.player1.turnToPlay = true;
        gameService.multiplayerGame = false;
        const spy = spyOn(service['wordService'], 'wordValidationInit').and.returnValue(commandCode);
        const spy2 = spyOn(service['objectiveService'], 'verifyObjective');
        gameService.log2990 = false;
        service.sendCommandPlace(command);
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        expect(gameService.addScore).toHaveBeenCalled();
        expect(spy2).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should call emit with sendCommandPlace when the command is not valid and the turnplay is true for the player 1', () => {
        jasmine.clock().install();
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const commandCode: CommandCode = { isValid: false, errorMsg: '', score: 0 };
        gameService.player1.turnToPlay = true;
        gameService.multiplayerGame = false;
        const spy = spyOn(service['socketComm'], 'passTurn');
        const spy2 = spyOn(service['wordService'], 'wordValidationInit').and.returnValue(commandCode);

        service.sendCommandPlace(command);
        jasmine.clock().tick(3000);
        expect(easelService.redrawEasel).toHaveBeenCalled();
        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();

        jasmine.clock().uninstall();
    });

    it('should call emit with sendCommandPlace when the turnplay is false for the player 1', () => {
        jasmine.clock().install();
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        gameService.player1.turnToPlay = false;
        gameService.multiplayerGame = false;

        service.sendCommandPlace(command);
        jasmine.clock().tick(3000);
        expect(easelService.redrawEasel).toHaveBeenCalled();

        jasmine.clock().uninstall();
    });

    /// /////////////// sendAction ////////////////////////////////
    it('should call emit with sendAction', () => {
        const socketEmit = spyOn(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const chatInput = '';
        const reserve = JSON.stringify(Array.from(service['reserveService'].letters));
        const reserveSize = (service['reserveService'].reserveSize = 0);
        const easel = (gameService.player1.easel.easelLetters = []);
        const score = (gameService.player1.score = 0);
        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const messageServer: MessageServer = {
            roomName,
            reserve,
            easel,
            command,
            score,
            reserveSize,
            chatInput,
            creator: service['gameService'].player1.creator,
        };
        gameService.multiplayerGame = true;

        service.sendAction(chatInput, command);

        expect(socketEmit).toHaveBeenCalledWith('sendActionToOppnent', messageServer);
        expect(gameService.alternateTurns).toHaveBeenCalled();
    });

    it('should call sendAction for solo game', () => {
        const chatInput = '';
        gameService.log2990 = true;

        const command: Command = { word: '', position: { x: 5, y: 5 }, direction: '' };
        const spy = spyOn(service['virtualPlayer'], 'play');
        const spy2 = spyOn(service['objectiveService'], 'verifyObjective');

        gameService.multiplayerGame = false;

        service.sendAction(chatInput, command);

        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    /// /////////////// getAction ////////////////////////////////
    it('should call listen to listen to get actions from the opposite player : score, easel letters, reserve, chat input', () => {
        const data: MessageServer = {
            score: 0,
            easel: [],
            reserve:
                '[[{"score":1,"charac":"a","img":"../../assets/letter-A.png"},9],[{"score":3,"charac":"b","img":"../../assets/letter-b.png"},2],[{"score":3,"chard":"c","img":"../../assets/letter-c.png"},2],[{"score":2,"charac":"d","img":"../../assets/letter-d.png"},3],[{"score":1,"charac":"e","img":"../../assets/letter-e.png"},15],[{"score":4,"charac":"f","img":"../../assets/letter-f.png"},2],[{"score":2,"charac":"g","img":"../../assets/letter-g.png"},2],[{"score":4,"charac":"h","img":"../../assets/letter-h.png"},2],[{"score":1,"charac":"i","img":"../../assets/letter-i.png"},8],[{"score":8,"charac":"j","img":"../../assets/letter-j.png"},1],[{"score":10,"charac":"k","img":"../../assets/letter-k.png"},1],[{"score":1,"charac":"l","img":"../../assets/letter-l.png"},5],[{"score":2,"charac":"m","img":"../../assets/letter-m.png"},3],[{"score":1,"charac":"n","img":"../../assets/letter-n.png"},6],[{"score":1,"charac":"o","img":"../../assets/letter-o.png"},6],[{"score":3,"charac":"p","img":"../../assets/letter-p.png"},2],[{"score":8,"charac":"q","img":"../../assets/letter-q.png"},1],[{"score":1,"charac":"r","img":"../../assets/letter-r.png"},6],[{"score":1,"charac":"s","img":"../../assets/letter-s.png"},6],[{"score":1,"charac":"t","img":"../../assets/letter-t.png"},6],[{"score":1,"charac":"u","img":"../../assets/letter-u.png"},6],[{"score":4,"charac":"v","img":"../../assets/letter-v.png"},2],[{"score":10,"charac":"w","img":"../../assets/letter-w.png"},1],[{"score":10,"charac":"x","img":"../../assets/letter-x.png"},1],[{"score":10,"charac":"y","img":"../../assets/letter-y.png"},1],[{"score":10,"charac":"z","img":"../../assets/letter-z.png"},1]]',
            reserveSize: 5,
            chatInput: '',
        };

        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.getAction();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen to get actions from the opposite player : score, easel letters, reserve', () => {
        const data: MessageServer = {
            score: 0,
            easel: [],
            reserve:
                '[[{"score":1,"charac":"a","img":"../../assets/letter-A.png"},9],[{"score":3,"charac":"b","img":"../../assets/letter-b.png"},2],[{"score":3,"chard":"c","img":"../../assets/letter-c.png"},2],[{"score":2,"charac":"d","img":"../../assets/letter-d.png"},3],[{"score":1,"charac":"e","img":"../../assets/letter-e.png"},15],[{"score":4,"charac":"f","img":"../../assets/letter-f.png"},2],[{"score":2,"charac":"g","img":"../../assets/letter-g.png"},2],[{"score":4,"charac":"h","img":"../../assets/letter-h.png"},2],[{"score":1,"charac":"i","img":"../../assets/letter-i.png"},8],[{"score":8,"charac":"j","img":"../../assets/letter-j.png"},1],[{"score":10,"charac":"k","img":"../../assets/letter-k.png"},1],[{"score":1,"charac":"l","img":"../../assets/letter-l.png"},5],[{"score":2,"charac":"m","img":"../../assets/letter-m.png"},3],[{"score":1,"charac":"n","img":"../../assets/letter-n.png"},6],[{"score":1,"charac":"o","img":"../../assets/letter-o.png"},6],[{"score":3,"charac":"p","img":"../../assets/letter-p.png"},2],[{"score":8,"charac":"q","img":"../../assets/letter-q.png"},1],[{"score":1,"charac":"r","img":"../../assets/letter-r.png"},6],[{"score":1,"charac":"s","img":"../../assets/letter-s.png"},6],[{"score":1,"charac":"t","img":"../../assets/letter-t.png"},6],[{"score":1,"charac":"u","img":"../../assets/letter-u.png"},6],[{"score":4,"charac":"v","img":"../../assets/letter-v.png"},2],[{"score":10,"charac":"w","img":"../../assets/letter-w.png"},1],[{"score":10,"charac":"x","img":"../../assets/letter-x.png"},1],[{"score":10,"charac":"y","img":"../../assets/letter-y.png"},1],[{"score":10,"charac":"z","img":"../../assets/letter-z.png"},1]]',
            reserveSize: 5,
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.getAction();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen to get actions from the opposite player :  reserve, command', () => {
        const data: MessageServer = {
            reserve:
                '[[{"score":1,"charac":"a","img":"../../assets/letter-A.png"},9],[{"score":3,"charac":"b","img":"../../assets/letter-b.png"},2],[{"score":3,"chard":"c","img":"../../assets/letter-c.png"},2],[{"score":2,"charac":"d","img":"../../assets/letter-d.png"},3],[{"score":1,"charac":"e","img":"../../assets/letter-e.png"},15],[{"score":4,"charac":"f","img":"../../assets/letter-f.png"},2],[{"score":2,"charac":"g","img":"../../assets/letter-g.png"},2],[{"score":4,"charac":"h","img":"../../assets/letter-h.png"},2],[{"score":1,"charac":"i","img":"../../assets/letter-i.png"},8],[{"score":8,"charac":"j","img":"../../assets/letter-j.png"},1],[{"score":10,"charac":"k","img":"../../assets/letter-k.png"},1],[{"score":1,"charac":"l","img":"../../assets/letter-l.png"},5],[{"score":2,"charac":"m","img":"../../assets/letter-m.png"},3],[{"score":1,"charac":"n","img":"../../assets/letter-n.png"},6],[{"score":1,"charac":"o","img":"../../assets/letter-o.png"},6],[{"score":3,"charac":"p","img":"../../assets/letter-p.png"},2],[{"score":8,"charac":"q","img":"../../assets/letter-q.png"},1],[{"score":1,"charac":"r","img":"../../assets/letter-r.png"},6],[{"score":1,"charac":"s","img":"../../assets/letter-s.png"},6],[{"score":1,"charac":"t","img":"../../assets/letter-t.png"},6],[{"score":1,"charac":"u","img":"../../assets/letter-u.png"},6],[{"score":4,"charac":"v","img":"../../assets/letter-v.png"},2],[{"score":10,"charac":"w","img":"../../assets/letter-w.png"},1],[{"score":10,"charac":"x","img":"../../assets/letter-x.png"},1],[{"score":10,"charac":"y","img":"../../assets/letter-y.png"},1],[{"score":10,"charac":"z","img":"../../assets/letter-z.png"},1]]',
            reserveSize: 5,
            command: {
                word: 'allo',
                position: { x: 1, y: 1 },
                direction: 'a',
            },
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.getAction();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// listenStartGameAccepted ////////////////////////////////
    it('should call listen to listen when the game is accepted by the creator and have the infos : room name and timer', () => {
        spyOn(service['socketComm'], 'emit');
        const data: MessageServer = {
            roomName: '',
            timer: { min: 0, sec: 0 },
        };

        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenStartGameAccepted();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen when the game is accepted by the creator', () => {
        spyOn(service['socketComm'], 'emit');
        const data: MessageServer = {};
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenStartGameAccepted();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// sendChat ////////////////////////////////
    it('should call emit with sendChat', () => {
        const socketEmit = spyOn<any>(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const chatInput = '';

        service.sendChat(chatInput);

        expect(socketEmit).toHaveBeenCalledWith('sendChat', { chatInput, roomName });
    });

    /// /////////////// sendAbandonGame ////////////////////////////////
    it('should call emit with sendAbandonGame', () => {
        const socketEmit = spyOn<any>(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const browserClose = true;
        gameService.multiplayerGame = true;

        service.sendAbandonGame(browserClose);

        expect(socketEmit).toHaveBeenCalledWith('abandonGame', { roomName, browserClose });
    });

    it('should call emit with sendAbandonGame', () => {
        const socketEmit = spyOn<any>(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const browserClose = true;
        gameService.multiplayerGame = false;

        service.sendAbandonGame(browserClose);

        expect(socketEmit).not.toHaveBeenCalledWith('abandonGame', { roomName, browserClose });
    });

    /// /////////////// initTime ////////////////////////////////
    it('should call emit with initTime', () => {
        const socketEmit = spyOn<any>(service['socketComm'], 'emit');
        const roomName = (gameService.roomName = '');
        const timer = (gameService.gameTimeTurn = { min: 0, sec: 0 });

        service.initTime();

        expect(socketEmit).toHaveBeenCalledWith('startTimer', { timer, roomName });
    });

    /// /////////////// receiveValidation ////////////////////////////////
    it('should call listen to listen in an interval of 3 sec if the we communicate just with the creator, so we can receive the command, easel, score to validate', () => {
        const data: MessageServer = {
            creator: true,
            command: {
                word: 'allo',
                position: { x: 1, y: 1 },
                direction: 'a',
            },
            commandCode: {
                isValid: true,
                errorMsg: '',
                score: 0,
            },
        };
        gameService.player1.creator = true;
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        jasmine.clock().install();
        service.receiveValidation();
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should call listen when the player is not the creator', () => {
        const data: MessageServer = {};
        gameService.player1.creator = false;
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        jasmine.clock().install();
        service.receiveValidation();
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should call listen to listen in an interval of 3 sec if the we communicate just with the creator, so we can receive the command, easel, score to validate', () => {
        const data: MessageServer = {
            creator: true,
            command: {
                word: '',
                position: { x: 5, y: 5 },
                direction: '',
            },
            commandCode: {
                isValid: true,
                errorMsg: '',
                score: 0,
            },
        };
        gameService.player1.creator = true;
        gameService.player1.turnToPlay = true;
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        jasmine.clock().install();
        service.receiveValidation();
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        expect(commandService.placeWord).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('should call listen to listen in an interval of 3 sec but the command is not valid', () => {
        const data: MessageServer = {
            creator: true,
            command: {
                word: '',
                position: { x: 5, y: 5 },
                direction: '',
            },
            commandCode: {
                isValid: false,
                errorMsg: '',
                score: 0,
            },
        };
        gameService.player1.creator = true;
        gameService.player1.turnToPlay = true;
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        const spy2 = spyOn<any>(service['socketComm'], 'passTurn');
        jasmine.clock().install();
        service.receiveValidation();
        jasmine.clock().tick(3000);
        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    /// /////////////// listenListRoomsWithDetailsResult ////////////////////////////////
    it('should call listen to listen to the details about the list room and a creator list ', () => {
        const data: MessageServer = {
            roomList: [''],
            creatorList: [''],
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenListRoomsWithDetailsResult();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen to the details about the creator list when its empty  ', () => {
        const data: MessageServer = {
            creatorList: [],
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenListRoomsWithDetailsResult();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen to the details about the list room when its empty  ', () => {
        const data: MessageServer = {
            roomList: [''],
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenListRoomsWithDetailsResult();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// listenStartGame ////////////////////////////////
    it('should call listen to listen to the request from the joiner to join the game ', () => {
        const data: MessageServer = {
            joinerName: '',
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenStartGame();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen to listen to an empty  the request and stay on hold', () => {
        const data: MessageServer = {};
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenStartGame();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// listenStartGameRejected ////////////////////////////////
    it('should call listen to listen when the game has rejected  ', () => {
        const data = 'listen';
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenStartGameRejected();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// receiveChat ////////////////////////////////
    it('should call listen when we have to receive an empty chat  ', () => {
        const data: MessageServer = {};
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.receiveChat();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen when we have to receive chat ', () => {
        const data: MessageServer = {
            chatInput: 'receiveChat',
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.receiveChat();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// listenEndGame ////////////////////////////////
    it('should call listen when the easel is empty to end the game ', () => {
        const data: MessageServer = {
            isEaselEmpty: true,
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenEndGame();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen when one of the players abandon the game so we have to end the game ', () => {
        const data: MessageServer = {
            isAbandon: true,
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenEndGame();
        expect(spy).toHaveBeenCalled();
    });

    it('should call listen when the two players pass their turn 3 times so we have to end the game ', () => {
        const data: MessageServer = {
            isThreePasses: true,
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.listenEndGame();
        expect(spy).toHaveBeenCalled();
    });

    /// /////////////// listenShowScoresClassic ////////////////////////////////
    it('should call listen when the scores are pushed ', () => {
        const score: Scores = { name: '', score: 1 };
        const data: MessageServer = {
            highScore: [score],
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        const spy2 = spyOn<any>(service['socketComm'], 'showScoreClassic');
        service.listenShowScoresClassic();
        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    /// /////////////// listenShowScores2990 ////////////////////////////////
    it('should call listen when the scores are pushed ', () => {
        const score: Scores = { name: '', score: 1 };
        const data: MessageServer = {
            highScore: [score],
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        const spy2 = spyOn<any>(service['socketComm'], 'showScore2990');
        service.listenShowScores2990();
        expect(spy).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    /// /////////////// joinerCanceled ////////////////////////////////
    it('should call listen when its joinerCanceled ', () => {
        const data: MessageServer = {
            isEaselEmpty: true,
        };
        const spy = spyOn<any>(service['socketComm'], 'listen').and.returnValue(of(data));
        service.joinerCanceled();
        expect(spy).toHaveBeenCalled();
    });
});
