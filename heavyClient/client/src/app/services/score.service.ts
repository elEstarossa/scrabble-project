import { Injectable } from '@angular/core';
import { Command } from '@app/classes/command';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { AZUR_TILE, BLUE_TILE, PINK_TILE, RED_TILE } from '@app/constants/array-constant';
import { EASEL_LENGTH, LETTERS_OBJECT, NB_TILES, NOT_A_LETTER, SCORE_BONUS, UNDEFINED_INDEX } from '@app/constants/constants';
import { CommandsService } from './commands.service';

@Injectable({
    providedIn: 'root',
})
export class ScoreService {
    indexOfLetterToCheck: number[] = [];
    dict: WordDictionnary = { title: '', description: '', words: [''] };
    constructor(private commandService: CommandsService) {}

    getScore(command: Command): number {
        this.spotNewLetters(command);
        const add = this.checkNewFormedWords(command);

        if (add !== UNDEFINED_INDEX) {
            const score = this.getWordBonusScore(command) + add;
            this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
            return score;
        }
        this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
        return UNDEFINED_INDEX;
    }
    getWordBonusScore(command: Command): number {
        let multiplyFact = 1;
        let score = 0;
        let counter = 0;
        for (let i = 0; i < command.word.length; i++) {
            if (i === this.indexOfLetterToCheck[counter]) {
                const posToCheck: Vec2 =
                    command.direction === 'h'
                        ? { x: command.position.x + i - 1, y: command.position.y - 1 }
                        : { x: command.position.x - 1, y: command.position.y + i - 1 };
                switch (true) {
                    case this.containBonus(posToCheck, RED_TILE):
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        multiplyFact *= 3;
                        break;
                    case this.containBonus(posToCheck, AZUR_TILE):
                        score += (LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0) * 2;
                        break;
                    case this.containBonus(posToCheck, PINK_TILE):
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        multiplyFact *= 2;
                        break;
                    case this.containBonus(posToCheck, BLUE_TILE):
                        score += (LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0) * 3;
                        break;
                    default:
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        break;
                }
                counter++;
            } else {
                score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
            }
        }

        score *= multiplyFact;
        if (this.indexOfLetterToCheck.length === EASEL_LENGTH) score += SCORE_BONUS;
        return score;
    }

    checkNewFormedWords(command: Command): number {
        let tempWord = '';
        let score = 0;
        for (const index of this.indexOfLetterToCheck) {
            let i = 0;
            if (this.getAxe(command) - 1 > 0 && this.getPreviousTile(command, index) !== NOT_A_LETTER) {
                do {
                    i++;
                } while (this.getAxe(command) - 1 - i >= 0 && this.getLetterWithDirection(command, index, i) !== NOT_A_LETTER);
                i--;
            } else {
                i = 0;
            }
            while (this.getAxe(command) - 1 - i < NB_TILES && (this.getLetterWithDirection(command, index, i) !== NOT_A_LETTER || i === 0)) {
                if (i === 0) tempWord += command.word.charAt(index);
                else tempWord += this.getLetterWithDirection(command, index, i).charac as string;
                i--;
            }
            if (tempWord.length >= 2) {
                if (this.wordInDictionnary(tempWord)) {
                    score += this.getLetterScore(tempWord);
                } else {
                    return UNDEFINED_INDEX;
                }
            }
            tempWord = '';
        }
        return score;
    }
    getLetterWithDirection(command: Command, index: number, i: number): Letter {
        return command.direction === 'h'
            ? this.commandService.gridTiles[command.position.y - 1 - i][command.position.x - 1 + index]
            : this.commandService.gridTiles[command.position.y - 1 + index][command.position.x - 1 - i];
    }
    getAxe(command: Command): number {
        return command.direction === 'h' ? command.position.y : command.position.x;
    }
    getPreviousTile(command: Command, index: number): Letter {
        return command.direction === 'h'
            ? this.commandService.gridTiles[command.position.y - 1 - 1][command.position.x - 1 + index]
            : this.commandService.gridTiles[command.position.y - 1 + index][command.position.x - 1 - 1];
    }

    getLetterScore(letter: string): number {
        let wordScore = 0;
        for (const char of letter) {
            wordScore += LETTERS_OBJECT.get(char)?.score ?? 0;
        }
        return wordScore;
    }
    spotNewLetters(command: Command) {
        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h') {
                if (this.commandService.gridTiles[command.position.y - 1][command.position.x - 1 + i] === NOT_A_LETTER) {
                    this.indexOfLetterToCheck.push(i);
                }
            } else {
                if (this.commandService.gridTiles[command.position.y - 1 + i][command.position.x - 1] === NOT_A_LETTER) {
                    this.indexOfLetterToCheck.push(i);
                }
            }
        }
    }
    containBonus(position: Vec2, bonus: Vec2[]): boolean {
        for (const pos of bonus) {
            if (pos.x === position.x && pos.y === position.y) return true;
        }
        return false;
    }
    resetIndex() {
        this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
    }
    wordInDictionnary(word: string): boolean {
        return UNDEFINED_INDEX !== this.dict.words.findIndex((element) => word === element);
    }
}
