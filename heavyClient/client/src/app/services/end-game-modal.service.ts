import { ElementRef, Injectable } from '@angular/core';
import { EaselObject } from '@app/classes/easel-object';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import {
    BOARD_WIDTH,
    EASEL_LENGTH,
    MEDIUM_PX,
    NB_LETTER_HAND,
    NB_TILES,
    NOT_A_LETTER,
    OFFSET_DRAW_LETTER,
    TILE_SIZE,
} from '@app/constants/constants';

@Injectable({
    providedIn: 'root',
})
export class EndGameModalService {
    easelOneCtx: CanvasRenderingContext2D;
    easelTwoCtx: CanvasRenderingContext2D;

    drawHands(): void {
        this.drawHand(this.easelOneCtx);
        this.drawHand(this.easelTwoCtx);
    }

    drawEasel(easel: EaselObject, ctx: CanvasRenderingContext2D) {
        let counter = 0;
        for (const lett of easel.easelLetters) {
            const pos = counter;
            if (lett && lett.charac !== NOT_A_LETTER.charac) {
                const image = new Image();
                image.src = './assets/letter-joker.png';
                image.onload = () => {
                    ctx.drawImage(image, pos * (BOARD_WIDTH / NB_TILES), 0, BOARD_WIDTH / NB_TILES, BOARD_WIDTH / NB_TILES);
                    this.drawLetter(lett, pos, MEDIUM_PX, ctx);
                    this.drawScore(lett, pos, MEDIUM_PX, ctx);
                };
            }
            counter++;
        }
    }

    setCanvasElements(easelOne: ElementRef<HTMLCanvasElement>, easelTwo: ElementRef<HTMLCanvasElement>): void {
        this.easelOneCtx = easelOne.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.easelTwoCtx = easelTwo.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    }
    drawLetter(letter: Letter, pos: number, pixelSize: TileContent, letterCtx: CanvasRenderingContext2D) {
        letterCtx.font = pixelSize.alpha.toString() + 'px' + ' Calibri';
        const ltrWidth = letterCtx.measureText(letter.charac).width;
        letterCtx.textBaseline = 'top';
        const ltrHeight = letterCtx.measureText(letter.charac).actualBoundingBoxDescent;

        letterCtx.fillText(
            letter.charac,
            pos * (BOARD_WIDTH / NB_TILES) + (TILE_SIZE / 2 - ltrWidth / OFFSET_DRAW_LETTER),
            TILE_SIZE / 2 - ltrHeight / 2,
        );
    }

    drawScore(letter: Letter, pos: number, pixelSize: TileContent, letterCtx: CanvasRenderingContext2D) {
        letterCtx.font = pixelSize.score.toString() + 'px' + ' Calibri';
        const scrWidth = letterCtx.measureText(letter.score.toString()).width;
        const scrHeight = letterCtx.measureText(letter.charac.toString()).actualBoundingBoxDescent;

        letterCtx.fillText(
            letter.score.toString(),
            pos * (BOARD_WIDTH / NB_TILES) + (TILE_SIZE / 2 - scrWidth / 2) + scrWidth,
            TILE_SIZE / 2 - scrHeight / 2 + scrHeight,
        );
    }
    private drawHand(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 5;
        for (let i = 0; i <= NB_LETTER_HAND; i++) {
            const pos = i;
            ctx.moveTo(pos * (BOARD_WIDTH / NB_TILES), 0);
            ctx.lineTo(pos * (BOARD_WIDTH / NB_TILES), BOARD_WIDTH / NB_TILES);
        }
        ctx.moveTo(0, 0);
        ctx.lineTo((EASEL_LENGTH + 1) * (BOARD_WIDTH / NB_TILES), 0);
        ctx.moveTo(0, BOARD_WIDTH / NB_TILES);
        ctx.lineTo((EASEL_LENGTH + 1) * (BOARD_WIDTH / NB_TILES), BOARD_WIDTH / NB_TILES);

        ctx.stroke();
    }
}
