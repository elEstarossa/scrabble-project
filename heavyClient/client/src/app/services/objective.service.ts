import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Command } from '@app/classes/command';
import { Objectives } from '@app/classes/objectives';
import { Player } from '@app/classes/player';
import { Vec2 } from '@app/classes/vec2';
import { AZUR_TILE, PINK_TILE } from '@app/constants/array-constant';
import {
    COUNTER_FOR_OBJS,
    FOUR_VOWELS,
    NB_TILES,
    OBJ,
    SCORE_FOR_PLACEMENT_OVER_THIRTY_POINTS,
    TARGET_ID0,
    TARGET_ID1,
    TARGET_ID2,
    TARGET_ID3,
    TARGET_ID4,
    TARGET_ID5,
    TARGET_ID6,
    TARGET_ID7,
    UNDEFINED_INDEX,
    VALIDATION_AWAIT,
} from '@app/constants/constants';
import { BehaviorSubject } from 'rxjs';
import { GameService } from './game.service';
import { ScoreService } from './score.service';

@Injectable({
    providedIn: 'root',
})
export class ObjectiveService {
    achivedOBS = new BehaviorSubject({ des: '', index: UNDEFINED_INDEX });

    constructor(private scoreService: ScoreService, private snackBar: MatSnackBar, private gameService: GameService) {}
    verifyObjective(command: Command, player: Player) {
        let secondAchived = false;
        for (let i = 0; i < player.objectives.length; i++) {
            if (!player.objectives[i].achived && this.isObjectifComplete(player.objectives[i], command, this.gameService.timeCounter)) {
                this.openSackBar(player.name + ' ' + player.objectives[i].description, secondAchived);
                player.score += player.objectives[i].pts;
                player.objectives[i].achived = true;
                secondAchived = true;
                if (this.gameService.multiplayerGame) this.achivedOBS.next({ des: player.objectives[i].description, index: i });
                else if (i !== 0) {
                    switch (player.creator) {
                        case true:
                            this.gameService.player2.objectives[i].achived = true;
                            break;
                        case false:
                            this.gameService.player1.objectives[i].achived = true;
                            break;
                    }
                }
            }
        }
    }

    generateObjectives() {
        const objectives = OBJ.slice();
        for (let i = 0; i < 3; i++) {
            const index = i === 1 ? 0 : Math.floor(Math.random() * objectives.length);
            this.gameService.player1.objectives.push(new Objectives(objectives[index].id, objectives[index].description, objectives[index].pts));
            objectives.splice(index, 1);
        }

        const index2 = Math.floor(Math.random() * objectives.length);
        this.gameService.player2.objectives.push(new Objectives(objectives[index2].id, objectives[index2].description, objectives[index2].pts));
        this.gameService.player2.objectives.push(
            new Objectives(
                this.gameService.player1.objectives[1].id,
                this.gameService.player1.objectives[1].description,
                this.gameService.player1.objectives[1].pts,
            ),
        );
        this.gameService.player2.objectives.push(
            new Objectives(
                this.gameService.player1.objectives[2].id,
                this.gameService.player1.objectives[2].description,
                this.gameService.player1.objectives[2].pts,
            ),
        );
    }

    private isObjectifComplete(objective: Objectives, command: Command, timeCounter: number): boolean {
        switch (objective.id) {
            case TARGET_ID0:
                return this.checkConsecutiveDirection(command, objective);
            case TARGET_ID1:
                return this.checkFourVowels(command);
            case TARGET_ID2:
                return this.checkContainsDoubleBonus(command, objective);
            case TARGET_ID3:
                return this.checkUnderFiveSec(command, timeCounter, objective);
            case TARGET_ID4:
                return this.checkWordBonusPlaced(command);
            case TARGET_ID5:
                return this.checkPlacementAlongEdge(command);
            case TARGET_ID6:
                return this.checkPlacementOverThirtyPoints(command);
            case TARGET_ID7:
                return this.checkConsecutiveExchange(command, objective);
        }
        return false;
    }
    private checkUnderFiveSec(command: Command, timeCounter: number, objective: Objectives): boolean {
        if (command.word !== '') {
            if (timeCounter <= COUNTER_FOR_OBJS) {
                objective.counter++;
            } else {
                objective.counter = 0;
            }
        }
        return objective.counter >= 3;
    }
    private checkConsecutiveDirection(command: Command, objective: Objectives): boolean {
        if (command.direction === '') {
            objective.save = '';
            objective.counter = 0;
        } else if (objective.save === command.direction) {
            objective.counter++;
        } else {
            objective.counter = 1;
            objective.save = command.direction;
        }

        return objective.counter >= 3;
    }

    private checkContainsDoubleBonus(command: Command, objective: Objectives) {
        let counter = 0;
        for (let i = 0; i < command.word.length; i++) {
            const posToCheck: Vec2 =
                command.direction === 'h'
                    ? { x: command.position.x + i - 1, y: command.position.y - 1 }
                    : { x: command.position.x - 1, y: command.position.y + i - 1 };
            if (this.scoreService.containBonus(posToCheck, PINK_TILE) || this.scoreService.containBonus(posToCheck, AZUR_TILE)) {
                counter++;
            }
        }
        if (counter >= 2) objective.counter = 1;
        return objective.counter >= 1;
    }

    private checkPlacementAlongEdge(command: Command): boolean {
        return (
            (command.direction === 'h' && (command.position.y === 1 || command.position.y === NB_TILES)) ||
            (command.direction === 'v' && (command.position.x === 1 || command.position.x === NB_TILES))
        );
    }

    private checkConsecutiveExchange(command: Command, objective: Objectives): boolean {
        if (command.word === '') {
            objective.counter++;
        } else {
            objective.counter = 0;
        }
        return objective.counter >= 3;
    }

    private checkWordBonusPlaced(command: Command): boolean {
        return command.word === 'bonus';
    }

    private checkPlacementOverThirtyPoints(command: Command) {
        return this.scoreService.getScore(command) >= SCORE_FOR_PLACEMENT_OVER_THIRTY_POINTS;
    }
    private checkFourVowels(command: Command): boolean {
        let vowelsCount = 0;
        for (let i = 0; i < command.word.length; i++) {
            const character = command.word.charAt(i).toLowerCase();
            if (['a', 'e', 'i', 'o', 'u'].indexOf(character.toLowerCase()) > UNDEFINED_INDEX) {
                vowelsCount++;
            }
        }

        return vowelsCount >= FOUR_VOWELS;
    }
    private openSackBar(msg: string, wait: boolean) {
        if (wait) {
            setTimeout(() => {
                this.snackBar.open('OBJECTIF ATTEINT PAR: ' + msg, 'fermer', {
                    horizontalPosition: 'center',
                    verticalPosition: 'top',
                });
                setTimeout(() => {
                    this.snackBar.dismiss();
                }, VALIDATION_AWAIT);
            }, VALIDATION_AWAIT);
        } else {
            this.snackBar.open('OBJECTIF ATTEINT PAR: ' + msg, 'fermer', {
                horizontalPosition: 'center',
                verticalPosition: 'top',
            });
            setTimeout(() => {
                this.snackBar.dismiss();
            }, VALIDATION_AWAIT);
        }
    }
}
