import { Injectable } from '@angular/core';
import { Command } from '@app/classes/command';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import {
    BOARD_HEIGHT,
    BOARD_WIDTH,
    CANEVAS_HEIGHT,
    CANEVAS_WIDTH,
    HAND_POSITION_START,
    H_ARROW,
    LEFTSPACE,
    LETTERS_OBJECT,
    MEDIUM_PX,
    NB_TILES,
    NOT_A_LETTER,
    TILE_SIZE,
    TOPSPACE,
    UNDEFINED_INDEX,
    UNDEFINED_POSITION,
    V_ARROW,
} from '@app/constants/constants';
import { CommandsService } from './commands.service';
import { GridService } from './grid.service';

@Injectable({
    providedIn: 'root',
})
export class SuperGridService {
    gridCtx: CanvasRenderingContext2D;
    gridIsClicked: boolean;
    currentTilePosition: Vec2 = UNDEFINED_POSITION;
    arrowDirection: string = H_ARROW;
    tempWord: string = '';
    initTile: Vec2 = UNDEFINED_POSITION;
    constructor(private gridService: GridService, private commandService: CommandsService) {}

    placeTempLetter(letter: Letter, position: Vec2) {
        const imgLetter = new Image();
        imgLetter.src = './assets/letter-joker.png';
        const pos: Vec2 = JSON.parse(JSON.stringify(position));
        imgLetter.onload = () => {
            this.gridCtx.drawImage(
                imgLetter,
                LEFTSPACE + ((pos.x - 1) * BOARD_WIDTH) / NB_TILES,
                TOPSPACE + ((pos.y - 1) * BOARD_WIDTH) / NB_TILES,
                BOARD_WIDTH / NB_TILES,
                BOARD_HEIGHT / NB_TILES,
            );

            this.gridService.drawLetter(letter, pos, MEDIUM_PX, this.gridCtx);
            this.gridService.drawScore(letter, pos, MEDIUM_PX, this.gridCtx);
            this.selectTile(pos);
        };
    }

    placeTempSteps(letter: Letter) {
        if (this.findNextTile()) {
            this.placeTempLetter(letter, this.currentTilePosition);
            this.tempWord += letter.charac;
            this.incrementDirection();
            if (this.findNextTile()) {
                this.selectGridTile(this.currentTilePosition);
            }
        }
    }
    findNextTile() {
        while (!this.commandService.tileIsEmpty(this.currentTilePosition)) {
            this.addLetterFromGrid();
            if (this.arrowDirection === H_ARROW ? this.currentTilePosition.x === NB_TILES : this.currentTilePosition.y === NB_TILES) {
                this.selectTile(this.currentTilePosition);
                return false;
            }
        }
        return true;
    }

    addLetterFromGrid() {
        this.selectTile(this.currentTilePosition);
        this.incrementDirection();
    }
    removeLastLetter(): boolean {
        let letterIsPlaced = true;
        this.clearTempCanvas();
        this.tempWord = this.tempWord.slice(0, UNDEFINED_INDEX);
        const saveWord = this.tempWord.slice();
        this.tempWord = '';
        this.currentTilePosition = JSON.parse(JSON.stringify(this.initTile));
        for (const charac of saveWord) this.placeTempSteps(LETTERS_OBJECT.get(charac) ?? NOT_A_LETTER);
        if (this.tempWord === '') {
            this.selectGridTile(this.currentTilePosition);
            letterIsPlaced = false;
        }
        return letterIsPlaced;
    }

    placeTempWord(command: Command) {
        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h') {
                this.placeTempLetter(LETTERS_OBJECT.get(command.word.charAt(i)) ?? NOT_A_LETTER, {
                    x: command.position.x + i,
                    y: command.position.y,
                });
            } else {
                this.placeTempLetter(LETTERS_OBJECT.get(command.word.charAt(i)) ?? NOT_A_LETTER, {
                    x: command.position.x,
                    y: command.position.y + i,
                });
            }
        }
    }

    switchArrowDirection() {
        if (this.arrowDirection === H_ARROW) this.arrowDirection = V_ARROW;
        else this.arrowDirection = H_ARROW;
    }

    resetArrowDirection() {
        this.arrowDirection = H_ARROW;
    }

    getDirectionFromArrow(): string {
        return this.arrowDirection === H_ARROW ? 'h' : 'v';
    }

    clearTempCanvas() {
        this.gridCtx.clearRect(0, 0, CANEVAS_WIDTH, CANEVAS_HEIGHT);
    }

    selectGridTile(tilePosition: Vec2) {
        if (tilePosition !== UNDEFINED_POSITION) {
            this.currentTilePosition = tilePosition;
            this.selectTile(tilePosition);
            this.drawArrowDirection(tilePosition);
        }
    }

    selectTile(pos: Vec2) {
        this.gridCtx.beginPath();
        this.gridCtx.lineWidth = 6;
        this.gridCtx.strokeStyle = 'red';
        this.gridCtx.rect(
            LEFTSPACE + ((pos.x - 1) * BOARD_WIDTH) / NB_TILES,
            TOPSPACE + ((pos.y - 1) * BOARD_HEIGHT) / NB_TILES,
            BOARD_WIDTH / NB_TILES,
            BOARD_WIDTH / NB_TILES,
        );
        this.gridCtx.stroke();
    }

    resetTempCanvas() {
        this.arrowDirection = H_ARROW;
        this.clearTempCanvas();
        this.tempWord = '';
    }
    letterEaselToChange(index: number) {
        this.gridCtx.beginPath();
        this.gridCtx.strokeStyle = 'green';
        this.gridCtx.lineWidth = 3;
        this.gridCtx.shadowColor = 'red';
        this.gridCtx.shadowBlur = 5;
        this.gridCtx.rect(
            LEFTSPACE + ((HAND_POSITION_START + index) * BOARD_WIDTH) / NB_TILES,
            TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2,
            BOARD_WIDTH / NB_TILES,
            BOARD_WIDTH / NB_TILES,
        );
        this.gridCtx.stroke();
    }
    letterEaselToSwap(index: number) {
        this.gridCtx.beginPath();
        this.gridCtx.strokeStyle = 'yellow';
        this.gridCtx.lineWidth = 3;
        this.gridCtx.shadowColor = 'red';
        this.gridCtx.shadowBlur = 5;
        this.gridCtx.rect(
            LEFTSPACE + ((HAND_POSITION_START + index) * BOARD_WIDTH) / NB_TILES,
            TOPSPACE + BOARD_HEIGHT + TOPSPACE / 2,
            BOARD_WIDTH / NB_TILES,
            BOARD_WIDTH / NB_TILES,
        );
        this.gridCtx.stroke();
    }
    private drawArrowDirection(pos: Vec2) {
        this.gridCtx.font = 'bold 20px system-ui';
        if (this.arrowDirection === H_ARROW) {
            this.gridCtx.fillText(
                this.arrowDirection,
                LEFTSPACE + ((pos.x - 1) * BOARD_WIDTH) / NB_TILES + TILE_SIZE / 3,
                TOPSPACE + ((pos.y - 1) * BOARD_HEIGHT) / NB_TILES + TILE_SIZE / 2,
                BOARD_WIDTH / NB_TILES,
            );
        } else {
            this.gridCtx.fillText(
                this.arrowDirection,
                LEFTSPACE + ((pos.x - 1) * BOARD_WIDTH) / NB_TILES + TILE_SIZE / 2,
                TOPSPACE + (pos.y * BOARD_HEIGHT) / NB_TILES - TILE_SIZE / 3,
                BOARD_WIDTH / NB_TILES,
            );
        }
    }

    private incrementDirection() {
        if (this.arrowDirection === H_ARROW && this.currentTilePosition.x < NB_TILES) this.currentTilePosition.x++;
        else if (this.arrowDirection === V_ARROW && this.currentTilePosition.y < NB_TILES) this.currentTilePosition.y++;
    }
}
