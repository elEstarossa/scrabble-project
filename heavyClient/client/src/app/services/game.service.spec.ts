/* eslint-disable max-lines */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Letter } from '@app/classes/letter';
import { Message } from '@app/classes/message';
import { GameService } from './game.service';

describe('GameService', () => {
    let service: GameService;
    let gridCtxSuperGrid: CanvasRenderingContext2D;
    let gridCtxEaselGrid: CanvasRenderingContext2D;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule],
        });
        service = TestBed.inject(GameService);

        jasmine.clock().uninstall();
        jasmine.clock().install();

        gridCtxSuperGrid = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;
        gridCtxEaselGrid = CanvasTestHelper['createCanvas'](600, 600).getContext('2d') as CanvasRenderingContext2D;
        service['superGridService'].gridCtx = gridCtxSuperGrid;
        service['easelOperationService'].gridCtx = gridCtxEaselGrid;
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
    /// //////////////////////////// getJoiner /////////////////////////////////
    it('should return a joiner when the creator is true', () => {
        service.player1.creator = true;
        const result = service.getJoiner();
        expect(result.creator).toBeFalse();
    });

    it('should return a a creator when the creator is false', () => {
        service.player1.creator = false;
        const result = service.getJoiner();
        expect(result.creator).toBeFalse();
    });

    /// //////////////////////////// getCreator /////////////////////////////////
    it('should return a creator when the creator is true', () => {
        service.player1.creator = true;
        const result = service.getCreator();
        expect(result.creator).toBeTrue();
    });

    it('should return a a joiner when the creator is false', () => {
        service.player1.creator = false;
        const result = service.getCreator();
        expect(result.creator).toBeFalse();
    });

    /// //////////////////////////// addScore /////////////////////////////////
    it('should addScore', () => {
        const number = 5;
        service.addScore(number);
        expect(service.player1.score).toBe(5);
    });

    it('should defineWhoStarts when the digit number is not 0', () => {
        spyOn(Math, 'random').and.returnValue(5);
        service.defineWhoStarts();
        expect(service.player2.turnToPlay).toBeTrue();
        expect(service.player1.turnToPlay).toBeFalse();
    });

    /// //////////////////////////// alternateTurns /////////////////////////////////
    it('should alternateTurns', () => {
        service.player1.turnToPlay = true;
        service.player2.turnToPlay = false;
        service.multiplayerGame = false;
        service.timer = { min: 0, sec: 30 };
        service.gameTimeTurn = { min: 1, sec: 0 };

        service.alternateTurns();
        jasmine.clock().tick(1000);
        expect(service.player1.turnToPlay).toBeFalse();
        expect(service.player2.turnToPlay).toBeTrue();
        expect(service.timer).toEqual({ min: 1, sec: 0 });
    });

    it('alternateTurns should make the endGameObs send a notification', () => {
        service.multiplayerGame = false;
        service.isGameOver = true;
        const spyEndGameNext = spyOn(service.endGameOBS, 'next').and.callFake(() => {
            return;
        });
        service.timer = { min: 0, sec: 30 };
        service.gameTimeTurn = { min: 1, sec: 0 };
        service.alternateTurns();
        jasmine.clock().tick(1000);
        expect(spyEndGameNext).toHaveBeenCalled();
        expect(service.player1.turnToPlay).toBeTrue();
        expect(service.player2.turnToPlay).toBeTrue();
    });

    it('alternateTurns should not call checkEndGame if this is a multiplayer game', () => {
        service.multiplayerGame = true;
        service.isGameOver = true;
        const spyCheckEndGame = spyOn(service, 'checkEndGame').and.callFake(() => {
            return;
        });
        service.timer = { min: 0, sec: 30 };
        service.gameTimeTurn = { min: 1, sec: 0 };
        service.alternateTurns();
        jasmine.clock().tick(1000);
        expect(spyCheckEndGame).not.toHaveBeenCalled();
    });
    // /// //////////////////////////// checkEndGame /////////////////////////////////
    it('should checkEndGame when its 6 pass turn ', () => {
        service.passTurnCounter = 6;

        service.checkEndGame();
        expect(service.isGameOver).toBeTrue();
        expect(service.isThreePasses).toBeTrue();
    });

    it('should checkEndGame when the player1 easel is empty', () => {
        spyOn(service.player1.easel, 'getEaselSize').and.returnValue(0);
        service['reserveService'].reserveSize = 0;

        service.checkEndGame();
        expect(service.isEaselEmpty).toBeTrue();
    });

    it('should checkEndGame when the player2 easel is empty and the reserve too', () => {
        spyOn(service.player2.easel, 'getEaselSize').and.returnValue(0);
        service['reserveService'].reserveSize = 0;

        service.checkEndGame();
        expect(service.isEaselEmpty).toBeTrue();
    });

    it('checkEndGame should not call addEaselLetterScore when the player2 easel is not empty but the reserve is', () => {
        spyOn(service.player1.easel, 'getEaselSize').and.returnValue(1);
        spyOn(service.player2.easel, 'getEaselSize').and.returnValue(1);
        const spyAddEaselLetterScore = spyOn(service, 'addEaselLetterScore');
        service['reserveService'].reserveSize = 0;

        service.checkEndGame();
        expect(spyAddEaselLetterScore).not.toHaveBeenCalled();
    });

    /// //////////////////////////// getWinnerName /////////////////////////////////

    it('should getWinnerName when one of the creator are a empty easel ', () => {
        service.player2.easel.easelSize = 0;
        service.getWinnerName();
        expect(service.player1.score).toEqual(0);
    });
    it('should add the score of the letters to the final score ', () => {
        service.player2.easel.easelSize = 100;
        service.getWinnerName();
        expect(service.player1.score).toEqual(0);
    });

    it('should getWinnerName when one of joiner are a empty easel ', () => {
        service.player1.easel.easelSize = 0;
        service.getWinnerName();
        expect(service.player2.score).toEqual(0);
    });

    it('should return the name of the creator when the score of creator is more than the joiner ', () => {
        service.player1.score = 5;
        service.player2.score = 2;
        const result = service.getWinnerName();
        expect(result).toEqual(service.player1.name);
    });
    it('should return the name of the joiner when the score of joiner is more than the creator ', () => {
        service.player1.score = 0;
        service.player2.score = 5;
        const result = service.getWinnerName();
        expect(result).toEqual(service.player2.name);
    });

    it('should return a string "egal" when the scores are equal ', () => {
        service.player1.score = 0;
        service.player2.score = 0;
        const result = service.getWinnerName();
        expect(result).toEqual('egal');
    });

    it('should return a string "egal" when the scores are equal ', () => {
        service.isAbandon = true;
        const result = service.getWinnerName();
        expect(result).toEqual(service.player1.name);
    });

    /// //////////////////////////// setIsGameOve /////////////////////////////////
    it('should setIsGameOve when the game is over ', () => {
        service.setIsGameOve();
        expect(service.isGameOver).toBeTrue();
    });

    /// //////////////////////////// getTimer /////////////////////////////////
    it('getTimer should return the correct time display when the seconds are equal to 0', () => {
        const time = '5 : 00';
        service.timer = { min: 5, sec: 0 };

        const result = service.getTimer();
        expect(result).toEqual(time);
    });

    it('getTimer should return the correct time display when the seconds are different than 0', () => {
        const time = '5 : 1';
        service.timer = { min: 5, sec: 1 };

        const result = service.getTimer();
        expect(result).toEqual(time);
    });

    /// //////////////////////////// getIsGameOver /////////////////////////////////
    it('should return true when the game is over ', () => {
        const result = service.getIsGameOver();
        expect(result).toEqual(service.isGameOver);
    });

    /// //////////////////////////// subEaselLetterScore /////////////////////////////////
    it('should call the subEaselLetterScore and deduct the score of the easel letters from the final score  ', () => {
        const letter: Letter[] = [{ score: 4, charac: 'a' }];
        service.player2.easel.easelLetters = letter;
        service.subEaselLetterScore();
        expect(service.player1.score).toEqual(0);
    });

    /// //////////////////////////// addEaselLetterScore /////////////////////////////////
    it('should call the addEaselLetterScore and add the score of the easel letters to a player score  ', () => {
        const letter: Letter[] = [{ score: 4, charac: 'a' }];
        service.player2.easel.easelLetters = letter;
        spyOn(service.player2.easel, 'getEaselSize').and.returnValue(0);
        service.addEaselLetterScore();
        expect(service.player1.score).toEqual(4);
    });

    it('should call the addEaselLetterScore and add the score of the easel letters to a player1 score  ', () => {
        const letter: Letter[] = [{ score: 4, charac: 'a' }];
        service.player2.easel.easelLetters = letter;
        spyOn(service.player2.easel, 'getEaselSize').and.returnValue(1);
        service.addEaselLetterScore();
        expect(service.player1.score).toEqual(4);
    });

    it('should call the addEaselLetterScore and add the score of the easel letters to a player2 score  ', () => {
        const letter: Letter[] = [{ score: 4, charac: 'a' }];
        service.player1.easel.easelLetters = letter;
        spyOn(service.player2.easel, 'getEaselSize').and.returnValue(1);
        service.addEaselLetterScore();
        expect(service.player2.score).toEqual(0);
    });

    /// //////////////////////////// getEndMessage /////////////////////////////////
    it('should getEndMessage when he detect three passes with a equal', () => {
        service.isThreePasses = true;
        spyOn(service, 'getWinnerName').and.returnValue('egal');
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'partie terminée pour la raison 3 passes consécutives\n Egalité: ',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    it('should getEndMessage when he detect three passes without equal', () => {
        service.isThreePasses = true;
        spyOn(service, 'getWinnerName').and.returnValue('');
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'partie terminée pour la raison 3 passes consécutives\n Gagnant: ',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    it('should getEndMessage when the easel is empty and the players are equal', () => {
        service.isThreePasses = false;
        service.isEaselEmpty = true;
        spyOn(service, 'getWinnerName').and.returnValue('egal');
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'partie terminée pour la raison chevalet et reserve vides\n Egalité: ',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    it('should getEndMessage when the easel is empty and the players are not equal', () => {
        service.isThreePasses = false;
        service.isEaselEmpty = true;
        spyOn(service, 'getWinnerName').and.returnValue('');
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'partie terminée pour la raison chevalet et reserve vides\n Gagnant: ',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    it('should getEndMessage when one of the players abandon the game', () => {
        service.isThreePasses = false;
        service.isEaselEmpty = false;
        service.isAbandon = true;
        spyOn(service, 'getWinnerName').and.returnValue('');
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'Votre adversaire a abondanné la partie! vous êtes le gagnant!',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    it('should getEndMessage when one of the players abandon the game', () => {
        service.isThreePasses = false;
        service.isEaselEmpty = false;
        service.isAbandon = false;
        const msg: Message = {
            colorRef: '',
            name: '',
            dm: '',
        };

        const result = service.getEndMessage();
        expect(result).toEqual(msg);
    });

    /// //////////////////////////// defineWhoStarts /////////////////////////////////
    it('defineWhoStarts should make the player1 turnToPlay true when the digit is 0', () => {
        service.player1.turnToPlay = false;
        service.player2.turnToPlay = false;
        spyOn(Math, 'round').and.returnValue(0);
        service.defineWhoStarts();
        expect(service.player1.turnToPlay).toBeTrue();
        expect(service.player2.turnToPlay).toBeFalse();
    });

    it('defineWhoStarts should make the player1 turnToPlay false when the digit is not 0', () => {
        service.player1.turnToPlay = false;
        service.player2.turnToPlay = false;
        spyOn(Math, 'round').and.returnValue(1);
        service.defineWhoStarts();
        expect(service.player1.turnToPlay).toBeFalse();
        expect(service.player2.turnToPlay).toBeTrue();
    });

    /// //////////////////////////// startTimer /////////////////////////////////
    it('startTimer should pass turns when the round is over', () => {
        service.gameTimeTurn = { min: 0, sec: 0 };
        service.isGameOver = false;
        const spyAlternateTurns = spyOn(service, 'alternateTurns');
        service.startTimer();
        jasmine.clock().tick(1001);
        expect(spyAlternateTurns).toHaveBeenCalled();
    });

    it('startTimer should decrement minutes and seconds when there are numbers at the min and the sec positions', () => {
        service.gameTimeTurn = { min: 1, sec: 0 };
        service.isGameOver = false;
        service.startTimer();
        jasmine.clock().tick(1001);
        expect(service.timer).toEqual({ min: 0, sec: 59 });
    });

    it('startTimer should decrement only seconds when there is no number at the min position', () => {
        service.gameTimeTurn = { min: 0, sec: 59 };
        service.isGameOver = false;
        service.startTimer();
        jasmine.clock().tick(1001);
        expect(service.timer).toEqual({ min: 0, sec: 58 });
    });

    it('startTimer should call clearInterval if the game is over', () => {
        service.gameTimeTurn = { min: 1, sec: 0 };
        service.isGameOver = true;
        const spyClearInterval = spyOn(global, 'clearInterval').and.callFake(() => {
            return;
        });
        service.startTimer();
        jasmine.clock().tick(1001);
        expect(spyClearInterval).toHaveBeenCalled();
    });
});
