/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { GamePageComponent } from './game-page.component';

describe('GamePageComponent', () => {
    let component: GamePageComponent;
    let fixture: ComponentFixture<GamePageComponent>;
    let multiplayerService: MultiplayerService;

    beforeEach(async () => {
        multiplayerService = jasmine.createSpyObj('MultiplayerService', ['sendAbandonGame']);

        await TestBed.configureTestingModule({
            declarations: [GamePageComponent],
            providers: [{ provide: MultiplayerService, useValue: multiplayerService }],
            imports: [MatDialogModule, RouterTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(GamePageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
