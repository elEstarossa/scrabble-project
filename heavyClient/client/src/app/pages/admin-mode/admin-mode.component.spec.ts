/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable @typescript-eslint/consistent-type-assertions */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { AdminModeComponent } from './admin-mode.component';

describe('AdminModeComponent', () => {
    let component: AdminModeComponent;
    let fixture: ComponentFixture<AdminModeComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<PopupDictionnaryModalComponent>>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        matDialogRefSpyObj = jasmine.createSpyObj('MatDialog', ['']);

        await TestBed.configureTestingModule({
            declarations: [AdminModeComponent],
            imports: [
                MatDialogModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
                MatTabsModule,
            ],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminModeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    // /// ///////////////// addPlayer /////////////////////////////////////////
    // it('should add a player in the list of the players', () => {
    //     const playerName = 'ani';
    //     component.addPlayer(playerName);
    //     expect(component.vPlayerNames).toEqual(['Bender', 'WALLE', 'COMPUTER', playerName]);
    // });

    // /// ///////////////// deletePlayer /////////////////////////////////////////
    // it('should delete a player in the list of the players', () => {
    //     const index = 0;
    //     const spy = spyOn(component.vPlayerNames, 'splice');
    //     component.deletePlayer(index);
    //     expect(spy).toHaveBeenCalled();
    // });

    // /// ///////////////// changeDictionnaryName /////////////////////////////////////////
    // it('should change the dictionnary name', () => {
    //     component.changeDictionnaryName('myDict');
    //     expect(matDialogSpyObj.open).toHaveBeenCalled();
    // });

    // /// ///////////////// deleteDictionnary /////////////////////////////////////////
    // // it('should delete the dictionnary if its the same title as the input', () => {
    // //     const title = 'myDict';
    // //     const index = 1;
    // //     const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: '', words: [''] }];
    // //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    // //     const spy = spyOn(component['dictionnaryService'], 'delete').and.returnValue(Promise.resolve(index));
    // //     component.deleteDictionnary(title);
    // //     expect(spy).toHaveBeenCalled();
    // // });
    // // it('should delete the dictionnary if its not the same title as the input', () => {
    // //     const title = 'dictionnary';
    // //     const index = 1;
    // //     const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: '', words: [''] }];
    // //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    // //     const spy = spyOn(component['dictionnaryService'], 'delete').and.returnValue(Promise.resolve(index));
    // //     component.deleteDictionnary(title);
    // //     expect(spy).not.toHaveBeenCalled();
    // // });

    // /// ///////////////// onChange /////////////////////////////////////////
    // it('should onChange if the read file is not valid ', () => {
    //     const event: Event = <Event>(<unknown>{
    //         target: {
    //             value: false,
    //         },
    //     });
    //     component.isNameAndFormatValid = false;
    //     const spy = spyOn(component['dictionnaryService'], 'readFile').and.returnValue(Promise.resolve(false));
    //     component.onChange(event);
    //     expect(spy).toHaveBeenCalled();
    //     expect(component.isNameAndFormatValid).toBeFalse();
    // });

    // it('should onChange if the read file is valid ', () => {
    //     const event: Event = <Event>(<unknown>{
    //         target: {
    //             value: false,
    //         },
    //     });
    //     component.isNameAndFormatValid = true;
    //     const spy = spyOn(component['dictionnaryService'], 'readFile').and.returnValue(Promise.resolve(true));
    //     component.onChange(event);
    //     expect(spy).toHaveBeenCalled();
    //     expect(component.isNameAndFormatValid).toBeTrue();
    // });

    // /// ///////////////// onSubmit /////////////////////////////////////////
    // it('should onSubmit ', () => {
    //     component.isNameAndFormatValid = false;
    //     const spy = spyOn(component['dictionnaryService'], 'create').and.returnValue(Promise.resolve());
    //     component.onSubmit();
    //     expect(spy).toHaveBeenCalled();
    //     expect(component.isNameAndFormatValid).toBeFalse();
    // });
    // /// ///////////////// resetParameters /////////////////////////////////////////
    // it('should resetParameters ', () => {
    //     component.resetParameters();
    //     expect(matDialogSpyObj.open).toHaveBeenCalled();
    // });

    // /// ///////////////// defaultDictIndex /////////////////////////////////////////
    // it('should get the defaultDictIndex if its the same title', () => {
    //     const listOfDictionnary: WordDictionnary[] = [{ title: 'Mon dictionnaire', description: '', words: [''] }];
    //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    //     const result = component.defaultDictIndex;
    //     expect(result).toEqual(0);
    // });

    // it('should get the defaultDictIndex if its not the same title', () => {
    //     const listOfDictionnary: WordDictionnary[] = [{ title: ' dictionnaire', description: '', words: [''] }];
    //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    //     const result = component.defaultDictIndex;
    //     expect(result).toEqual(-1);
    // });

    // /// ///////////////// changeDictionnaryDescription /////////////////////////////////////////
    // it('should change the dict description', () => {
    //     const description = '';
    //     const listOfDictionnary: WordDictionnary[] = [{ title: 'Mon dictionnaire', description: '', words: [''] }];
    //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    //     component.changeDictionnaryDescription(description);
    //     expect(matDialogSpyObj.open).toHaveBeenCalled();
    // });

    // /// ///////////////// downloadDictionnary /////////////////////////////////////////
    // it('should downloadDictionnary', () => {
    //     const index = 0;
    //     const listOfDictionnary: WordDictionnary[] = [{ title: 'dictionnary', description: '', words: [''] }];
    //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    //     component.downloadDictionnary(index);
    //     expect(index).toEqual(0);
    // });
});
