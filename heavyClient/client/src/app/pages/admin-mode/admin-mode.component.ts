import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { PopupModifyDescDictComponent } from '@app/components/popup/popup-modify-desc-dict/popup-modify-desc-dict.component';
import { PopupModifyDictionnaryComponent } from '@app/components/popup/popup-modify-dictionnary/popup-modify-dictionnary.component';
import { PopupResetComponent } from '@app/components/popup/popup-reset/popup-reset.component';
import { ELEMENT_NOT_FOUNDED, VIRTUAL_PLAYER_NAMES } from '@app/constants/constants';
import { DictionaryService } from '@app/services/dictionnary.service';
@Component({
    selector: 'app-admin-mode',
    templateUrl: './admin-mode.component.html',
    styleUrls: ['./admin-mode.component.scss'],
})
export class AdminModeComponent implements OnInit {
    @ViewChild('inputFile') myInputVariable: ElementRef;
    vPlayerNames = VIRTUAL_PLAYER_NAMES;
    fileForm: FormGroup;
    isErrorMessage: string;
    isNameAndFormatValid: boolean;
    dialogRef2: MatDialogRef<PopupModifyDictionnaryComponent>;
    dialogRef3: MatDialogRef<PopupModifyDescDictComponent>;
    dialogRef4: MatDialogRef<PopupResetComponent>;
    dialogRef: MatDialogRef<PopupDictionnaryModalComponent>;
    constructor(public dictionnaryService: DictionaryService, public dialog: MatDialog, private formBuilder: FormBuilder) {}
    ngOnInit(): void {
        this.dictionnaryService.getList();
        this.fileForm = this.formBuilder.group({
            newfile: ['', Validators.required],
        });
    }
    onSubmit() {
        this.dictionnaryService.create().then(
            () => {
                this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                this.dialogRef.componentInstance.title = 'Televersement Reussi';
                this.dialogRef.componentInstance.message = 'Le dictionnaire a été envoyé au serveur !';
            },
            (error) => {
                this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                this.dialogRef.componentInstance.title = 'Echec du televersement';
                this.dialogRef.componentInstance.message = error;
                this.reset();
            },
        );
    }
    onChange(event: Event) {
        this.dictionnaryService.readFile(event).then(
            (valid) => {
                if (!valid) {
                    this.isNameAndFormatValid = false;
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Echec du televersement';
                    this.dialogRef.componentInstance.message = this.dictionnaryService.errorMessage;
                    this.reset();
                } else this.isNameAndFormatValid = true;
            },
            (error) => {
                this.isErrorMessage = error;
            },
        );
    }
    addPlayer(playerName: string) {
        this.vPlayerNames.push(playerName);
    }

    deletePlayer(index: number) {
        this.vPlayerNames.splice(index, 1);
    }
    reset() {
        this.myInputVariable.nativeElement.value = '';
    }
    changeDictionnaryName(title: string) {
        this.dialogRef2 = this.dialog.open(PopupModifyDictionnaryComponent, {
            data: title,
        });
    }
    changeDictionnaryDescription(description: string) {
        this.dialogRef3 = this.dialog.open(PopupModifyDescDictComponent, {
            data: description,
        });
    }
    deleteDictionnary(title: string) {
        let counter = 0;
        for (const val of this.dictionnaryService.listOfDictionnary) {
            if (title === val.title) {
                this.dictionnaryService.delete(counter).then(
                    () => {
                        this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                        this.dictionnaryService.getList();
                        this.dialogRef.componentInstance.title = 'Suppression réussie';
                        this.dialogRef.componentInstance.message = 'Le dictionnaire a été supprimé !';
                    },
                    (error) => {
                        this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                        this.dialogRef.componentInstance.title = 'Echec de la suppression';
                        this.dialogRef.componentInstance.message = error;
                    },
                );
                this.dictionnaryService.deletionDialogForDict = false;
                return;
            } else {
                counter++;
            }
        }
    }
    downloadDictionnary(index: number) {
        const name = index === this.defaultDictIndex ? 'dictionnary.json' : this.dictionnaryService.listOfDictionnary[index].title + '.json';
        this.dictionnaryService.download(name);
    }
    get defaultDictIndex() {
        for (let i = 0; i < this.dictionnaryService.listOfDictionnary.length; i++) {
            if (this.dictionnaryService.listOfDictionnary[i].title === 'Mon dictionnaire') return i;
        }
        return ELEMENT_NOT_FOUNDED;
    }
    resetParameters() {
        this.dialogRef4 = this.dialog.open(PopupResetComponent);
    }
}
