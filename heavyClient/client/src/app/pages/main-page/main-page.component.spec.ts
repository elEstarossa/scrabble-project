/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { MainPageComponent } from '@app/pages/main-page/main-page.component';

describe('MainPageComponent', () => {
    let component: MainPageComponent;
    let fixture: ComponentFixture<MainPageComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientModule, MatDialogModule, MatSnackBarModule],
            declarations: [MainPageComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// //////////////// dispModal /////////////////////////////////////////////////
    it(' should dispModal and its open the main page modal component', () => {
        component.dispModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });
    /// //////////////// dispModalHighScore /////////////////////////////////////////////////
    it(' should dispModal and its open the main page modal component', () => {
        component.dispModalHighScore();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    /// //////////////// dispModalLog2990 /////////////////////////////////////////////////
    it(' should dispModal and its open the main page LOG2990 modal component', () => {
        component.dispModalLog2990();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
        expect(component['gameService'].log2990).toBeTrue();
    });
});
