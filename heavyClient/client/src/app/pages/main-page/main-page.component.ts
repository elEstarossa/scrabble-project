import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HighScoreModalComponent } from '@app/components/modal/highscore-modal/highscore-modal.component';
import { MainPageLog2990ModalComponent } from '@app/components/modal/main-page-log2990-modal/main-page-log2990-modal.component';
import { MainPageModalComponent } from '@app/components/modal/main-page-modal/main-page-modal.component';
import { DictionaryService } from '@app/services/dictionnary.service';
import { GameService } from '@app/services/game.service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
    readonly title: string = 'LOG2990';
    message: BehaviorSubject<string> = new BehaviorSubject<string>('');

    constructor(public dialog: MatDialog, private gameService: GameService, private dictionnaryService: DictionaryService) {}

    ngOnInit() {
        this.dictionnaryService.getList();
    }

    dispModal() {
        this.gameService.log2990 = false;
        this.dialog.open(MainPageModalComponent, { disableClose: true });
    }

    dispModalLog2990() {
        this.gameService.log2990 = true;
        this.dialog.open(MainPageLog2990ModalComponent, { disableClose: true });
    }

    dispModalHighScore() {
        this.dialog.open(HighScoreModalComponent, { disableClose: true });
    }
}
