import { CommandCode } from './comand-code';
import { Command } from './command';
import { Letter } from './letter';
import { Scores } from './scores';
import { GameTime } from './time';

export interface MessageServer {
    roomName?: string;
    command?: Command;
    commandCode?: CommandCode;
    reserve?: string;
    easel?: Letter[];
    reserveSize?: number;
    creator?: boolean;
    score?: number;
    chatInput?: string;
    timer?: GameTime;
    isAbandon?: boolean;
    isEaselEmpty?: boolean;
    isThreePasses?: boolean;
    roomList?: string[];
    allRoomList?: string[];
    creatorList?: string[];
    allRoomListLog2990?: string[];
    creatorListLog2990?: string[];
    joinerName?: string;
    creatorName?: string;
    browserClose?: boolean;
    highScore?: Scores[];
    log2990?: boolean;
}
