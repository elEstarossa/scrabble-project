export class Objectives {
    counter: number = 0;
    id: number;
    description: string;
    pts: number;
    achived: boolean = false;
    save: string = '';
    constructor(id: number, description: string, pts: number) {
        this.id = id;
        this.description = description;
        this.pts = pts;
    }
}
