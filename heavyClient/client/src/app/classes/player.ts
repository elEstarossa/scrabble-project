import { EaselObject } from './easel-object';
import { Objectives } from './objectives';

export class Player {
    name: string = '';
    score: number = 0;
    easel: EaselObject;
    turnToPlay: boolean = false;
    creator: boolean = false;
    objectives: Objectives[] = [];

    constructor(name: string) {
        this.name = name;
        this.easel = new EaselObject(false);
    }
}
