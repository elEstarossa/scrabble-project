import { Command } from './command';

export interface JvP {
    command: Command;
    score: number;
    easelLetters: string;
}
