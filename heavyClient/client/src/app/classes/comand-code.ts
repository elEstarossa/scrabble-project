export interface CommandCode {
    isValid: boolean;
    errorMsg: string;
    score: number;
}
