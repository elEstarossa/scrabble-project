export interface TileContent {
    alpha: number;
    score: number;
    label: string;
}
