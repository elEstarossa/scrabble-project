import { Vec2 } from './vec2';

export interface Command {
    word: string;
    position: Vec2;
    direction: string;
}
