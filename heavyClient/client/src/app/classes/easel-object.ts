import { NB_LETTER_HAND, NOT_A_LETTER, UNDEFINED_INDEX } from '@app/constants/constants';
import { Letter } from './letter';

export class EaselObject {
    easelSize: number = NB_LETTER_HAND;
    easelLetters = new Array<Letter>(NB_LETTER_HAND);
    foundLetter = new Array<boolean>(NB_LETTER_HAND);
    indexOfUsedLetters: number[] = [];
    posTempLetters = new Array<boolean>(NB_LETTER_HAND);
    indexTempLetters = new Array<number>();
    indexToMove: number = UNDEFINED_INDEX;

    constructor(initEasel: boolean) {
        this.foundLetter.fill(initEasel);
        this.easelLetters.fill(NOT_A_LETTER);
        this.posTempLetters.fill(initEasel);
    }

    getEaselSize(): number {
        let length = NB_LETTER_HAND;
        if (this.easelLetters)
            for (const c of this.easelLetters) {
                if (c && c.charac === NOT_A_LETTER.charac) {
                    length--;
                }
            }
        return length;
    }

    contains(word: string): boolean {
        let found = false;
        let first = true;
        for (let i = 0; i < word.length; i++) {
            if (found || first) {
                first = false;
                found = false;
                for (let j = 0; j < NB_LETTER_HAND; j++) {
                    if (word.charAt(i) === this.easelLetters[j].charac && !this.foundLetter[j]) {
                        this.foundLetter[j] = true;
                        this.indexOfUsedLetters.push(j);
                        found = true;
                        break;
                    }
                }
            } else {
                break;
            }
        }
        return found;
    }

    add(letter: Letter, index: number): void {
        this.easelLetters[index] = letter;
    }

    resetUsedLetters(): void {
        this.foundLetter.fill(false);
        this.indexOfUsedLetters.splice(0, this.indexOfUsedLetters.length);
    }
    resetTempIndex() {
        this.posTempLetters.fill(false);
        this.indexTempLetters.splice(0, this.indexTempLetters.length);
    }

    toString(): string {
        let easelToString = '';
        for (const letter of this.easelLetters) {
            if (letter === NOT_A_LETTER) easelToString += '- | ';
            else easelToString = easelToString + letter.charac + ' | ';
        }
        return easelToString;
    }
    pointInEasel(): number {
        let points = 0;
        for (const letter of this.easelLetters) {
            points += letter.score;
        }
        return points;
    }
    setTempLetter() {
        this.posTempLetters = this.foundLetter;
    }
}
