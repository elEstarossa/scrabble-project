export interface Message {
    colorRef: string;
    name: string;
    dm: string;
}
