import { Component, OnInit } from '@angular/core';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';

@Component({
    selector: 'app-creator-player',
    templateUrl: './creator-player.component.html',
    styleUrls: ['./creator-player.component.scss'],
})
export class CreatorPlayerComponent implements OnInit {
    creator = '';

    constructor(public gameService: GameService, private multiplayerService: MultiplayerService, private socketComm: SocketCommunicationService) {}

    ngOnInit() {
        this.initializeCreatorUser();
    }

    initializeCreatorUser() {
        if (this.gameService.player1.creator && this.gameService.multiplayerGame) {
            this.setMultiplayerActionsCreator();
        }
    }

    setMultiplayerActionsCreator() {
        this.socketComm.receiveReserveFirstTurn();
        this.socketComm.getWhoStarts();
        this.socketComm.getPassTurn();
        this.multiplayerService.receiveValidation();
        this.multiplayerService.getAction();
        this.multiplayerService.receiveChat();
        this.socketComm.getTime();
        this.multiplayerService.listenEndGame();
        this.socketComm.getAchived();
    }
}
