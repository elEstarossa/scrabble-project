/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCard, MatCardContent, MatCardHeader, MatCardSubtitle, MatCardTitle } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { CreatorPlayerComponent } from './creator-player.component';

describe('CreatorPlayerComponent', () => {
    let component: CreatorPlayerComponent;
    let fixture: ComponentFixture<CreatorPlayerComponent>;
    let gameService: GameService;
    let multiplayerServiceSpy: jasmine.SpyObj<MultiplayerService>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CreatorPlayerComponent, MatCardContent, MatCardHeader, MatCardTitle, MatCard, MatCardSubtitle],
            providers: [GameService, { provide: MultiplayerService, useValue: multiplayerServiceSpy }, { provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CreatorPlayerComponent);
        component = fixture.componentInstance;
        gameService = TestBed.inject(GameService);
        multiplayerServiceSpy = jasmine.createSpyObj('MultiplayerService', [
            'receiveReserveFirstTurn',
            'getWhoStarts',
            'getPassTurn',
            'receiveValidation',
            'getAction',
            'receiveChat',
            'getTime',
            'listenEndGame',
        ]);
        component['multiplayerService'] = multiplayerServiceSpy;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call initializeCreatorUser', () => {
        gameService.player1.creator = true;
        gameService.multiplayerGame = true;
        const creatorSpy = spyOn(component, 'setMultiplayerActionsCreator');
        component.initializeCreatorUser();
        expect(creatorSpy).toHaveBeenCalled();
    });

    it('should call setMultiplayerActionsCreator', () => {
        component.setMultiplayerActionsCreator();
        expect(multiplayerServiceSpy.receiveValidation).toHaveBeenCalled();
        expect(multiplayerServiceSpy.getAction).toHaveBeenCalled();
        expect(multiplayerServiceSpy.receiveChat).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenEndGame).toHaveBeenCalled();
    });
});
