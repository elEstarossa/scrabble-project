import { Component, OnInit } from '@angular/core';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';

@Component({
    selector: 'app-joiner-player',
    templateUrl: './joiner-player.component.html',
    styleUrls: ['./joiner-player.component.scss'],
})
export class JoinerPlayerComponent implements OnInit {
    constructor(public gameService: GameService, private multiplayerService: MultiplayerService, private socketComm: SocketCommunicationService) {}

    ngOnInit(): void {
        this.initializeJoinerUser();
    }

    initializeJoinerUser() {
        if (!this.gameService.player1.creator && this.gameService.multiplayerGame) {
            this.setMultiplayerActionsJoiner();
        }
    }
    setMultiplayerActionsJoiner() {
        this.socketComm.receiveReserveFirstTurn();
        this.socketComm.getWhoStarts();
        this.socketComm.getPassTurn();
        this.multiplayerService.receiveValidation();
        this.multiplayerService.getAction();
        this.multiplayerService.receiveChat();
        this.socketComm.getTime();
        this.multiplayerService.listenEndGame();
        this.socketComm.getAchived();
    }
}
