/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCard, MatCardContent, MatCardHeader, MatCardSubtitle, MatCardTitle } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { JoinerPlayerComponent } from './joiner-player.component';

describe('JoinerPlayerComponent', () => {
    let component: JoinerPlayerComponent;
    let fixture: ComponentFixture<JoinerPlayerComponent>;
    let gameService: GameService;
    let multiplayerServiceSpy: jasmine.SpyObj<MultiplayerService>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [JoinerPlayerComponent, MatCardContent, MatCardHeader, MatCardTitle, MatCard, MatCardSubtitle],
            providers: [GameService, { provide: MultiplayerService, useValue: multiplayerServiceSpy }, { provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(JoinerPlayerComponent);
        component = fixture.componentInstance;
        gameService = TestBed.inject(GameService);
        multiplayerServiceSpy = jasmine.createSpyObj('MultiplayerService', [
            'receiveReserveFirstTurn',
            'getWhoStarts',
            'getPassTurn',
            'receiveValidation',
            'getAction',
            'receiveChat',
            'getTime',
            'listenEndGame',
        ]);
        component['multiplayerService'] = multiplayerServiceSpy;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// /////////////////// initializeJoinerUser ///////////////////////////
    it('should call initializeJoinerUser when its not the creator as a principal player', () => {
        gameService.player1.creator = false;
        gameService.multiplayerGame = true;
        const joinerSpy = spyOn(component, 'setMultiplayerActionsJoiner');
        component.initializeJoinerUser();
        expect(joinerSpy).toHaveBeenCalled();
    });

    it('should call initializeJoinerUser when its not the creator as a principal player', () => {
        gameService.player1.creator = true;
        gameService.multiplayerGame = false;

        const joinerSpy = spyOn(component, 'setMultiplayerActionsJoiner');
        component.initializeJoinerUser();
        expect(joinerSpy).not.toHaveBeenCalled();
    });

    /// /////////////////// setMultiplayerActionsJoiner ///////////////////////////
    it('should call all the functions from the multiplayer service when we initialise the joiner ', () => {
        component.setMultiplayerActionsJoiner();
        expect(multiplayerServiceSpy.receiveValidation).toHaveBeenCalled();
        expect(multiplayerServiceSpy.getAction).toHaveBeenCalled();
        expect(multiplayerServiceSpy.receiveChat).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenEndGame).toHaveBeenCalled();
    });
});
