/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DefaultValueAccessor } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatIcon } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
import { UNDEFINED_INDEX } from '@app/constants/constants';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';
import { BehaviorSubject } from 'rxjs';

describe('PlayAreaComponent', () => {
    let component: PlayAreaComponent;
    let fixture: ComponentFixture<PlayAreaComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;
    let multiplayerServiceSpy: jasmine.SpyObj<MultiplayerService>;
    let socketCommSpy: jasmine.SpyObj<SocketCommunicationService>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        multiplayerServiceSpy = jasmine.createSpyObj('MultiplayerService', [
            'joinerFillEasel',
            'passTurn',
            'deleteRoom',
            'sendAbandonGame',
            'sendAction',
        ]);
        socketCommSpy = jasmine.createSpyObj('SocketCommunicationService', ['emit', 'sendReserve', 'joinerFillEasel', 'passTurn', 'sendAchived']);
        await TestBed.configureTestingModule({
            declarations: [PlayAreaComponent, MatButton, MatIcon, DefaultValueAccessor],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MultiplayerService, useValue: multiplayerServiceSpy },
                { provide: SocketCommunicationService, useValue: socketCommSpy },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [MatDialogModule, RouterTestingModule, HttpClientTestingModule, MatSnackBarModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component['gameService'].passOBS = new BehaviorSubject<string>('res');
        component['socketComm'].resetEaselOBS = new BehaviorSubject<string>('');
        const obj = { des: '', index: UNDEFINED_INDEX };
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        component['objectiveService'].achivedOBS = new BehaviorSubject<any>(obj);
        jasmine.clock().uninstall();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// ///////////////////////////////// ngOnInit and subscribers ///////////////////////////////////////////////
    it('NgOnInit should call eventClickService.unClick when turn is passed', async () => {
        jasmine.clock().install();
        const spy = spyOn(component['eventClickService'], 'unClick');
        component['gameService'].passOBS.next('res');
        component.ngOnInit();

        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('NgOnInit should call eventClickService.unClick when easel is reset', async () => {
        jasmine.clock().install();
        const spy = spyOn(component['eventClickService'], 'unClick');
        component['socketComm'].resetEaselOBS.next('reset');
        component.ngOnInit();

        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('NgOnInit should not call eventClickService.unClick when easel is not reset', async () => {
        jasmine.clock().install();
        const spy = spyOn(component['eventClickService'], 'unClick');
        component['socketComm'].resetEaselOBS.next('');
        component.ngOnInit();

        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('NgOnInit should not call socketComm.sendAchieved when an objective is not achieved', async () => {
        jasmine.clock().install();
        component['objectiveService'].achivedOBS.next({ des: '', index: UNDEFINED_INDEX });
        component.ngOnInit();

        jasmine.clock().tick(1);
        expect(socketCommSpy.sendAchived).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('NgOnInit should call socketComm.sendAchieved when an objective is achieved', async () => {
        jasmine.clock().install();
        component['objectiveService'].achivedOBS.next({ des: 'a', index: 1 });
        component.ngOnInit();

        jasmine.clock().tick(1);
        expect(socketCommSpy.sendAchived).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    /// ///////////////////////////////// disModal ///////////////////////////////////////////////
    it(' should dispModal and it opens the leave modal component', () => {
        component.dispModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    /// ///////////////////////////////// passTurn ///////////////////////////////////////////////
    it(' should passTurn on calling the pass turn function in multiplayer service', () => {
        component.passTurn();
        expect(socketCommSpy.passTurn).toHaveBeenCalled();
    });

    /// ///////////////////////////////// disableButton ///////////////////////////////////////////////
    it(' should disableButton on the home page', () => {
        const expected = component['gameService'].player1.turnToPlay;
        const result = component.disableButton();
        expect(result).toEqual(!expected);
    });

    /// ///////////////////////////////// unloadHandler ///////////////////////////////////////////////
    it(' should unloadHandler when the multiplayerGame is true', () => {
        component['gameService'].multiplayerGame = true;
        component.unloadHandler();
        expect(multiplayerServiceSpy.sendAbandonGame).toHaveBeenCalled();
    });

    it(' should unloadHandler when the multiplayerGame is false', () => {
        component['gameService'].multiplayerGame = false;
        component.unloadHandler();
        expect(multiplayerServiceSpy.sendAbandonGame).not.toHaveBeenCalled();
    });

    /// ///////////////////////////////// incrementSize ///////////////////////////////////////////////
    it(' should incrementSize when the boolean is true and pixel index is less than 2', () => {
        component.pxIndex = 0;
        component.incrementSize(true);
        expect(component.pxIndex).toEqual(1);
    });

    it(' should incrementSize when the boolean is false and pixel index is more  than 0', () => {
        component.pxIndex = 5;
        component.incrementSize(false);
        expect(component.pxIndex).toEqual(4);
    });

    it(' should incrementSize when the boolean is false and pixel index is more  than 0', () => {
        component.pxIndex = 25;
        component.incrementSize(true);
        expect(component.pxIndex).toEqual(25);
    });

    /// ///////////////////////////////// scroll ///////////////////////////////////////////////
    it('should call scroll for deltaY equal to  1', () => {
        const buttonEvent = {
            deltaY: 1,
        } as WheelEvent;

        const spy = spyOn(component['eventClickService'], 'swapLetter');

        component.scroll(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    /// ///////////////////////////////// handleKeyEvent ///////////////////////////////////////////////
    it('should call handleKeyEvent for Backspace', () => {
        const buttonEvent = {
            key: 'Backspace',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        const spy = spyOn(component['eventClickService'], 'onBackSpaceLetters');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for Enter', () => {
        const buttonEvent = {
            key: 'Enter',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        const spy = spyOn(component, 'onPlaceLetters');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for Escape', () => {
        const buttonEvent = {
            key: 'Escape',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        const spy = spyOn(component['eventClickService'], 'onEscapeLetters');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for ArrowLeft', () => {
        const buttonEvent = {
            key: 'ArrowLeft',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        const spy = spyOn(component['eventClickService'], 'swapLetter');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for ArrowRight', () => {
        const buttonEvent = {
            key: 'ArrowRight',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        const spy = spyOn(component['eventClickService'], 'swapLetter');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for nothing', () => {
        const buttonEvent = {
            key: '',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        component['eventClickService'].lastGridClickPosition.x = 2;
        const spy = spyOn(component['eventClickService'], 'keyValidation');

        component.handleKeyEvent(buttonEvent);
        expect(spy).toHaveBeenCalled();
    });

    it('should call handleKeyEvent for nothing', () => {
        const buttonEvent = {
            key: '',
            preventDefault: jasmine.createSpyObj,
            stopPropagation: jasmine.createSpyObj,
        } as KeyboardEvent;

        component['eventClickService'].lastGridClickPosition.x = -1;
        const spy = spyOn(component['eventClickService'], 'keyValidation');

        component.handleKeyEvent(buttonEvent);
        expect(spy).not.toHaveBeenCalled();
    });
    /// ///////////////////////////////// onPlaceLetters ///////////////////////////////////////////////
    it('onPlaceLetters should call onPlaceLetters of eventClickService', () => {
        const spy = spyOn(component.eventClickService, 'onPlaceLetters');
        component.onPlaceLetters();
        expect(spy).toHaveBeenCalled();
    });

    /// ///////////////////////////////// quit ///////////////////////////////////////////////
    it('quit should quit the room', () => {
        component.quit();
        expect(multiplayerServiceSpy.deleteRoom).toHaveBeenCalled();
    });
});
