import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TileContent } from '@app/classes/tile-content';
import { LeaveGameModalComponent } from '@app/components/modal/leave-game-modal/leave-game-modal.component';
import { CANEVAS_HEIGHT, CANEVAS_WIDTH, DEFAULT_DICTIONNARY, MEDIUM_PX, RESERVE_SIZE, SIZES, UNDEFINED_INDEX } from '@app/constants/constants';
import { CommandsService } from '@app/services/commands.service';
import { DictionaryService } from '@app/services/dictionnary.service';
import { EaselOperationsService } from '@app/services/easel-operations.service';
import { EventClickService } from '@app/services/event-click.service';
import { GameService } from '@app/services/game.service';
import { GridService } from '@app/services/grid.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { ObjectiveService } from '@app/services/objective.service';
import { ReserveService } from '@app/services/reserve.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';
import { SuperGridService } from '@app/services/super-grid.service';
import { VirtualPlayerService } from '@app/services/virtual-player.service';

@Component({
    selector: 'app-play-area',
    templateUrl: './play-area.component.html',
    styleUrls: ['./play-area.component.scss'],
})
export class PlayAreaComponent implements AfterViewInit, OnInit {
    @ViewChild('gridCanvas', { static: false }) private gridCanvas!: ElementRef<HTMLCanvasElement>;
    @ViewChild('letterCanvas', { static: false }) private letterCanvas!: ElementRef<HTMLCanvasElement>;
    @ViewChild('superCanvas', { static: false }) private superCanvas!: ElementRef<HTMLCanvasElement>;

    allSizes: TileContent[] = SIZES;
    pxSize: TileContent = MEDIUM_PX;
    pxIndex: number = 1;
    remainingLetters: number = RESERVE_SIZE;

    private canvasSize = { x: CANEVAS_WIDTH, y: CANEVAS_HEIGHT };

    constructor(
        private readonly gridService: GridService,
        public objectiveService: ObjectiveService,
        public dialog: MatDialog,
        private commandService: CommandsService,
        private easelOperations: EaselOperationsService,
        public gameService: GameService,
        private reserveService: ReserveService,
        private multiplayerService: MultiplayerService,
        private superGridService: SuperGridService,
        private router: Router,
        private virtualPlayer: VirtualPlayerService,
        public eventClickService: EventClickService,
        private socketComm: SocketCommunicationService,
        private dictionnaryService: DictionaryService,
    ) {
        if (this.gameService.player1.creator) {
            this.easelOperations.fillEasel(this.gameService.player1.easel, true);
            this.objectiveService.generateObjectives();
            if (this.gameService.multiplayerGame) {
                this.socketComm.sendReserve();
                this.socketComm.sendObjectives();
            } else {
                this.multiplayerService.createSoloRoom();
                this.easelOperations.fillEasel(this.gameService.player2.easel, false);
                this.gameService.defineWhoStarts();
                this.gameService.startTimer();

                if (this.gameService.player2.turnToPlay) this.virtualPlayer.play();
            }
        } else {
            setTimeout(() => {
                if (this.gameService.multiplayerGame) {
                    this.socketComm.joinerFillEasel();
                    this.socketComm.getObjectives();
                    this.dictionnaryService.getDict(DEFAULT_DICTIONNARY);
                }
            }, 0);
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    unloadHandler() {
        if (this.gameService.multiplayerGame) this.multiplayerService.sendAbandonGame(true);
    }

    @HostListener('window:keydown', ['$event'])
    handleKeyEvent(event: KeyboardEvent) {
        switch (event.key) {
            case 'Backspace':
                this.eventClickService.onBackSpaceLetters();
                break;
            case 'Enter':
                this.onPlaceLetters();
                break;
            case 'Escape':
                this.eventClickService.onEscapeLetters();
                break;
            case 'ArrowLeft':
                this.eventClickService.swapLetter('LEFT');
                break;
            case 'ArrowRight':
                this.eventClickService.swapLetter('RIGHT');
                break;
            default:
                if (this.eventClickService.lastGridClickPosition.x !== UNDEFINED_INDEX) this.eventClickService.keyValidation(event.key);
                break;
        }
    }
    @HostListener('mousewheel', ['$event'])
    scroll(event: WheelEvent) {
        const direction = event.deltaY > 0 ? 'LEFT' : 'RIGHT';
        this.eventClickService.swapLetter(direction);
    }
    async ngOnInit(): Promise<void> {
        this.reserveService.sizeObs.subscribe((res) => {
            setTimeout(() => {
                this.remainingLetters = res;
            }, 0);
        });

        this.gameService.passOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.eventClickService.unClick();
                }
            }, 0);
        });

        this.socketComm.resetEaselOBS.subscribe((res) => {
            setTimeout(() => {
                if (res === 'reset') {
                    this.eventClickService.unClick();
                }
            }, 0);
        });
        this.objectiveService.achivedOBS.subscribe((res) => {
            setTimeout(() => {
                if (res.des !== '') {
                    this.socketComm.sendAchived(res.des, res.index);
                }
            }, 0);
        });
    }

    ngAfterViewInit(): void {
        this.gridService.gridContext = this.gridCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.commandService.gridCtx = this.gridCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.commandService.letterCtx = this.letterCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.easelOperations.gridCtx = this.gridCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.superGridService.gridCtx = this.superCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;

        this.gridService.drawCentralTile();
        this.gridService.drawPositions();
        this.gridService.drawBox();
        this.gridService.drawGrid();
        this.gridService.drawEasel();
        this.gridCanvas.nativeElement.focus();
    }

    get width(): number {
        return this.canvasSize.x;
    }

    get height(): number {
        return this.canvasSize.y;
    }

    dispModal() {
        this.eventClickService.unClick();
        this.dialog.open(LeaveGameModalComponent, { disableClose: true });
    }

    passTurn() {
        this.eventClickService.unClick();
        this.socketComm.passTurn();
    }
    quit() {
        this.multiplayerService.deleteRoom(this.gameService.roomName);
        this.router.navigateByUrl('/home').then(() => location.reload());
    }

    disableButton() {
        return !this.gameService.player1.turnToPlay;
    }

    onPlaceLetters() {
        this.eventClickService.onPlaceLetters();
    }

    incrementSize(increment: boolean) {
        this.eventClickService.unClick();
        if (increment && this.pxIndex < 2) {
            this.pxIndex++;
        } else if (!increment && this.pxIndex > 0) {
            this.pxIndex--;
        }

        this.pxSize = SIZES[this.pxIndex];
        this.commandService.pixelSize = this.pxSize;
        this.easelOperations.pixelSize = this.pxSize;

        this.commandService.redefineLetterSize();
        this.easelOperations.redrawEasel(this.gameService.player1.easel);
    }
}
