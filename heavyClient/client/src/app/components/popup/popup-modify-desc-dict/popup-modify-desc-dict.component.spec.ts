/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { EMPTY } from 'rxjs';
import { PopupModifyDescDictComponent } from './popup-modify-desc-dict.component';

describe('PopupModifyDescDictComponent', () => {
    let component: PopupModifyDescDictComponent;
    let fixture: ComponentFixture<PopupModifyDescDictComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<PopupDictionnaryModalComponent>>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogRefSpyObj = jasmine.createSpyObj('matDialogRefSpyObj', ['close']);
        matDialogRefSpyObj.componentInstance = {
            title: '',
            message: '',
            dialog: {} as MatDialog,
            dialogRef: {} as MatDialogRef<PopupDictionnaryModalComponent>,
        };
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);

        await TestBed.configureTestingModule({
            declarations: [PopupModifyDescDictComponent],
            imports: [
                MatDialogModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
                MatCardModule,
            ],
            providers: [
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MAT_DIALOG_DATA, useValue: {} },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopupModifyDescDictComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    /// ////////////////////////////  onSubmit  //////////////////////////////////////
    it('should onSubmit when the new description is the same as our description ', () => {
        const description = component.dictionnaryDescription;
        component.getIndexOfDescription(description);

        component.onSubmit(description);

        expect(component.hiddenFirstWrongMessage).toBeTrue();
    });

    it('should onSubmit when the new description is not the same as our description ', () => {
        const description = 'allo';
        component.getIndexOfDescription(description);
        component.getTitle(description);

        component.onSubmit(description);

        expect(component.hiddenFirstWrongMessage).toBeFalse();
    });

    it('should onSubmit when description validation is true', async () => {
        const description = 'allo';
        component.getIndexOfDescription(description);
        component.getTitle(description);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        matDialogSpyObj.open.and.returnValue({ afterClosed: () => EMPTY, componentInstance: { title: 'a', message: 'yeet' } } as any);
        spyOn(component.dictionnaryService, 'descriptionValidation').and.returnValue(true);
        spyOn(component.dictionnaryService, 'modify').and.returnValue(Promise.resolve());
        spyOn(component, 'windowReload').and.callFake(() => {
            return;
        });

        await component.onSubmit(description);

        expect(component.hiddenFirstWrongMessage).toBeFalse();
    });

    it('should onSubmit when description validation is false', async () => {
        const description = 'allo';
        component.getIndexOfDescription(description);
        component.getTitle(description);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        matDialogSpyObj.open.and.returnValue({ afterClosed: () => EMPTY, componentInstance: { title: 'a', message: 'yeet' } } as any);
        spyOn(component.dictionnaryService, 'descriptionValidation').and.returnValue(false);

        await component.onSubmit(description);

        expect(component.dialogRef.componentInstance.title).toEqual('Echec de modification');
    });

    it('should onSubmit when description validation is true and promise rejected', async () => {
        const description = 'allo';
        component.getIndexOfDescription(description);
        component.getTitle(description);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        matDialogSpyObj.open.and.returnValue({ afterClosed: () => EMPTY, componentInstance: { title: 'a', message: 'yeet' } } as any);
        spyOn(component.dictionnaryService, 'descriptionValidation').and.returnValue(true);
        spyOn(component.dictionnaryService, 'modify').and.returnValue(Promise.reject('error'));
        spyOn(component, 'windowReload').and.callFake(() => {
            return;
        });

        await component.onSubmit(description);

        expect(component.hiddenFirstWrongMessage).toBeFalse();
        expect(component.dialogRef.componentInstance.message).toEqual('error');
    });

    /// ////////////////////////////  getIndexOfDescription  //////////////////////////////////////
    it('should getIndexOfDescription if its not the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getIndexOfDescription(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual(-1);
    });

    it('should getIndexOfDescription if its the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: '', description: 'myDict', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getIndexOfDescription(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual(0);
    });

    /// ////////////////////////////  getTitle  //////////////////////////////////////
    it('should get an empty Title if its not the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getTitle(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual('');
    });

    it('should get a Title if its not the same description', () => {
        const description = 'myDict2';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: 'myDict2', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getTitle(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual('myDict');
    });

    /// ////////////////////////////  windowReload  //////////////////////////////////////
    it('should windowReload', async () => {
        const $window = jasmine.createSpyObj('Window', ['location']);
        $window.location = jasmine.createSpyObj('Location', ['reload']);
        await component.windowReload($window);
        expect($window.location.reload).toHaveBeenCalled();
    });
});
