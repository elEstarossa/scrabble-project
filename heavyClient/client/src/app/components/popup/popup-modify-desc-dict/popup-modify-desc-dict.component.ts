import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { ELEMENT_NOT_FOUNDED } from '@app/constants/constants';
import { DictionaryService } from '@app/services/dictionnary.service';

@Component({
    selector: 'app-popup-modify-desc-dict-modal',
    templateUrl: './popup-modify-desc-dict.component.html',
    styleUrls: ['./popup-modify-desc-dict.component.scss'],
})
export class PopupModifyDescDictComponent implements OnInit {
    dictionnaryDescription: string;
    hiddenFirstWrongMessage: boolean = false;
    newDescription: string;
    description: string;
    message: string;
    dialogRef: MatDialogRef<PopupDictionnaryModalComponent>;
    constructor(
        @Inject(MAT_DIALOG_DATA) public titlePassedWithDialog: string,
        public dictionnaryService: DictionaryService,
        public dialog: MatDialog,
    ) {}
    ngOnInit(): void {
        this.dictionnaryDescription = this.titlePassedWithDialog;
    }
    onSubmit(newDescription: string) {
        const index = this.getIndexOfDescription(this.dictionnaryDescription);
        const newTitle = this.getTitle(this.dictionnaryDescription);
        const dict: WordDictionnary = { title: newTitle, description: newDescription, words: [] };
        if (newDescription === this.dictionnaryDescription) {
            this.hiddenFirstWrongMessage = true;
            return;
        }
        if (this.dictionnaryService.descriptionValidation(newTitle)) {
            this.dictionnaryService.modify(index, dict).then(
                () => {
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Modification réussie';
                    this.dialogRef.componentInstance.message = 'Le dictionnaire a été modifié !';
                    this.windowReload(window);
                },
                (error) => {
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Echec de la modification';
                    this.dialogRef.componentInstance.message = error;
                },
            );
        } else {
            this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
            this.dialogRef.componentInstance.title = 'Echec de modification';
            this.dialogRef.componentInstance.message = 'Le nom existe déjà. Choississez un autre !';
        }
    }
    getIndexOfDescription(description: string) {
        this.dictionnaryService.getList();
        for (let i = 0; i < this.dictionnaryService.listOfDictionnary.length; i++) {
            if (this.dictionnaryService.listOfDictionnary[i].description === description) {
                return i;
            }
        }
        return ELEMENT_NOT_FOUNDED;
    }
    getTitle(description: string) {
        this.dictionnaryService.getList();
        for (const val of this.dictionnaryService.listOfDictionnary) {
            if (val.description === description) {
                return val.title;
            }
        }
        return '';
    }
    windowReload($window: Window) {
        $window.location.reload();
    }
}
