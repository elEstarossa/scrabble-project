/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { MuliplayerCreateModalComponent } from '@app/components/modal/muliplayer-create-modal/muliplayer-create-modal.component';
import { PopupModifyDictionnaryComponent } from './popup-modify-dictionnary.component';

describe('PopupModifyDictionnaryComponent', () => {
    let component: PopupModifyDictionnaryComponent;
    let fixture: ComponentFixture<PopupModifyDictionnaryComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<MuliplayerCreateModalComponent>>;

    beforeEach(async () => {
        matDialogRefSpyObj = jasmine.createSpyObj('matDialogRefSpyObj', ['close']);

        await TestBed.configureTestingModule({
            declarations: [PopupModifyDictionnaryComponent],
            imports: [
                MatDialogModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopupModifyDictionnaryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    /// ////////////////////////////  onSubmit  //////////////////////////////////////
    it('should onSubmit when the new description is the same as our description ', async () => {
        const description = component.dictionnaryTitle;
        component.getIndexOfTitle(description);

        const spy = spyOn(component, 'windowReload').and.callFake(() => {
            return;
        });
        await component.onSubmit(description);
        expect(spy).not.toHaveBeenCalled();
        expect(component.hiddenFirstWrongMessage).toBeTrue();
    });

    it('should onSubmit when the new description is not the same as our description ', async () => {
        const description = 'allo';
        component.getIndexOfTitle(description);
        component.getDescription(description);

        const spy = spyOn(component, 'windowReload').and.callFake(() => {
            return;
        });
        await component.onSubmit(description);
        expect(spy).not.toHaveBeenCalled();
        expect(component.hiddenFirstWrongMessage).toBeFalse();
    });

    it('should onSubmit when the new description is not the same as our description ', async () => {
        const description = 'allo';
        component.getIndexOfTitle(description);
        component.getDescription(description);
        spyOn(component.dictionnaryService, 'modify').and.returnValue(Promise.resolve());
        const spy = spyOn(component, 'windowReload').and.callFake(() => {
            return;
        });
        await component.onSubmit(description);
        expect(component.hiddenFirstWrongMessage).toBeFalse();
        expect(spy).toHaveBeenCalled();
    });

    it('onSubmit should make a warning when promise is rejected ', async () => {
        const description = 'allo';
        component.getIndexOfTitle(description);
        component.getDescription(description);

        spyOn(component.dictionnaryService, 'modify').and.returnValue(Promise.reject('error'));
        await component.onSubmit(description);
        expect(component.dialogRef.componentInstance.title).toEqual('Echec de la modification');
    });

    it('onSubmit should make a warning when nameValidation is false', async () => {
        const description = 'allo';
        component.getIndexOfTitle(description);
        component.getDescription(description);
        spyOn(component.dictionnaryService, 'nameValidation').and.returnValue(false);

        // spyOn(component.dictionnaryService, 'modify').and.returnValue(Promise.reject('error'));
        await component.onSubmit(description);
        expect(component.dialogRef.componentInstance.title).toEqual('Echec de la modification');
    });
    /// ////////////////////////////  windowReload  //////////////////////////////////////
    it('should windowReload', async () => {
        const $window = jasmine.createSpyObj('Window', ['location']);
        $window.location = jasmine.createSpyObj('Location', ['reload']);
        await component.windowReload($window);
        expect($window.location.reload).toHaveBeenCalled();
    });

    /// ////////////////////////////  getIndexOfTitle  //////////////////////////////////////
    it('should getIndexOfTitle if its not the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getIndexOfTitle(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual(0);
    });

    it('should getIndexOfTitle if its the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: '', description: 'myDict', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getIndexOfTitle(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual(-1);
    });

    /// ////////////////////////////  getDescription  //////////////////////////////////////
    it('should get an empty description if its not the same description', () => {
        const description = 'myDict';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: 'hey', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getDescription(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual('hey');
    });

    it('should get a description if its not the same description', () => {
        const description = 'myDict2';
        const spy = spyOn(component['dictionnaryService'], 'getList');

        const listOfDictionnary: WordDictionnary[] = [{ title: 'myDict', description: 'myDict2', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
        const result = component.getDescription(description);

        expect(spy).toHaveBeenCalled();
        expect(result).toEqual('');
    });
});
