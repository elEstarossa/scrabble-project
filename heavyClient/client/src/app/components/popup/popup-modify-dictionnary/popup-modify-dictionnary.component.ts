import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { ELEMENT_NOT_FOUNDED } from '@app/constants/constants';
import { DictionaryService } from '@app/services/dictionnary.service';
@Component({
    selector: 'app-popup-modify-dictionnary-modal',
    templateUrl: './popup-modify-dictionnary.component.html',
    styleUrls: ['./popup-modify-dictionnary.component.scss'],
})
export class PopupModifyDictionnaryComponent implements OnInit {
    // @Input() dictionnaryTitle: string;
    dictionnaryTitle: string;
    hiddenFirstWrongMessage: boolean = false;
    newTitle: string;
    title: string;
    message: string;
    dialogRef: MatDialogRef<PopupDictionnaryModalComponent>;
    constructor(
        @Inject(MAT_DIALOG_DATA) public titlePassedWithDialog: string,
        public dictionnaryService: DictionaryService,
        public dialog: MatDialog,
    ) {}
    ngOnInit(): void {
        this.dictionnaryTitle = this.titlePassedWithDialog;
    }
    onSubmit(newTitle: string) {
        const index = this.getIndexOfTitle(this.dictionnaryTitle);
        const newDescription = this.getDescription(this.dictionnaryTitle);
        const dict: WordDictionnary = { title: newTitle, description: newDescription, words: [] };
        if (newTitle === this.dictionnaryTitle) {
            this.hiddenFirstWrongMessage = true;
            return;
        }
        if (this.dictionnaryService.nameValidation(newTitle)) {
            this.dictionnaryService.modify(index, dict).then(
                () => {
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Modification réussie';
                    this.dialogRef.componentInstance.message = 'Le dictionnaire a été modifié !';
                    this.windowReload(window);
                },
                (error) => {
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Echec de la modification';
                    this.dialogRef.componentInstance.message = error;
                },
            );
        } else {
            this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
            this.dialogRef.componentInstance.title = 'Echec de la modification';
            this.dialogRef.componentInstance.message = 'Le nom existe déjà. Choississez un autre !';
        }
    }
    getIndexOfTitle(title: string) {
        this.dictionnaryService.getList();
        for (let i = 0; i < this.dictionnaryService.listOfDictionnary.length; i++) {
            if (this.dictionnaryService.listOfDictionnary[i].title === title) {
                return i;
            }
        }
        return ELEMENT_NOT_FOUNDED;
    }
    getDescription(title: string) {
        this.dictionnaryService.getList();
        for (const val of this.dictionnaryService.listOfDictionnary) {
            if (val.title === title) {
                return val.description;
            }
        }
        return '';
    }
    windowReload($window: Window) {
        $window.location.reload();
    }
}
