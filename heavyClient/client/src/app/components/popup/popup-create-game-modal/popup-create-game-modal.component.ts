import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { Router } from '@angular/router';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: './popup-create-game-modal.component.html',
    styleUrls: ['./popup-create-game-modal.component.scss'],
})
export class PopupCreateGameModalComponent {
    title: string = 'Erreur';
    message: string = 'Le dictionnaire choisi ne fait plus partie de la liste';

    constructor(@Inject(MAT_DIALOG_DATA) public dialog: MatDialog, public dialogRef: MatDialogRef<PopupCreateGameModalComponent>) {}
}
