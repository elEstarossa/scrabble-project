import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MuliplayerCreateModalComponent } from '@app/components/modal/muliplayer-create-modal/muliplayer-create-modal.component';
import { PopupDictionnaryModalComponent } from './popup-dictionnary-modal.component';

describe('PopupDictionnaryModalComponent', () => {
    let component: PopupDictionnaryModalComponent;
    let fixture: ComponentFixture<PopupDictionnaryModalComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<MuliplayerCreateModalComponent>>;

    beforeEach(async () => {
        matDialogRefSpyObj = jasmine.createSpyObj('matDialogRefSpyObj', ['close']);

        await TestBed.configureTestingModule({
            declarations: [PopupDictionnaryModalComponent],
            imports: [
                MatDialogModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopupDictionnaryModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
