import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-popup-dictionnary-modal',
    templateUrl: './popup-dictionnary-modal.component.html',
    styleUrls: ['./popup-dictionnary-modal.component.scss'],
})
export class PopupDictionnaryModalComponent {
    title: string = 'Suppresion Reussi';
    message: string = 'Objet enlevé de la liste';

    constructor(@Inject(MAT_DIALOG_DATA) public dialog: MatDialog, public dialogRef: MatDialogRef<PopupDictionnaryModalComponent>) {}
}
