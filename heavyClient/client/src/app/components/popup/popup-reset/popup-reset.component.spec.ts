/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { PopupResetComponent } from './popup-reset.component';

describe('PopupResetComponent', () => {
    let component: PopupResetComponent;
    let fixture: ComponentFixture<PopupResetComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<PopupDictionnaryModalComponent>>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogRefSpyObj = jasmine.createSpyObj('matDialogRefSpyObj', ['close']);
        matDialogRefSpyObj.componentInstance = {
            title: '',
            message: '',
            dialog: {} as MatDialog,
            dialogRef: {} as MatDialogRef<PopupDictionnaryModalComponent>,
        };
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);

        await TestBed.configureTestingModule({
            declarations: [PopupResetComponent],
            imports: [
                MatDialogModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MAT_DIALOG_DATA, useValue: {} },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PopupResetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    /// ////////////////////////////  onSubmit  //////////////////////////////////////
    // it('should onSubmit when the new description is the same as our description ', () => {
    //     const listOfDictionnary: WordDictionnary[] = [
    //         { title: 'myDict', description: 'myDict2', words: [''] },
    //         { title: 'myDict', description: 'myDict2', words: [''] },
    //     ];
    //     component['dictionnaryService'].listOfDictionnary = listOfDictionnary;
    //     const index = 1;

    //     const spy = spyOn(component['dictionnaryService'], 'delete').and.returnValue(Promise.resolve(index));
    //     component.onSubmit();

    //     expect(spy).toHaveBeenCalled();
    // });
});
