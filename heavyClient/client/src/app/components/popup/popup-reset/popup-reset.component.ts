import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PopupDictionnaryModalComponent } from '@app/components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { DictionaryService } from '@app/services/dictionnary.service';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: './popup-reset.component.html',
    styleUrls: ['./popup-reset.component.scss'],
})
export class PopupResetComponent {
    dialogRef: MatDialogRef<PopupDictionnaryModalComponent>;
    constructor(public dialog: MatDialog, private dictionnaryService: DictionaryService) {}
    onSubmit() {
        let counter = 1;
        this.dictionnaryService.getList();
        while (counter < this.dictionnaryService.listOfDictionnary.length) {
            this.dictionnaryService.delete(1).then(
                () => {
                    if ((counter = this.dictionnaryService.listOfDictionnary.length - 1)) {
                        this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                        this.dictionnaryService.getList();
                        this.dialogRef.componentInstance.title = 'Suppression réussie';
                        this.dialogRef.componentInstance.message = 'Le dictionnaire a été supprimé !';
                    }
                },
                (error) => {
                    this.dialogRef = this.dialog.open(PopupDictionnaryModalComponent);
                    this.dialogRef.componentInstance.title = 'Echec de la suppression';
                    this.dialogRef.componentInstance.message = error;
                },
            );
            counter++;
        }
        window.location.reload();
    }
}
