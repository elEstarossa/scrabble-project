import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CommandCode } from '@app/classes/comand-code';
import { Message } from '@app/classes/message';
import { ConvertGameModalComponent } from '@app/components/modal/convert-game-modal/convert-game-modal.component';
import { EndGameModalComponent } from '@app/components/modal/end-game-modal/end-game-modal.component';
import { COMMAND_ECHANGER, COMMAND_PASSER, COMMAND_PLACER, DEFAULT_DICTIONNARY, EMPTY_COMMAND, EMPTY_COMMAND_CODE } from '@app/constants/constants';
import { ChatHandlerService } from '@app/services/chat-handler.service';
import { DictionaryService } from '@app/services/dictionnary.service';
import { EventClickService } from '@app/services/event-click.service';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { ReserveService } from '@app/services/reserve.service';
import { ScoreService } from '@app/services/score.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';
import { VirtualPlayerService } from '@app/services/virtual-player.service';
import { WordsService } from '@app/services/words.service';
@Component({
    selector: 'app-chatbar',
    templateUrl: './chatbar.component.html',
    styleUrls: ['./chatbar.component.scss'],
})
export class ChatbarComponent implements OnInit {
    @ViewChild('scrollbox', { static: false }) scrollbox: ElementRef;
    chatInput: string = '';
    commandTxt: string = '';
    chatMessages: Message[] = [];

    placementValidity: CommandCode = JSON.parse(JSON.stringify(EMPTY_COMMAND_CODE));

    constructor(
        public chatHandler: ChatHandlerService,
        public gameService: GameService,
        private multiplayerService: MultiplayerService,
        public dialog: MatDialog,
        private virtualPlayer: VirtualPlayerService,
        private socketComm: SocketCommunicationService,
        public reserveService: ReserveService,
        public eventClickService: EventClickService,
        public dictionnaryService: DictionaryService,
        public scoreService: ScoreService,
        public wordService: WordsService,
    ) {}

    ngOnInit(): void {
        this.dictionnaryService.dictionnaryOBS.subscribe((res) => {
            setTimeout(() => {
                this.scoreService.dict = res;
                this.wordService.dict = res;
            }, 0);
        });
        this.virtualPlayer.hintOBS.subscribe((res) => {
            setTimeout(() => {
                if (res.length !== 0) {
                    if (res.length < 3)
                        this.chatMessages.push({
                            colorRef: 'black',
                            name: this.gameService.player1.name,
                            dm: 'ATTENTION : il y a moins de 3 possibilités.',
                        });
                    for (const chat of res) {
                        this.chatMessages.push({
                            colorRef: 'yellow',
                            name: this.gameService.player1.name,
                            dm: '**indice : ' + chat,
                        });
                    }
                }
            }, 0);
        });
        this.chatHandler.helpCommandOBS.subscribe((res) => {
            setTimeout(() => {
                if (res) {
                    this.chatMessages.push({
                        colorRef: 'green',
                        name: this.gameService.player1.name,
                        dm: 'AIDE: ',
                    });
                    for (const line of this.chatHandler.helpTab) {
                        this.chatMessages.push({
                            colorRef: 'yellow',
                            name: this.gameService.player1.name,
                            dm: '',
                        });
                        this.chatMessages.push({
                            colorRef: 'yellow',
                            name: this.gameService.player1.name,
                            dm: '*: ' + line,
                        });
                    }

                    this.scrollBar();
                }
            }, 0);
        });
        this.chatHandler.reserveCommandOBS.subscribe((res) => {
            setTimeout(() => {
                if (res) {
                    this.chatMessages.push({
                        colorRef: 'green',
                        name: this.gameService.player1.name,
                        dm: 'RESERVE: ',
                    });
                    for (const letter of this.reserveService.arrayOfReserveLetters) {
                        this.chatMessages.push({
                            colorRef: 'yellow',
                            name: this.gameService.player1.name,
                            dm: '*: ' + letter,
                        });
                    }

                    this.scrollBar();
                }
            }, 0);
        });
        this.virtualPlayer.jvChatOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.chatMessages.push({
                        colorRef: 'opponent',
                        name: this.gameService.player2.name,
                        dm: this.gameService.player2.name + ': ' + res,
                    });
                    this.gameService.alternateTurns();
                    this.scrollBar();
                }
            }, 0);
        });

        this.gameService.passOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.commandTxt = '!passer';
                    this.placementValidity.isValid = true;
                    this.virtualPlayer.play();
                    this.printCommand();
                }
            }, 0);
        });
        this.socketComm.printValidation.subscribe((res) => {
            setTimeout(() => {
                if (res.errorMsg !== 'init') {
                    if (res.errorMsg === '!passer') this.commandTxt = '!passer';
                    this.placementValidity = res;
                    this.printCommand();
                }
            }, 0);
        });
        this.multiplayerService.printValidation.subscribe((res) => {
            setTimeout(() => {
                if (res.errorMsg !== 'init') {
                    if (res.errorMsg === '!passer') this.commandTxt = '!passer';
                    this.placementValidity = res;
                    this.printCommand();
                }
            }, 0);
        });

        this.multiplayerService.endGameObs.subscribe((res) => {
            setTimeout(() => {
                if (res === 'abandon') {
                    this.convertGame();
                } else if (res !== 'init') {
                    this.printEndGame();
                }
            }, 0);
        });
        this.gameService.endGameOBS.subscribe((res) => {
            setTimeout(() => {
                if (res) {
                    this.socketComm.sendScore();
                    this.printEndGame();
                }
            }, 0);
        });
        this.socketComm.chatInputOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.chatMessages.push({
                        colorRef: 'opponent',
                        name: this.gameService.player2.name,
                        dm: this.gameService.player2.name + ': ' + res,
                    });
                    this.scrollBar();
                }
            }, 0);
        });
        this.multiplayerService.chatInputOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.chatMessages.push({
                        colorRef: 'opponent',
                        name: this.gameService.player2.name,
                        dm: this.gameService.player2.name + ': ' + res,
                    });
                    this.scrollBar();
                }
            }, 0);
        });
        this.eventClickService.confirmPlacementOBS.subscribe((res: string) => {
            setTimeout(() => {
                if (res !== 'init') {
                    this.chatInput = res;
                    this.chatActionValidate();
                }
            }, 0);
        });
    }

    convertGame() {
        this.multiplayerService.deleteRoom(this.gameService.roomName);
        this.multiplayerService.createSoloRoom();
        this.dictionnaryService.getDict(DEFAULT_DICTIONNARY);
        this.gameService.player2.name = this.virtualPlayer.generateVirtualPlayerName();
        this.gameService.multiplayerGame = false;
        this.gameService.startTimer();
        if (this.gameService.player2.turnToPlay) this.virtualPlayer.play();
        this.dialog.open(ConvertGameModalComponent);
    }

    printEndGame() {
        this.chatMessages.push(this.gameService.getEndMessage());
        this.dialog.open(EndGameModalComponent);
        this.gameService.isGameOver = true;
        this.scrollBar();
    }

    chatActionValidate() {
        const inputFirstWord = this.chatInput.split(' ')[0];
        const isCommand = inputFirstWord === COMMAND_ECHANGER || inputFirstWord === COMMAND_PASSER || inputFirstWord === COMMAND_PLACER;
        const isGameOver = this.gameService.getIsGameOver();
        if (isCommand && isGameOver) {
            this.chatMessages.push({
                colorRef: 'black',
                name: 'système: ',
                dm: 'vous ne pouvez pas jouer cette partie est terminée!',
            });
        } else {
            this.chatAction();
        }
    }
    chatAction() {
        this.placementValidity = this.chatHandler.chatImputValidation(this.chatInput);
        this.commandTxt = this.chatInput;
        this.multiplayerService.chatInput = this.chatInput;
        this.chatInput = '';

        if (!this.placementValidity.isValid || this.placementValidity.errorMsg === 'change') {
            this.printCommand();
        }
    }
    scrollBar() {
        setTimeout(() => {
            this.scrollbox.nativeElement.scrollTop = this.scrollbox.nativeElement.scrollHeight;
        }, 0);
    }
    printCommand() {
        if (this.placementValidity.isValid) {
            this.chatMessages.push({
                colorRef: 'green',
                name: this.gameService.player1.name,
                dm: this.gameService.player1.name + ': ' + this.commandTxt,
            });
            this.chatHandler.command = JSON.parse(JSON.stringify(EMPTY_COMMAND));
        } else if (this.placementValidity.errorMsg !== '' && this.placementValidity.errorMsg !== 'msg') {
            this.chatMessages.push({
                colorRef: 'red',
                name: this.gameService.player1.name,
                dm: 'ERROR: ' + this.placementValidity.errorMsg,
            });
        } else if (this.placementValidity.errorMsg === 'msg') {
            this.chatMessages.push({
                colorRef: 'black',
                name: this.gameService.player1.name,
                dm: this.gameService.player1.name + ': ' + this.commandTxt,
            });
            this.multiplayerService.sendChat(this.commandTxt);
        }
        this.commandTxt = '';
        this.scrollBar();
    }

    unClick() {
        this.eventClickService.unClick();
    }
}
