/* eslint-disable max-lines */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatHint, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { CommandCode } from '@app/classes/comand-code';
import { Message } from '@app/classes/message';
import { Player } from '@app/classes/player';
import { EMPTY_COMMAND, EMPTY_COMMAND_CODE } from '@app/constants/constants';
import { ChatHandlerService } from '@app/services/chat-handler.service';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';
import { VirtualPlayerService } from '@app/services/virtual-player.service';
import { BehaviorSubject } from 'rxjs';
import { ChatbarComponent } from './chatbar.component';

describe('ChatbarComponent', () => {
    let component: ChatbarComponent;
    let fixture: ComponentFixture<ChatbarComponent>;
    let chatHandlerService: jasmine.SpyObj<ChatHandlerService>;
    let multiplayerService: MultiplayerService;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;
    let gameService: jasmine.SpyObj<GameService>;
    let virtualPlayer: VirtualPlayerService;
    let command: CommandCode;
    let socketComm: SocketCommunicationService;
    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        chatHandlerService = jasmine.createSpyObj('ChatHandlerService', ['chatImputValidation']);
        chatHandlerService.helpCommandOBS = new BehaviorSubject<boolean>(false);
        chatHandlerService.reserveCommandOBS = new BehaviorSubject<boolean>(false);

        multiplayerService = jasmine.createSpyObj('MultiplayerService', ['sendChat', 'createSoloRoom', 'deleteRoom']);
        command = { isValid: false, errorMsg: 'init', score: 0 };
        multiplayerService.printValidation = new BehaviorSubject(command);
        multiplayerService.chatInputOBS = new BehaviorSubject('');
        multiplayerService.endGameObs = new BehaviorSubject('');

        socketComm = jasmine.createSpyObj('SocketCommunicationService', ['sendScore']);
        socketComm.printValidation = new BehaviorSubject(EMPTY_COMMAND_CODE);
        socketComm.chatInputOBS = new BehaviorSubject('');

        virtualPlayer = jasmine.createSpyObj('VirtualPlayerService', ['play', 'generateVirtualPlayerName']);
        virtualPlayer.jvChatOBS = new BehaviorSubject('init');
        virtualPlayer.hintOBS = new BehaviorSubject(['init']);

        gameService = jasmine.createSpyObj('GameService', [
            'getWinnerName',
            'getRoom',
            'getIsGameOver',
            'getEndMessage',
            'sendScore',
            'alternateTurns',
            'startTimer',
        ]);
        gameService.getWinnerName.and.returnValue('egal');
        gameService.player1 = new Player('ali');
        gameService.player2 = new Player('adam');
        gameService.passOBS = new BehaviorSubject('init');
        gameService.endGameOBS = new BehaviorSubject<boolean>(false);

        await TestBed.configureTestingModule({
            declarations: [ChatbarComponent, MatLabel, MatHint, MatInput],
            providers: [
                { provide: ChatHandlerService, useValue: chatHandlerService },
                { provide: MultiplayerService, useValue: multiplayerService },
                { provide: VirtualPlayerService, useValue: virtualPlayer },
                { provide: GameService, useValue: gameService },
                { provide: SocketCommunicationService, useValue: socketComm },
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [RouterTestingModule, MatDialogModule, HttpClientTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// //////////////// NgOnInit ////////////////////////////////////////////////
    it('ngOnInit should call in the beginning when the subscriber is reset', async () => {
        virtualPlayer.jvChatOBS.next('a');
        jasmine.clock().install();
        const spy = spyOn(component, 'scrollBar');
        component.ngOnInit();
        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printCommand with printValidation', async () => {
        command = { isValid: false, errorMsg: '!passer', score: 0 };
        socketComm.printValidation.next(command);
        jasmine.clock().install();
        const spy = spyOn(component, 'printCommand');
        component.ngOnInit();
        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printCommand with printValidation', async () => {
        command = { isValid: false, errorMsg: 'a', score: 0 };
        socketComm.printValidation.next(command);
        jasmine.clock().install();
        const spy = spyOn(component, 'printCommand');
        component.ngOnInit();
        jasmine.clock().tick(1);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printCommand when the turn is passed', async () => {
        gameService.passOBS.next('a');
        jasmine.clock().install();
        const spy = spyOn(component, 'printCommand');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printCommand when the turn is passed', () => {
        command = { isValid: false, errorMsg: 'a', score: 0 };
        multiplayerService.printValidation.next(command);
        jasmine.clock().install();
        const spy = spyOn(component, 'printCommand');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printCommand when the turn is passed', () => {
        command = { isValid: false, errorMsg: 'a', score: 0 };
        multiplayerService.printValidation.next(EMPTY_COMMAND_CODE);
        jasmine.clock().install();
        const spy = spyOn(component, 'printCommand');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should not push when the turn is passed', () => {
        socketComm.chatInputOBS.next('init');
        jasmine.clock().install();
        const spy = spyOn(component.chatMessages, 'push');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should not push when the turn is passed', () => {
        multiplayerService.chatInputOBS.next('init');
        jasmine.clock().install();
        const spy = spyOn(component.chatMessages, 'push');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call convertGame when someone leave the game', () => {
        command = { isValid: false, errorMsg: 'a', score: 0 };
        multiplayerService.endGameObs.next('abandon');
        jasmine.clock().install();
        const spy = spyOn(component, 'convertGame');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should not call convertGame when someone does not leave the game', () => {
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
        command = { isValid: false, errorMsg: 'a', score: 0 };
        multiplayerService.endGameObs.next('init');
        jasmine.clock().install();
        const spy = spyOn(component, 'convertGame');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).not.toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should not call convertGame when someone does not leave the game', () => {
        command = { isValid: false, errorMsg: 'a', score: 0 };
        component.eventClickService.confirmPlacementOBS.next('a');
        jasmine.clock().install();
        const spy = spyOn(component, 'chatActionValidate');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    it('ngOnInit should call printEndGame when someone does not leave the game', () => {
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
        command = { isValid: false, errorMsg: 'a', score: 0 };
        gameService.endGameOBS.next(true);
        jasmine.clock().install();
        const spy = spyOn(component, 'printEndGame');
        component.ngOnInit();
        jasmine.clock().tick(0);
        expect(spy).toHaveBeenCalled();
        jasmine.clock().uninstall();
    });

    /// //////////////// printEndGame ////////////////////////////////////////////////
    it('should printEndGame when he detect three passes', async () => {
        gameService.isThreePasses = true;
        gameService.getWinnerName.and.returnValue('egal');
        const msg = gameService.getEndMessage();
        component.chatMessages.push(msg);
        const spy = spyOn(component.chatMessages, 'push');
        component.printEndGame();
        expect(spy).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(1);
    });
    // /////////////////// chatActionValidate ////////////////////////////////////////////////
    it('should chatActionValidate when the input is the command : echanger  ', async () => {
        component.chatInput = '!echanger abc';
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
        gameService.getIsGameOver.and.returnValue(true);
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'vous ne pouvez pas jouer cette partie est terminée!',
        };
        component.chatMessages.push(msg);
        const spy = spyOn(component.chatMessages, 'push');
        component.chatActionValidate();
        expect(spy).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(1);
    });
    it('should chatActionValidate when the input is the command : passer ', async () => {
        component.chatInput = '!passer';
        gameService.getIsGameOver.and.returnValue(true);
        const msg: Message = {
            colorRef: 'black',
            name: 'système: ',
            dm: 'vous ne pouvez pas jouer cette partie est terminée!',
        };
        component.chatMessages.push(msg);
        const spy = spyOn(component.chatMessages, 'push');
        component.chatActionValidate();
        expect(spy).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(1);
    });
    it('should chatActionValidate when the input is the command wrong ', async () => {
        component.chatInput = '!mami';
        gameService.getIsGameOver.and.returnValue(true);

        const spy = spyOn(component, 'chatAction');
        component.chatActionValidate();
        expect(spy).toHaveBeenCalled();
    });

    // /////////////////// printCommand ////////////////////////////////////////////////
    it('should printCommand when placement validity is true  ', async () => {
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
        const commands = (component.commandTxt = '!passer');
        const msg: Message = {
            colorRef: 'green',
            name: 'ali: ',
            dm: 'ali : ' + commands,
        };
        component.chatMessages.push(msg);
        const spy = spyOn(component.chatMessages, 'push');
        component.printCommand();
        expect(spy).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(1);
        expect(component['chatHandler'].command).toEqual(EMPTY_COMMAND);
    });
    it('should printCommand when placement validity is false  ', async () => {
        component.placementValidity = { isValid: false, errorMsg: '', score: 0 };
        const commands = (component.commandTxt = '!passer');
        const msg: Message = {
            colorRef: 'green',
            name: 'ali: ',
            dm: 'ali : ' + commands,
        };
        component.chatMessages.push(msg);

        component.printCommand();

        expect(component.commandTxt).toEqual('');
    });

    it('should printCommand when placement validity error message is empty or juste "msg" ', async () => {
        component.placementValidity = { isValid: false, errorMsg: 'hey', score: 0 };
        const msg: Message = {
            colorRef: 'red',
            name: 'ali: ',
            dm: 'ERROR : ',
        };
        const spy = spyOn(component.chatMessages, 'push');
        component.chatMessages.push(msg);

        component.printCommand();

        expect(spy).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(0);
    });
    it('should printCommand when placement validity error message is "msg" ', async () => {
        component.placementValidity = { isValid: false, errorMsg: 'msg', score: 0 };
        const msg: Message = {
            colorRef: 'black',
            name: 'ali: ',
            dm: 'ali : ',
        };
        const spy = spyOn(component.chatMessages, 'push');
        component.chatMessages.push(msg);

        component.printCommand();

        expect(spy).toHaveBeenCalled();
        expect(multiplayerService.sendChat).toHaveBeenCalled();
        expect(component.chatMessages.length).toEqual(0);
    });

    // // /////////////////// chatAction ////////////////////////////////////////////////
    it('should printCommand when the boolean isValid is false or the error message is "change" ', () => {
        chatHandlerService.chatImputValidation.and.returnValue({
            isValid: false,
            errorMsg: 'change',
            score: 0,
        });
        component.placementValidity = { isValid: false, errorMsg: 'change', score: 0 };
        const spy = spyOn(component, 'printCommand');

        component.chatAction();
        expect(spy).toHaveBeenCalled();
    });
    it('should printCommand when the boolean isValid is false or the error message is " " ', () => {
        chatHandlerService.chatImputValidation.and.returnValue({
            isValid: true,
            errorMsg: 'change',
            score: 0,
        });
        component.placementValidity = { isValid: true, errorMsg: '', score: 0 };
        const spy = spyOn(component, 'printCommand');

        component.chatAction();
        expect(spy).toHaveBeenCalled();
    });
    it('should printCommand when the boolean isValid is false or the error message is "change" ', () => {
        chatHandlerService.chatImputValidation.and.returnValue({
            isValid: true,
            errorMsg: '',
            score: 0,
        });
        component.placementValidity = { isValid: false, errorMsg: '', score: 0 };

        component.chatAction();
        expect(component.chatInput).toEqual('');
    });

    // // /////////////////// convertGame ////////////////////////////////////////////////
    it('should convertGame when its the turn of the opponent player" ', () => {
        gameService.player2.turnToPlay = true;
        component.convertGame();
        expect(gameService.startTimer).toHaveBeenCalled();
        expect(gameService.multiplayerGame).toBeFalse();
        expect(virtualPlayer.play).toHaveBeenCalled();
    });

    it('should convertGame when its the turn of the creator player" ', () => {
        gameService.player2.turnToPlay = false;
        component.convertGame();
        expect(gameService.startTimer).toHaveBeenCalled();
        expect(gameService.multiplayerGame).toBeFalse();
    });

    // // /////////////////// unClick ////////////////////////////////////////////////
    it('should call the unClick function from the event click service" ', () => {
        const spy = spyOn(component['eventClickService'], 'unClick');

        component.unClick();

        expect(spy).toHaveBeenCalled();
    });
});
