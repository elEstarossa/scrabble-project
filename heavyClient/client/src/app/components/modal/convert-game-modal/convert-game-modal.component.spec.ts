import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConvertGameModalComponent } from './convert-game-modal.component';

describe('ConvertGameModalComponent', () => {
    let component: ConvertGameModalComponent;
    let fixture: ComponentFixture<ConvertGameModalComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ConvertGameModalComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ConvertGameModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
