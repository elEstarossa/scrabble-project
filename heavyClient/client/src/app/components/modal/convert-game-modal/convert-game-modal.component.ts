import { Component } from '@angular/core';

@Component({
    selector: 'app-convert-game-modal',
    templateUrl: './convert-game-modal.component.html',
    styleUrls: ['./convert-game-modal.component.scss'],
})
export class ConvertGameModalComponent {}
