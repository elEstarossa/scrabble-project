import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClassicModeScoreModalComponent } from '@app/components/modal/classic-mode-score-modal/classic-mode-score-modal.component';
import { LogModeScoreModalComponent } from '@app/components/modal/log-mode-score-modal/log-mode-score-modal.component';

@Component({
    selector: 'app-highscore-modal',
    templateUrl: './highscore-modal.component.html',
    styleUrls: ['./highscore-modal.component.scss'],
})
export class HighScoreModalComponent {
    constructor(public dialog: MatDialog) {}
    dispModalClassic() {
        this.dialog.open(ClassicModeScoreModalComponent, { disableClose: true });
    }
    dispModal2990() {
        this.dialog.open(LogModeScoreModalComponent, { disableClose: true });
    }
}
