import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { HighScoreModalComponent } from './highscore-modal.component';

describe('HighScoreModalComponent', () => {
    let component: HighScoreModalComponent;
    let fixture: ComponentFixture<HighScoreModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        await TestBed.configureTestingModule({
            declarations: [HighScoreModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [MatDialogModule, HttpClientTestingModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HighScoreModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display the modal in classic mode', () => {
        component.dispModalClassic();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    it('should display the modal in LOG2990 mode', () => {
        component.dispModal2990();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });
});
