import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GameTime } from '@app/classes/time';
import { PLAYER_NAME_MAX_LENGTH, PLAYER_NAME_MIN_LENGTH, VIRTUAL_PLAYER_NAMES, VIRTUAL_PLAYER_NAMES_EX } from '@app/constants/constants';
import { CommandsService } from '@app/services/commands.service';
import { DictionaryService } from '@app/services/dictionnary.service';
import { GameService } from '@app/services/game.service';
import { ScoreService } from '@app/services/score.service';
import { VirtualPlayerService } from '@app/services/virtual-player.service';
import { WordsService } from '@app/services/words.service';

@Component({
    selector: 'app-singleplayer-create-modal',
    templateUrl: './singleplayer-create-modal.component.html',
    styleUrls: ['./singleplayer-create-modal.component.scss'],
})
export class SingleplayerCreateModalComponent implements OnInit {
    @Input() isDictionnarySelected = false;
    isInputError = false;
    errorMessage = '';
    playerName: string = '';
    isDictionnaryExistant: boolean = false;
    description: string | undefined = '';
    timer: GameTime = { min: 1, sec: 0 };
    isPlayerNameValid = false;
    virtualPlayerName = 'wallE';
    lvls: string[] = ['Expert', 'Débutant'];
    isErrorMessage: string;
    dictionnaryForm: FormGroup;
    animal: string;
    name: string;
    vrLvl: string = 'Débutant';

    constructor(
        private gameService: GameService,
        private router: Router,
        public commandService: CommandsService,
        public dictionnaryService: DictionaryService,
        private dictionnaryFormBuilder: FormBuilder,
        private virtualPlayer: VirtualPlayerService,
        private scoreService: ScoreService,
        private wordService: WordsService,
    ) {
        this.dictionnaryForm = this.dictionnaryFormBuilder.group({ dictionnary: [''] });
    }
    // a check si le async est obligatoire ou a supprimer a cause de lerreur quil genere dans les tests
    async ngOnInit() {
        this.dictionnaryForm.get('dictionnary')?.markAsTouched();
        this.dictionnaryForm.get('dictionnary')?.setValue('default');
        this.description = this.dictionnaryService.listOfDictionnary[0].description;
        this.subscribeDescriptionToDictionnaryName();
        this.isDictionnaryStillAvailable();
        this.dictionnaryService.dictionnaryOBS.subscribe((res) => {
            setTimeout(() => {
                this.scoreService.dict = res;
                this.wordService.dict = res;
            }, 0);
        });
    }

    subscribeDescriptionToDictionnaryName() {
        this.dictionnaryService.getList();
        this.dictionnaryForm.get('dictionnary')?.valueChanges.subscribe((value) => {
            for (const val of this.dictionnaryService.listOfDictionnary) {
                if (value === val.title) {
                    this.description = val.description;
                }
            }
        });
    }

    validatePlayerName(playerName: string): boolean {
        const isValidLength = playerName.length <= PLAYER_NAME_MAX_LENGTH && playerName.length >= PLAYER_NAME_MIN_LENGTH;
        const containsNoSpace = playerName.split(' ').join('') === playerName;
        const isOnlyChars = /^[a-zA-Z]+$/.test(playerName);
        if (!(isValidLength && containsNoSpace && isOnlyChars)) {
            if (playerName.length > PLAYER_NAME_MAX_LENGTH) {
                this.errorMessage = `Votre nom ne doit pas depasser ${PLAYER_NAME_MAX_LENGTH} caractères`;
            } else if (playerName.length < PLAYER_NAME_MIN_LENGTH) {
                this.errorMessage = `Votre nom ne doit avoir au moin ${PLAYER_NAME_MIN_LENGTH} caractères`;
            } else if (!containsNoSpace) {
                this.errorMessage = 'Votre nom ne doit pas contenir des espaces';
            } else {
                this.errorMessage = 'Votre nom doit contenir uniquement des lettres';
            }
            this.isInputError = true;
        } else {
            this.isInputError = false;
        }
        return isValidLength && containsNoSpace && isOnlyChars;
    }
    onPlayerNameChange() {
        this.isPlayerNameValid = true;
        if (!this.validatePlayerName(this.playerName)) {
            this.isPlayerNameValid = false;
        }
        this.virtualPlayerName = this.generateVirtualPlayerName();
    }

    generateVirtualPlayerName() {
        let randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        let names;
        if (!this.virtualPlayer.expert) names = VIRTUAL_PLAYER_NAMES;
        else names = VIRTUAL_PLAYER_NAMES_EX;

        while (VIRTUAL_PLAYER_NAMES[randomNameInex] === this.playerName) {
            randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        }
        return names[randomNameInex];
    }

    startGame() {
        if (this.isDictionnaryStillAvailable()) {
            this.gameService.player1.name = this.playerName;
            this.gameService.player2.name = this.virtualPlayerName;
            this.gameService.multiplayerGame = false;
            this.gameService.gameTimeTurn = this.timer;
            this.router.navigateByUrl('/game');
        }
    }

    change(dictName: string) {
        this.dictionnaryService.getDict(dictName);
        if (dictName) {
            this.isDictionnarySelected = true;
        }
    }
    setLevelJv(event: Event): void {
        if ((event.target as HTMLInputElement)?.value === 'Expert') {
            this.vrLvl = 'Expert';
            this.virtualPlayer.expert = true;
            this.virtualPlayerName = this.generateVirtualPlayerName();
        } else {
            this.vrLvl = 'Débutant';
            this.virtualPlayer.expert = false;
            this.virtualPlayerName = this.generateVirtualPlayerName();
        }
    }
    isDictionnaryStillAvailable(): boolean {
        this.dictionnaryService.getList();
        this.dictionnaryForm.get('dictionnary')?.valueChanges.subscribe((value) => {
            for (const val of this.dictionnaryService.listOfDictionnary) {
                if (value === val.title) {
                    this.isDictionnaryExistant = true;
                }
            }
        });
        return this.isDictionnaryExistant;
    }
}
