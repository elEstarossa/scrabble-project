/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/consistent-type-assertions */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { PLAYER_NAME_MAX_LENGTH, VIRTUAL_PLAYER_NAMES_EX } from '@app/constants/constants';
import { SingleplayerCreateModalComponent } from './singleplayer-create-modal.component';

describe('SingleplayerCreateModalComponent', () => {
    let component: SingleplayerCreateModalComponent;
    let fixture: ComponentFixture<SingleplayerCreateModalComponent>;
    let router: Router;
    const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
    const dictionnaryFormBuilder: FormBuilder = new FormBuilder();
    let dictionnaryForm: FormGroup;

    beforeEach(async () => {
        dictionnaryForm = dictionnaryFormBuilder.group({ dictionnary: [''] });

        await TestBed.configureTestingModule({
            declarations: [SingleplayerCreateModalComponent],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: FormGroup, useValue: dictionnaryForm },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [
                HttpClientTestingModule,
                MatSnackBarModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        router = TestBed.inject(Router);
        fixture = TestBed.createComponent(SingleplayerCreateModalComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// //////////////////////////  validatePlayerName  /////////////////////////
    it('validatePlayerName should return false if the username is too short', () => {
        const name = 'a';
        expect(component.validatePlayerName(name)).toBeFalsy();
    });

    it('validatePlayerName should return false if the username contains spaces', () => {
        const name = 'aaaa b';
        expect(component.validatePlayerName(name)).toBeFalsy();
    });

    it('validatePlayerName should return false if the username contains numbers', () => {
        const name = 'aaaaa7';
        expect(component.validatePlayerName(name)).toBeFalsy();
    });

    it('validatePlayerName should return false if the username contains numbers', () => {
        const errorMessage = `Votre nom ne doit pas depasser ${PLAYER_NAME_MAX_LENGTH} caractères`;

        const name = 'aldricaldricaldricaldric';
        component.validatePlayerName(name);
        expect(component.errorMessage).toEqual(errorMessage);
    });
    /// //////////////////////////  OnPlayerNameChange()  /////////////////////////
    it('OnPlayerNameChangeshould should change isPlayerNameValid to false if the name is not valid', () => {
        component.isPlayerNameValid = true;
        component.playerName = 'a';
        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeFalsy();
    });

    it('OnPlayerNameChangeshould should change isPlayerNameValid to true if the name is valid', () => {
        component.isPlayerNameValid = false;
        component.playerName = 'aaaaa';
        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeTruthy();
    });

    /// //////////////////////////  StartGame  /////////////////////////
    it('StartGame should send the user in game', () => {
        component.isDictionnaryExistant = true;
        const spy = router.navigateByUrl;
        component.startGame();
        expect(spy).toHaveBeenCalled();
    });

    it('StartGame should not start if the dictionnary is not available', () => {
        component.isDictionnaryExistant = false;
        component.startGame();
        expect(component['gameService'].multiplayerGame).toBeFalse();
    });

    /// //////////////////////////  generateVirtualPlayerName  /////////////////////////
    it('generateVirtualPlayerName should generate randomly the virtual player name if its the virtual player expert', () => {
        component['virtualPlayer'].expert = false;
        spyOn(Math, 'random').and.returnValue(0);
        expect(component.generateVirtualPlayerName()).toEqual('Bender');
    });

    it('generateVirtualPlayerName should generate randomly the virtual player name if its not the virtual player expert', () => {
        component['virtualPlayer'].expert = true;
        const names = ['BESAF', 'KBE7', 'WA3AR'];
        component.generateVirtualPlayerName();
        expect(names).toEqual(VIRTUAL_PLAYER_NAMES_EX);
    });

    /// //////////////////////////  setLevelJv  /////////////////////////
    it('setLevelJv should set the level on expert ', () => {
        const event: Event = <Event>(<unknown>{
            target: {
                value: 'Expert',
            },
        });
        component.setLevelJv(event);
        expect(component['virtualPlayer'].expert).toBeTrue();
    });

    it('setLevelJv should set the level on beginner', () => {
        const event: Event = <Event>(<unknown>{
            target: {
                value: 'Beginner',
            },
        });
        component.setLevelJv(event);
        expect(component['virtualPlayer'].expert).toBeFalse();
    });

    /// //////////////////////////  subscribeDescriptionToDictionnaryName  /////////////////////////
    it('subscribeDescriptionToDictionnaryName ', () => {
        const spy = spyOn(component['dictionnaryService'], 'getList');
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.subscribeDescriptionToDictionnaryName();
        component.dictionnaryForm.get('dictionnary')?.setValue('default');
        expect(spy).toHaveBeenCalled();
    });

    it('subscribeDescriptionToDictionnaryName -> description should not be assigned if the dictionnary is not there', () => {
        const spy = spyOn(component['dictionnaryService'], 'getList');
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: 'alo', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.subscribeDescriptionToDictionnaryName();
        component.dictionnaryForm.get('dictionnary')?.setValue('b');
        expect(spy).toHaveBeenCalled();
        expect(component.description).not.toEqual('alo');
    });

    /// //////////////////////////  change  /////////////////////////
    it('change if the dictionnary name is valid ', () => {
        const listOfDictionnary: WordDictionnary[] = [{ title: 'dictionnary', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        const dictName = 'dictionnary';
        const spy2 = spyOn(component['dictionnaryService'], 'getDict');

        component.change(dictName);
        expect(spy2).toHaveBeenCalled();
        expect(component.isDictionnarySelected).toBeTrue();
    });

    it('change if the dictionnary name is valid ', () => {
        const dictName = '';
        const spy2 = spyOn(component['dictionnaryService'], 'getDict');

        component.change(dictName);
        expect(spy2).toHaveBeenCalled();
        expect(component.isDictionnarySelected).toBeFalse();
    });

    /// //////////////////////////  isDictionnaryStillAvailable  /////////////////////////
    it('isDictionnaryStillAvailable should assign dictionnary existent if the dictionnary is there', () => {
        const spy2 = spyOn(component['dictionnaryService'], 'getList');
        component.isDictionnaryExistant = false;
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.isDictionnaryStillAvailable();
        component.dictionnaryForm.get('dictionnary')?.setValue('default');
        expect(spy2).toHaveBeenCalled();
        expect(component.isDictionnaryExistant).toBeTrue();
    });

    it('isDictionnaryStillAvailable should not assign dictionnary existent if the dictionnary is not there ', () => {
        const spy2 = spyOn(component['dictionnaryService'], 'getList');
        component.isDictionnaryExistant = false;
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.isDictionnaryStillAvailable();
        component.dictionnaryForm.get('dictionnary')?.setValue('alo');
        expect(spy2).toHaveBeenCalled();
        expect(component.isDictionnaryExistant).toBeFalse();
    });

    /// //////////////////////////  ngOnInit  /////////////////////////
    it('ngOnInit ', async () => {
        const listOfDictionnary: WordDictionnary[] = [{ title: 'dictionnary', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        const spy2 = spyOn(component.dictionnaryForm, 'get');
        const spy1 = spyOn(component, 'subscribeDescriptionToDictionnaryName');
        const spy3 = spyOn(component, 'isDictionnaryStillAvailable');

        component.ngOnInit();
        expect(spy2).toHaveBeenCalled();
        expect(spy1).toHaveBeenCalled();
        expect(spy3).toHaveBeenCalled();
    });
});
