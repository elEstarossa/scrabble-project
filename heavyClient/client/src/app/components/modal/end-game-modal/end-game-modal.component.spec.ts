import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogActions, MatDialogContent } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EndGameModalComponent } from './end-game-modal.component';

describe('EndGameModalComponent', () => {
    let component: EndGameModalComponent;
    let fixture: ComponentFixture<EndGameModalComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EndGameModalComponent, MatDialogActions, MatDialogContent],
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule],

            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EndGameModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
