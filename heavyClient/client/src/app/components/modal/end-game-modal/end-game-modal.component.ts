import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { BOARD_WIDTH, EASEL_LENGTH, NB_TILES } from '@app/constants/constants';
import { EndGameModalService } from '@app/services/end-game-modal.service';
import { GameService } from '@app/services/game.service';

@Component({
    selector: 'app-end-game-modal',
    templateUrl: './end-game-modal.component.html',
    styleUrls: ['./end-game-modal.component.scss'],
})
export class EndGameModalComponent implements AfterViewInit {
    @ViewChild('easelOne', { static: false }) private easelOne!: ElementRef<HTMLCanvasElement>;
    @ViewChild('easelTwo', { static: false }) private easelTwo!: ElementRef<HTMLCanvasElement>;
    private canvasSize = { x: EASEL_LENGTH * (BOARD_WIDTH / NB_TILES), y: BOARD_WIDTH / NB_TILES };
    constructor(private endGameService: EndGameModalService, public gameService: GameService) {}

    ngAfterViewInit(): void {
        this.endGameService.setCanvasElements(this.easelOne, this.easelTwo);
        this.endGameService.drawEasel(this.gameService.getCreator().easel, this.endGameService.easelOneCtx);

        this.endGameService.drawEasel(this.gameService.getJoiner().easel, this.endGameService.easelTwoCtx);

        this.endGameService.drawHands();
    }
    get width(): number {
        return this.canvasSize.x;
    }

    get height(): number {
        return this.canvasSize.y;
    }
}
