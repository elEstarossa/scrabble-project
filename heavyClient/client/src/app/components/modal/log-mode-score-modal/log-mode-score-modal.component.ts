import { Component, OnInit } from '@angular/core';
import { Scores } from '@app/classes/scores';
import { MultiplayerService } from '@app/services/multiplayer.service';

@Component({
    selector: 'app-log-mode-score-modal',
    templateUrl: './log-mode-score-modal.component.html',
    styleUrls: ['./log-mode-score-modal.component.scss'],
})
export class LogModeScoreModalComponent implements OnInit {
    listOfScores2990: Scores[] = [];
    displayedColumns: string[] = ['name', 'score'];
    constructor(private multiplayerService: MultiplayerService) {}
    async ngOnInit(): Promise<void> {
        this.addValueToStandings();
    }
    async addValueToStandings() {
        this.multiplayerService.listenShowScores2990();
        this.multiplayerService.listOfScores2990OBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== []) {
                    this.listOfScores2990 = res;
                }
            }, 0);
        });
    }
}
