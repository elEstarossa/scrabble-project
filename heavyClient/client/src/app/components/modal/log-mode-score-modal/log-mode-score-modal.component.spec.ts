/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Scores } from '@app/classes/scores';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { BehaviorSubject } from 'rxjs';
import { LogModeScoreModalComponent } from './log-mode-score-modal.component';

describe('LogModeScore', () => {
    let component: LogModeScoreModalComponent;
    let fixture: ComponentFixture<LogModeScoreModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;
    let multiplayerService: MultiplayerService;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        multiplayerService = jasmine.createSpyObj('MultiplayerService', ['listenShowScores2990']);
        // multiplayerService.listOfScores2990OBS = new BehaviorSubject(TABLEOFSCORE2990);
        await TestBed.configureTestingModule({
            declarations: [LogModeScoreModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MultiplayerService, useValue: multiplayerService },
            ],
            imports: [MatDialogModule, HttpClientTestingModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LogModeScoreModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should add', () => {
        const data: Scores = {
            name: '',
            score: 1,
        };
        jasmine.clock().install();
        multiplayerService.listOfScores2990OBS = new BehaviorSubject([data]);
        component.addValueToStandings();
        jasmine.clock().tick(0);
        jasmine.clock().uninstall();

        expect(multiplayerService.listenShowScores2990).toHaveBeenCalled();
    });
});
