/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { AppMaterialModule } from '@app/modules/material.module';
import { MainPageLog2990ModalComponent } from './main-page-log2990-modal.component';

describe('MainPageLog2990ModalComponent', () => {
    let component: MainPageLog2990ModalComponent;
    let fixture: ComponentFixture<MainPageLog2990ModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);

        await TestBed.configureTestingModule({
            declarations: [MainPageLog2990ModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [MatDialogModule, AppMaterialModule, HttpClientTestingModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MainPageLog2990ModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('dispMultiplayerModal should open the multiplayer modal', () => {
        component.dispMultiplayerModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    it('cancelObjectiveMode should put the mode activated in false', () => {
        component.cancelObjectiveMode();
        expect(component['gameService'].log2990).toBeFalse();
    });

    it('soloGame should call set the players name and open the single player modal', () => {
        component.soloGame();
        expect(component['gameService'].player2.name).toEqual('');
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });
});
