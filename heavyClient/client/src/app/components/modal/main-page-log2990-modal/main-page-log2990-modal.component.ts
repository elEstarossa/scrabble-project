import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MultiplayerModalComponent } from '@app/components/modal/multiplayer-modal/multiplayer-modal.component';
import { SingleplayerCreateModalComponent } from '@app/components/modal/singleplayer-create-modal/singleplayer-create-modal.component';
import { ONE_MINUT_TIMER } from '@app/constants/constants';
import { GameService } from '@app/services/game.service';

@Component({
    selector: 'app-main-page-log2990-modal',
    templateUrl: './main-page-log2990-modal.component.html',
    styleUrls: ['./main-page-log2990-modal.component.scss'],
})
export class MainPageLog2990ModalComponent {
    constructor(public dialog: MatDialog, private gameService: GameService) {}

    dispMultiplayerModal() {
        this.dialog.open(MultiplayerModalComponent, { disableClose: true });
    }

    cancelObjectiveMode() {
        this.gameService.log2990 = false;
    }

    soloGame() {
        this.gameService.player2.name = '';
        this.gameService.player1.name = '';
        this.gameService.gameTimeTurn = ONE_MINUT_TIMER;
        this.gameService.multiplayerGame = false;
        this.gameService.player1.creator = true;
        this.gameService.player2.creator = false;
        this.dialog.open(SingleplayerCreateModalComponent, { disableClose: true });
    }
}
