import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MultiplayerModalComponent } from '@app/components/modal/multiplayer-modal/multiplayer-modal.component';
import { SingleplayerCreateModalComponent } from '@app/components/modal/singleplayer-create-modal/singleplayer-create-modal.component';
import { ONE_MINUT_TIMER } from '@app/constants/constants';
import { GameService } from '@app/services/game.service';

@Component({
    selector: 'app-modal',
    templateUrl: './main-page-modal.component.html',
    styleUrls: ['./main-page-modal.component.scss'],
})
export class MainPageModalComponent {
    constructor(public dialog: MatDialog, private gameService: GameService) {}
    dispMultiplayerModal() {
        this.dialog.open(MultiplayerModalComponent, { disableClose: true });
    }
    soloGame() {
        this.gameService.player2.name = 'robot';
        this.gameService.player1.name = 'mounib';
        this.gameService.gameTimeTurn = ONE_MINUT_TIMER;
        this.gameService.multiplayerGame = false;
        this.gameService.player1.creator = true;
        this.gameService.player2.creator = false;
        this.dialog.open(SingleplayerCreateModalComponent, { disableClose: true });
    }
}
