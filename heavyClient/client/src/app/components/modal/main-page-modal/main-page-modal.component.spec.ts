import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MainPageModalComponent } from './main-page-modal.component';

describe('ModalComponent', () => {
    let component: MainPageModalComponent;
    let fixture: ComponentFixture<MainPageModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);

        await TestBed.configureTestingModule({
            declarations: [MainPageModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],

            imports: [MatDialogModule, HttpClientTestingModule, MatSnackBarModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MainPageModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('dispMultiplayerModal should open the multiplayer modal component', () => {
        component.dispMultiplayerModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    it('should call soloGame', () => {
        component.soloGame();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });
});
