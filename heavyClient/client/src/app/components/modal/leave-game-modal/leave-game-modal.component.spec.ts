/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogActions } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { LeaveGameModalComponent } from './leave-game-modal.component';

describe('LeaveGameModalComponent', () => {
    let component: LeaveGameModalComponent;
    let fixture: ComponentFixture<LeaveGameModalComponent>;
    let routerSpyObj: jasmine.SpyObj<Router>;
    let gameServiceSpyObj: jasmine.SpyObj<GameService>;
    let multiplayerService: jasmine.SpyObj<MultiplayerService>;

    beforeEach(async () => {
        routerSpyObj = jasmine.createSpyObj('Router', ['navigateByUrl']);
        multiplayerService = jasmine.createSpyObj('MultiplayerService', ['sendAbandonGame']);
        gameServiceSpyObj = jasmine.createSpyObj('GameService', ['resetModals']);

        await TestBed.configureTestingModule({
            declarations: [LeaveGameModalComponent, MatDialogActions],
            providers: [
                { provide: Router, useValue: routerSpyObj },
                { provide: GameService, useValue: gameServiceSpyObj },
                { provide: MultiplayerService, useValue: multiplayerService },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [RouterTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LeaveGameModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should abandonGame', async () => {
        const spy = spyOn(component, 'windowReload');
        await component.abandonGame();
        expect(spy).toHaveBeenCalled();
    });

    it('should windowReload', async () => {
        const $window = jasmine.createSpyObj('Window', ['location']);
        $window.location = jasmine.createSpyObj('Location', ['reload']);
        component.windowReload($window);
        expect($window.location.reload).toHaveBeenCalled();
    });
});
