import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MultiplayerService } from '@app/services/multiplayer.service';

@Component({
    selector: 'app-leave-game-modal',
    templateUrl: './leave-game-modal.component.html',
    styleUrls: ['./leave-game-modal.component.scss'],
})
export class LeaveGameModalComponent {
    constructor(private multiplayerService: MultiplayerService, private router: Router) {}
    async abandonGame() {
        this.multiplayerService.sendAbandonGame();
        await this.router.navigateByUrl('/home');
        this.windowReload(window);
    }

    windowReload($window: Window) {
        $window.location.reload();
    }
}
