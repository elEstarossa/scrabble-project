import { APP_BASE_HREF } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { AppMaterialModule } from '@app/modules/material.module';
import { MultiplayerModalComponent } from './multiplayer-modal.component';

describe('MultiplayerModalComponent', () => {
    let component: MultiplayerModalComponent;
    let fixture: ComponentFixture<MultiplayerModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        await TestBed.configureTestingModule({
            declarations: [MultiplayerModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [MatDialogModule, AppMaterialModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MultiplayerModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('dispMultiplayerCreateModal should open the multiplayer create game modal', () => {
        component.dispMultiplayerCreateModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });

    it('dispMultiplayerCreateModal should open the multiplayer join game modal', () => {
        component.dispMultiplayerJoinModal();
        expect(matDialogSpyObj.open).toHaveBeenCalled();
    });
});
