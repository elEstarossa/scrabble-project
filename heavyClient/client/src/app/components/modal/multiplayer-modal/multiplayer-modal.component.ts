import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MuliplayerCreateModalComponent } from '@app/components/modal/muliplayer-create-modal/muliplayer-create-modal.component';
import { MuliplayerJoinModalComponent } from '@app/components/modal/muliplayer-join-modal/muliplayer-join-modal.component';

@Component({
    selector: 'app-multiplayer-modal',
    templateUrl: './multiplayer-modal.component.html',
    styleUrls: ['./multiplayer-modal.component.scss'],
})
export class MultiplayerModalComponent {
    constructor(public dialog: MatDialog) {}

    dispMultiplayerCreateModal() {
        this.dialog.open(MuliplayerCreateModalComponent, { disableClose: true });
    }

    dispMultiplayerJoinModal() {
        this.dialog.open(MuliplayerJoinModalComponent, { disableClose: true });
    }
}
