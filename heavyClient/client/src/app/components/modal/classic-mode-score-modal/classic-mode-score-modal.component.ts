import { Component, OnInit } from '@angular/core';
import { Scores } from '@app/classes/scores';
import { MultiplayerService } from '@app/services/multiplayer.service';

@Component({
    selector: 'app-classic-mode-score-modal',
    templateUrl: './classic-mode-score-modal.component.html',
    styleUrls: ['./classic-mode-score-modal.component.scss'],
})
export class ClassicModeScoreModalComponent implements OnInit {
    listOfScoresClassic: Scores[] = [];
    displayedColumns: string[] = ['name', 'score'];
    constructor(private multiplayerService: MultiplayerService) {}
    async ngOnInit(): Promise<void> {
        this.addValueToStandings();
    }
    async addValueToStandings() {
        this.multiplayerService.listenShowScoresClassic();
        this.multiplayerService.listOfScoresClassicOBS.subscribe((res) => {
            setTimeout(() => {
                if (res !== []) {
                    this.listOfScoresClassic = res;
                }
            }, 0);
        });
    }
}
