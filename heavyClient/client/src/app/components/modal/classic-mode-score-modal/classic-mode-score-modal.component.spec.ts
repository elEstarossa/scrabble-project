/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Scores } from '@app/classes/scores';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { BehaviorSubject } from 'rxjs';
import { ClassicModeScoreModalComponent } from './classic-mode-score-modal.component';

describe('ClassicModeScoreModalComponent', () => {
    let component: ClassicModeScoreModalComponent;
    let fixture: ComponentFixture<ClassicModeScoreModalComponent>;
    let matDialogSpyObj: jasmine.SpyObj<MatDialog>;
    let multiplayerService: MultiplayerService;

    beforeEach(async () => {
        matDialogSpyObj = jasmine.createSpyObj('MatDialog', ['open']);
        multiplayerService = jasmine.createSpyObj('MultiplayerService', ['listenShowScoresClassic']);
        // multiplayerService.listOfScoresClassicOBS = new BehaviorSubject(TABLEOFSCORECLASSIC);
        await TestBed.configureTestingModule({
            declarations: [ClassicModeScoreModalComponent],
            providers: [
                { provide: MatDialog, useValue: matDialogSpyObj },
                { provide: MultiplayerService, useValue: multiplayerService },
            ],
            imports: [MatDialogModule, HttpClientTestingModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ClassicModeScoreModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should addValueToStandings on calling the listener to show the classical scores', () => {
        const data: Scores = {
            name: '',
            score: 1,
        };
        jasmine.clock().install();
        multiplayerService.listOfScoresClassicOBS = new BehaviorSubject([data]);
        component.addValueToStandings();
        jasmine.clock().tick(0);
        jasmine.clock().uninstall();

        expect(multiplayerService.listenShowScoresClassic).toHaveBeenCalled();
    });
});
