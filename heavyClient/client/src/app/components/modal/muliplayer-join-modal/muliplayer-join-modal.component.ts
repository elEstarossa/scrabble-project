import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { PLAYER_NAME_MAX_LENGTH, PLAYER_NAME_MIN_LENGTH, UNDEFINED_INDEX, VALIDATION_AWAIT } from '@app/constants/constants';
import { DictionaryService } from '@app/services/dictionnary.service';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { SocketCommunicationService } from '@app/services/socket-communication.service';

@Component({
    selector: 'app-muliplayer-join-modal',
    templateUrl: './muliplayer-join-modal.component.html',
    styleUrls: ['./muliplayer-join-modal.component.scss'],
})
export class MuliplayerJoinModalComponent implements OnInit {
    playerName = '';
    creatorName = '';
    roomName = '';

    isPlayerNameValid = false;
    isSamePlayerName = false;
    roomChosen = false;
    isWaitingAccept = false;
    roomNames: string[] = [];
    creatorNames: string[] = [];
    isInputError = false;
    errorMessage = '';

    constructor(
        private router: Router,
        private gameService: GameService,
        private multiplayerService: MultiplayerService,
        private dialogRef: MatDialogRef<MuliplayerJoinModalComponent>,
        private socketComm: SocketCommunicationService,
        private snackBar: MatSnackBar,
        public dictionnaryService: DictionaryService, // private scoreService: ScoreService, // private wordService: WordsService,
    ) {}

    ngOnInit(): void {
        this.gameService.multiplayerGame = true;
        this.socketComm.getWhoStarts();
        this.multiplayerService.getAllRoomsWithDetails();
        this.multiplayerService.listenListRoomsWithDetailsResult();
        this.multiplayerService.roomListOBS.subscribe((response) => {
            setTimeout(() => {
                if (response !== [[''], ['']]) {
                    if (this.gameService.log2990) {
                        this.roomNames = response[3];
                        this.creatorNames = response[4];
                    } else {
                        this.roomNames = response[0];
                        this.creatorNames = response[1];
                    }
                }
            }, 0);
        });
        this.multiplayerService.rejectedOBS.subscribe((res) => {
            setTimeout(() => {
                if (res === 'did') {
                    this.playerName = '';
                    this.creatorName = '';
                    this.roomName = '';
                    this.isPlayerNameValid = false;
                    this.isSamePlayerName = false;
                    this.roomChosen = false;
                    this.isWaitingAccept = false;
                    this.isWaitingAccept = false;
                    this.snackBar.open('Cette adversaire ne veut pas jouer avec vous :(', 'fermer');
                    setTimeout(() => {
                        this.snackBar.dismiss();
                    }, VALIDATION_AWAIT);
                } else if (res === 'not') {
                    this.isWaitingAccept = false;
                    this.gameService.player1.name = this.playerName;
                    this.gameService.player1.creator = false;
                    this.gameService.player2.name = this.creatorName;
                    this.gameService.player2.creator = true;
                    this.router.navigateByUrl('/game');
                    this.dialogRef.close();
                }
            }, 0);
        });
        this.multiplayerService.roomIsFullOBS.subscribe((res) => {
            setTimeout(() => {
                if (res === 'roomIsFull') {
                    this.isWaitingAccept = false;
                    this.snackBar.open("Cette partie n'est plus disponible!", 'fermer');
                    setTimeout(() => {
                        this.snackBar.dismiss();
                    }, VALIDATION_AWAIT);
                }
            }, 0);
        });
    }
    onPlayerNameChange() {
        this.isPlayerNameValid = true;
        this.isSamePlayerName = false;
        if (!this.validatePlayerName(this.playerName)) {
            this.isPlayerNameValid = false;
        }
        if (this.creatorName === this.playerName) {
            this.isSamePlayerName = true;
        }
    }
    cancelJoin() {
        if (this.isWaitingAccept) this.multiplayerService.cancelJoin(this.roomName);
        this.isWaitingAccept = false;
    }
    onSelectRoom() {
        this.isSamePlayerName = false;
        this.gameService.roomName = this.roomName;
        this.creatorName = this.creatorNames[this.getCreatorIndex(this.roomName)];

        if (this.creatorName === this.playerName) {
            this.isSamePlayerName = true;
        }
        this.roomChosen = true;
    }

    selectRandomRoom() {
        this.roomName = this.roomNames[Math.floor(Math.random() * this.roomNames.length)];
        this.isSamePlayerName = false;
        this.gameService.roomName = this.roomName;

        this.creatorName = this.creatorNames[this.getCreatorIndex(this.roomName)];

        if (this.creatorName === this.playerName) {
            this.isSamePlayerName = true;
        }
        this.roomChosen = true;
    }

    getCreatorIndex(roomName: string): number {
        let counter = 0;
        for (const name of this.roomNames) {
            if (name === roomName) return counter;
            counter++;
        }
        return UNDEFINED_INDEX;
    }
    validatePlayerName(playerName: string): boolean {
        const isValidLength = playerName.length <= PLAYER_NAME_MAX_LENGTH && playerName.length >= PLAYER_NAME_MIN_LENGTH;
        const containsNoSpace = playerName.split(' ').join('') === playerName;
        const isOnlyChars = /^[a-zA-Z]+$/.test(playerName);
        if (!(isValidLength && containsNoSpace && isOnlyChars)) {
            if (playerName.length > PLAYER_NAME_MAX_LENGTH) {
                this.errorMessage = `Votre nom ne doit pas depasser ${PLAYER_NAME_MAX_LENGTH} caractères`;
            } else if (playerName.length < PLAYER_NAME_MIN_LENGTH) {
                this.errorMessage = `Votre nom ne doit avoir au moin ${PLAYER_NAME_MIN_LENGTH} caractères`;
            } else if (!containsNoSpace) {
                this.errorMessage = 'Votre nom ne doit pas contenir des espaces';
            } else {
                this.errorMessage = 'Votre nom doit contenir uniquement des lettres';
            }
            this.isInputError = true;
        } else {
            this.isInputError = false;
        }
        return isValidLength && containsNoSpace && isOnlyChars;
    }

    startGame() {
        this.isWaitingAccept = true;
        localStorage.setItem('roomName', JSON.stringify(this.roomName));
        localStorage.setItem('joiner', JSON.stringify(true));
        this.multiplayerService.joinRoom(this.roomName, this.playerName);
        for (const room of this.roomNames) {
            if (room === this.roomName) {
                const index = this.roomNames.indexOf(room);
                this.roomNames.slice(index, 1);
            }
        }
        this.multiplayerService.listenStartGameAccepted();
        this.multiplayerService.listenStartGameRejected();
    }
}
