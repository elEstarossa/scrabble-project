/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { Letter } from '@app/classes/letter';
import { MessageServer } from '@app/classes/message-server';
import { Player } from '@app/classes/player';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { ReserveService } from '@app/services/reserve.service';
import { BehaviorSubject } from 'rxjs';
import { MuliplayerJoinModalComponent } from './muliplayer-join-modal.component';

describe('MuliplayerJoinModalComponent', () => {
    let component: MuliplayerJoinModalComponent;
    let fixture: ComponentFixture<MuliplayerJoinModalComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<MuliplayerJoinModalComponent>>;
    let router: jasmine.SpyObj<Router>;
    let reserveService: jasmine.SpyObj<ReserveService>;
    let multiplayerServiceSpy: jasmine.SpyObj<MultiplayerService>;
    let gameService: jasmine.SpyObj<GameService>;
    let message: MessageServer;

    beforeEach(async () => {
        matDialogRefSpyObj = jasmine.createSpyObj('MatDialogRef', ['close']);

        reserveService = jasmine.createSpyObj('ReserveService', ['']);
        reserveService.letters = new Map<Letter, number>();
        reserveService.sizeObs = new BehaviorSubject(0);

        message = { timer: { min: 0, sec: 0 } };

        gameService = jasmine.createSpyObj('GameService', ['setRoom']);
        gameService.player1 = new Player('Gilles');
        gameService.player2 = new Player('Abdou');
        gameService.gameTimeTurn = message.timer ?? { min: 0, sec: 0 };
        gameService.roomName = 'room1';

        multiplayerServiceSpy = jasmine.createSpyObj('MultiplayerService', [
            'listenStartGame',
            'getAllRoomsWithDetails',
            'listenListRoomsWithDetailsResult',
            'rejectJoinRoom',
            'createRoom',
            'acceptJoinRoom',
            'listenStartGameAccepted',
            'listenStartGameRejected',
            'joinRoom',
            'cancelJoin',
        ]);
        multiplayerServiceSpy.roomListOBS = new BehaviorSubject([[''], ['']]);
        multiplayerServiceSpy.rejectedOBS = new BehaviorSubject('dani');
        multiplayerServiceSpy.roomIsFullOBS = new BehaviorSubject('init');

        router = jasmine.createSpyObj('Router', ['navigateByUrl']);

        await TestBed.configureTestingModule({
            declarations: [MuliplayerJoinModalComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
                { provide: ReserveService, useValue: reserveService },
                { provide: MultiplayerService, useValue: multiplayerServiceSpy },
                { provide: Router, useValue: router },
                { provide: GameService, useValue: gameService },
                { provide: APP_BASE_HREF, useValue: {} },
            ],
            imports: [MatDialogModule, HttpClientTestingModule, MatSnackBarModule, BrowserAnimationsModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MuliplayerJoinModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /// ///////////////////////////////////////// ngOnInit ///////////////////////
    it('should ngOnInit in the beginning', async () => {
        jasmine.clock().install();
        component.playerName = '';
        component.creatorName = '';
        component.roomName = '';
        component.isPlayerNameValid = false;
        component.isSamePlayerName = false;
        component.roomChosen = false;
        component.isWaitingAccept = false;
        component['multiplayerService'].rejectedOBS = new BehaviorSubject('did');
        component.ngOnInit();
        expect(multiplayerServiceSpy.getAllRoomsWithDetails).toHaveBeenCalled();

        jasmine.clock().tick(0);
        jasmine.clock().uninstall();
    });

    it('should ngOnInit in the beginning', async () => {
        jasmine.clock().install();
        component.playerName = '';
        component['multiplayerService'].rejectedOBS = new BehaviorSubject('not');
        component.ngOnInit();
        expect(multiplayerServiceSpy.getAllRoomsWithDetails).toHaveBeenCalled();

        jasmine.clock().tick(0);
        jasmine.clock().uninstall();
    });

    it('should ngOnInit in the beginning', async () => {
        jasmine.clock().install();
        component.playerName = '';
        component['multiplayerService'].roomIsFullOBS = new BehaviorSubject('roomIsFull');
        component.ngOnInit();
        expect(multiplayerServiceSpy.getAllRoomsWithDetails).toHaveBeenCalled();
        expect(component.isWaitingAccept).toBeFalse();

        jasmine.clock().tick(0);
        jasmine.clock().uninstall();
    });
    /// ///////////////////////////////////////// onPlayerNameChange ///////////////////////
    it('should onPlayerNameChange when the player name is valid', () => {
        component.isPlayerNameValid = true;
        component.isPlayerNameValid = false;
        spyOn(component, 'validatePlayerName').and.returnValue(false);

        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeFalse();
    });

    it('should onPlayerNameChange when the player name is valid', () => {
        component.isPlayerNameValid = true;
        component.isPlayerNameValid = false;
        spyOn(component, 'validatePlayerName').and.returnValue(true);
        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeTrue();
    });

    it('should onPlayerNameChange when the player name is the same as the name and creator in the room', () => {
        component.isPlayerNameValid = true;
        component.playerName = 'dani';
        component.validatePlayerName('dani');

        component.onPlayerNameChange();
        expect(component.isSamePlayerName).toBeFalse();
    });

    // /// ///////////////////////////////////////// onSelectRoom ///////////////////////
    it('should onSelectRoom when the player name is the same as the creator and both are in the same room', () => {
        component.creatorNames = ['Gilles'];
        component.roomNames = [''];
        const spy1 = spyOn(component, 'getCreatorIndex');

        component.onSelectRoom();
        expect(component.isSamePlayerName).toBeFalse();
        expect(spy1).toHaveBeenCalled();
    });

    it('should put the boolean isSamePlayerName on true when the players have the same name', () => {
        component.creatorNames = ['Gilles'];
        component.roomNames = [''];
        const spy1 = spyOn(component, 'getCreatorIndex').and.returnValue(0);
        component.playerName = gameService.player1.name;

        component.onSelectRoom();
        expect(component.creatorName).toEqual(component.playerName);
        expect(spy1).toHaveBeenCalled();
        expect(component.isSamePlayerName).toBeTrue();
    });

    // /// ///////////////////////////////////////// getCreatorIndex ///////////////////////
    it('should getCreatorIndex when the name of the room is saved in the room names list and return a counter', () => {
        const roomName = 'room';
        component.roomNames = ['room'];

        const result = component.getCreatorIndex(roomName);
        expect(result).toEqual(0);
    });

    it('should getCreatorIndex when the name of the room is saved in the room names list and return a counter', () => {
        const roomName = 'room';
        component.roomNames = ['room1'];

        const result = component.getCreatorIndex(roomName);
        expect(result).toEqual(-1);
    });
    /// ///////////////////////////////////////// validatePlayerName ///////////////////////
    it('should validatePlayerName return true', () => {
        const playerName = 'aldric';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeTrue();
    });

    it('should validatePlayerName return false', () => {
        const playerName = 'aldricaldricaldricaldricaldric';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeFalse();
    });

    it('should validatePlayerName return false', () => {
        const playerName = 'aldric!';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeFalse();
    });

    it('should validatePlayerName and return false because the name contain a "!" ', () => {
        const playerName = 'aldric a';
        const errorMessage = 'Votre nom ne doit pas contenir des espaces';
        component.validatePlayerName(playerName);
        expect(component.errorMessage).toEqual(errorMessage);
    });

    /// ///////////////////////////////////// startGame ///////////////////////
    it('should startGame return true', async () => {
        component.isWaitingAccept = false;
        component.roomNames = [''];
        component.roomName = component.roomNames[0];
        spyOn(component.roomName, 'indexOf').and.returnValue(0);
        const spy1 = spyOn(component.roomNames, 'slice');
        component.startGame();

        expect(multiplayerServiceSpy.joinRoom).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenStartGameAccepted).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenStartGameRejected).toHaveBeenCalled();
        expect(component.isWaitingAccept).toBeTrue();
        expect(spy1).toHaveBeenCalled();
    });

    it('should startGame return false', async () => {
        component.isWaitingAccept = false;
        component.roomNames = [''];
        component.roomName = 'dani';
        spyOn(component.roomName, 'indexOf').and.returnValue(0);
        component.startGame();

        expect(multiplayerServiceSpy.joinRoom).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenStartGameAccepted).toHaveBeenCalled();
        expect(multiplayerServiceSpy.listenStartGameRejected).toHaveBeenCalled();
        expect(component.isWaitingAccept).toBeTrue();
    });

    /// ///////////////////////////////////// selectRandomRoom ///////////////////////
    it('selctRandomRoom should select a random Room', () => {
        component.creatorNames = [];
        component.roomNames = [];
        const spy1 = spyOn(component, 'getCreatorIndex');

        component.selectRandomRoom();
        expect(component.isSamePlayerName).toBeFalse();
        expect(spy1).toHaveBeenCalled();
    });

    it('should put the boolean isSamePlayerName on true when the players have the same name', () => {
        component.creatorNames = ['Gilles'];
        component.roomNames = [''];
        const spy1 = spyOn(component, 'getCreatorIndex').and.returnValue(0);
        component.playerName = gameService.player1.name;

        component.selectRandomRoom();
        expect(component.creatorName).toEqual(component.playerName);
        expect(spy1).toHaveBeenCalled();
        expect(component.isSamePlayerName).toBeTrue();
    });

    /// ///////////////////////////////////// cancelJoin ///////////////////////
    it('cancelJoin should cancel the joining action', () => {
        component.isWaitingAccept = true;

        component.cancelJoin();
        expect(component.isSamePlayerName).toBeFalse();
        expect(multiplayerServiceSpy.cancelJoin).toHaveBeenCalled();
        expect(component.isWaitingAccept).toBeFalse();
    });

    it('cancelJoin should cancel the joining action', () => {
        component.isWaitingAccept = false;

        component.cancelJoin();
        expect(component.isSamePlayerName).toBeFalse();
        expect(component.isWaitingAccept).toBeFalse();
    });
});
