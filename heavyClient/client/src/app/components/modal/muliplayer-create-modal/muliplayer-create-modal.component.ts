import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GameTime } from '@app/classes/time';
import { PLAYER_NAME_MAX_LENGTH, PLAYER_NAME_MIN_LENGTH, VIRTUAL_PLAYER_NAMES } from '@app/constants/constants';
import { CommandsService } from '@app/services/commands.service';
import { DictionaryService } from '@app/services/dictionnary.service';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';

@Component({
    selector: 'app-muliplayer-create-modal',
    templateUrl: './muliplayer-create-modal.component.html',
    styleUrls: ['./muliplayer-create-modal.component.scss'],
})
export class MuliplayerCreateModalComponent implements OnInit {
    @Input() isDictionnarySelected = false;
    isInputError = false;
    errorMessage = '';
    playerName: string = '';
    description: string | undefined = '';
    joiner: string = '';
    roomName: string = '';
    isWaitingJoiner = false;
    isPlayerJoined = false;
    isPlayerNameValid = false;
    isRoomNameValid = true;
    roomList: string[];
    timer: GameTime = { min: 1, sec: 0 };

    dictionnaryForm: FormGroup;

    constructor(
        private multiplayerService: MultiplayerService,
        private gameService: GameService,
        private router: Router,
        private dialogRef: MatDialogRef<MuliplayerCreateModalComponent>,
        public commandService: CommandsService,
        public dictionnaryService: DictionaryService,
        private dictionnaryFormBuilder: FormBuilder,
    ) {
        this.dictionnaryForm = this.dictionnaryFormBuilder.group({ dictionnary: [''] });
    }

    ngOnInit(): void {
        this.gameService.multiplayerGame = true;
        this.multiplayerService.listenStartGame();
        this.multiplayerService.getAllRoomsWithDetails();
        this.multiplayerService.listenListRoomsWithDetailsResult();
        this.multiplayerService.joinerCanceled();
        this.multiplayerService.roomListOBS.subscribe((response) => {
            setTimeout(() => {
                this.roomList = response[2];
            }, 0);
        });
        this.multiplayerService.joinedOBS.subscribe((response) => {
            setTimeout(() => {
                if (response) {
                    this.isPlayerJoined = true;
                    this.joiner = this.gameService.player2.name;
                }
            }, 0);
        });
        this.multiplayerService.joinerCancelOBS.subscribe((response) => {
            setTimeout(() => {
                if (response === 'joinerLeft') {
                    this.isPlayerJoined = false;
                    this.isWaitingJoiner = true;
                }
            }, 0);
        });
    }

    subscribeDescriptionToDictionnaryName() {
        this.dictionnaryService.getList();
        this.dictionnaryForm.get('dictionnary')?.valueChanges.subscribe((value) => {
            for (const val of this.dictionnaryService.listOfDictionnary) {
                if (value === val.title) {
                    this.description = val.description;
                }
            }
        });
    }

    onPlayerNameChange() {
        this.isPlayerNameValid = true;
        if (!this.validatePlayerName(this.playerName)) {
            this.isPlayerNameValid = false;
        }
    }

    onRoomNameChange() {
        this.isRoomNameValid = !(this.roomList.indexOf(this.roomName) >= 0);
    }

    validatePlayerName(playerName: string): boolean {
        const isValidLength = playerName.length <= PLAYER_NAME_MAX_LENGTH && playerName.length >= PLAYER_NAME_MIN_LENGTH;
        const containsNoSpace = playerName.split(' ').join('') === playerName;
        const isOnlyChars = /^[a-zA-Z]+$/.test(playerName);
        if (!(isValidLength && containsNoSpace && isOnlyChars)) {
            if (playerName.length > PLAYER_NAME_MAX_LENGTH) {
                this.errorMessage = `Votre nom ne doit pas depasser ${PLAYER_NAME_MAX_LENGTH} caractères`;
            } else if (playerName.length < PLAYER_NAME_MIN_LENGTH) {
                this.errorMessage = `Votre nom ne doit avoir au moin ${PLAYER_NAME_MIN_LENGTH} caractères`;
            } else if (!containsNoSpace) {
                this.errorMessage = 'Votre nom ne doit pas contenir des espaces';
            } else {
                this.errorMessage = 'Votre nom doit contenir uniquement des lettres';
            }
            this.isInputError = true;
        } else {
            this.isInputError = false;
        }
        return isValidLength && containsNoSpace && isOnlyChars;
    }

    acceptJoiner() {
        this.multiplayerService.acceptJoinRoom(this.gameService.roomName);
        this.gameService.player2.creator = false;
        this.dialogRef.close();
        this.router.navigateByUrl('/game');
    }

    rejectJoiner() {
        this.isPlayerJoined = false;
        this.multiplayerService.rejectJoinRoom(this.roomName);
        this.isWaitingJoiner = true;
    }

    delet() {
        if (this.isWaitingJoiner) {
            this.multiplayerService.deleteRoom(this.gameService.roomName);
        }
        this.isPlayerJoined = false;
        this.isWaitingJoiner = false;
        this.isRoomNameValid = false;
        this.isPlayerNameValid = false;
        this.playerName = '';
        this.joiner = '';
        this.roomName = '';
    }
    startGame() {
        this.gameService.player1.name = this.playerName;
        this.gameService.player1.creator = true;
        this.gameService.roomName = this.roomName;
        this.gameService.gameTimeTurn = this.timer;
        this.gameService.timer = this.timer;
        localStorage.setItem('roomName', JSON.stringify(this.roomName));
        localStorage.setItem('creator', JSON.stringify(true));
        this.multiplayerService.createRoom();
        this.isWaitingJoiner = true;
    }

    createSoloGame() {
        this.gameService.player1.name = this.playerName;
        this.gameService.player2.name = this.generateVirtualPlayerName();
        this.gameService.multiplayerGame = false;
        this.gameService.gameTimeTurn = this.timer;
        this.delet();
        this.router.navigateByUrl('/game');
    }

    generateVirtualPlayerName() {
        let randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        while (VIRTUAL_PLAYER_NAMES[randomNameInex] === this.playerName) {
            randomNameInex = Math.floor(Math.random() * VIRTUAL_PLAYER_NAMES.length);
        }

        return VIRTUAL_PLAYER_NAMES[randomNameInex];
    }

    change(dictName: string) {
        this.dictionnaryService.getDict(dictName);
        if (dictName) {
            this.isDictionnarySelected = true;
        }
    }
}
