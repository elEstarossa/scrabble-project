/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable dot-notation */
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { Letter } from '@app/classes/letter';
import { Player } from '@app/classes/player';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { GameService } from '@app/services/game.service';
import { MultiplayerService } from '@app/services/multiplayer.service';
import { ReserveService } from '@app/services/reserve.service';
import { BehaviorSubject } from 'rxjs';
import { MuliplayerCreateModalComponent } from './muliplayer-create-modal.component';

describe('MuliplayerCreateModalComponent', () => {
    let component: MuliplayerCreateModalComponent;
    let fixture: ComponentFixture<MuliplayerCreateModalComponent>;
    let matDialogRefSpyObj: jasmine.SpyObj<MatDialogRef<MuliplayerCreateModalComponent>>;
    let reserveService: jasmine.SpyObj<ReserveService>;
    let router: jasmine.SpyObj<Router>;
    let gameService: jasmine.SpyObj<GameService>;
    let multiplayerServiceSpy: jasmine.SpyObj<MultiplayerService>;
    let joinObsBool: boolean;

    beforeEach(async () => {
        router = jasmine.createSpyObj('Router', ['navigateByUrl']);
        reserveService = jasmine.createSpyObj('ReserveService', ['']);
        reserveService.letters = new Map<Letter, number>();

        gameService = jasmine.createSpyObj('GameService', ['setRoom']);
        gameService.player1 = new Player('Gilles');
        gameService.player2 = new Player('Abdou');

        multiplayerServiceSpy = jasmine.createSpyObj('MultiplayerService', [
            'listenStartGame',
            'getAllRoomsWithDetails',
            'listenListRoomsWithDetailsResult',
            'rejectJoinRoom',
            'createRoom',
            'acceptJoinRoom',
            'deleteRoom',
            'joinerCanceled',
        ]);
        multiplayerServiceSpy.roomListOBS = new BehaviorSubject([[''], ['']]);
        multiplayerServiceSpy.joinedOBS = new BehaviorSubject(joinObsBool);
        multiplayerServiceSpy.joinerCancelOBS = new BehaviorSubject('init');

        matDialogRefSpyObj = jasmine.createSpyObj('matDialogRefSpyObj', ['close']);

        await TestBed.configureTestingModule({
            declarations: [MuliplayerCreateModalComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: matDialogRefSpyObj },
                { provide: MultiplayerService, useValue: multiplayerServiceSpy },
                { provide: GameService, useValue: gameService },
                { provide: ReserveService, useValue: reserveService },
                { provide: Router, useValue: router },
                { provide: APP_BASE_HREF, useValue: {} },
                { provide: NG_VALUE_ACCESSOR, useExisting: {} },
            ],
            imports: [
                MatDialogModule,
                MatInputModule,
                MatSelectModule,
                MatFormFieldModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                BrowserAnimationsModule,
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MuliplayerCreateModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        jasmine.clock().install();

        expect(component).toBeTruthy();
        jasmine.clock().tick(1);
        jasmine.clock().uninstall();
    });
    it('should ngOnInit in the beginning', () => {
        jasmine.clock().install();
        component.playerName = '';
        component.ngOnInit();
        expect(multiplayerServiceSpy.getAllRoomsWithDetails).toHaveBeenCalled();
        jasmine.clock().tick(0);
        jasmine.clock().uninstall();
    });

    it('should ngOnInit in the beginning with the mj service ', async () => {
        jasmine.clock().install();
        multiplayerServiceSpy.joinerCancelOBS = new BehaviorSubject('joinerLeft');
        component.ngOnInit();
        expect(component.isPlayerJoined).toBeFalse();
        jasmine.clock().tick(0);
        jasmine.clock().uninstall();
    });

    // /// ///////////////////////////////////////// onPlayerNameChange ///////////////////////

    it('should onPlayerNameChange when the player name is valid ', () => {
        component.isPlayerNameValid = true;
        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeFalse();
    });

    it('should onPlayerNameChange when the player name is secondly valid', () => {
        spyOn(component, 'validatePlayerName').and.returnValue(true);
        component.onPlayerNameChange();
        expect(component.isPlayerNameValid).toBeTrue();
    });

    /// ///////////////////////////////////////// onRoomNameChange ///////////////////////
    it('should onRoomNameChange', () => {
        const room = 'danone';
        const roomList = (component.roomList = ['']);
        roomList.push(room);
        component.onRoomNameChange();
        expect(component.isRoomNameValid).toBeFalse();
    });

    /// ///////////////////////////////////////// validatePlayerName ///////////////////////
    it('should validatePlayerName and return true because the name is valid', () => {
        const playerName = 'aldric';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeTrue();
    });

    it('should validatePlayerName and return false because the name is more than 15 caracters ', () => {
        const playerName = 'aldricaldricaldricaldricaldric';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeFalse();
    });

    it('should validatePlayerName and return false because the name contain a "!" ', () => {
        const playerName = 'aldric!';
        const expected = component.validatePlayerName(playerName);
        expect(expected).toBeFalse();
    });

    it('should validatePlayerName and return false because the name contain a "!" ', () => {
        const playerName = 'aldric a';
        const errorMessage = 'Votre nom ne doit pas contenir des espaces';
        component.validatePlayerName(playerName);
        expect(component.errorMessage).toEqual(errorMessage);
    });

    // /// ///////////////////////////////////////// acceptJoiner ///////////////////////
    it('should acceptJoiner', () => {
        multiplayerServiceSpy.acceptJoinRoom('');
        component.acceptJoiner();
        expect(gameService.player2.creator).toBeFalse();
        expect(router.navigateByUrl.calls.first().args[0]).toBe('/game');
    });

    /// ///////////////////////////////////////// startGame ///////////////////////
    it('should startGame and call the create and set up room functions', () => {
        component.startGame();
        expect(multiplayerServiceSpy.createRoom).toHaveBeenCalled();
    });

    /// ///////////////////////////////////////// rejectJoiner ///////////////////////
    it('should rejectJoiner when the player is not joined', () => {
        component.isPlayerJoined = false;
        component.rejectJoiner();
        expect(multiplayerServiceSpy.rejectJoinRoom).toHaveBeenCalled();
    });

    /// ///////////////////////////////////////// createSoloGame ///////////////////////
    it('createSoloGame should call generateVirtualPlayerName and navigate to the game page', () => {
        const name = 'COMPUTER';
        const spyGenerateVirtual = spyOn(component, 'generateVirtualPlayerName').and.callFake(() => {
            return name;
        });
        component.createSoloGame();
        expect(spyGenerateVirtual).toHaveBeenCalled();
        expect(router.navigateByUrl.calls.first().args[0]).toBe('/game');
    });

    /// ///////////////////////////////////////// generateVirtualPlayerName ///////////////////////
    it('generateVirtualPlayerName should call and navigate to the game page', () => {
        spyOn(Math, 'random').and.returnValue(0);
        expect(component.generateVirtualPlayerName()).toEqual('Bender');
    });

    /// ///////////////////////////////////////// delete ///////////////////////
    it('should delete by calling the delete room function in multiplayer service', () => {
        component.isWaitingJoiner = true;
        component.delet();
        expect(multiplayerServiceSpy.deleteRoom).toHaveBeenCalled();
    });

    it('should put the all validation for mj game in false', () => {
        component.isWaitingJoiner = false;
        component.delet();
        expect(component.isPlayerJoined).toBeFalse();
        expect(component.isWaitingJoiner).toBeFalse();
        expect(component.isPlayerNameValid).toBeFalse();
    });

    /// ///////////////////////////////////////// change ///////////////////////
    it('should change if the userInput is true', () => {
        const dictName = 'monDict';

        component.change(dictName);
        expect(component.isDictionnarySelected).toBeTrue();
    });
    it('should change if the userInput is false', () => {
        const dictName = '';

        component.change(dictName);
        expect(component.isDictionnarySelected).toBeFalse();
    });

    /// ///////////////////////////////////////// subscribeDescriptionToDictionnaryName ///////////////////////
    it('subscribeDescriptionToDictionnaryName ', () => {
        const spy = spyOn(component['dictionnaryService'], 'getList');
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: '', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.subscribeDescriptionToDictionnaryName();
        component.dictionnaryForm.get('dictionnary')?.setValue('default');
        expect(spy).toHaveBeenCalled();
    });
    it('subscribeDescriptionToDictionnaryName -> description should not be assigned if the dictionnary is not there', () => {
        const spy = spyOn(component['dictionnaryService'], 'getList');
        const listOfDictionnary: WordDictionnary[] = [{ title: 'default', description: 'alo', words: [''] }];
        component['dictionnaryService'].listOfDictionnary = listOfDictionnary;

        component.subscribeDescriptionToDictionnaryName();
        component.dictionnaryForm.get('dictionnary')?.setValue('b');
        expect(spy).toHaveBeenCalled();
        expect(component.description).not.toEqual('alo');
    });
});
