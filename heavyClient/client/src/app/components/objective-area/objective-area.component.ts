import { Component } from '@angular/core';
import { GameService } from '@app/services/game.service';
import { ObjectiveService } from '@app/services/objective.service';

@Component({
    selector: 'app-objective-area',
    templateUrl: './objective-area.component.html',
    styleUrls: ['./objective-area.component.scss'],
})
export class ObjectiveAreaComponent {
    constructor(public objectiveService: ObjectiveService, public gameService: GameService) {}
}
