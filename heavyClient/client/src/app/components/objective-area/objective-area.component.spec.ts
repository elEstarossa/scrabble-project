import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ObjectiveAreaComponent } from './objective-area.component';

describe('ObjectiveAreaComponent', () => {
    let component: ObjectiveAreaComponent;
    let fixture: ComponentFixture<ObjectiveAreaComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ObjectiveAreaComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [{ provide: APP_BASE_HREF, useValue: {} }],
            imports: [HttpClientTestingModule, MatSnackBarModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ObjectiveAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
