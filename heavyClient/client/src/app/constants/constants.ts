import { CommandCode } from '@app/classes/comand-code';
import { Command } from '@app/classes/command';
import { Letter } from '@app/classes/letter';
import { TileContent } from '@app/classes/tile-content';
import { Vec2 } from '@app/classes/vec2';
// grid-service
export const BOARD_WIDTH = 600;
export const BOARD_HEIGHT = 600;
export const NB_LETTER_HAND = 7;
export const CLEAR_RECT_FIX = 5;
export const HAND_POSITION_START = 4;
export const HAND_POSITION_END = 11;
export const CTX_PX = 15;
export const ADJUSTEMENT_TOPSPACE = 5;
export const LIGHT_GREEN_CASE = 5;
export const H_ARROW = '→';
export const V_ARROW = '↓';
export const HEIGHT_IN_BOARD = 10;
export const NUMBER_OF_TILES = 40;

// play-area-comp
export const CANEVAS_WIDTH = 700;
export const CANEVAS_HEIGHT = 700;
export const NB_TILES = 15;
export const TOPSPACE = 25;
export const LEFTSPACE = 50;
export const UNDEFINED_INDEX = -1;
export const UNDEFINED_POSITION = { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX };
export const EMPTY_COMMAND: Command = { position: { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX }, word: '', direction: '' };
export const EMPTY_COMMAND_CODE: CommandCode = { isValid: false, errorMsg: 'init', score: 0 };
export const PLACER_CODE = 1;
export const EXCHANGE_CODE = 2;
export const SKIP_CODE = 3;
export const TEXTMSG_CODE = 4;

// Tile content all sizes
export const SMALL_PX: TileContent = { alpha: 20, score: 10, label: 'small' };
export const MEDIUM_PX: TileContent = { alpha: 23, score: 12, label: 'medium' };
export const LARGE_PX: TileContent = { alpha: 27, score: 13, label: 'large' };
export const SIZES: TileContent[] = [SMALL_PX, MEDIUM_PX, LARGE_PX];

// offset used for drawing a letter
export const OFFSET_DRAW_LETTER = 1.25;

// LETTERS
export const NOT_A_LETTER: Letter = { score: 0, charac: '1' };
export const A: Letter = { score: 1, charac: 'a' };
export const B: Letter = { score: 3, charac: 'b' };
export const C: Letter = { score: 3, charac: 'c' };
export const D: Letter = { score: 2, charac: 'd' };
export const E: Letter = { score: 1, charac: 'e' };
export const F: Letter = { score: 4, charac: 'f' };
export const G: Letter = { score: 2, charac: 'g' };
export const H: Letter = { score: 4, charac: 'h' };
export const I: Letter = { score: 1, charac: 'i' };
export const J: Letter = { score: 8, charac: 'j' };
export const K: Letter = { score: 10, charac: 'k' };
export const L: Letter = { score: 1, charac: 'l' };
export const M: Letter = { score: 2, charac: 'm' };
export const N: Letter = { score: 1, charac: 'n' };
export const O: Letter = { score: 1, charac: 'o' };
export const P: Letter = { score: 3, charac: 'p' };
export const Q: Letter = { score: 8, charac: 'q' };
export const R: Letter = { score: 1, charac: 'r' };
export const S: Letter = { score: 1, charac: 's' };
export const T: Letter = { score: 1, charac: 't' };
export const U: Letter = { score: 1, charac: 'u' };
export const V: Letter = { score: 4, charac: 'v' };
export const W: Letter = { score: 10, charac: 'w' };
export const X: Letter = { score: 10, charac: 'x' };
export const Y: Letter = { score: 10, charac: 'y' };
export const Z: Letter = { score: 10, charac: 'z' };

export const QTY_A = 9;
export const QTY_E = 15;
export const QTY_I = 8;
export const QTY_L = 5;
export const QTY_O = 6;
export const LETTERS_RESERVE_QTY = new Map<Letter, number>([
    [A, QTY_A],
    [B, 2],
    [C, 2],
    [D, 3],
    [E, QTY_E],
    [F, 2],
    [G, 2],
    [H, 2],
    [I, QTY_I],
    [J, 1],
    [K, 1],
    [L, QTY_L],
    [M, 3],
    [N, QTY_O],
    [O, QTY_O],
    [P, 2],
    [Q, 1],
    [R, QTY_O],
    [S, QTY_O],
    [T, QTY_O],
    [U, QTY_O],
    [V, 2],
    [W, 1],
    [X, 1],
    [Y, 1],
    [Z, 1],
]);

export const LETTERS_OBJECT = new Map<string, Letter>([
    ['a', A],
    ['à', A],
    ['â', A],
    ['b', B],
    ['ç', C],
    ['c', C],
    ['d', D],
    ['e', E],
    ['é', E],
    ['è', E],
    ['ê', E],
    ['f', F],
    ['g', G],
    ['h', H],
    ['i', I],
    ['î', I],
    ['j', J],
    ['k', K],
    ['l', L],
    ['m', M],
    ['n', N],
    ['o', O],
    ['p', P],
    ['q', Q],
    ['r', R],
    ['s', S],
    ['t', T],
    ['ù', U],
    ['u', U],
    ['v', V],
    ['w', W],
    ['x', X],
    ['y', Y],
    ['z', Z],
]);

export const FIRST_POSITION_BOARD = { x: 8, y: 8 } as Vec2;

// chatBarComponent
export const SET_INTERVAL = 100;
export const MAX_CHARACTER = 512;
export const COMMAND_PLACER = '!placer';
export const COMMAND_ECHANGER = '!echanger';
export const COMMAND_CHANGER = '!échanger';
export const COMMAND_PASSER = '!passer';
export const COMMAND_INDICE = '!indice';
export const COMMAND_AIDE = '!aide';
export const ASCI_A = 97;
export const ASCI_O = 111;
export const MAX_SIZE = 15;
export const TIME_FOR_SET_TIME_OUT = 3000;
export const COMMAND_POSITION = 8;
export const POURCENT = 100;

// player name validation
export const PLAYER_NAME_MAX_LENGTH = 15;
export const PLAYER_NAME_MIN_LENGTH = 5;

// game mode
export const CLASSIC_GAME_MODE = 'CLASSIC_MODE';

// reserve
export const RESERVE_SIZE = 100;
export const TILE_SIZE = BOARD_WIDTH / NB_TILES;

// easel
export const EASEL_LENGTH = 7;
export const TEMP_CANVAS_STROKE = 6;
export const VALIDATION_AWAIT = 3000;
export const ONE_MINUT_TIMER = { min: 1, sec: 0 };
export const POSITION_FOR_MANIPULATION = -1;

export const GRID_CTX_FIX_ERROR = 40;
export const GRID_CTX_TEN = 10;
export const ASCI_CODE_A = 97;
export const SCORE_BONUS = 50;

export const EASEL_SIZE_FOR_MANIPULATION = 6;
export const REMOVED_LETTERS = 11;

// difficulté joueur virtuel
export const EASY_DIFFICULTY = 'EASY';
export const HARD_DIFFICULTY = 'HARD';

// list of possible virtual player names
export const VIRTUAL_PLAYER_NAMES = ['Bender', 'WALLE', 'COMPUTER'];
export const VIRTUAL_PLAYER_NAMES_EX = ['BESAF', 'KBE7', 'WA3AR'];
// words service
export const GENERATE_REGEX = -2;

// virtual player service
export const TIME_FOR_JV_TO_PLAY = 3000;
export const PROB_FOR_SMALL_SCORE = 6;
export const PROB_FOR_MEDIUM_SCORE = 12;
export const PROB_FOR_MAX_SCORE = 18;

export const PROB_FOR_PLACEMENT = 80;
export const PROB_FOR_EXCHANGE = 90;

export const SMALL_SCORE = 40;
export const MEDIUM_SCORE = 70;
export const MAX_SCORE = 100;
export const PASSING_ROUND = 17000;

// game service
export const TIMER_INTERVAL = 1000;
export const PASS_TURN_MAX = 6;
export const THIRTY_SEC = 30;

// easel dimensions
export const EASEL_MIN_X = 210;
export const EASEL_MAX_X = 490;
export const EASEL_MIN_Y = 635;
export const EASEL_MAX_Y = 680;
export const EASEL_LETTER_LENGTH = 40;

// multiplayer service
// objective service
export const OBJ = [
    { id: 0, description: ' Placer un mot dans la même direction pendant 3 tours consécutifs +40pts', pts: 40 },
    { id: 1, description: ' Placer un mot qui contient au moins quatre voyelles +40pts', pts: 40 },
    { id: 2, description: ' Placer un mot qui contient deux bonus double +20pts', pts: 20 },
    { id: 3, description: ' Placer en moins de cinq seconde 3 tours de suite +20pts', pts: 20 },
    { id: 4, description: ' Placer le mot <<bonus>> +30pts', pts: 30 },
    { id: 5, description: " Placer un mot le long d'une extremité +20pts", pts: 20 },
    { id: 6, description: ' Faire un placement qui vaut 30+ points +30pts', pts: 30 },
    { id: 7, description: ' Échanger 3 fois de suite +20pts', pts: 20 },
];

export const TARGET_ID0 = 0;
export const TARGET_ID1 = 1;
export const TARGET_ID2 = 2;
export const TARGET_ID3 = 3;
export const TARGET_ID4 = 4;
export const TARGET_ID5 = 5;
export const TARGET_ID6 = 6;
export const TARGET_ID7 = 7;

export const SCORE_FOR_PLACEMENT_OVER_THIRTY_POINTS = 30;
export const FOUR_VOWELS = 4;
export const COUNTER_FOR_OBJS = 8;

export const ELEMENT_NOT_FOUNDED = -1;

export const DEFAULT_DICTIONNARY = 'Mon dictionnaire';
export const FIFTY_NINE_SEC = 59;
