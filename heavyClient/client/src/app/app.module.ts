import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HighScoreModalComponent } from '@app/components/modal/highscore-modal/highscore-modal.component';
import { SingleplayerCreateModalComponent } from '@app/components/modal/singleplayer-create-modal/singleplayer-create-modal.component';
import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
import { SidebarComponent } from '@app/components/sidebar/sidebar.component';
import { CreatorPlayerComponent } from '@app/components/users/creator-player/creator-player.component';
import { JoinerPlayerComponent } from '@app/components/users/joiner-player/joiner-player.component';
import { AppRoutingModule } from '@app/modules/app-routing.module';
import { AppMaterialModule } from '@app/modules/material.module';
import { AppComponent } from '@app/pages/app/app.component';
import { GamePageComponent } from '@app/pages/game-page/game-page.component';
import { MainPageComponent } from '@app/pages/main-page/main-page.component';
import { MaterialPageComponent } from '@app/pages/material-page/material-page.component';
import { ChatbarComponent } from './components/chatbar/chatbar.component';
import { ClassicModeScoreModalComponent } from './components/modal/classic-mode-score-modal/classic-mode-score-modal.component';
import { ConvertGameModalComponent } from './components/modal/convert-game-modal/convert-game-modal.component';
import { EndGameModalComponent } from './components/modal/end-game-modal/end-game-modal.component';
import { LeaveGameModalComponent } from './components/modal/leave-game-modal/leave-game-modal.component';
import { LogModeScoreModalComponent } from './components/modal/log-mode-score-modal/log-mode-score-modal.component';
import { MainPageLog2990ModalComponent } from './components/modal/main-page-log2990-modal/main-page-log2990-modal.component';
import { MainPageModalComponent } from './components/modal/main-page-modal/main-page-modal.component';
import { MuliplayerCreateModalComponent } from './components/modal/muliplayer-create-modal/muliplayer-create-modal.component';
import { MuliplayerJoinModalComponent } from './components/modal/muliplayer-join-modal/muliplayer-join-modal.component';
import { MultiplayerModalComponent } from './components/modal/multiplayer-modal/multiplayer-modal.component';
import { ObjectiveAreaComponent } from './components/objective-area/objective-area.component';
import { PopupCreateGameModalComponent } from './components/popup/popup-create-game-modal/popup-create-game-modal.component';
import { PopupDictionnaryModalComponent } from './components/popup/popup-dictionnary-modal/popup-dictionnary-modal.component';
import { PopupModifyDescDictComponent } from './components/popup/popup-modify-desc-dict/popup-modify-desc-dict.component';
import { PopupModifyDictionnaryComponent } from './components/popup/popup-modify-dictionnary/popup-modify-dictionnary.component';
import { PopupResetComponent } from './components/popup/popup-reset/popup-reset.component';
import { AdminModeComponent } from './pages/admin-mode/admin-mode.component';
/**
 * Main module that is used in main.ts.
 * All automatically generated components will appear in this module.
 * Please do not move this module in the module folder.
 * Otherwise Angular Cli will not know in which module to put new component
 */
@NgModule({
    declarations: [
        AppComponent,
        GamePageComponent,
        MainPageComponent,
        MaterialPageComponent,
        PlayAreaComponent,
        SidebarComponent,
        MainPageModalComponent,
        CreatorPlayerComponent,
        JoinerPlayerComponent,
        ChatbarComponent,
        LeaveGameModalComponent,
        MultiplayerModalComponent,
        MuliplayerCreateModalComponent,
        MuliplayerJoinModalComponent,
        EndGameModalComponent,
        SingleplayerCreateModalComponent,
        HighScoreModalComponent,
        ClassicModeScoreModalComponent,
        AdminModeComponent,
        ConvertGameModalComponent,
        MainPageLog2990ModalComponent,
        ObjectiveAreaComponent,
        PopupCreateGameModalComponent,
        PopupDictionnaryModalComponent,
        PopupModifyDictionnaryComponent,
        PopupModifyDescDictComponent,
        PopupResetComponent,
        LogModeScoreModalComponent,
    ],
    imports: [
        AppMaterialModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatSelectModule,
    ],
    providers: [
        {
            provide: APP_BASE_HREF,
            useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM(),
            deps: [PlatformLocation],
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
