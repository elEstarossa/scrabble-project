import { WordDictionnary } from '@app/classes/word-dictionnary';
import * as fs from 'fs';
import { Service } from 'typedi';
const dir = './app/assets/';
@Service()
export class DictionaryService {
    listOfDictionnary: WordDictionnary[] = [];
    constructor() {
        this.fileList();
    }
    fileList() {
        fs.readdir(dir, (err: unknown, files: unknown[]) => {
            files.forEach((file) => {
                const content = JSON.parse(fs.readFileSync(dir + file, 'utf8'));
                this.listOfDictionnary.push(content);
            });
        });
    }
    get dictionaries(): WordDictionnary[] {
        return this.listOfDictionnary;
    }
    add(dictionary: WordDictionnary) {
        const data = JSON.stringify(dictionary);
        fs.writeFile(dir + dictionary.title + '.json', data, (err: unknown) => {
            if (err) throw err;
        });
        this.listOfDictionnary.push({ title: dictionary.title, description: dictionary.description, words: dictionary.words });
    }
    async delete(index: number) {
        return new Promise(() => {
            this.deleteFile(this.listOfDictionnary[index].title);
            this.listOfDictionnary.splice(index, 1);
        });
    }
    deleteFile(fileName: string) {
        const path = dir + fileName + '.json';
        fs.unlink(path, (err: unknown) => {
            if (err) throw err;
        });
    }
    async modify(index: number, title: string, desc: string) {
        return new Promise(() => {
            const fileName = this.listOfDictionnary[index].title;
            this.listOfDictionnary[index].title = title;
            this.listOfDictionnary[index].description = desc;
            this.modifyFile(index, fileName);
        });
    }
    async modifyFile(index: number, fileName: string) {
        return new Promise((resolve, reject) => {
            const path = dir + fileName + '.json';
            const content = JSON.parse(fs.readFileSync(path, 'utf8'));
            const title = this.listOfDictionnary[index].title;
            content.title = title;
            content.description = this.listOfDictionnary[index].description;
            const newPath = dir + title + '.json';
            fs.writeFileSync(newPath, JSON.stringify(content));
            fs.unlink(path, (error: unknown) => {
                if (error) {
                    reject(error);
                    throw error;
                }
            });
        });
    }
}
