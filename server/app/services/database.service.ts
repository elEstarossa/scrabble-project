import { Scores } from '@app/classes/scores';
import { Db, MongoClient } from 'mongodb';
import { Service } from 'typedi';
const DATABASE_URL = 'mongodb+srv://ouss:123@log2990.m8fyg.mongodb.net/LOG2990?retryWrites=true&w=majority';
const DB_NAME = 'LOG2990';
const DATABASE_COLLECTION_CLASSIC = 'Classic';
const DATABASE_COLLECTION_2990 = 'Mode2990';

@Service()
export class DatabaseService {
    listToCompareToDBValue: Scores[] = [];
    private db: Db;
    private client: MongoClient;
    async start(url: string = DATABASE_URL): Promise<MongoClient | null> {
        try {
            const client = await MongoClient.connect(url);
            this.client = client;
            this.db = client.db(DB_NAME);
        } catch {
            throw new Error('Database connection error');
        }
        return this.client;
    }
    async closeConnection(): Promise<void> {
        return this.client.close();
    }
    get database(): Db {
        return this.db;
    }
    async sendTableOfHighScores(mode: string) {
        if (mode === 'classic') {
            const result = await this.db.collection(DATABASE_COLLECTION_CLASSIC).find<Scores>({}).sort({ score: -1 }).toArray();
            return result;
        } else {
            const result = await this.db.collection(DATABASE_COLLECTION_2990).find<Scores>({}).sort({ score: -1 }).toArray();
            return result;
        }
    }

    async addScore(highscoresTable: Scores[], mode: string): Promise<void> {
        if (mode === 'classic') {
            for (const score of highscoresTable) {
                await this.db.collection(DATABASE_COLLECTION_CLASSIC).insertOne(score);
            }
            this.db.collection(DATABASE_COLLECTION_CLASSIC).find({}).sort({ score: -1 });
        } else {
            for (const score of highscoresTable) {
                await this.db.collection(DATABASE_COLLECTION_2990).insertOne(score);
            }
            this.db.collection(DATABASE_COLLECTION_2990).find({}).sort({ score: -1 });
        }
    }
    async fiveHighScoreVerification(score: Scores[], mode: string): Promise<void> {
        if (mode === 'classic') {
            const resultat = await this.db.collection(DATABASE_COLLECTION_CLASSIC).find<Scores>({}).sort({ score: -1 }).toArray();
            for (const player of score) {
                for (let i = 0; i < resultat.length; i++) {
                    if (player.score > resultat[i].score) {
                        score.push({ name: resultat[i].name, score: resultat[i].score });
                        resultat[i].name = player.name;
                        resultat[i].score = player.score;
                        i = resultat.length;
                    } else if (player.score === resultat[i].score && player.name !== resultat[i].name) {
                        resultat[i].name = resultat[i].name + ' et ' + player.name;
                        i = resultat.length;
                    }
                }
            }

            await this.db.collection(DATABASE_COLLECTION_CLASSIC).drop();
            this.addScore(resultat, mode);
        } else {
            const resultat = await this.db.collection(DATABASE_COLLECTION_2990).find<Scores>({}).sort({ score: -1 }).toArray();
            for (const player of score) {
                for (let i = 0; i < resultat.length; i++) {
                    if (player.score > resultat[i].score) {
                        score.push({ name: resultat[i].name, score: resultat[i].score });
                        resultat[i].name = player.name;
                        resultat[i].score = player.score;
                        i = resultat.length;
                    } else if (player.score === resultat[i].score && player.name !== resultat[i].name) {
                        resultat[i].name = resultat[i].name + ' et ' + player.name;
                        i = resultat.length;
                    }
                }
            }

            await this.db.collection(DATABASE_COLLECTION_2990).drop();
            this.addScore(resultat, mode);
        }
    }
}
