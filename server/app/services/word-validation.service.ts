import * as dictionnary from '@app/assets/dictionnary.json';
import { Command } from '@app/classes/command';
import { CommandCode } from '@app/classes/command-code';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
import { WordDictionnary } from '@app/classes/word-dictionnary';
import {
    AZUR_TILE,
    BLUE_TILE,
    LETTERS_OBJECT,
    MAX_LENGTH,
    MULTIPLY_2,
    MULTIPLY_3,
    NA,
    NB_TILES,
    PINK_TILE,
    RED_TILE,
    SCORE_BONUS,
    UNDEFINED_INDEX,
} from '@app/constants/constants';
import { Service } from 'typedi';
@Service()
export class WordValidationService {
    indexOfLetterToCheck: number[] = [];

    wordValidationInit(command: Command, usedPosition: Letter[][]): CommandCode {
        this.spotNewLetters(command, usedPosition);

        const placement: CommandCode = { isValid: false, score: 0, errorMsg: '' };

        if (this.wordInDictionnary(command.word)) {
            placement.score += this.getWordBonusScore(command);
            const otherWords: CommandCode = this.checkNewFormedWords(command, usedPosition);

            if (otherWords.isValid) {
                placement.score += otherWords.score;
                placement.isValid = true;
            } else {
                this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
                return otherWords;
            }
        } else {
            this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
            return { isValid: false, errorMsg: 'Votre mot: ' + command.word + ', ne figure pas dans le dictionnaire!', score: 0 };
        }
        this.placeWordInserver(command, usedPosition);
        this.indexOfLetterToCheck.splice(0, this.indexOfLetterToCheck.length);
        return placement;
    }

    placeWordInserver(command: Command, usedPosition: Letter[][]) {
        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h')
                usedPosition[command.position.y - 1][command.position.x - 1 + i] = LETTERS_OBJECT.get(command.word.charAt(i)) ?? NA;
            else usedPosition[command.position.y - 1 + i][command.position.x - 1] = LETTERS_OBJECT.get(command.word.charAt(i)) ?? NA;
        }
    }
    getWordBonusScore(command: Command): number {
        let multiplyFact = 1;
        let score = 0;
        let counter = 0;
        for (let i = 0; i < command.word.length; i++) {
            if (i === this.indexOfLetterToCheck[counter]) {
                const posToCheck: Vec2 =
                    command.direction === 'h'
                        ? { x: command.position.x + i - 1, y: command.position.y - 1 }
                        : { x: command.position.x - 1, y: command.position.y + i - 1 };
                switch (true) {
                    case this.containBonus(posToCheck, RED_TILE):
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        multiplyFact *= MULTIPLY_3;
                        break;
                    case this.containBonus(posToCheck, AZUR_TILE):
                        score += (LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0) * MULTIPLY_2;
                        break;
                    case this.containBonus(posToCheck, PINK_TILE):
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        multiplyFact *= MULTIPLY_2;
                        break;
                    case this.containBonus(posToCheck, BLUE_TILE):
                        score += (LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0) * MULTIPLY_3;
                        break;
                    default:
                        score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
                        break;
                }
                counter++;
            } else {
                score += LETTERS_OBJECT.get(command.word.charAt(i))?.score ?? 0;
            }
        }

        score *= multiplyFact;
        if (this.indexOfLetterToCheck.length === MAX_LENGTH) score += SCORE_BONUS;
        return score;
    }

    checkNewFormedWords(command: Command, usedPosition: Letter[][]): CommandCode {
        let tempWord = '';
        let score = 0;
        for (const index of this.indexOfLetterToCheck) {
            let i = 0;
            if (this.getAxe(command) - 1 > 0 && this.getPreviousTile(command, usedPosition, index) !== NA) {
                do {
                    i++;
                } while (this.getAxe(command) - 1 - i >= 0 && this.getLetterWithDirection(command, usedPosition, index, i) !== NA);
                i--;
            } else {
                i = 0;
            }
            while (this.getAxe(command) - 1 - i < NB_TILES && (this.getLetterWithDirection(command, usedPosition, index, i) !== NA || i === 0)) {
                if (i === 0) tempWord += command.word.charAt(index);
                else tempWord += this.getLetterWithDirection(command, usedPosition, index, i).charac;
                i--;
            }
            if (tempWord.length >= 2) {
                if (this.wordInDictionnary(tempWord)) {
                    score += this.getLetterScore(tempWord);
                } else {
                    return { isValid: false, errorMsg: 'Votre placement génère un/des mot(s) qui ne figure(nt) pas dans le dictionnaire!', score: 0 };
                }
            }
            tempWord = '';
        }
        return { isValid: true, score, errorMsg: '' };
    }
    getLetterWithDirection(command: Command, usedPosition: Letter[][], index: number, i: number): Letter {
        return command.direction === 'h'
            ? usedPosition[command.position.y - 1 - i][command.position.x - 1 + index]
            : usedPosition[command.position.y - 1 + index][command.position.x - 1 - i];
    }
    getAxe(command: Command): number {
        return command.direction === 'h' ? command.position.y : command.position.x;
    }
    getPreviousTile(command: Command, usedPosition: Letter[][], index: number): Letter {
        return command.direction === 'h'
            ? usedPosition[command.position.y - 1 - 1][command.position.x - 1 + index]
            : usedPosition[command.position.y - 1 + index][command.position.x - 1 - 1];
    }

    getLetterScore(letter: string): number {
        let wordScore = 0;
        for (const char of letter) {
            wordScore += LETTERS_OBJECT.get(char)?.score ?? NA.score;
        }
        return wordScore;
    }
    spotNewLetters(command: Command, usedPosition: Letter[][]) {
        for (let i = 0; i < command.word.length; i++) {
            if (command.direction === 'h') {
                if (usedPosition[command.position.y - 1][command.position.x - 1 + i] === NA) {
                    this.indexOfLetterToCheck.push(i);
                }
            } else {
                if (usedPosition[command.position.y - 1 + i][command.position.x - 1] === NA) {
                    this.indexOfLetterToCheck.push(i);
                }
            }
        }
    }
    wordInDictionnary(word: string): boolean {
        const dictword = dictionnary as WordDictionnary;
        return UNDEFINED_INDEX !== dictword.words.findIndex((element) => word === element);
    }
    containBonus(position: Vec2, bonus: Vec2[]): boolean {
        for (const pos of bonus) {
            if (pos.x === position.x && pos.y === position.y) return true;
        }
        return false;
    }

    tileIsEmpty(position: Vec2, usedPosition: Letter[][]) {
        return usedPosition[position.y - 1][position.x - 1] === NA;
    }
}
