import { Command } from '@app/classes/command';
import { CommandCode } from '@app/classes/command-code';
import { Letter } from '@app/classes/letter';
import { A, E, I, LETTERS_OBJECT, matrixOfLetter, NA, NB_TILES, NOT_A_LETTER, O, R, T, U } from '@app/constants/constants';
import { WordValidationService } from '@app/services/word-validation.service';
import { expect } from 'chai';
describe('WordValidation service', () => {
    let wordValidationService: WordValidationService;

    beforeEach(async () => {
        wordValidationService = new WordValidationService();
        wordValidationService.indexOfLetterToCheck = [];
    });

    it('isValid should be true', () => {
        const usedPosition = new Array<Letter[]>(NB_TILES);
        const command: Command = { word: 'allo', position: { x: 8, y: 8 }, direction: 'h' };
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        const commandCode: CommandCode = wordValidationService.wordValidationInit(command, usedPosition);
        expect(commandCode.isValid).to.equal(true);
    });
    it('isValid should be false', () => {
        const usedPosition = new Array<Letter[]>(NB_TILES);
        const command: Command = { word: 'sdsf', position: { x: 8, y: 8 }, direction: 'h' };
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        const commandCode: CommandCode = wordValidationService.wordValidationInit(command, usedPosition);
        expect(commandCode.isValid).to.equal(false);
    });
    it('this word is not attached', () => {
        const usedPosition = new Array<Letter[]>(NB_TILES);
        const command: Command = { word: 'sdsf', position: { x: 2, y: 1 }, direction: 'h' };
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        const commandCode: CommandCode = wordValidationService.wordValidationInit(command, usedPosition);
        expect(commandCode.isValid).to.equal(false);
    });
    it('this word is not attached vertically', () => {
        const usedPosition = new Array<Letter[]>(NB_TILES);
        const command: Command = { word: 'sdsf', position: { x: 2, y: 1 }, direction: 'v' };
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NOT_A_LETTER);
        }
        const commandCode: CommandCode = wordValidationService.wordValidationInit(command, usedPosition);
        expect(commandCode.isValid).to.equal(false);
    });
    it('should spot the new letters horizontally', () => {
        matrixOfLetter[7][7] = A;
        const command: Command = { word: 'allo', position: { x: 8, y: 8 }, direction: 'h' };
        wordValidationService.spotNewLetters(command, matrixOfLetter);
        expect(wordValidationService.indexOfLetterToCheck.length).to.equal(3);
    });
    it('should know that this tile is not empty', () => {
        matrixOfLetter[7][7] = A;
        const command: Command = { word: 'allo', position: { x: 8, y: 8 }, direction: 'h' };
        wordValidationService.tileIsEmpty(command.position, matrixOfLetter);
        expect(wordValidationService.tileIsEmpty(command.position, matrixOfLetter)).to.not.equal(NA);
    });
    it('should spot the new letters vertically', () => {
        matrixOfLetter[7][7] = A;
        const command: Command = { word: 'allo', position: { x: 8, y: 8 }, direction: 'v' };
        wordValidationService.spotNewLetters(command, matrixOfLetter);
        expect(wordValidationService.indexOfLetterToCheck.length).to.equal(3);
    });
    it('should attached a new word to another ', () => {
        const command: Command = { word: 'bon', position: { x: 8, y: 8 }, direction: 'h' };
        for (let i = 0; i < command.word.length; ++i) {
            matrixOfLetter[command.position.x + i - 1][command.position.y - 1] = LETTERS_OBJECT.get(command.word.charAt(i)) ?? NA;
        }
        const secondCommand: Command = { word: 'bonjour', position: { x: 8, y: 8 }, direction: 'h' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(secondCommand, matrixOfLetter);
        expect(commandCode.isValid).to.equal(true);
    });
    it('should attached a new word to another in two different direction', () => {
        const command: Command = { word: 'bon', position: { x: 8, y: 8 }, direction: 'h' };
        for (let i = 0; i < command.word.length; ++i) {
            matrixOfLetter[command.position.x + i - 1][command.position.y - 1] = LETTERS_OBJECT.get(command.word.charAt(i)) ?? NA;
        }
        const secondCommand: Command = { word: 'nouveau', position: { x: 10, y: 8 }, direction: 'v' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(secondCommand, matrixOfLetter);
        expect(commandCode.isValid).to.equal(true);
    });
    it('should create a new word', () => {
        const newmatrixOfLetter: Letter[][] = [
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, R, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, U, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, I, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, T, O, I, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, E, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
        ];
        const thirdCommand: Command = { word: 'image', position: { x: 10, y: 8 }, direction: 'v' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(thirdCommand, newmatrixOfLetter);
        expect(commandCode.isValid).to.equal(true);
    });
    it('should return the right score', () => {
        const command: Command = { word: 'ta', position: { x: 8, y: 8 }, direction: 'h' };
        const realScore = 2;
        expect(realScore).to.equal(wordValidationService.getLetterScore(command.word));
    });
    it('should go return the right score with the letter bonus', () => {
        const command: Command = { word: 'table', position: { x: 4, y: 8 }, direction: 'h' };
        const realScore = 9;
        const usedPosition = new Array<Letter[]>(NB_TILES);
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NA);
        }
        expect(realScore).to.equal(wordValidationService.wordValidationInit(command, usedPosition).score);
    });
    it('should go return the right score with the letter bonus', () => {
        const command: Command = { word: 'constitution', position: { x: 1, y: 8 }, direction: 'h' };
        const realScore = 54;
        const usedPosition = new Array<Letter[]>(NB_TILES);
        for (let i = 0; i < usedPosition.length; ++i) {
            usedPosition[i] = new Array<Letter>(NB_TILES).fill(NA);
        }
        expect(realScore).to.equal(wordValidationService.wordValidationInit(command, usedPosition).score);
    });
    it('should create a new word', () => {
        const newmatrixOfLetter: Letter[][] = [
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, R, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, U, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, I, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, T, O, I, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, E, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
        ];
        const thirdCommand: Command = { word: 'ma', position: { x: 7, y: 9 }, direction: 'h' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(thirdCommand, newmatrixOfLetter);
        expect(commandCode.isValid).to.equal(true);
    });
    it('should create a new word', () => {
        const newmatrixOfLetter: Letter[][] = [
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, R, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, U, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, I, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, T, O, I, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, E, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
        ];
        const thirdCommand: Command = { word: 'et', position: { x: 7, y: 9 }, direction: 'h' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(thirdCommand, newmatrixOfLetter);
        expect(commandCode.isValid).to.equal(false);
    });
    it('should create a new word', () => {
        const newmatrixOfLetter: Letter[][] = [
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, T, O, I, T, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
        ];
        const realScore = 18;
        const thirdCommand: Command = { word: 'truite', position: { x: 11, y: 4 }, direction: 'v' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(thirdCommand, newmatrixOfLetter);
        expect(commandCode.score).to.equal(realScore);
    });
    it('should get the high score bonus', () => {
        const realScore = 68;
        const newmatrixOfLetter: Letter[][] = [
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
            [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
        ];
        const thirdCommand: Command = { word: 'bonjour', position: { x: 8, y: 8 }, direction: 'h' };
        const commandCode: CommandCode = wordValidationService.wordValidationInit(thirdCommand, newmatrixOfLetter);
        expect(commandCode.score).to.equal(realScore);
    });
});
