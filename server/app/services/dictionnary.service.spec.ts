/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import { WordDictionnary } from '@app/classes/word-dictionnary';
import { expect } from 'chai';
import * as fs from 'fs';
import * as Sinon from 'sinon';
import { DictionaryService } from './dictionnary.service';

describe('dictionnary service', () => {
    let dictionnaryService: DictionaryService;

    let writeFile: Sinon.SinonStub;
    let unlink: Sinon.SinonStub;

    beforeEach(() => {
        dictionnaryService = new DictionaryService();
        writeFile = Sinon.stub(fs, 'writeFile').returns();
        unlink = Sinon.stub(fs, 'unlink').returns();
    });

    afterEach(() => {
        writeFile.restore();
        unlink.restore();
    });

    it('adds dictionnary', () => {
        const newDictionnary: WordDictionnary = {
            title: 'test dictionnary',
            description: 'dictionnary for testing purposes',
            words: ['test', 'testing'],
        };
        dictionnaryService.add(newDictionnary);

        expect(dictionnaryService.dictionaries[0].title).to.equal(newDictionnary.title);
        expect(dictionnaryService.dictionaries[0].description).to.equal(newDictionnary.description);
        expect(dictionnaryService.dictionaries[0].words).to.equal(newDictionnary.words);
    });

    it('deletes dictionnary', () => {
        const newDictionnary: WordDictionnary = {
            title: 'newDictionnary',
            description: 'dictionnary for testing purposes',
            words: ['test', 'testing'],
        };
        dictionnaryService.listOfDictionnary = [newDictionnary];
        dictionnaryService.delete(0);

        expect(dictionnaryService.dictionaries.length).to.equal(0);
    });

    it('deletes dictionnary file', () => {
        const newDictionnary: WordDictionnary = {
            title: 'newDictionnary',
            description: 'dictionnary for testing purposes',
            words: ['test', 'testing'],
        };
        dictionnaryService.listOfDictionnary = [newDictionnary];
        dictionnaryService.deleteFile('fileName');

        Sinon.assert.calledWith(unlink, './app/assets/fileName.json');
    });
});
