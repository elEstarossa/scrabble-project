/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import { Scores } from 'app/classes/scores';
import { expect } from 'chai';
import { Db, MongoClient } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { DatabaseService } from './database.service';

describe('database service', () => {
    let databaseService: DatabaseService;
    const DB_NAME = 'LOG2990';
    const DATABASE_COLLECTION_CLASSIC = 'Classic';
    let mongoServer: MongoMemoryServer;
    let db: Db;

    const highscore1: Scores[] = [
        {
            name: 'first',
            score: 100,
        },
    ];
    const highscore2: Scores[] = [
        {
            name: 'clone',
            score: 100,
        },
    ];
    const highscore3: Scores[] = [
        {
            name: 'last',
            score: 10,
        },
        {
            name: 'first',
            score: 30,
        },
        {
            name: 'second',
            score: 25,
        },
        {
            name: 'third',
            score: 20,
        },
        {
            name: 'fourth',
            score: 15,
        },
    ];
    beforeEach(async () => {
        databaseService = new DatabaseService();
        mongoServer = await MongoMemoryServer.create();
        const mongoUri = mongoServer.getUri();
        await MongoClient.connect(mongoUri).then((client: MongoClient) => {
            db = client.db(DB_NAME);
        });
        databaseService = new DatabaseService();
        databaseService['db'] = db;
    });
    after(() => {
        databaseService.closeConnection();
        mongoServer.stop();
    });
    afterEach(async () => {
        databaseService['db'].collection(DATABASE_COLLECTION_CLASSIC).deleteMany({});
    });
    it('should return a client when connection is established', async () => {
        return databaseService.start(mongoServer.getUri()).then((result: MongoClient) => {
            return expect(result.db(DB_NAME).collection(DATABASE_COLLECTION_CLASSIC).collectionName).to.equals(DATABASE_COLLECTION_CLASSIC);
        });
    });
    it('should throw an error when connection is not established', async (done) => {
        try {
            databaseService.start('Url non existant');
        } catch (err) {
            expect(err).to.eql(new Error('Database connection error'));
        }
        done();
    });
    it('should get the scores stored in collection', async () => {
        await databaseService.addScore(highscore1, 'classic');
        return databaseService.sendTableOfHighScores('classic').then((result: Scores[]) => {
            return expect(result[0].name).to.equals(highscore1[0].name);
        });
    });
    it('should get the scores stored in collection with new one', async () => {
        await databaseService.addScore(highscore3, 'classic');
        await databaseService.fiveHighScoreVerification(highscore1, 'classic');
        return databaseService.sendTableOfHighScores('classic').then((result: Scores[]) => {
            return expect(result[0].name).to.equals(highscore1[0].name);
        });
    });
    it('should return the bd', async () => {
        const bdTest: Db = databaseService.database;
        expect(bdTest.collection(DATABASE_COLLECTION_CLASSIC).collectionName).to.equal(DATABASE_COLLECTION_CLASSIC);
    });
    it('should return two name because the score is the same', async () => {
        await databaseService.addScore(highscore1, 'classic');
        await databaseService.fiveHighScoreVerification(highscore2, 'classic');
        const nameClone = highscore1[0].name + ' et ' + highscore2[0].name;
        return databaseService.sendTableOfHighScores('classic').then((result: Scores[]) => {
            return expect(result[0].name).to.equals(nameClone);
        });
    });
});
