export interface CommandCode {
    isValid: boolean;
    score: number;
    errorMsg: string;
}
