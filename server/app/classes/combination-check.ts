export interface CombinationCheck {
    letter: string;
    x: number;
    y: number;
}
