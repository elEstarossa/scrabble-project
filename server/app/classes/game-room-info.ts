import { Letter } from '@app/classes/letter';
import { NA, NB_TILES, NOT_A_LETTER, RESERVE_SIZE } from '@app/constants/constants';
import { Socket } from 'socket.io';
import { GameTime } from './time';

export class GameRoomInfo {
    roomName: string;
    creatorName: string;
    usedPosition = new Array<Letter[]>(NB_TILES);
    passTurnCounter = 0;
    reserveServer = new Map<Letter, number>();
    reserveSize: number = RESERVE_SIZE;
    creatorEasel = new Array<Letter>();
    joinerEasel = new Array<Letter>();
    timer: GameTime;
    turnChange: boolean = false;
    gameEnd: boolean = false;
    creatorSocket: Socket;
    joinerSocket: Socket;
    isRoomFull: boolean = false;
    objectives: number[] = [];
    soloGame: boolean = false;

    constructor(roomName: string, creatorName: string, timer: GameTime) {
        this.roomName = roomName;
        this.creatorName = creatorName;
        this.timer = timer;
        for (let i = 0; i < this.usedPosition.length; ++i) {
            this.usedPosition[i] = new Array<Letter>(NB_TILES).fill(NA);
        }
    }

    checkEaselEmpty(): boolean {
        return (this.getEaselSize(this.creatorEasel) === 0 || this.getEaselSize(this.joinerEasel) === 0) && this.reserveSize === 0;
    }
    getEaselSize(easelLetters: Letter[]): number {
        let length = 7;
        if (easelLetters)
            for (const c of easelLetters) {
                if (c && c.charac === NOT_A_LETTER.charac) {
                    length--;
                }
            }
        return length;
    }
}
