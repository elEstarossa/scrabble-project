/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable prettier/prettier */
import { A, B, NOT_A_LETTER } from '@app/constants/constants';
import { expect } from 'chai';
import { describe } from 'mocha';
import { GameRoomInfo } from './game-room-info';
import { Letter } from './letter';
import { GameTime } from './time';

describe('GameRoomInfo', () => {
    /// /////////////////  creatinf the room ////////////////////////////////////
    it('should create a simple game room info ', () => {
        const timer: GameTime = { min: 5, sec: 5 };
        const gameRoomInfo: GameRoomInfo = new GameRoomInfo('roomName', 'ali', timer);

        expect(gameRoomInfo.timer).to.equals(timer);
    });

    /// /////////////////  checkEaselEmpty ////////////////////////////////////
    it('should return false when the easel of the creator or joiner is empty', () => {
        const timer: GameTime = { min: 0, sec: 0 };
        const gameRoomInfo: GameRoomInfo = new GameRoomInfo('roomName', 'ali', timer);
        gameRoomInfo.reserveSize = 0;
        const letter = (gameRoomInfo.creatorEasel = []);
        gameRoomInfo.getEaselSize(letter);
        const result = gameRoomInfo.checkEaselEmpty();

        expect(result).to.equals(false);
    });

    /// /////////////////  getEaselSize ////////////////////////////////////
    it('should return the length of the easel when the tab of letters contain letters', () => {
        const timer: GameTime = { min: 0, sec: 0 };
        const gameRoomInfo: GameRoomInfo = new GameRoomInfo('roomName', 'ali', timer);
        const letter: Letter[] = [A, B];
        const result = gameRoomInfo.getEaselSize(letter);

        expect(result).to.equals(7);
    });

    it('should return the length of the easel when the tab of letters contain nothing', () => {
        const timer: GameTime = { min: 0, sec: 0 };
        const gameRoomInfo: GameRoomInfo = new GameRoomInfo('roomName', 'ali', timer);
        const letter: Letter[] = [NOT_A_LETTER];
        const result = gameRoomInfo.getEaselSize(letter);

        expect(result).to.equals(6);
    });
});
