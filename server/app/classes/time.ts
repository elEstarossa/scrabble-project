export interface GameTime {
    min: number;
    sec: number;
}
