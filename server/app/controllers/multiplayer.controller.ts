import { CommandCode } from '@app/classes/command-code';
import { GameRoomInfo } from '@app/classes/game-room-info';
import { Letter } from '@app/classes/letter';
import { MessageServer } from '@app/classes/message-server';
import { Scores } from '@app/classes/scores';
import { GameTime } from '@app/classes/time';
import { BOTH_EASEL_FILLED, EMPTY_COMMAND, MAX_SKIP_TURN, TIME_FOR_SET_INTERVAL } from '@app/constants/constants';
import { DatabaseService } from '@app/services/database.service';
import { WordValidationService } from '@app/services/word-validation.service';
import * as io from 'socket.io';
import { Service } from 'typedi';

@Service()
export class MultiplayerController {
    gamesInProgress = new Map();
    empty: Letter[][] = [];
    allRoomNames: string[] = [];
    allCreators: string[] = [];
    emptyGame: GameRoomInfo;
    ioServer: io.Server;

    constructor(private socketServer: io.Server, private wordValidationService: WordValidationService, private databaseService: DatabaseService) {
        this.ioServer = this.socketServer;
    }

    handleMultipayerEvent(): void {
        this.ioServer.on('connection', (socket) => {
            socket.on('create', (message: MessageServer) => {
                this.allRoomNames.push(message.roomName ?? '');
                this.allCreators.push(message.creatorName ?? '');

                this.gamesInProgress.set(
                    message.roomName,
                    new GameRoomInfo(message.roomName ?? '', message.creatorName ?? '', message.timer ?? { min: 0, sec: 0 }),
                );

                socket.join(message.roomName ?? '');
                this.gamesInProgress.get(message.roomName ?? '').creatorSocket = socket;
                this.gamesInProgress.get(message.roomName).log2990 = message.log2990 ?? false;
                this.resendRooms();
            });
            socket.on('createSolo', (message: MessageServer) => {
                this.allRoomNames.push(socket.id);
                this.gamesInProgress.set(socket.id, new GameRoomInfo(socket.id, message.creatorName ?? '', message.timer ?? { min: 0, sec: 0 }));
                socket.join(socket.id);
                this.gamesInProgress.get(socket.id).soloGame = true;
                this.gamesInProgress.get(socket.id).log2990 = message.log2990 ?? false;
                this.resendRooms();
            });

            socket.on('join', (message: MessageServer, cb) => {
                if (!this.gamesInProgress.get(message.roomName).isRoomFull) {
                    socket.join(message.roomName ?? '');
                    this.gamesInProgress.get(message.roomName ?? '').joinerSocket = socket;
                    this.gamesInProgress.get(message.roomName ?? '').isRoomFull = true;
                    socket.to(message.roomName ?? '').emit('startGame', { joinerName: message.joinerName });
                    cb('joined');
                } else {
                    cb('roomIsFull');
                }
            });
            socket.on('cancelJoin', (message: MessageServer) => {
                this.gamesInProgress.get(message.roomName).isRoomFull = false;
                this.resendRooms();
                socket.to(message.roomName ?? '').emit('joinerCanceled');
                socket.leave(message.roomName ?? '');
            });
            socket.on('listRoomsWithDetails', () => {
                this.resendRooms();
            });

            socket.on('deleteRoom', (message: MessageServer) => {
                this.gamesInProgress.get(message.roomName).gameEnd = true;
                this.allCreators.splice(this.allRoomNames.indexOf(message.roomName ?? ''), 1);
                this.allRoomNames.splice(this.allRoomNames.indexOf(message.roomName ?? ''), 1);
                setTimeout(() => {
                    this.gamesInProgress.delete(message.roomName);
                }, TIME_FOR_SET_INTERVAL);

                this.resendRooms();
            });

            socket.on('acceptJoinRoom', (message: MessageServer) => {
                this.ioServer
                    .to(message.roomName ?? '')
                    .emit('startGameAccepted', { timer: this.gamesInProgress.get(message.roomName).timer, roomName: message.roomName });
            });

            socket.on('rejectJoinRoom', (message: MessageServer) => {
                this.gamesInProgress.get(message.roomName).isRoomFull = false;
                this.resendRooms();
                this.ioServer.to(message.roomName ?? '').emit('startGameRejected', { roomName: message.roomName });
                this.gamesInProgress.get(message.roomName).joinerSocket.leave(message.roomName);
            });

            socket.on('sendingPlaceCommand', (messageServer: MessageServer) => {
                const commandCode: CommandCode = this.wordValidationService.wordValidationInit(
                    messageServer.command ?? EMPTY_COMMAND,
                    this.gamesInProgress.get(messageServer.roomName ?? '')?.usedPosition ?? this.empty,
                );

                const message: MessageServer = {
                    command: messageServer.command,
                    commandCode,
                    creator: messageServer.creator,
                };
                this.ioServer.to(messageServer.roomName ?? '').emit('returnValidation', message);
            });

            socket.on('updateReserveInServer', (gameName: string, map: string, size: number, easel: Letter[], creator: boolean) => {
                this.gamesInProgress.get(gameName).reserveServer = new Map(JSON.parse(map));
                if (creator) this.gamesInProgress.get(gameName).creatorEasel = easel;
                else this.gamesInProgress.get(gameName).joinerEasel = easel;
                this.gamesInProgress.get(gameName).reserverServerSize = size;
                if (size === BOTH_EASEL_FILLED)
                    this.ioServer
                        .to(gameName)
                        .emit('updateReserveInClient', JSON.stringify(Array.from(this.gamesInProgress.get(gameName).reserveServer)), size, easel);
            });
            socket.on('sendObjectives', (gameName: string, objectives: number[]) => {
                this.gamesInProgress.get(gameName).objectives = objectives;
            });
            socket.on('getObjectives', (gameName: string, cb) => {
                cb(this.gamesInProgress.get(gameName).objectives);
            });
            socket.on('sendAchived', (gameName: string, description: string, index: number) => {
                socket.to(gameName).emit('getAchived', description, index);
            });
            socket.on('sendReserveJoin', (gameName: string) => {
                this.ioServer
                    .to(gameName)
                    .emit(
                        'updateReserveInClient',
                        JSON.stringify(Array.from(this.gamesInProgress.get(gameName).reserveServer)),
                        this.gamesInProgress.get(gameName).reserverServerSize,
                        this.gamesInProgress.get(gameName).creatorEasel,
                    );
            });

            socket.on('defineStarter', (gameName: string) => {
                const randomStarter: number = Math.round(Math.random());
                this.ioServer.to(gameName).emit('whoStarts', randomStarter);
            });
            socket.on('passTurn', (gameName: string) => {
                this.gamesInProgress.get(gameName).passTurnCounter++;
                this.gamesInProgress.get(gameName).turnChange = true;
                if (this.gamesInProgress.get(gameName).passTurnCounter >= MAX_SKIP_TURN) {
                    this.gamesInProgress.get(gameName).gameEnd = true;
                    this.ioServer.to(gameName).emit('endGame', { isThreePasses: true });
                } else socket.to(gameName).emit('getPassTurn');
            });
            socket.on('sendActionToOppnent', (message: MessageServer) => {
                this.gamesInProgress.get(message.roomName).passTurnCounter = 0;
                this.gamesInProgress.get(message.roomName).turnChange = true;
                this.gamesInProgress.get(message.roomName).reserveSize = message.reserveSize;
                if (message.creator) this.gamesInProgress.get(message.roomName).joinerEasel = message.easel;
                else this.gamesInProgress.get(message.roomName).creatorEasel = message.easel;
                socket.to(message.roomName ?? '').emit('getAction', message);
                if (this.gamesInProgress.get(message.roomName).checkEaselEmpty()) {
                    this.gamesInProgress.get(message.roomName).gameEnd = true;
                    this.ioServer.to(message.roomName ?? '').emit('endGame', { isEaselEmpty: true });
                }
            });
            socket.on('sendChat', (message: MessageServer) => {
                socket.to(message.roomName ?? '').emit('receiveChat', message);
            });
            socket.on('startTimer', (message: MessageServer) => {
                const timeEmit: GameTime = message.timer ?? { min: 0, sec: 0 };
                const interval = setInterval(() => {
                    if (!this.gamesInProgress.get(message.roomName).gameEnd) {
                        if (this.gamesInProgress.get(message.roomName).turnChange) {
                            this.gamesInProgress.get(message.roomName).turnChange = false;
                            timeEmit.min = this.gamesInProgress.get(message.roomName).timer.min;
                            timeEmit.sec = this.gamesInProgress.get(message.roomName).timer.sec;
                        }
                        if (timeEmit.sec === 0 && timeEmit.min !== 0) {
                            timeEmit.min--;
                            timeEmit.sec = 59;
                        } else if (timeEmit.min === 0 && timeEmit.sec === 0) {
                            this.gamesInProgress.get(message.roomName).turnChange = false;
                            timeEmit.min = this.gamesInProgress.get(message.roomName).timer.min;
                            timeEmit.sec = this.gamesInProgress.get(message.roomName).timer.sec;
                        } else {
                            timeEmit.sec--;
                        }
                        this.ioServer.to(message.roomName ?? '').emit('getTime', timeEmit);
                    } else clearInterval(interval);
                }, TIME_FOR_SET_INTERVAL);
            });
            socket.on('abandonGame', (message: MessageServer) => {
                if (this.gamesInProgress.get(message.roomName)) this.gamesInProgress.get(message.roomName).gameEnd = true;
                socket.to(message.roomName ?? '').emit('endGame', { isAbandon: true });
            });
            socket.on('showScoreClassic', async () => {
                const highestScores = await this.databaseService.sendTableOfHighScores('classic');
                this.ioServer.emit('showScoreClassic', { highScore: highestScores });
            });
            socket.on('showScore2990', async () => {
                const highestScores = await this.databaseService.sendTableOfHighScores('2990');
                this.ioServer.emit('showScore2990', { highScore: highestScores });
            });
            socket.on('sendScoreClassic', async (score: Scores[]) => {
                socket.emit('score added for classic', await this.databaseService.fiveHighScoreVerification(score, 'classic'));
            });
            socket.on('sendScore2990', async (score: Scores[]) => {
                socket.emit('score added for 2990', await this.databaseService.fiveHighScoreVerification(score, '2990'));
            });
        });
    }
    resendRooms() {
        const roomAvailable: string[] = [];
        const roomAvailableLog2990: string[] = [];
        const creatorAvailable: string[] = [];
        const creatorAvailableLog2990: string[] = [];
        for (let i = 0; i < this.allRoomNames.length; i++) {
            if (!this.gamesInProgress.get(this.allRoomNames[i]).isRoomFull && !this.gamesInProgress.get(this.allRoomNames[i]).soloGame) {
                switch (this.gamesInProgress.get(this.allRoomNames[i]).log2990) {
                    case true:
                        roomAvailableLog2990.push(this.allRoomNames[i]);
                        creatorAvailableLog2990.push(this.allCreators[i]);
                        break;
                    case false:
                        roomAvailable.push(this.allRoomNames[i]);
                        creatorAvailable.push(this.allCreators[i]);
                        break;
                }
            }
        }

        this.ioServer.emit('listRoomsWithDetailsResult', {
            roomList: roomAvailable,
            allRoomList: this.allRoomNames,
            creatorList: creatorAvailable,
            allRoomListLog2990: roomAvailableLog2990,
            creatorListLog2990: creatorAvailableLog2990,
        });
    }
}
