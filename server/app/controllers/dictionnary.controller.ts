import { WordDictionnary } from '@app/classes/word-dictionnary';
import { HTTP_STATUS_CREATED, HTTP_SUCCESSFUL_REQUEST } from '@app/constants/constants';
import { DictionaryService } from '@app/services/dictionnary.service';
import { Request, Response, Router } from 'express';
import { Service } from 'typedi';
@Service()
export class DictionnaryController {
    router: Router;
    constructor(private readonly dictionaryService: DictionaryService) {
        this.configureRouter();
    }
    private configureRouter(): void {
        this.router = Router();
        this.router.get('/', async (req: Request, res: Response) => {
            const dictionnaire = this.dictionaryService.dictionaries;
            res.json(dictionnaire);
        });
        this.router.post('/', (req: Request, res: Response) => {
            const dict: WordDictionnary = { title: req.body.title, description: req.body.description, words: req.body.words };
            this.dictionaryService.add(dict);
            res.status(HTTP_STATUS_CREATED).json({ message: 'Dictionnaire enregistré !' });
        });
        this.router.delete('/:id', (req: Request, res: Response) => {
            const index: number = +req.params.id;
            this.dictionaryService.delete(index);
            res.status(HTTP_SUCCESSFUL_REQUEST).json({ message: 'Dictionnaire supprimé !' });
        });
        this.router.put('/:id', (req: Request, res: Response) => {
            const index: number = +req.params.id;
            const title = req.body.title;
            const desc = req.body.description;
            this.dictionaryService.modify(index, title, desc);
            res.status(HTTP_SUCCESSFUL_REQUEST).json({ message: 'Dictionnaire modifié !' });
        });
        this.router.get('/:id', (req: Request, res: Response) => {
            const index: number = +req.params.id;
            const title = req.body.title;
            const desc = req.body.description;
            this.dictionaryService.modify(index, title, desc);
            res.status(HTTP_SUCCESSFUL_REQUEST).json({ message: 'Dictionnaire modifié !' });
        });
    }
}
