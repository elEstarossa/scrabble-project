/* eslint-disable @typescript-eslint/no-magic-numbers */
import { Application } from '@app/app';
import { expect } from 'chai';
import * as fs from 'fs';
import * as supertest from 'supertest';
import { Container } from 'typedi';
describe('DictionnaryController', () => {
    let expressApp: Express.Application;

    beforeEach(async () => {
        const app = Container.get(Application);
        // eslint-disable-next-line dot-notation
        expressApp = app.app;
    });

    it('should return the dictionnary.json', async () => {
        const dir = './app/assets/dictionnary.json';
        const dictionnaire = JSON.parse(fs.readFileSync(dir, 'utf-8'));
        return supertest(expressApp)
            .get('/api/dict')
            .expect([dictionnaire])
            .then((response) => {
                expect(response.body).to.deep.equal([dictionnaire]);
            });
    });
});
