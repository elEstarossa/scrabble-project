/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable dot-notation */
/* eslint-disable max-lines */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Letter } from '@app/classes/letter';
import { MessageServer } from '@app/classes/message-server';
import { EMPTY_COMMAND, LETTERS_RESERVE_QTY } from '@app/constants/constants';
import { DatabaseService } from '@app/services/database.service';
import { WordValidationService } from '@app/services/word-validation.service';
import { expect } from 'chai';
import { createServer } from 'http';
import { afterEach } from 'mocha';
import * as Sinon from 'sinon';
import * as ioServer from 'socket.io';
import { io, Socket } from 'socket.io-client';
import { MultiplayerController } from './multiplayer.controller';

describe('MultiplayerController', () => {
    let multiplayerController: MultiplayerController;
    let serverSocket: ioServer.Server;
    let wordValidationService: WordValidationService;
    let databaseService: DatabaseService;
    let socket: Socket;
    const wsUri = 'ws://localhost:3000';
    const delay = async (ms: number) => new Promise((res) => setTimeout(res, ms));

    beforeEach((done) => {
        const httpServer = createServer();
        serverSocket = new ioServer.Server(httpServer, { cors: { origin: '*', methods: ['GET', 'POST'] } });
        wordValidationService = new WordValidationService();
        multiplayerController = new MultiplayerController(serverSocket, wordValidationService, databaseService);
        multiplayerController.handleMultipayerEvent();
        socket = io(wsUri);
        done();
    });

    afterEach(() => {
        socket.close();
    });

    it('creates the room', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'create') {
                    const message: MessageServer = {
                        roomName: 'room',
                        creatorName: 'creatorPlayer',
                        timer: {
                            min: 0,
                            sec: 0,
                        },
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: any) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();

        expect(spy.called).to.equal(true);
    });

    it('creates solo', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'createSolo') {
                    const message: MessageServer = {
                        roomName: 'roomSolo',
                        creatorName: 'creatorPlayer',
                        timer: {
                            min: 0,
                            sec: 0,
                        },
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: any) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();

        expect(spy.called).to.equal(true);
    });

    it('creates the room with mode log2990', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'create') {
                    const message: MessageServer = {
                        roomName: 'roomLog2990',
                        log2990: true,
                        creatorName: 'creatorPlayer',
                        timer: {
                            min: 0,
                            sec: 0,
                        },
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: any) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        const room = multiplayerController.gamesInProgress.get('roomLog2990');
        expect(room.log2990).to.be.true;
        expect(spy.called).to.equal(true);
    });

    it('creates', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'create') {
                    const message: MessageServer = {
                        roomName: 'room',
                        joinerName: 'soloPlayer',
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        const roomData = multiplayerController.gamesInProgress.get('room');
        expect(roomData.roomName).to.equal('room');
        expect(roomData.isRoomFull).to.equal(false);

        expect(spy.called).to.equal(true);
    });

    it('joins the room', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer, cb: () => {}) => void) => {
                if (eventName === 'join') {
                    const message: MessageServer = {
                        roomName: 'room',
                        joinerName: 'joinerPlayer',
                    };
                    const cb = () => {
                        return {};
                    };
                    callback(message, cb);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('room');
                return {
                    emit: (event: string) => {
                        expect(event).to.equal('startGame');
                        return { joinerName: 'joinerPlayer' };
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('room', { isRoomFull: false });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as unknown as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('lists room with details', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'listRoomsWithDetails') {
                    const message: MessageServer = {};
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: (eventName: string, eventData: { roomList: string[]; creatorList: string[] }) => {
                expect(eventName).to.equal('listRoomsWithDetailsResult');
                expect(eventData.roomList).to.be.an('array').that.is.empty;
                expect(eventData.creatorList).to.be.an('array').that.is.empty;
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('deletes room', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'deleteRoom') {
                    const message: MessageServer = {
                        roomName: 'roomToDelete',
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        multiplayerController.gamesInProgress.set('roomToDelete', { gameEnd: false });
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        const room = multiplayerController.gamesInProgress.get('roomToDelete');
        expect(room.gameEnd).to.be.true;
        expect(spy.called).to.equal(true);
    });

    it('accepts join room', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'acceptJoinRoom') {
                    const message: MessageServer = {
                        roomName: 'roomToAcceptJoin',
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('roomToAcceptJoin');
                return {
                    emit: (event: string, message: MessageServer) => {
                        expect(event).to.equal('startGameAccepted');
                        expect(message).to.eql({ timer: { min: 0, sec: 0 }, roomName: 'roomToAcceptJoin' });
                    },
                };
            },
        } as ioServer.Server;
        multiplayerController.gamesInProgress.set('roomToAcceptJoin', { timer: { min: 0, sec: 0 } });
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('rejects join room', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'rejectJoinRoom') {
                    const message: MessageServer = {
                        roomName: 'roomToRejectJoin',
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
            leave: () => {},
        };
        multiplayerController.gamesInProgress.set('roomToRejectJoin', { isRoomFull: false });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
            to: (roomName: string) => {
                expect(roomName).to.equal('roomToRejectJoin');
                return {
                    emit: (event: string, message: MessageServer) => {
                        expect(event).to.equal('startGameRejected');
                        expect(message).to.eql({ roomName: 'roomToRejectJoin' });
                    },
                };
            },
        } as unknown as ioServer.Server;
        multiplayerController.gamesInProgress.set('roomToRejectJoin', { joinerSocket: { leave: () => {} } });
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('sends place command', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'sendingPlaceCommand') {
                    const message: MessageServer = {
                        roomName: 'roomToSendCommand',
                        command: EMPTY_COMMAND,
                        creatorName: 'creatorPlayer',
                    };
                    callback(message);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('roomToSendCommand');
                return {
                    emit: (event: string, message: MessageServer) => {
                        expect(event).to.equal('returnValidation');
                        expect(message).to.eql({
                            command: { position: { x: -1, y: -1 }, word: '', direction: '' },
                            commandCode: {
                                isValid: false,
                                errorMsg: 'Votre mot: , ne figure pas dans le dictionnaire!',
                                score: 0,
                            },
                            creator: undefined,
                        });
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('updates reserve in server when creator is true', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string, map: string, size: number, easel: Letter[], creator: boolean) => void) => {
                if (eventName === 'updateReserveInServer') {
                    const letters = new Map<Letter, number>(LETTERS_RESERVE_QTY);
                    const mapArg = JSON.stringify(Array.from(letters));
                    callback('gameName', mapArg, 2, [], true);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, reserve: unknown, size: number, easel: Letter[]) => {
                        expect(event).to.equal('updateReserveInClient');
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', {});
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('updates reserve in server when creator is false', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string, map: string, size: number, easel: Letter[], creator: boolean) => void) => {
                if (eventName === 'updateReserveInServer') {
                    const letters = new Map<Letter, number>(LETTERS_RESERVE_QTY);
                    const mapArg = JSON.stringify(Array.from(letters));
                    callback('gameName', mapArg, 2, [], false);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, reserve: unknown, size: number, easel: Letter[]) => {
                        expect(event).to.equal('updateReserveInClient');
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', {});
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('sends reserve join', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string) => void) => {
                if (eventName === 'sendReserveJoin') {
                    callback('gameName');
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { reserveServer: [] });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, reserve: unknown, size: number, easel: Letter[]) => {
                        expect(event).to.equal('updateReserveInClient');
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('defines starter', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string) => void) => {
                if (eventName === 'defineStarter') {
                    callback('gameName');
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { reserveServer: [] });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, randomStarter: number) => {
                        expect(event).to.equal('whoStarts');
                        expect(randomStarter).to.be.lessThanOrEqual(1);
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('passes turn', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string) => void) => {
                if (eventName === 'passTurn') {
                    callback('gameName');
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string) => {
                        expect(event).to.equal('getPassTurn');
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { passTurnCounter: 0 });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('ends game when reached 6 consecutive passes', () => {
        const spySocket = {
            on: (eventName: string, callback: (gameName: string) => void) => {
                if (eventName === 'passTurn') {
                    callback('gameName');
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string) => {
                        expect(event).to.equal('getPassTurn');
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { passTurnCounter: 6 });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, eventData: { isThreePasses: boolean }) => {
                        expect(event).to.equal('endGame');
                        expect(eventData).to.eql({ isThreePasses: true });
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('sends action to opponent when creator is true', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'sendActionToOppnent') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, message: MessageServer) => {
                        const eventData: MessageServer = {
                            roomName: 'gameName',
                            reserveSize: 4,
                            easel: [
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'b' },
                            ],
                            creator: true,
                        };
                        expect(event).to.equal('getAction');
                        expect(message).to.eql(eventData);
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { checkEaselEmpty: () => false });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('sends action to opponent when creator is false', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'sendActionToOppnent') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: false,
                    };
                    callback(event);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, message: MessageServer) => {
                        const eventData: MessageServer = {
                            roomName: 'gameName',
                            reserveSize: 4,
                            easel: [
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'b' },
                            ],
                            creator: false,
                        };
                        expect(event).to.equal('getAction');
                        expect(message).to.eql(eventData);
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { checkEaselEmpty: () => false });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('sends endGame when easel is empty', () => {
        let endGameCalled = 0;
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'sendActionToOppnent') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, message: MessageServer) => {
                        const eventData: MessageServer = {
                            roomName: 'gameName',
                            reserveSize: 4,
                            easel: [
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'a' },
                                { score: 1, charac: 'b' },
                            ],
                            creator: true,
                        };
                        expect(event).to.equal('getAction');
                        expect(message).to.eql(eventData);
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { checkEaselEmpty: () => true });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, eventData: { isEaselEmpty: boolean }) => {
                        if (event === 'endGame') {
                            endGameCalled++;
                            expect(eventData).to.eql({ isEaselEmpty: true });
                        }
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
        expect(endGameCalled).to.equal(1);
    });

    it('sends chat', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'sendChat') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                    };
                    callback(event);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, message: MessageServer) => {
                        expect(event).to.equal('receiveChat');
                        expect(message).to.eql({
                            roomName: 'gameName',
                        });
                    },
                };
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('starts timer', async () => {
        const clock = Sinon.useFakeTimers();

        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'startTimer') {
                    expect(spy.called).to.equal(true);
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                    await delay(1010);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', { gameEnd: false, turnChange: true, timer: { min: 0, sec: 0 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, timer: { min: number; sec: number }) => {
                        expect(event).to.equal('getTime');
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        clock.tick(1010);
        expect(spy.called).to.equal(true);

        Sinon.restore();
    });
    it('starts timer', async () => {
        const clock = Sinon.useFakeTimers();

        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'startTimer') {
                    expect(spy.called).to.equal(true);
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                    await delay(1010);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', { gameEnd: false, turnChange: true, timer: { min: 1, sec: 0 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, timer: { min: number; sec: number }) => {
                        expect(event).to.equal('getTime');
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        clock.tick(1010);
        expect(spy.called).to.equal(true);

        Sinon.restore();
    });
    it('starts timer', async () => {
        const clock = Sinon.useFakeTimers();

        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'startTimer') {
                    expect(spy.called).to.equal(true);
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                    await delay(1010);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', { gameEnd: false, turnChange: true, timer: { min: 0, sec: 10 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, timer: { min: number; sec: number }) => {
                        expect(event).to.equal('getTime');
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        clock.tick(1010);
        expect(spy.called).to.equal(true);

        Sinon.restore();
    });
    it('starts timer', async () => {
        const clock = Sinon.useFakeTimers();

        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'startTimer') {
                    expect(spy.called).to.equal(true);
                    const event: MessageServer = {
                        roomName: 'gameName',
                        reserveSize: 4,
                        easel: [
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'a' },
                            { score: 1, charac: 'b' },
                        ],
                        creator: true,
                    };
                    callback(event);
                    await delay(1010);
                }
            },
            join: () => {},
            emit: () => {},
        };

        multiplayerController.gamesInProgress.set('gameName', { gameEnd: true, turnChange: true, timer: { min: 0, sec: 0 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, timer: { min: number; sec: number }) => {
                        expect(event).to.equal('getTime');
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        clock.tick(1010);
        expect(spy.called).to.equal(true);

        Sinon.restore();
    });
    it('abandons game when browser is closed', () => {
        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'abandonGame') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                        browserClose: true,
                    };
                    callback(event);
                    await delay(8000);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { gameEnd: false, turnChange: true, timer: { min: 0, sec: 0 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, eventData: { isAbandon: boolean }) => {
                        expect(event).to.equal('endGame');
                        expect(eventData).to.eql({ isAbandon: true });
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('abandons game when browser is not closed', () => {
        const spySocket = {
            on: async (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'abandonGame') {
                    const event: MessageServer = {
                        roomName: 'gameName',
                        browserClose: false,
                    };
                    callback(event);
                    await delay(8000);
                }
            },
            join: () => {},
            emit: () => {},
        };
        multiplayerController.gamesInProgress.set('gameName', { gameEnd: false, turnChange: true, timer: { min: 0, sec: 0 } });
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('gameName');
                return {
                    emit: (event: string, eventData: { isAbandon: boolean }) => {
                        expect(event).to.equal('endGame');
                        expect(eventData).to.eql({ isAbandon: true });
                    },
                };
            },
        } as ioServer.Server;
        const spy = Sinon.spy(spySocket, 'on');
        multiplayerController.handleMultipayerEvent();
        expect(spy.called).to.equal(true);
    });

    it('Cancels Join', () => {
        const spySocket = {
            on: (eventName: string, callback: (message: MessageServer) => void) => {
                if (eventName === 'cancelJoin') {
                    const message: MessageServer = {
                        roomName: 'room',
                    };
                    callback(message);
                }
            },
            to: (roomName: string) => {
                expect(roomName).to.equal('room');
                return {
                    emit: (event: string) => {
                        expect(event).to.equal('joinerCanceled');
                    },
                };
            },
            leave: () => {},
            join: () => {},
            emit: () => {},
        };
        multiplayerController.ioServer = {
            on: (eventName: string, callBackfunction: (socket: unknown) => void) => {
                if (eventName === 'connection') callBackfunction(spySocket);
            },
            emit: () => {},
        } as unknown as ioServer.Server;
        multiplayerController.gamesInProgress.set('room', { isRoomFull: true });
        const spy = Sinon.spy(spySocket, 'on');
        Sinon.spy(spySocket, 'to');
        multiplayerController.handleMultipayerEvent();
        const roomData = multiplayerController.gamesInProgress.get('room');
        expect(spy.called).to.equal(true);
        expect(roomData.isRoomFull).to.equal(false);
    });
});
