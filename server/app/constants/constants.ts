import { Command } from '@app/classes/command';
import { CommandCode } from '@app/classes/command-code';
import { Letter } from '@app/classes/letter';
import { Vec2 } from '@app/classes/vec2';
// grid-service
export const BOARD_WIDTH = 600;
export const BOARD_HEIGHT = 600;
export const NB_LETTER_HAND = 7;
export const HAND_POSITION_START = 4;
export const HAND_POSITION_END = 11;
export const CTX_PX = 15;
export const ADJUSTEMENT_TOPSPACE = 5;

// play-area-comp
export const CANEVAS_WIDTH = 700;
export const CANEVAS_HEIGHT = 700;
export const NB_TILES = 15;
export const TOPSPACE = 25;
export const LEFTSPACE = 50;
export const UNDEFINED_INDEX = -1;
// LETTERS
export const NOT_A_LETTER: Letter = { score: 0, charac: '1' };
export const A: Letter = { score: 1, charac: 'a' };
export const B: Letter = { score: 3, charac: 'b' };
export const C: Letter = { score: 3, charac: 'c' };
export const D: Letter = { score: 2, charac: 'd' };
export const E: Letter = { score: 1, charac: 'e' };
export const F: Letter = { score: 4, charac: 'f' };
export const G: Letter = { score: 2, charac: 'g' };
export const H: Letter = { score: 4, charac: 'h' };
export const I: Letter = { score: 1, charac: 'i' };
export const J: Letter = { score: 8, charac: 'j' };
export const K: Letter = { score: 10, charac: 'k' };
export const L: Letter = { score: 1, charac: 'l' };
export const M: Letter = { score: 2, charac: 'm' };
export const N: Letter = { score: 1, charac: 'n' };
export const O: Letter = { score: 1, charac: 'o' };
export const P: Letter = { score: 3, charac: 'p' };
export const Q: Letter = { score: 8, charac: 'q' };
export const R: Letter = { score: 1, charac: 'r' };
export const S: Letter = { score: 1, charac: 's' };
export const T: Letter = { score: 1, charac: 't' };
export const U: Letter = { score: 1, charac: 'u' };
export const V: Letter = { score: 4, charac: 'v' };
export const W: Letter = { score: 10, charac: 'w' };
export const X: Letter = { score: 10, charac: 'x' };
export const Y: Letter = { score: 10, charac: 'y' };
export const Z: Letter = { score: 10, charac: 'z' };
export const FIRST_POSITION_BOARD = { x: 8, y: 8 } as Vec2;

export const LETTERS_OBJECT = new Map<string, Letter>([
    ['a', A],
    ['à', A],
    ['â', A],
    ['b', B],
    ['c', C],
    ['ç', C],
    ['d', D],
    ['e', E],
    ['é', E],
    ['è', E],
    ['ê', E],
    ['f', F],
    ['g', G],
    ['h', H],
    ['i', I],
    ['î', I],
    ['j', J],
    ['k', K],
    ['l', L],
    ['m', M],
    ['n', N],
    ['o', O],
    ['ô', O],
    ['p', P],
    ['q', Q],
    ['r', R],
    ['s', S],
    ['t', T],
    ['u', U],
    ['v', V],
    ['w', W],
    ['x', X],
    ['y', Y],
    ['z', Z],
]);

//
export const RED_TILE: Vec2[] = [
    { x: 0, y: 0 },
    { x: 7, y: 0 },
    { x: 14, y: 0 },
    { x: 0, y: 7 },
    { x: 14, y: 7 },
    { x: 0, y: 14 },
    { x: 7, y: 14 },
    { x: 14, y: 14 },
];

export const AZUR_TILE: Vec2[] = [
    { x: 3, y: 0 },
    { x: 11, y: 0 },
    { x: 6, y: 2 },
    { x: 8, y: 2 },
    { x: 0, y: 3 },
    { x: 7, y: 3 },
    { x: 14, y: 3 },
    { x: 2, y: 6 },
    { x: 6, y: 6 },
    { x: 8, y: 6 },
    { x: 12, y: 6 },
    { x: 3, y: 7 },
    { x: 11, y: 7 },
    { x: 2, y: 8 },
    { x: 6, y: 8 },
    { x: 8, y: 8 },
    { x: 12, y: 8 },
    { x: 0, y: 11 },
    { x: 7, y: 11 },
    { x: 14, y: 11 },
    { x: 6, y: 12 },
    { x: 8, y: 12 },
    { x: 3, y: 14 },
    { x: 11, y: 14 },
];
export const BLUE_TILE: Vec2[] = [
    { x: 5, y: 1 },
    { x: 9, y: 1 },
    { x: 1, y: 5 },
    { x: 5, y: 5 },
    { x: 9, y: 5 },
    { x: 13, y: 5 },
    { x: 1, y: 9 },
    { x: 5, y: 9 },
    { x: 9, y: 9 },
    { x: 13, y: 9 },
    { x: 5, y: 13 },
    { x: 9, y: 13 },
];
export const PINK_TILE: Vec2[] = [
    { x: 1, y: 1 },
    { x: 2, y: 2 },
    { x: 3, y: 3 },
    { x: 4, y: 4 },
    { x: 13, y: 1 },
    { x: 12, y: 2 },
    { x: 11, y: 3 },
    { x: 10, y: 4 },
    { x: 1, y: 13 },
    { x: 2, y: 12 },
    { x: 3, y: 11 },
    { x: 4, y: 10 },
    { x: 13, y: 13 },
    { x: 12, y: 12 },
    { x: 11, y: 11 },
    { x: 10, y: 10 },
];
// chatBarComponent
export const SET_INTERVAL = 100;
export const MAX_CHARACTER = 512;
export const COMMAND_PLACER = 'placer';
export const COMMAND_ECHANGER = 'échanger';
export const COMMAND_PASSER = 'passer';
export const ASCI_A = 97;
export const ASCI_O = 111;
export const MAX_SIZE = 15;
// player name validation
export const PLAYER_NAME_MAX_LENGTH = 15;
export const PLAYER_NAME_MIN_LENGTH = 5;

// game mode
export const CLASSIC_GAME_MODE = 'CLASSIC_MODE';

export const LEAVE_NAVIGATOR_TIMER = 5000;
export const EMPTY_COMMAND: Command = { position: { x: UNDEFINED_INDEX, y: UNDEFINED_INDEX }, word: '', direction: '' };
export const EMPTY_COMMAND_CODE: CommandCode = { isValid: false, errorMsg: '', score: 0 };
export const RESERVE_SIZE = 100;
export const BOTH_EASEL_FILLED = 86;
// test
export const NA: Letter = { score: 0, charac: '1' };
export const matrixOfLetter: Letter[][] = [
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
    [NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA],
];

// mj controller
export const MAX_SKIP_TURN = 6;
export const TIME_FOR_SET_INTERVAL = 1000;
export const FIVE_SEC_PLAYER_LEFT = 5000;
export const QTY_A = 9;
export const QTY_E = 15;
export const QTY_I = 8;
export const QTY_L = 5;
export const QTY_O = 6;

export const LETTERS_RESERVE_QTY = new Map<Letter, number>([
    [A, QTY_A],
    [B, 2],
    [C, 2],
    [D, 3],
    [E, QTY_E],
    [F, 2],
    [G, 2],
    [H, 2],
    [I, QTY_I],
    [J, 1],
    [K, 1],
    [L, QTY_L],
    [M, 3],
    [N, QTY_O],
    [O, QTY_O],
    [P, 2],
    [Q, 1],
    [R, QTY_O],
    [S, QTY_O],
    [T, QTY_O],
    [U, QTY_O],
    [V, 2],
    [W, 1],
    [X, 1],
    [Y, 1],
    [Z, 1],
]);

// word validation service
export const MAX_LENGTH = 7;
export const SCORE_BONUS = 50;
export const MULTIPLY_3 = 3;
export const MULTIPLY_2 = 3;
// Dictionnary controller and service

export const HTTP_SUCCESSFUL_REQUEST = 200;
export const HTTP_STATUS_CREATED = 201;
export const HTTP_BAD_REQUEST = 400;
