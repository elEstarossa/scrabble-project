### Librairies à ajouter pour heavy client

Vous aurez besoin de 2 librairies pour votre projet :

- `electron` : permets de développer une application Electron. [Page NPM](https://www.npmjs.com/package/electron)
- `electron-packager` : permets de créer un exécutable à partir de votre application Electron. [Page NPM](https://www.npmjs.com/package/electron-packager) . Il existe d'autres alternatives (`electron-forge` par exemple), mais `electron-packager` est l'outil le plus populaire avec une bonne documentation.

Dans les 2 cas, assurez-vous d'installer les paquets npm comme des dépendances de développement en faisant `npm install --save-dev electron` avec au moins la version **20.0.3** et `npm install --save-dev electron-packager` avec la version **16.0.0**. Ceci minimisera la taille de vos exécutables.


# Lancement app client lourd 
pour lancer l'apli sur le client lourd : il faut lancer la commande "npm run start:electron"

## client léger 
- Le client léger sera fait en Kotlin + Xml.
- j'ai créer un template vite de l'application.
- télechager Android studio 
- lancer le projet sur un émulateur que vous créez ou sur un device que vous connecter à votre ordi.

## server 

- Le serveur run en ce moment sur le local. 
